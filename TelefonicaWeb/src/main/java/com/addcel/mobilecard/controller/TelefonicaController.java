package com.addcel.mobilecard.controller;

import org.springframework.stereotype.Controller;
import com.addcel.mobilecard.model.vo.CancelacionRequest;
import com.addcel.mobilecard.model.vo.CompraProductoRequest;
import com.addcel.mobilecard.model.vo.ConsultaRequest;
import com.addcel.mobilecard.model.vo.CrearUsuarioRequest;
import com.addcel.mobilecard.model.vo.EnvioSMSRequest;
import com.addcel.mobilecard.model.vo.LoginRequest;
import com.addcel.mobilecard.model.vo.ProductosDisponiblesRequest;
import com.addcel.mobilecard.model.vo.RecargaRequest;
import com.addcel.mobilecard.model.vo.ValidarSMSTokenRequest;
import com.addcel.mobilecard.services.TelefonicaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class TelefonicaController {
  private static final Logger logger = LoggerFactory.getLogger(TelefonicaController.class);
  @Autowired
  private TelefonicaService service;

  @RequestMapping({"/"})
  public ModelAndView home() {
    logger.info("Dentro del servicio: /");
    return new ModelAndView("portal/welcome");
  }

  @RequestMapping({"/portal/recarga"})
  public ModelAndView login(@ModelAttribute("recarga") RecargaRequest recarga, ModelMap modelo) {
    logger.debug("Dentro del servicio: /recarga");
    ModelAndView mav = null;
    try {
      logger.debug("Iniciar pantalla, recarga");
      mav = new ModelAndView("portal/recarga", "recarga", new RecargaRequest());
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  @RequestMapping(value = {"/portal/recarga-final"},
      method = {org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView loginFinal(@ModelAttribute("recarga") RecargaRequest recarga, ModelMap modelo) {
    logger.debug("Dentro del servicio: /portal/recarga");
    return this.service.recargaMvno(recarga);
  }

  @RequestMapping({"/portal/cancelacion"})
  public ModelAndView cancelacion(@ModelAttribute("cancelacion") CancelacionRequest cancelacion,
      ModelMap modelo) {
    logger.debug("Dentro del servicio: /cancelacion");
    ModelAndView mav = null;
    try {
      logger.debug("Iniciar pantalla, cancelacion");
      mav = new ModelAndView("portal/cancelacion", "cancelacion", new CancelacionRequest());
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  @RequestMapping(value = {"/portal/cancelacion-final"},
      method = {org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView cancelacionFinal(
      @ModelAttribute("cancelacion") CancelacionRequest cancelacion, ModelMap modelo) {
    logger.debug("Dentro del servicio: /portal/cancelacion");
    return this.service.cancelacionTranID(cancelacion);
  }

  @RequestMapping({"/portal/consulta"})
  public ModelAndView consulta(@ModelAttribute("consulta") ConsultaRequest consulta, ModelMap modelo) {
    logger.debug("Dentro del servicio: /consulta");
    ModelAndView mav = null;
    try {
      logger.debug("Iniciar pantalla, consulta");
      mav = new ModelAndView("portal/consulta", "consulta", new CancelacionRequest());
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  @RequestMapping(value = {"/portal/consulta-final"},
      method = {org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView consultaFinal(@ModelAttribute("consulta") ConsultaRequest consulta,
      ModelMap modelo) {
    logger.debug("Dentro del servicio: /portal/consulta");
    return this.service.consultaTranID(consulta);
  }

  @RequestMapping({"/portal/login"})
  public ModelAndView login(@ModelAttribute("login") LoginRequest login, ModelMap modelo) {
    logger.debug("Dentro del servicio: /login");
    ModelAndView mav = null;
    try {
      logger.debug("Iniciar pantalla, login");
      mav = new ModelAndView("portal/login", "login", new LoginRequest());
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  @RequestMapping(value = {"/portal/login-final"},
      method = {org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView loginFinal(@ModelAttribute("login") LoginRequest login, ModelMap modelo) {
    logger.debug("Dentro del servicio: /portal/login");
    return this.service.login(login);
  }

  @RequestMapping({"/portal/prodDisp"})
  public ModelAndView prodDisp(@ModelAttribute("prodDisp") ProductosDisponiblesRequest prodDisp,
      ModelMap modelo) {
    logger.debug("Dentro del servicio: /prodDisp");
    ModelAndView mav = null;
    try {
      logger.debug("Iniciar pantalla, prodDisp");
      mav = new ModelAndView("portal/prodDisp", "prodDisp", new ProductosDisponiblesRequest());
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  @RequestMapping(value = {"/portal/prodDisp-final"},
      method = {org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView prodDispFinal(
      @ModelAttribute("prodDisp") ProductosDisponiblesRequest prodDisp, ModelMap modelo) {
    logger.debug("Dentro del servicio: /portal/prodDisp");
    return this.service.productosDisponibles(prodDisp);
  }

  @RequestMapping({"/portal/compraProducto"})
  public ModelAndView compraProducto(
      @ModelAttribute("compraProducto") CompraProductoRequest producto, ModelMap modelo) {
    logger.debug("Dentro del servicio: /compraProducto");
    ModelAndView mav = null;
    try {
      logger.debug("Iniciar pantalla, compraProducto");
      mav =
          new ModelAndView("portal/compraProducto", "compraProducto", new CompraProductoRequest());
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  @RequestMapping(value = {"/portal/compraProducto-final"},
      method = {org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView compraProductoFinal(
      @ModelAttribute("compraProducto") CompraProductoRequest producto, ModelMap modelo) {
    logger.debug("Dentro del servicio: /portal/compraProducto-final");
    return this.service.compraProducto(producto);
  }

  @RequestMapping({"/portal/envioSMS"})
  public ModelAndView envioSMS(@ModelAttribute("envioSMS") EnvioSMSRequest envioSMS, ModelMap modelo) {
    logger.debug("Dentro del servicio: /envioSMS");
    ModelAndView mav = null;
    try {
      logger.debug("Iniciar pantalla, envioSMS");
      mav = new ModelAndView("portal/envioSMS", "envioSMS", new EnvioSMSRequest());
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  @RequestMapping(value = {"/portal/envioSMS-final"},
      method = {org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView envioSMSFinal(@ModelAttribute("envioSMS") EnvioSMSRequest envioSMS,
      ModelMap modelo) {
    logger.debug("Dentro del servicio: /portal/envioSMS-final");
    return this.service.envioSMS(envioSMS);
  }

  @RequestMapping({"/portal/usuario"})
  public ModelAndView usuario(@ModelAttribute("usuario") CrearUsuarioRequest usuario,
      ModelMap modelo) {
    logger.debug("Dentro del servicio: /usuario");
    ModelAndView mav = null;
    try {
      logger.debug("Iniciar pantalla, usuario");
      mav = new ModelAndView("portal/usuario", "usuario", new CrearUsuarioRequest());
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  @RequestMapping(value = {"/portal/usuario-final"},
      method = {org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView usuarioFinal(@ModelAttribute("usuario") CrearUsuarioRequest usuario,
      ModelMap modelo) {
    logger.debug("Dentro del servicio: /portal/usuario-final");
    return this.service.crearUsuario(usuario);
  }

  @RequestMapping({"/portal/envioSMSAdmin"})
  public ModelAndView envioSMSAdmin(@ModelAttribute("envioSMSAdmin") EnvioSMSRequest envioSMS,
      ModelMap modelo) {
    logger.debug("Dentro del servicio: /envioSMSAdmin");
    ModelAndView mav = null;
    try {
      logger.debug("Iniciar pantalla, envioSMSAdmin");
      mav = new ModelAndView("portal/envioSMSAdmin", "envioSMSAdmin", new EnvioSMSRequest());
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  @RequestMapping(value = {"/portal/envioSMSAdmin-final"},
      method = {org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView envioSMSAdminFinal(@ModelAttribute("envioSMSAdmin") EnvioSMSRequest envioSMS,
      ModelMap modelo) {
    logger.debug("Dentro del servicio: /portal/envioSMSAdmin-final");
    return this.service.envioSMSAdmin(envioSMS);
  }

  @RequestMapping({"/portal/validarSMSToken"})
  public ModelAndView validarSMSToken(
      @ModelAttribute("validarSMSToken") ValidarSMSTokenRequest validarSMSToken, ModelMap modelo) {
    logger.debug("Dentro del servicio: /validarSMSToken");
    ModelAndView mav = null;
    try {
      logger.debug("Iniciar pantalla, envioSMSAdmin");
      mav =
          new ModelAndView("portal/validarSMSToken", "validarSMSToken",
              new ValidarSMSTokenRequest());
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  @RequestMapping(value = {"/portal/validarSMSToken-final"},
      method = {org.springframework.web.bind.annotation.RequestMethod.POST})
  public ModelAndView validarSMSTokenFinal(
      @ModelAttribute("validarSMSToken") ValidarSMSTokenRequest validarSMSToken, ModelMap modelo) {
    logger.debug("Dentro del servicio: /portal/validarSMSToken-final");
    return this.service.validarSMSToken(validarSMSToken);
  }
}
