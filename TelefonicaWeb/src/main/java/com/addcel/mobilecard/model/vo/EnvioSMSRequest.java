package com.addcel.mobilecard.model.vo;

public final class EnvioSMSRequest {
  private String sessionId;
  private String country;
  private String brandId;
  private String destinationNumber;
  private String smsText;

  public String getSessionId() {
    return this.sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public String getCountry() {
    return this.country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getBrandId() {
    return this.brandId;
  }

  public void setBrandId(String brandId) {
    this.brandId = brandId;
  }

  public String getDestinationNumber() {
    return this.destinationNumber;
  }

  public void setDestinationNumber(String destinationNumber) {
    this.destinationNumber = destinationNumber;
  }

  public String getSmsText() {
    return this.smsText;
  }

  public void setSmsText(String smsText) {
    this.smsText = smsText;
  }
}
