package com.addcel.mobilecard.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class ConsultaResponse {
  private int idError;
  private String mensajeError;
  private String operationStatus;

  public int getIdError() {
    return this.idError;
  }

  public void setIdError(int idError) {
    this.idError = idError;
  }

  public String getMensajeError() {
    return this.mensajeError;
  }

  public void setMensajeError(String mensajeError) {
    this.mensajeError = mensajeError;
  }

  public String getOperationStatus() {
    return this.operationStatus;
  }

  public void setOperationStatus(String operationStatus) {
    this.operationStatus = operationStatus;
  }
}
