package com.addcel.mobilecard.model.vo;

public final class EnvioSMSResponse {
  private int idError;
  private String mensajeError;

  public int getIdError() {
    return this.idError;
  }

  public void setIdError(int idError) {
    this.idError = idError;
  }

  public String getMensajeError() {
    return this.mensajeError;
  }

  public void setMensajeError(String mensajeError) {
    this.mensajeError = mensajeError;
  }
}
