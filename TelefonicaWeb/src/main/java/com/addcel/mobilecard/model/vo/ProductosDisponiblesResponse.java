package com.addcel.mobilecard.model.vo;

import com.addcel.ws.clientes.simfonics.PortalService.AvailableProductsTypeAvailableProduct;

public final class ProductosDisponiblesResponse {
  private int idError;
  private String mensajeError;
  private AvailableProductsTypeAvailableProduct[] productosDisponibles;

  public int getIdError() {
    return this.idError;
  }

  public void setIdError(int idError) {
    this.idError = idError;
  }

  public String getMensajeError() {
    return this.mensajeError;
  }

  public void setMensajeError(String mensajeError) {
    this.mensajeError = mensajeError;
  }

  public AvailableProductsTypeAvailableProduct[] getProductosDisponibles() {
    return this.productosDisponibles;
  }

  public void setProductosDisponibles(AvailableProductsTypeAvailableProduct[] productosDisponibles) {
    this.productosDisponibles = productosDisponibles;
  }
}
