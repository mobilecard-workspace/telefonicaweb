package com.addcel.mobilecard.model.vo;

public final class CrearUsuarioRequest {
  private String country;
  private String brandId;
  private int parentId;
  private String language;
  private String firstName;
  private String lastName;
  private String identityNum;
  private String userName;
  private String password;
  private String msisdn;

  public String getCountry() {
    return this.country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getBrandId() {
    return this.brandId;
  }

  public void setBrandId(String brandId) {
    this.brandId = brandId;
  }

  public int getParentId() {
    return this.parentId;
  }

  public void setParentId(int parentId) {
    this.parentId = parentId;
  }

  public String getLanguage() {
    return this.language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return this.lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getIdentityNum() {
    return this.identityNum;
  }

  public void setIdentityNum(String identityNum) {
    this.identityNum = identityNum;
  }

  public String getUserName() {
    return this.userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getMsisdn() {
    return this.msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }
}
