package com.addcel.mobilecard.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class RecargaResponse {
  private int idError;
  private String mensajeError;
  private int transactionId;
  private double newBalance;
  private String topupId;

  public int getIdError() {
    return this.idError;
  }

  public void setIdError(int idError) {
    this.idError = idError;
  }

  public String getMensajeError() {
    return this.mensajeError;
  }

  public void setMensajeError(String mensajeError) {
    this.mensajeError = mensajeError;
  }

  public int getTransactionId() {
    return this.transactionId;
  }

  public void setTransactionId(int transactionId) {
    this.transactionId = transactionId;
  }

  public double getNewBalance() {
    return this.newBalance;
  }

  public void setNewBalance(double newBalance) {
    this.newBalance = newBalance;
  }

  public String getTopupId() {
    return this.topupId;
  }

  public void setTopupId(String topupId) {
    this.topupId = topupId;
  }
}
