package com.addcel.mobilecard.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class ConsultaRequest {
  private String mvno;
  private String msisdn;
  private String transactionId;
  private String transactionTypeId;
  private String origin;

  public String getMvno() {
    return this.mvno;
  }

  public void setMvno(String mvno) {
    this.mvno = mvno;
  }

  public String getMsisdn() {
    return this.msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }

  public String getTransactionId() {
    return this.transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public String getTransactionTypeId() {
    return this.transactionTypeId;
  }

  public void setTransactionTypeId(String transactionTypeId) {
    this.transactionTypeId = transactionTypeId;
  }

  public String getOrigin() {
    return this.origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }
}
