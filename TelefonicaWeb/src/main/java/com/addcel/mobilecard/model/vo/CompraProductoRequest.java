package com.addcel.mobilecard.model.vo;

public final class CompraProductoRequest {
  private String sessionId;
  private String country;
  private String brandId;
  private String productsId;
  private String subscriptionId;
  private String purchasePropertyKey;
  private String purchasePropertyValue;

  public String getSessionId() {
    return this.sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public String getCountry() {
    return this.country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getBrandId() {
    return this.brandId;
  }

  public void setBrandId(String brandId) {
    this.brandId = brandId;
  }

  public String getProductsId() {
    return this.productsId;
  }

  public void setProductsId(String productsId) {
    this.productsId = productsId;
  }

  public String getSubscriptionId() {
    return this.subscriptionId;
  }

  public void setSubscriptionId(String subscriptionId) {
    this.subscriptionId = subscriptionId;
  }

  public String getPurchasePropertyKey() {
    return this.purchasePropertyKey;
  }

  public void setPurchasePropertyKey(String purchasePropertyKey) {
    this.purchasePropertyKey = purchasePropertyKey;
  }

  public String getPurchasePropertyValue() {
    return this.purchasePropertyValue;
  }

  public void setPurchasePropertyValue(String purchasePropertyValue) {
    this.purchasePropertyValue = purchasePropertyValue;
  }
}
