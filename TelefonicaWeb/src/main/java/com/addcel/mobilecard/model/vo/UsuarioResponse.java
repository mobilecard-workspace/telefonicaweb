package com.addcel.mobilecard.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class UsuarioResponse {
  private long idUsuario;
  private String login;
  private int idError;
  private String mensajeError;

  public long getIdUsuario() {
    return this.idUsuario;
  }

  public void setIdUsuario(long idUsuario) {
    this.idUsuario = idUsuario;
  }

  public String getLogin() {
    return this.login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public int getIdError() {
    return this.idError;
  }

  public void setIdError(int idError) {
    this.idError = idError;
  }

  public String getMensajeError() {
    return this.mensajeError;
  }

  public void setMensajeError(String mensajeError) {
    this.mensajeError = mensajeError;
  }
}
