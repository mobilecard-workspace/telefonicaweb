package com.addcel.mobilecard.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class RecargaRequest {
  private String mvno;
  private String msisdn;
  private String amount;
  private String currency;
  private String origin;
  private String creditCard;
  private String topupId;
  private String transactionTypeId;
  private String zipcode;
  private String city;
  private String state;
  private String pointPurchase;

  public String getMvno() {
    return this.mvno;
  }

  public void setMvno(String mvno) {
    this.mvno = mvno;
  }

  public String getMsisdn() {
    return this.msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }

  public String getAmount() {
    return this.amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public String getCurrency() {
    return this.currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getOrigin() {
    return this.origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public String getCreditCard() {
    return this.creditCard;
  }

  public void setCreditCard(String creditCard) {
    this.creditCard = creditCard;
  }

  public String getTopupId() {
    return this.topupId;
  }

  public void setTopupId(String topupId) {
    this.topupId = topupId;
  }

  public String getTransactionTypeId() {
    return this.transactionTypeId;
  }

  public void setTransactionTypeId(String transactionTypeId) {
    this.transactionTypeId = transactionTypeId;
  }

  public String getZipcode() {
    return this.zipcode;
  }

  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }

  public String getCity() {
    return this.city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getState() {
    return this.state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getPointPurchase() {
    return this.pointPurchase;
  }

  public void setPointPurchase(String pointPurchase) {
    this.pointPurchase = pointPurchase;
  }
}
