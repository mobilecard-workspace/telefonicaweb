package com.addcel.mobilecard.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class UsuarioRequest {
  private long usuario;
  private String login;
  private String password;
  private String telefono;
  private String registro;
  private String nombre;
  private String apellido;
  private String materno;
  private String tarjeta;
  private String vigencia;
  private int tipotarjeta;
  private int proveedor;
  private String mail;
  private String imei;
  private String tipo;
  private String software;
  private String modelo;
  private String cp;
  private String dom_amex;
  private String terminos;
  private int tipo_cliente;

  public long getUsuario() {
    return this.usuario;
  }

  public void setUsuario(long usuario) {
    this.usuario = usuario;
  }

  public String getLogin() {
    return this.login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getTelefono() {
    return this.telefono;
  }

  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

  public String getRegistro() {
    return this.registro;
  }

  public void setRegistro(String registro) {
    this.registro = registro;
  }

  public String getNombre() {
    return this.nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getApellido() {
    return this.apellido;
  }

  public void setApellido(String apellido) {
    this.apellido = apellido;
  }

  public String getMaterno() {
    return this.materno;
  }

  public void setMaterno(String materno) {
    this.materno = materno;
  }

  public String getTarjeta() {
    return this.tarjeta;
  }

  public void setTarjeta(String tarjeta) {
    this.tarjeta = tarjeta;
  }

  public String getVigencia() {
    return this.vigencia;
  }

  public void setVigencia(String vigencia) {
    this.vigencia = vigencia;
  }

  public int getTipotarjeta() {
    return this.tipotarjeta;
  }

  public void setTipotarjeta(int tipotarjeta) {
    this.tipotarjeta = tipotarjeta;
  }

  public int getProveedor() {
    return this.proveedor;
  }

  public void setProveedor(int proveedor) {
    this.proveedor = proveedor;
  }

  public String getMail() {
    return this.mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  public String getImei() {
    return this.imei;
  }

  public void setImei(String imei) {
    this.imei = imei;
  }

  public String getTipo() {
    return this.tipo;
  }

  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  public String getSoftware() {
    return this.software;
  }

  public void setSoftware(String software) {
    this.software = software;
  }

  public String getModelo() {
    return this.modelo;
  }

  public void setModelo(String modelo) {
    this.modelo = modelo;
  }

  public String getCp() {
    return this.cp;
  }

  public void setCp(String cp) {
    this.cp = cp;
  }

  public String getDom_amex() {
    return this.dom_amex;
  }

  public void setDom_amex(String dom_amex) {
    this.dom_amex = dom_amex;
  }

  public String getTerminos() {
    return this.terminos;
  }

  public void setTerminos(String terminos) {
    this.terminos = terminos;
  }

  public int getTipo_cliente() {
    return this.tipo_cliente;
  }

  public void setTipo_cliente(int tipo_cliente) {
    this.tipo_cliente = tipo_cliente;
  }
}
