package com.addcel.mobilecard.model.vo;

public final class ProductosDisponiblesRequest {
  private String sessionId;
  private String country;
  private String brandId;
  private String language;
  private String msisdn;

  public String getSessionId() {
    return this.sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public String getCountry() {
    return this.country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getBrandId() {
    return this.brandId;
  }

  public void setBrandId(String brandId) {
    this.brandId = brandId;
  }

  public String getLanguage() {
    return this.language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getMsisdn() {
    return this.msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }
}
