package com.addcel.mobilecard.model.vo;

public final class ValidarSMSTokenResponse {
  private int idError;
  private String mensajeError;
  private String customerId;

  public int getIdError() {
    return this.idError;
  }

  public void setIdError(int idError) {
    this.idError = idError;
  }

  public String getMensajeError() {
    return this.mensajeError;
  }

  public void setMensajeError(String mensajeError) {
    this.mensajeError = mensajeError;
  }

  public String getCustomerId() {
    return this.customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }
}
