package com.addcel.mobilecard.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class CancelacionResponse {
  private int idError;
  private String mensajeError;
  private double newBalance;

  public int getIdError() {
    return this.idError;
  }

  public void setIdError(int idError) {
    this.idError = idError;
  }

  public String getMensajeError() {
    return this.mensajeError;
  }

  public void setMensajeError(String mensajeError) {
    this.mensajeError = mensajeError;
  }

  public double getNewBalance() {
    return this.newBalance;
  }

  public void setNewBalance(double newBalance) {
    this.newBalance = newBalance;
  }
}
