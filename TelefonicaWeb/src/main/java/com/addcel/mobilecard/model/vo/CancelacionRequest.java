package com.addcel.mobilecard.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CancelacionRequest {
  private String mvno;
  private String msisdn;
  private String origin;
  private String transactionId;
  private String transactionTypeId;
  private String topupId;

  public String getMvno() {
    return this.mvno;
  }

  public void setMvno(String mvno) {
    this.mvno = mvno;
  }

  public String getMsisdn() {
    return this.msisdn;
  }

  public void setMsisdn(String msisdn) {
    this.msisdn = msisdn;
  }

  public String getTransactionId() {
    return this.transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public String getTransactionTypeId() {
    return this.transactionTypeId;
  }

  public void setTransactionTypeId(String transactionTypeId) {
    this.transactionTypeId = transactionTypeId;
  }

  public String getTopupId() {
    return this.topupId;
  }

  public void setTopupId(String topupId) {
    this.topupId = topupId;
  }

  public String getOrigin() {
    return this.origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }
}
