package com.addcel.mobilecard.services;

import com.addcel.mobilecard.model.vo.CancelacionRequest;
import com.addcel.mobilecard.model.vo.CancelacionResponse;
import com.addcel.mobilecard.model.vo.ConsultaRequest;
import com.addcel.mobilecard.model.vo.ConsultaResponse;
import com.addcel.mobilecard.model.vo.RecargaRequest;
import com.addcel.mobilecard.model.vo.RecargaResponse;
import com.addcel.ws.clientes.simfonics.TopUpInterface.CancelationRequest;
import com.addcel.ws.clientes.simfonics.TopUpInterface.CancelationResponse;
import com.addcel.ws.clientes.simfonics.TopUpInterface.Credentials;
import com.addcel.ws.clientes.simfonics.TopUpInterface.InterfaceTPProxy;
import com.addcel.ws.clientes.simfonics.TopUpInterface.QueryRequest;
import com.addcel.ws.clientes.simfonics.TopUpInterface.QueryResponse;
import com.addcel.ws.clientes.simfonics.TopUpInterface.RechargeRequest;
import com.addcel.ws.clientes.simfonics.TopUpInterface.RechargeResponse;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class TopUpService {
  private static final Logger logger = LoggerFactory.getLogger(TopUpService.class);
  private static final Credentials credentials = new Credentials("fullcarga", "topup");
  private static final InterfaceTPProxy proxy = new InterfaceTPProxy();
  private static long TOPUP_ID = 901090L;

  public RecargaResponse recargaMvno(RecargaRequest recarga) {
    RechargeRequest rechargeInput = null;
    RechargeResponse rechargeResp = null;
    RecargaResponse resp = new RecargaResponse();
    try {
      resp.setTopupId(""+TOPUP_ID++);
      rechargeInput = new RechargeRequest();
      rechargeInput.setCredentials(credentials);
      rechargeInput.setMvno(new BigInteger(recarga.getMvno()));
      rechargeInput.setMsisdn(new BigInteger(recarga.getMsisdn()));
      rechargeInput.setAmount(new BigInteger(recarga.getAmount()));
      rechargeInput.setCurrency(recarga.getCurrency());
      rechargeInput.setOrigin(recarga.getOrigin());
      rechargeInput.setCreditCard(new BigInteger(recarga.getCreditCard()));
      rechargeInput.setTopupId(new BigInteger(resp.getTopupId()));
      rechargeInput.setTransactionTypeId(new BigInteger(recarga.getTransactionTypeId()));

      logger.debug("Datos envio Recarga:");

      logger.debug("Mvno: " + recarga.getMvno());
      logger.debug("Msisdn: " + recarga.getMsisdn());
      logger.debug("Amount: " + recarga.getAmount());
      logger.debug("Currency: " + recarga.getCurrency());
      logger.debug("Currency: " + recarga.getOrigin());
      logger.debug("CreditCard: " + recarga.getCreditCard());
      logger.debug("TopupId: " + resp.getTopupId());
      logger.debug("TransactionTypeId: " + recarga.getTransactionTypeId());
      
      logger.info("Invocando WS en: {}", proxy.getEndpoint());
      rechargeResp = proxy.recharge(rechargeInput);
      resp.setIdError((rechargeResp.getResult() != null)
          && ("OK".equalsIgnoreCase(rechargeResp.getResult())) ? 0 : -1);
      resp.setMensajeError(rechargeResp.getResponseMessage());
      resp.setTransactionId(rechargeResp.getTransactionId() != null ? rechargeResp
          .getTransactionId().intValue() : 0);
      resp.setNewBalance(rechargeResp.getNewBalance() != null ? rechargeResp.getNewBalance()
          .doubleValue() : 0.0D);

      logger.debug("Result: " + rechargeResp.getResult());
      logger.debug("ResponseMessage: " + rechargeResp.getResponseMessage());
      logger.debug("TransactionId: " + rechargeResp.getTransactionId());
      logger.debug("NewBalance: " + rechargeResp.getNewBalance());
      logger.debug("TopupId: " + resp.getTopupId());
    } catch (Exception e) {
      resp.setIdError(-2);
      resp.setMensajeError("Error en el servicio de recarga: " + e.getMessage());
      logger.error("Error servicio recarga: {}", e);
    }
    return resp;
  }

  public CancelacionResponse cancelacionTranID(CancelacionRequest cancelacion) {
    CancelationRequest cancelationInput = null;
    CancelationResponse cancelationResp = null;
    CancelacionResponse resp = new CancelacionResponse();
    try {
      cancelationInput = new CancelationRequest();
      cancelationInput.setCredentials(credentials);
      cancelationInput.setMvno(new BigInteger(cancelacion.getMvno()));
      cancelationInput.setMsisdn(new BigInteger(cancelacion.getMsisdn()));
      cancelationInput.setOrigin(cancelacion.getOrigin());
      cancelationInput.setTransactionId(new BigInteger(cancelacion.getTransactionId()));
      cancelationInput.setTransactionTypeId(new BigInteger(cancelacion.getTransactionTypeId()));

      logger.debug("Datos envio Cancelacion:");
      logger.debug("Mvno: " + cancelacion.getMvno());
      logger.debug("Msisdn: " + cancelacion.getMsisdn());
      logger.debug("Origin: " + cancelacion.getOrigin());
      logger.debug("TransactionId: " + cancelacion.getTransactionId());
      logger.debug("TopupId: " + cancelacion.getTopupId());
      logger.debug("TransactionTypeId: " + cancelacion.getTransactionTypeId());

      logger.info("Invocando WS en: {}", proxy.getEndpoint());
      cancelationResp = proxy.cancelation(cancelationInput);
      resp.setIdError((cancelationResp.getResult() != null)
          && ("OK".equalsIgnoreCase(cancelationResp.getResult())) ? 0 : -1);
      resp.setMensajeError(cancelationResp.getResponseMessage());
      resp.setNewBalance(cancelationResp.getNewBalance() != null ? cancelationResp.getNewBalance()
          .doubleValue() : 0.0D);

      logger.debug("Result: " + cancelationResp.getResult());
      logger.debug("ResponseMessage: " + cancelationResp.getResponseMessage());
      logger.debug("NewBalance: " + cancelationResp.getNewBalance());
    } catch (Exception e) {
      resp.setIdError(-2);
      resp.setMensajeError("Error en el servicio de cancelacion de recarga: " + e.getMessage());
      logger.error("Error servicio cancelacion: {}", e);
    }
    return resp;
  }

  public ConsultaResponse consultaTranID(ConsultaRequest consulta) {
    QueryRequest queryInput = null;
    QueryResponse queryResp = null;
    ConsultaResponse resp = new ConsultaResponse();
    try {
      queryInput = new QueryRequest();
      queryInput.setCredentials(credentials);
      queryInput.setMvno(new BigInteger(consulta.getMvno()));
      queryInput.setMsisdn(new BigInteger(consulta.getMsisdn()));
      queryInput.setOrigin(consulta.getOrigin());
      queryInput.setTransactionId(new BigInteger(consulta.getTransactionId()));
      queryInput.setTransactionTypeId(new BigInteger(consulta.getTransactionTypeId()));

      logger.debug("Datos envio Consulta:");
      logger.debug("Mvno: " + consulta.getMvno());
      logger.debug("Msisdn: " + consulta.getMsisdn());
      logger.debug("Origin: " + consulta.getOrigin());
      logger.debug("TransactionId: " + consulta.getTransactionId());
      logger.debug("TransactionTypeId: " + consulta.getTransactionTypeId());

      logger.info("Invocando WS en: {}", proxy.getEndpoint());
      queryResp = proxy.query(queryInput);
      resp.setIdError((queryResp.getResult() != null)
          && ("OK".equalsIgnoreCase(queryResp.getResult())) ? 0 : -1);
      resp.setMensajeError(queryResp.getResponseMessage());
      if ("1".equals(queryResp.getResponseMessage())) {
        resp.setOperationStatus("DONE");
      } else if ("2".equals(queryResp.getResponseMessage())) {
        resp.setOperationStatus("CANCELLED");
      } else if ("3".equals(queryResp.getResponseMessage())) {
        resp.setOperationStatus("INPROGRESS");
      } else if ("4".equals(queryResp.getResponseMessage())) {
        resp.setOperationStatus("FAILED");
      }
      logger.debug("Result: " + queryResp.getResult());
      logger.debug("ResponseMessage: " + queryResp.getResponseMessage());
      logger.debug("OperationStatus: " + queryResp.getOperationStatus());
    } catch (Exception e) {
      resp.setIdError(-2);
      resp.setMensajeError("Error en el servicio de consulta de recarga: " + e.getMessage());
      logger.error("Error servicio consulta: {}", e);
    }
    return resp;
  }
}
