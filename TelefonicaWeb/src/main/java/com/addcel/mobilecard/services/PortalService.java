package com.addcel.mobilecard.services;

import com.addcel.mobilecard.model.vo.CompraProductoRequest;
import com.addcel.mobilecard.model.vo.CompraProductoResponse;
import com.addcel.mobilecard.model.vo.EnvioSMSRequest;
import com.addcel.mobilecard.model.vo.EnvioSMSResponse;
import com.addcel.mobilecard.model.vo.LoginRequest;
import com.addcel.mobilecard.model.vo.ProductosDisponiblesRequest;
import com.addcel.mobilecard.model.vo.ProductosDisponiblesResponse;
import com.addcel.ws.clientes.simfonics.PortalService.GetAvailableProductsRequest;
import com.addcel.ws.clientes.simfonics.PortalService.GetAvailableProductsResponse;
import com.addcel.ws.clientes.simfonics.PortalService.Login;
import com.addcel.ws.clientes.simfonics.PortalService.PortalServiceProxy;
import com.addcel.ws.clientes.simfonics.PortalService.ProductTypeProduct;
import com.addcel.ws.clientes.simfonics.PortalService.PurchaseProduct;
import com.addcel.ws.clientes.simfonics.PortalService.PurchaseProductResponse;
import com.addcel.ws.clientes.simfonics.PortalService.SendSMSRequest;
import com.addcel.ws.clientes.simfonics.PortalService.SendSMSResponse;
import java.math.BigInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class PortalService {
  private static final Logger logger = LoggerFactory.getLogger(PortalService.class);

  public com.addcel.mobilecard.model.vo.LoginResponse login(LoginRequest login) {
    Login loginInput = null;
    com.addcel.ws.clientes.simfonics.PortalService.LoginResponse loginResp = null;
    com.addcel.mobilecard.model.vo.LoginResponse resp =
        new com.addcel.mobilecard.model.vo.LoginResponse();
    try {
      loginInput = new Login();
      loginInput.setUsername(login.getUsername());
      loginInput.setPassword(login.getPassword());
      loginInput.setCountry(login.getCountry());
      loginInput.setBrandId(new BigInteger(login.getBrandId()));

      logger.debug("Datos envio login:");
      logger.debug("Username: " + loginInput.getUsername());
      logger.debug("Password: " + loginInput.getPassword());
      logger.debug("Country: " + loginInput.getCountry());
      logger.debug("BrandId: " + loginInput.getBrandId());

      loginResp = new PortalServiceProxy().login(loginInput);
      resp.setIdError((loginResp.getResult() != null)
          && ("OK".equalsIgnoreCase(loginResp.getResult())) ? 0 : -1);
      resp.setMensajeError(loginResp.getResultMessage());
      resp.setCustomerId(loginResp.getCustomerId() != null ? loginResp.getCustomerId().intValue()
          : 0);
      resp.setSessionId(loginResp.getSessionId());

      logger.debug("Result: " + loginResp.getResult());
      logger.debug("ResultMessage: " + loginResp.getResultMessage());
      logger.debug("CustomerId: " + loginResp.getCustomerId());
      logger.debug("SessionId: " + loginResp.getSessionId());
    } catch (Exception e) {
      resp.setIdError(-2);
      resp.setMensajeError("Error en el servicio de login: " + e.getMessage());
      logger.error("Error servicio login: {}", e);
    }
    return resp;
  }

  public ProductosDisponiblesResponse productosDisponibles(ProductosDisponiblesRequest prodDisp) {
    GetAvailableProductsRequest getAvailableProductsInput = null;
    GetAvailableProductsResponse prodDispResp = null;
    ProductosDisponiblesResponse resp = new ProductosDisponiblesResponse();
    try {
      getAvailableProductsInput = new GetAvailableProductsRequest();
      getAvailableProductsInput.setSessionId(prodDisp.getSessionId());
      getAvailableProductsInput.setCountry(prodDisp.getCountry());
      getAvailableProductsInput.setBrandId(prodDisp.getBrandId() != null ? new BigInteger(prodDisp
          .getBrandId()) : new BigInteger("0"));
      getAvailableProductsInput.setLanguage(prodDisp.getLanguage());
      getAvailableProductsInput.setMsisdn(prodDisp.getMsisdn() != null ? new BigInteger(prodDisp
          .getMsisdn()) : new BigInteger("0"));

      logger.debug("Datos envio Productos Disponibles:");
      logger.debug("SessionId" + prodDisp.getSessionId());
      logger.debug("Country: " + prodDisp.getCountry());
      logger.debug("BrandId: " + prodDisp.getBrandId());
      logger.debug("Language: " + prodDisp.getLanguage());
      logger.debug("Msisdn: " + prodDisp.getMsisdn());

      prodDispResp = new PortalServiceProxy().getAvailableProducts(getAvailableProductsInput);
      resp.setIdError((prodDispResp.getResult() != null)
          && ("OK".equalsIgnoreCase(prodDispResp.getResult())) ? 0 : -1);
      resp.setMensajeError(prodDispResp.getResultMessage());
      resp.setProductosDisponibles(prodDispResp.getGetAvailableProducts());

      logger.debug("Result: " + prodDispResp.getResult());
      logger.debug("ResponseMessage: " + prodDispResp.getResultMessage());
      logger.debug("ProductosDisponibles: "
          + (prodDispResp.getGetAvailableProducts() != null ? prodDispResp
              .getGetAvailableProducts().length : 0));
    } catch (Exception e) {
      resp.setIdError(-2);
      resp.setMensajeError("Error en el servicio de productos Disponibles: " + e.getMessage());
      logger.error("Error servicio productos Disponibles: {}", e);
    }
    return resp;
  }

  public CompraProductoResponse compraProducto(CompraProductoRequest producto) {
    PurchaseProduct purchaseProductInput = null;
    PurchaseProductResponse purchaseProductResp = null;
    CompraProductoResponse resp = new CompraProductoResponse();
    try {
      purchaseProductInput = new PurchaseProduct();
      purchaseProductInput.setSessionId(producto.getSessionId());
      purchaseProductInput.setCountry(producto.getCountry());
      purchaseProductInput.setBrandId(new BigInteger(producto.getBrandId()));
      purchaseProductInput.setSubscriptionId(new BigInteger(producto.getSubscriptionId()));
      purchaseProductInput.setProducts(new ProductTypeProduct[1]);
      purchaseProductInput.getProducts()[0] =
          new ProductTypeProduct(new BigInteger(producto.getProductsId()));

      logger.debug("Datos envio compraProducto:");
      logger.debug("SessionId: " + producto.getSessionId());
      logger.debug("Country: " + producto.getCountry());
      logger.debug("BrandId: " + producto.getBrandId());
      logger.debug("SubscriptionId: " + producto.getSubscriptionId());
      logger.debug("ProductsId: " + producto.getProductsId());
      logger.debug("PurchasePropertyKey: " + producto.getPurchasePropertyKey());
      logger.debug("PurchasePropertyValue: " + producto.getPurchasePropertyValue());

      purchaseProductResp = new PortalServiceProxy().purchaseProduct(purchaseProductInput);
      resp.setIdError((purchaseProductResp.getResult() != null)
          && ("OK".equalsIgnoreCase(purchaseProductResp.getResult())) ? 0 : -1);
      resp.setMensajeError(purchaseProductResp.getResultMessage());

      logger.debug("Result: " + purchaseProductResp.getResult());
      logger.debug("ResponseMessage: " + purchaseProductResp.getResultMessage());
    } catch (Exception e) {
      resp.setIdError(-2);
      resp.setMensajeError("Error en el servicio de compra Producto: " + e.getMessage());
      logger.error("Error servicio compraProducto: {}", e);
    }
    return resp;
  }

  public EnvioSMSResponse envioSMS(EnvioSMSRequest envioSMS) {
    SendSMSRequest sendSMSInput = null;
    SendSMSResponse sendSMSResp = null;
    EnvioSMSResponse resp = new EnvioSMSResponse();
    try {
      sendSMSInput = new SendSMSRequest();
      sendSMSInput.setSessionId(envioSMS.getSessionId());
      sendSMSInput.setCountry(envioSMS.getCountry());
      sendSMSInput.setBrandId(envioSMS.getBrandId() != null ? new BigInteger(envioSMS.getBrandId())
          : new BigInteger("0"));
      sendSMSInput.setDestinationNumber(envioSMS.getDestinationNumber());
      sendSMSInput.setSmsText(envioSMS.getSmsText());

      logger.debug("Datos envio Productos Disponibles:");
      logger.debug("SessionId" + envioSMS.getSessionId());
      logger.debug("Country: " + envioSMS.getCountry());
      logger.debug("BrandId: " + envioSMS.getBrandId());
      logger.debug("DestinationNumber: " + envioSMS.getDestinationNumber());
      logger.debug("SmsText" + envioSMS.getSmsText());

      sendSMSResp = new PortalServiceProxy().sendSMS(sendSMSInput);
      resp.setIdError((sendSMSResp.getResult() != null)
          && ("OK".equalsIgnoreCase(sendSMSResp.getResult())) ? 0 : -1);
      resp.setMensajeError(sendSMSResp.getResultMessage());

      logger.debug("Result: " + sendSMSResp.getResult());
      logger.debug("ResponseMessage: " + sendSMSResp.getResultMessage());
    } catch (Exception e) {
      resp.setIdError(-2);
      resp.setMensajeError("Error en el servicio de envio SMS: " + e.getMessage());
      logger.error("Error servicio envio SMS: {}", e);
    }
    return resp;
  }
}
