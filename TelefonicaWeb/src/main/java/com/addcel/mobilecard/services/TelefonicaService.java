package com.addcel.mobilecard.services;

import com.addcel.mobilecard.model.vo.CancelacionRequest;
import com.addcel.mobilecard.model.vo.CancelacionResponse;
import com.addcel.mobilecard.model.vo.CompraProductoRequest;
import com.addcel.mobilecard.model.vo.CompraProductoResponse;
import com.addcel.mobilecard.model.vo.ConsultaRequest;
import com.addcel.mobilecard.model.vo.ConsultaResponse;
import com.addcel.mobilecard.model.vo.CrearUsuarioRequest;
import com.addcel.mobilecard.model.vo.CrearUsuarioResponse;
import com.addcel.mobilecard.model.vo.EnvioSMSRequest;
import com.addcel.mobilecard.model.vo.EnvioSMSResponse;
import com.addcel.mobilecard.model.vo.LoginRequest;
import com.addcel.mobilecard.model.vo.LoginResponse;
import com.addcel.mobilecard.model.vo.ProductosDisponiblesRequest;
import com.addcel.mobilecard.model.vo.ProductosDisponiblesResponse;
import com.addcel.mobilecard.model.vo.RecargaRequest;
import com.addcel.mobilecard.model.vo.RecargaResponse;
import com.addcel.mobilecard.model.vo.ValidarSMSTokenRequest;
import com.addcel.mobilecard.model.vo.ValidarSMSTokenResponse;
import com.addcel.mobilecard.utils.ManejadorFechas;
import java.util.Calendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

@Service
public class TelefonicaService {
  private static final Logger logger = LoggerFactory.getLogger(TelefonicaService.class);
  @Autowired
  private TopUpService topUpService;
  @Autowired
  private PortalService portalService;
  @Autowired
  private AdminService adminService;
  private String sessionId;
  private Calendar timeSession;

  public ModelAndView recargaMvno(RecargaRequest recarga) {
    ModelAndView mav = null;
    RecargaResponse resp = new RecargaResponse();
    try {
      resp = this.topUpService.recargaMvno(recarga);
      mav = new ModelAndView("portal/recarga", "recarga", recarga);
      mav.addObject("resp", resp);
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  public ModelAndView cancelacionTranID(CancelacionRequest cancelacion) {
    ModelAndView mav = null;
    CancelacionResponse resp = new CancelacionResponse();
    try {
      resp = this.topUpService.cancelacionTranID(cancelacion);
      mav = new ModelAndView("portal/cancelacion", "cancelacion", cancelacion);
      mav.addObject("resp", resp);
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  public ModelAndView consultaTranID(ConsultaRequest consulta) {
    ConsultaResponse resp = new ConsultaResponse();
    ModelAndView mav = null;
    try {
      resp = this.topUpService.consultaTranID(consulta);
      mav = new ModelAndView("portal/consulta", "consulta", consulta);
      mav.addObject("resp", resp);
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  public ModelAndView login(LoginRequest login) {
    LoginResponse resp = new LoginResponse();
    ModelAndView mav = null;
    try {
      resp = this.portalService.login(login);
      mav = new ModelAndView("portal/login", "login", login);
      mav.addObject("resp", resp);
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  public ModelAndView productosDisponibles(ProductosDisponiblesRequest prodDisp) {
    ProductosDisponiblesResponse resp = new ProductosDisponiblesResponse();
    ModelAndView mav = null;
    try {
      resp = this.portalService.productosDisponibles(prodDisp);
      mav = new ModelAndView("portal/prodDisp", "productosDisponibles", prodDisp);
      mav.addObject("resp", resp);
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  public ModelAndView compraProducto(CompraProductoRequest producto) {
    CompraProductoResponse resp = new CompraProductoResponse();
    ModelAndView mav = null;
    try {
      resp = this.portalService.compraProducto(producto);
      mav = new ModelAndView("portal/compraProducto", "compraProducto", producto);
      mav.addObject("resp", resp);
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  public ModelAndView envioSMS(EnvioSMSRequest envioSMS) {
    EnvioSMSResponse resp = new EnvioSMSResponse();
    ModelAndView mav = null;
    try {
      resp = this.portalService.envioSMS(envioSMS);
      mav = new ModelAndView("portal/envioSMS", "envioSMS", envioSMS);
      mav.addObject("resp", resp);
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  public ModelAndView crearUsuario(CrearUsuarioRequest usuario) {
    CrearUsuarioResponse resp = new CrearUsuarioResponse();
    ModelAndView mav = null;
    try {
      resp = this.adminService.crearUsuario(usuario);
      mav = new ModelAndView("portal/usuario", "usuario", usuario);
      mav.addObject("resp", resp);
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  public ModelAndView envioSMSAdmin(EnvioSMSRequest envioSMS) {
    EnvioSMSResponse resp = new EnvioSMSResponse();
    ModelAndView mav = null;
    try {
      resp = this.adminService.envioSMS(envioSMS);
      mav = new ModelAndView("portal/envioSMSAdmin", "envioSMSAdmin", envioSMS);
      mav.addObject("resp", resp);
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  public ModelAndView validarSMSToken(ValidarSMSTokenRequest validarSMSToken) {
    ValidarSMSTokenResponse resp = new ValidarSMSTokenResponse();
    ModelAndView mav = null;
    try {
      resp = this.adminService.validarSMSToken(validarSMSToken);
      mav = new ModelAndView("portal/validarSMSToken", "validarSMSToken", validarSMSToken);
      mav.addObject("resp", resp);
    } catch (Exception e) {
      mav = new ModelAndView("portal/error");
      mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
    }
    return mav;
  }

  public String validaSessionID() {
    LoginRequest login = new LoginRequest();
    LoginResponse resp = new LoginResponse();
    if (this.sessionId == null) {
      this.timeSession = Calendar.getInstance();
      login.setBrandId("6");
      login.setCountry("MX");
      login.setPassword("simfonics");
      login.setUsername("TESTCHED@TEST.COM");

      resp = this.portalService.login(login);

      this.sessionId = resp.getSessionId();
    } else if (ManejadorFechas.diferenciasDeFechas(this.timeSession, Calendar.getInstance()) > 5L) {
      this.timeSession = Calendar.getInstance();
      login.setBrandId("6");
      login.setCountry("MX");
      login.setPassword("simfonics");
      login.setUsername("TESTCHED@TEST.COM");

      resp = this.portalService.login(login);
      this.sessionId = resp.getSessionId();
    }
    return this.sessionId;
  }
}
