package com.addcel.mobilecard.services;

import com.addcel.mobilecard.model.vo.CrearUsuarioRequest;
import com.addcel.mobilecard.model.vo.CrearUsuarioResponse;
import com.addcel.mobilecard.model.vo.EnvioSMSRequest;
import com.addcel.mobilecard.model.vo.EnvioSMSResponse;
import com.addcel.mobilecard.model.vo.ValidarSMSTokenRequest;
import com.addcel.mobilecard.model.vo.ValidarSMSTokenResponse;
import com.addcel.ws.clientes.simfonics.AdminService.AdminServiceProxy;
import com.addcel.ws.clientes.simfonics.AdminService.CreateCustomer;
import com.addcel.ws.clientes.simfonics.AdminService.CreateCustomerResponse;
import com.addcel.ws.clientes.simfonics.AdminService.CustomerPropertiesTypeCustomerProperty;
import com.addcel.ws.clientes.simfonics.AdminService.SendSMSRequest;
import com.addcel.ws.clientes.simfonics.AdminService.SendSMSResponse;
import com.addcel.ws.clientes.simfonics.AdminService.ValidateSMSToken;
import com.addcel.ws.clientes.simfonics.AdminService.ValidateSMSTokenResponse;
import java.math.BigInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class AdminService {
  private static final Logger logger = LoggerFactory.getLogger(AdminService.class);

  public CrearUsuarioResponse crearUsuario(CrearUsuarioRequest usuario) {
    CreateCustomer createCustomerInput = null;
    CreateCustomerResponse createCustomerResp = null;
    CrearUsuarioResponse resp = new CrearUsuarioResponse();
    try {
      createCustomerInput = new CreateCustomer();
      createCustomerInput.setCountry(usuario.getCountry());
      createCustomerInput.setBrandId(new BigInteger(usuario.getBrandId()));
      createCustomerInput.setLanguage(usuario.getLanguage());
      createCustomerInput.setMsisdn(new BigInteger(usuario.getMsisdn()));
      createCustomerInput.setUserName(usuario.getUserName());
      createCustomerInput.setPassword(usuario.getPassword());
      createCustomerInput.setFirstName(usuario.getFirstName());
      createCustomerInput.setLastName(usuario.getLastName());
      createCustomerInput.setCustomerProperties(new CustomerPropertiesTypeCustomerProperty[0]);

      logger.debug("Datos envio login:");
      logger.debug("Country: " + createCustomerInput.getCountry());
      logger.debug("BrandId: " + createCustomerInput.getBrandId());
      logger.debug("Language: " + createCustomerInput.getLanguage());
      logger.debug("Msisdn: " + createCustomerInput.getMsisdn());
      logger.debug("UserName: " + createCustomerInput.getUserName());
      logger.debug("Password: " + createCustomerInput.getPassword());
      logger.debug("FirstName: " + createCustomerInput.getFirstName());
      logger.debug("LastName: " + createCustomerInput.getLastName());

      createCustomerResp = new AdminServiceProxy().createCustomer(createCustomerInput);
      resp.setIdError((createCustomerResp.getResult() != null)
          && ("OK".equalsIgnoreCase(createCustomerResp.getResult())) ? 0 : -1);
      resp.setMensajeError(createCustomerResp.getResultMessage());

      logger.debug("Result: " + createCustomerResp.getResult());
      logger.debug("ResultMessage: " + createCustomerResp.getResultMessage());
    } catch (Exception e) {
      resp.setIdError(-2);
      resp.setMensajeError("Error en el servicio de crearUsuario: " + e.getMessage());
      logger.error("Error servicio crearUsuario: {}", e);
    }
    return resp;
  }

  public EnvioSMSResponse envioSMS(EnvioSMSRequest envioSMS) {
    SendSMSRequest sendSMSInput = null;
    SendSMSResponse sendSMSResp = null;
    EnvioSMSResponse resp = new EnvioSMSResponse();
    try {
      sendSMSInput = new SendSMSRequest();

      sendSMSInput.setCountry(envioSMS.getCountry());
      sendSMSInput.setBrandId(envioSMS.getBrandId() != null ? new BigInteger(envioSMS.getBrandId())
          : new BigInteger("0"));
      sendSMSInput.setDestinationNumber(envioSMS.getDestinationNumber());
      sendSMSInput.setSmsText(envioSMS.getSmsText());

      logger.debug("Datos envio Productos Disponibles:");

      logger.debug("Country: " + envioSMS.getCountry());
      logger.debug("BrandId: " + envioSMS.getBrandId());
      logger.debug("DestinationNumber: " + envioSMS.getDestinationNumber());
      logger.debug("SmsText" + envioSMS.getSmsText());

      sendSMSResp = new AdminServiceProxy().sendSMS(sendSMSInput);
      resp.setIdError((sendSMSResp.getResult() != null)
          && ("OK".equalsIgnoreCase(sendSMSResp.getResult())) ? 0 : -1);
      resp.setMensajeError(sendSMSResp.getResultMessage());

      logger.debug("Result: " + sendSMSResp.getResult());
      logger.debug("ResponseMessage: " + sendSMSResp.getResultMessage());
    } catch (Exception e) {
      resp.setIdError(-2);
      resp.setMensajeError("Error en el servicio de envio SMS: " + e.getMessage());
      logger.error("Error servicio envio SMS: {}", e);
    }
    return resp;
  }

  public ValidarSMSTokenResponse validarSMSToken(ValidarSMSTokenRequest validarSMSToken) {
    ValidateSMSToken validateSMSTokenInput = null;
    ValidateSMSTokenResponse sendSMSResp = null;
    ValidarSMSTokenResponse resp = new ValidarSMSTokenResponse();
    try {
      validateSMSTokenInput = new ValidateSMSToken();

      validateSMSTokenInput.setCountry(validarSMSToken.getCountry());
      validateSMSTokenInput.setBrandId(validarSMSToken.getBrandId() != null ? new BigInteger(
          validarSMSToken.getBrandId()) : new BigInteger("0"));
      validateSMSTokenInput.setTokenType(validarSMSToken.getTokenType() != null ? new BigInteger(
          validarSMSToken.getTokenType()) : new BigInteger("0"));
      validateSMSTokenInput.setToken(validarSMSToken.getToken());

      logger.debug("Datos envio Productos Disponibles:");

      logger.debug("Country: " + validarSMSToken.getCountry());
      logger.debug("BrandId: " + validarSMSToken.getBrandId());
      logger.debug("Token: " + validarSMSToken.getToken());
      logger.debug("TokenType: " + validarSMSToken.getTokenType());

      sendSMSResp = new AdminServiceProxy().validateSMSToken(validateSMSTokenInput);
      resp.setIdError((sendSMSResp.getResult() != null)
          && ("OK".equalsIgnoreCase(sendSMSResp.getResult())) ? 0 : -1);
      resp.setMensajeError(sendSMSResp.getResultMessage());
      resp.setCustomerId(sendSMSResp.getCustomerId() != null ? sendSMSResp.getCustomerId()
          .toString() : "");

      logger.debug("Result: " + sendSMSResp.getResult());
      logger.debug("ResponseMessage: " + sendSMSResp.getResultMessage());
      logger.debug("CustomerId: " + sendSMSResp.getCustomerId());
    } catch (Exception e) {
      resp.setIdError(-2);
      resp.setMensajeError("Error en el servicio de validar SMS token: " + e.getMessage());
      logger.error("Error servicio validar SMS token: {}", e);
    }
    return resp;
  }
}
