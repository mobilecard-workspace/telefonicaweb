package com.addcel.mobilecard.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.GeneralSecurityException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class UtilsService {
  private static final Logger logger = LoggerFactory.getLogger(UtilsService.class);
  @Autowired
  private ObjectMapper mapperJk;

  public <T> String objectToJson(T object) {
    String json = null;
    try {
      json = mapperJk.writeValueAsString(object);
    } catch (JsonGenerationException e) {
      logger.error("1.- Error al generar json: {}", e);
    } catch (JsonMappingException e) {
      logger.error("2.- Error al mapperar objeccto: {}", e);
    } catch (IOException e) {
      logger.error("3.- Error al generar json: {}", e);
    }
    return json;
  }

  public <T> Object jsonToObject(String json, Class<T> clase) {
    Object obj = null;
    try {
      obj = mapperJk.readValue(json, clase);
    } catch (JsonParseException e) {
      logger.error("Error al parsear cadena json 1: {}", e);
    } catch (JsonMappingException e) {
      logger.error("Error al parsear cadena json 2: {}", e);
    } catch (IOException e) {
      logger.error("Error al parsear cadena json 3: {}", e);
    }
    return obj;
  }

  public String peticionUrlPostParams(String urlRemota, String parametros) {
    String respuesta = null;
    URL url;
    try {
      url = new URL(urlRemota);
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      // Se indica que se escribira en la conexi�n
      con.setDoOutput(true);
      con.setRequestMethod("POST");
      // Se escribe los parametros enviados a la url
      OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
      wr.write(parametros);
      wr.close();
      if (con.getResponseCode() == HttpURLConnection.HTTP_OK
          || con.getResponseCode() == HttpURLConnection.HTTP_ACCEPTED) {
        BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String jsonString;
        while ((jsonString = br.readLine()) != null) {
          sb.append(jsonString);
        }
        respuesta = sb.toString();
      } else {
        respuesta = "ERROR";
      }
      con.disconnect();
    } catch (MalformedURLException e) {
      logger.error("ERROR URL malformada: {}", e);
    } catch (IOException e) {
      logger.error("ERROR IO : {}", e);
    }
    return respuesta;
  }

  public String peticionHttpsUrlParams(String urlServicio, String parametros) {
    InputStream is = null;
    OutputStream os = null;
    InputStreamReader isr = null;
    BufferedWriter writer = null;
    BufferedReader br = null;
    HttpURLConnection conn = null;
    URL url = null;
    String inputLine = null;
    StringBuffer sbf = new StringBuffer();

    HostnameVerifier hv = new HostnameVerifier() {
      public boolean verify(String urlHostName, SSLSession session) {
        logger.debug("Warning: URL Host: " + urlHostName + " vs. " + session.getPeerHost());
        return true;
      }
    };

    try {
      trustAllHttpsCertificates();
      HttpsURLConnection.setDefaultHostnameVerifier(hv);
      url = new URL(urlServicio);
      conn = (HttpURLConnection) url.openConnection();
      if (conn != null) {
        conn.setConnectTimeout(10000);
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        if (parametros != null) {
          conn.setDoOutput(true);
          os = conn.getOutputStream();
          writer = new BufferedWriter(new OutputStreamWriter(os));
          writer.write(parametros);
          writer.flush();
          writer.close();
          os.flush();
          os.close();
          conn.connect();
        }
        is = conn.getInputStream();
        isr = new InputStreamReader(is);
        br = new BufferedReader(isr);
        while ((inputLine = br.readLine()) != null) {
          sbf.append(inputLine);
        }
        // logger.debug("Respuesta notificacion: {}", sbf);
      }
    } catch (GeneralSecurityException e) {
      logger.info("Error Al generar certificados: {}", e);
    } catch (MalformedURLException e) {
      logger.info("error al conectar con la URL: " + urlServicio + "\n {}", e);
    } catch (ProtocolException e) {
      logger.info("Error al enviar datos: {}", e);
    } catch (IOException e) {
      logger.info("Error al algo: {}", e);
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (conn != null) {
        try {
          conn.disconnect();
        } catch (Exception e) {
          logger.error("Exception : ", e);
        }
      }
      if (br != null) {
        try {
          br.close();
        } catch (Exception e) {
          logger.error("Exception : ", e);
        }
      }
      if (isr != null) {
        try {
          isr.close();
        } catch (Exception e) {
          logger.error("Exception : ", e);
        }
      }
      if (is != null) {
        try {
          is.close();
        } catch (Exception e) {
          logger.error("Exception : ", e);
        }
      }
    }
    return sbf.toString();
  }

  public static class miTM implements javax.net.ssl.TrustManager, javax.net.ssl.X509TrustManager {
    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
      return null;
    }

    public boolean isServerTrusted(java.security.cert.X509Certificate[] certs) {
      return true;
    }

    public boolean isClientTrusted(java.security.cert.X509Certificate[] certs) {
      return true;
    }

    public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
        throws java.security.cert.CertificateException {
      return;
    }

    public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
        throws java.security.cert.CertificateException {
      return;
    }
  }

  private static void trustAllHttpsCertificates() throws Exception {
    // Create a trust manager that does not validate certificate chains:
    javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
    javax.net.ssl.TrustManager tm = new miTM();
    trustAllCerts[0] = tm;
    javax.net.ssl.SSLContext sc = javax.net.ssl.SSLContext.getInstance("SSL");
    sc.init(null, trustAllCerts, null);
    javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

  }
}
