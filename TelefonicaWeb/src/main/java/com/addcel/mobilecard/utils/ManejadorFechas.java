package com.addcel.mobilecard.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public final class ManejadorFechas {
  private ManejadorFechas() {}

  public static String getFechaActual() {
    java.util.Date ahora = new java.util.Date();
    SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
    return formateador.format(ahora);
  }

  public static String getHoraActual() {
    java.util.Date ahora = new java.util.Date();
    SimpleDateFormat formateador = new SimpleDateFormat("hh:mm:ss");
    return formateador.format(ahora);
  }

  public static java.sql.Date sumarFechasDias(java.sql.Date fch, int dias) {
    Calendar cal = new GregorianCalendar();
    cal.setTimeInMillis(fch.getTime());
    cal.add(5, dias);
    return new java.sql.Date(cal.getTimeInMillis());
  }

  public static synchronized java.sql.Date restarFechasDias(java.sql.Date fch, int dias) {
    Calendar cal = new GregorianCalendar();
    cal.setTimeInMillis(fch.getTime());
    cal.add(5, -dias);
    return new java.sql.Date(cal.getTimeInMillis());
  }

  public static synchronized int diferenciasDeFechas(java.util.Date fechaInicial,
      java.util.Date fechaFinal) {
    DateFormat df = DateFormat.getDateInstance(2);
    String fechaInicioString = df.format(fechaInicial);
    try {
      fechaInicial = df.parse(fechaInicioString);
    } catch (ParseException localParseException) {
    }
    String fechaFinalString = df.format(fechaFinal);
    try {
      fechaFinal = df.parse(fechaFinalString);
    } catch (ParseException localParseException1) {
    }
    long fechaInicialMs = fechaInicial.getTime();
    long fechaFinalMs = fechaFinal.getTime();
    long diferencia = fechaFinalMs - fechaInicialMs;
    double dias = Math.floor(diferencia / 86400000L);
    return (int) dias;
  }

  public static synchronized long diferenciasDeFechas(Calendar fechaInicial, Calendar fechaFinal) {
    long milis1 = fechaInicial.getTimeInMillis();
    long milis2 = fechaFinal.getTimeInMillis();

    long diff = milis2 - milis1;

    return diff / 60000L;
  }

  public static synchronized java.util.Date deStringToDate(String fecha) {
    SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
    try {
      return formatoDelTexto.parse(fecha);
    } catch (ParseException ex) {
      ex.printStackTrace();
    }
    return null;
  }
}
