package com.addcel.mobilecard.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class PeticionPost {
  private static final Logger logger = LoggerFactory.getLogger(PeticionPost.class);

  private URL url;

  String data;

  public PeticionPost(String url) throws MalformedURLException {
    logger.info("Intentando conectar con: " + url);
    this.url = new URL(url);
    data = "";

  }

  public void add(String propiedad, String valor) throws UnsupportedEncodingException {
    logger.info("Setting data: " + propiedad + ": " + valor);
    if (data.length() > 0)
      data += "&" + URLEncoder.encode(propiedad, "UTF-8") + "=" + URLEncoder.encode(valor, "UTF-8");
    else
      data += URLEncoder.encode(propiedad, "UTF-8") + "=" + URLEncoder.encode(valor, "UTF-8");
  }

  public String getRespuesta() throws IOException {
    String respuesta = "";
    OutputStream os = null;
    HttpURLConnection conn = null;
    BufferedWriter writer = null;
    BufferedReader br = null;
    InputStream is = null;
    InputStreamReader isr = null;
    String inputLine = null;
    StringBuffer sbf = new StringBuffer();
    try {
      conn = (HttpURLConnection) url.openConnection();
      if (conn != null) {
        conn.setDoOutput(true);
        conn.setConnectTimeout(10000);
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        if (data != null) {
          conn.setDoOutput(true);
          os = conn.getOutputStream();
          writer = new BufferedWriter(new OutputStreamWriter(os));
          writer.write(data);
          writer.flush();

          conn.connect();
        }
        logger.info("Conexion satisfactoria... ");
        br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        while ((inputLine = br.readLine()) != null) {
          sbf.append(inputLine);
        }
        logger.info("Respuesta de URL: " + sbf.toString());
        writer.close();
        os.close();
        respuesta = "Respuesta enviada con exito. ";
      }
    } catch (Exception e) {
      logger.error("Error al leer la url " + url, e);
    } finally {
      if (conn != null) {
        try {
          conn.disconnect();
        } catch (Exception e) {
          logger.error("Exception : ", e);
        }
      }
      if (br != null) {
        try {
          br.close();
        } catch (Exception e) {
          logger.error("Exception : ", e);
        }
      }

      if (isr != null) {
        try {
          isr.close();
        } catch (Exception e) {
          logger.error("Exception : ", e);
        }
      }
      if (is != null) {
        try {
          is.close();
        } catch (Exception e) {
          logger.error("Exception : ", e);
        }
      }
    }
    return respuesta;
  }
}
