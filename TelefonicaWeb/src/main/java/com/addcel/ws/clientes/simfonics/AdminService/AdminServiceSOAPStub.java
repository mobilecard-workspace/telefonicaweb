/**
 * AdminServiceSOAPStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class AdminServiceSOAPStub extends org.apache.axis.client.Stub implements com.addcel.ws.clientes.simfonics.AdminService.AdminService {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[38];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
        _initOperationDesc4();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("checkUsernameSecurityAnswer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "userName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "securityQuestion"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "securityAnswer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getUsernameSecurityQuestion");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "userName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "securityQuestion"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("sendResetPasswordMail");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "userName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("createCustomer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "createCustomer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">createCustomer"), com.addcel.ws.clientes.simfonics.AdminService.CreateCustomer.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">createCustomerResponse"));
        oper.setReturnClass(com.addcel.ws.clientes.simfonics.AdminService.CreateCustomerResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "createCustomerResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getCustomerPropertiesByBrand");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "language"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandProperties"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "brandPropertiesType"), com.addcel.ws.clientes.simfonics.AdminService.BrandPropertiesTypeBrandProperty[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("", "brandProperty"));
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getBrandProducts");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "language"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandProducts"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "brandProductsType"), com.addcel.ws.clientes.simfonics.AdminService.BrandProductsTypeBrandProduct[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("", "brandProduct"));
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getIncidentTypesByBrand");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "language"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "incidentTypes"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "incidentTypesType"), com.addcel.ws.clientes.simfonics.AdminService.IncidentTypesTypeIncidentType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("", "incidentType"));
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getOrdersCatalogByBrand");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ordersTypeId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ordersCategoryId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ordersSubCategoryId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "ordersCatalog"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "ordersCatalogType"), com.addcel.ws.clientes.simfonics.AdminService.OrdersCatalogTypeOrder[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("", "order"));
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getIncidentCategoriesByBrand");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "typeId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "language"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "incidentCategories"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "incidentCategoriesType"), com.addcel.ws.clientes.simfonics.AdminService.IncidentCategoriesTypeIncidentCategory[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("", "incidentCategory"));
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getIncidentSubCategoriesByBrand");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "typeId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "categoryId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "language"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "incidentSubCategories"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "incidentSubCategoriesType"), com.addcel.ws.clientes.simfonics.AdminService.IncidentSubCategoriesTypeIncidentSubCategory[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("", "incidentSubCategory"));
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("changeUserPassword");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "token"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "newPassword"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "sessionId"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getUsageTypeNames");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "language"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "usageTypeNames"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "usageTypeNames"), com.addcel.ws.clientes.simfonics.AdminService.UsageTypeNamesUsageTypeName[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("", "usageTypeName"));
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("topupValidation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "msisdn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "amount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"), java.math.BigDecimal.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("addVoucherByMsisdn");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "msisdn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "voucherPinCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "balance"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"), java.math.BigDecimal.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "currency"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("addBalanceByMsisdn");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "msisdn"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "amount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"), java.math.BigDecimal.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "paymentReference"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "productId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "balance"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"), java.math.BigDecimal.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "currency"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("checkUserName");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "userName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "responseMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultCheckUserName"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("checkIdentityNum");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "identityNum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "responseMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultCheckIdentityNum"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("sendEmailValidation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "userName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "mailLink"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("validateUserMail");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "userName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("validateSMSToken");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "validateSMSToken"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">validateSMSToken"), com.addcel.ws.clientes.simfonics.AdminService.ValidateSMSToken.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">validateSMSTokenResponse"));
        oper.setReturnClass(com.addcel.ws.clientes.simfonics.AdminService.ValidateSMSTokenResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "validateSMSTokenResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("checkNickName");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "nickName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "isAvailable"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("doPayment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "language"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "amount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"), java.math.BigDecimal.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "currency"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "paymentOperationId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "paymentTypeId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "description"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "creditCard"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "creditCardType"), com.addcel.ws.clientes.simfonics.AdminService.CreditCardType.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "transactionResult"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getPaymentTypeNames");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "language"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "paymentTypeNames"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "paymentTypeNamesType"), com.addcel.ws.clientes.simfonics.AdminService.PaymentTypeNamesTypePaymentTypeName[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("", "paymentTypeName"));
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getPaymentMethodNames");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "language"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "paymentMethodNames"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "paymentMethodNamesType"), com.addcel.ws.clientes.simfonics.AdminService.PaymentMethodNamesTypePaymentMethodName[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("", "paymentMethodName"));
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[23] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getPurchaseProductParameters");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "language"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "productTypeId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "purchaseParameters"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "productsParametersType"), com.addcel.ws.clientes.simfonics.AdminService.ProductsParametersTypePurchaseParameter[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("", "purchaseParameter"));
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[24] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getPaymentOperationNames");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "language"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "paymentOperationNames"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "paymentOperationNamesType"), com.addcel.ws.clientes.simfonics.AdminService.PaymentOperationNamesTypePaymentOperationName[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("", "paymentOperationName"));
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[25] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("recruiterValidation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "country"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "brandId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "msisdn_recruiter"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "result"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "resultMessage"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "isValid"), org.apache.axis.description.ParameterDesc.OUT, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"), java.math.BigInteger.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[26] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("sendSMS");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "sendSMSRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">sendSMSRequest"), com.addcel.ws.clientes.simfonics.AdminService.SendSMSRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">sendSMSResponse"));
        oper.setReturnClass(com.addcel.ws.clientes.simfonics.AdminService.SendSMSResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "sendSMSResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[27] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetFileEntityTypeList");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "GetFileEntityTypeListRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetFileEntityTypeListRequest"), com.addcel.ws.clientes.simfonics.AdminService.GetFileEntityTypeListRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetFileEntityTypeListResponse"));
        oper.setReturnClass(com.addcel.ws.clientes.simfonics.AdminService.GetFileEntityTypeListResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "GetFileEntityTypeListResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[28] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ChangeUserPasswordTuenti");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "changeUserPasswordTuenti"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">changeUserPasswordTuenti"), com.addcel.ws.clientes.simfonics.AdminService.ChangeUserPasswordTuenti.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">changeUserPasswordTuentiResponse"));
        oper.setReturnClass(com.addcel.ws.clientes.simfonics.AdminService.ChangeUserPasswordTuentiResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "changeUserPasswordTuentiResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[29] = oper;

    }

    private static void _initOperationDesc4(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLogisticPropertiesByBrand");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "GetLogisticPropertiesByBrandRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetLogisticPropertiesByBrandRequest"), com.addcel.ws.clientes.simfonics.AdminService.GetLogisticPropertiesByBrandRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetLogisticPropertiesByBrandResponse"));
        oper.setReturnClass(com.addcel.ws.clientes.simfonics.AdminService.GetLogisticPropertiesByBrandResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "GetLogisticPropertiesByBrandResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[30] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLogisticOperatorNames");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "GetLogisticOperatorNamesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetLogisticOperatorNamesRequest"), com.addcel.ws.clientes.simfonics.AdminService.GetLogisticOperatorNamesRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetLogisticOperatorNamesResponse"));
        oper.setReturnClass(com.addcel.ws.clientes.simfonics.AdminService.GetLogisticOperatorNamesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "GetLogisticOperatorNamesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[31] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetLogisticStatusName");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "GetLogisticStatusNameRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetLogisticStatusNameRequest"), com.addcel.ws.clientes.simfonics.AdminService.GetLogisticStatusNameRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetLogisticStatusNameResponse"));
        oper.setReturnClass(com.addcel.ws.clientes.simfonics.AdminService.GetLogisticStatusNameResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "GetLogisticStatusNameResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[32] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetPortabilityOperatorNames");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "GetPortabilityOperatorNamesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityOperatorNamesRequest"), com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityOperatorNamesRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityOperatorNamesResponse"));
        oper.setReturnClass(com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityOperatorNamesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "GetPortabilityOperatorNamesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[33] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetPortabilityStatusNames");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "GetPortabilityStatusNamesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityStatusNamesRequest"), com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityStatusNamesRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityStatusNamesResponse"));
        oper.setReturnClass(com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityStatusNamesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "GetPortabilityStatusNamesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[34] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetPortabilityPropertiesByBrand");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "GetPortabilityPropertiesByBrandRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityPropertiesByBrandRequest"), com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityPropertiesByBrandRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityPropertiesByBrandResponse"));
        oper.setReturnClass(com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityPropertiesByBrandResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "GetPortabilityPropertiesByBrandResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[35] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getRecurrencyTypeNames");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "getRecurrencyTypeNamesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">getRecurrencyTypeNamesRequest"), com.addcel.ws.clientes.simfonics.AdminService.GetRecurrencyTypeNamesRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">getRecurrencyTypeNamesResponse"));
        oper.setReturnClass(com.addcel.ws.clientes.simfonics.AdminService.GetRecurrencyTypeNamesResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "getRecurrencyTypeNamesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[36] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("validateIdentityNum");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "ValidateIdentityNumRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">ValidateIdentityNumRequest"), com.addcel.ws.clientes.simfonics.AdminService.ValidateIdentityNumRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">ValidateIdentityNumResponse"));
        oper.setReturnClass(com.addcel.ws.clientes.simfonics.AdminService.ValidateIdentityNumResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "ValidateIdentityNumResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[37] = oper;

    }

    public AdminServiceSOAPStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public AdminServiceSOAPStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public AdminServiceSOAPStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">>>customerPropertiesType>customerProperty>keyValues>keyValue");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.CustomerPropertiesTypeCustomerPropertyKeyValuesKeyValue.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">>customerPropertiesType>customerProperty>keyValues");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.CustomerPropertiesTypeCustomerPropertyKeyValuesKeyValue[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">>>customerPropertiesType>customerProperty>keyValues>keyValue");
            qName2 = new javax.xml.namespace.QName("", "keyValue");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">brandProductsType>brandProduct");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.BrandProductsTypeBrandProduct.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">brandPropertiesType>brandProperty");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.BrandPropertiesTypeBrandProperty.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">changeUserPasswordTuenti");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.ChangeUserPasswordTuenti.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">changeUserPasswordTuentiResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.ChangeUserPasswordTuentiResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">createCustomer");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.CreateCustomer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">createCustomerResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.CreateCustomerResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">customerPropertiesType>customerProperty");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.CustomerPropertiesTypeCustomerProperty.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">fileEntityTypeList>fileEntityType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.FileEntityTypeListFileEntityType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetFileEntityTypeListRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetFileEntityTypeListRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetFileEntityTypeListResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetFileEntityTypeListResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetLogisticOperatorNamesRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetLogisticOperatorNamesRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetLogisticOperatorNamesResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetLogisticOperatorNamesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetLogisticPropertiesByBrandRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetLogisticPropertiesByBrandRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetLogisticPropertiesByBrandResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetLogisticPropertiesByBrandResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetLogisticStatusNameRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetLogisticStatusNameRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetLogisticStatusNameResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetLogisticStatusNameResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityOperatorNamesRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityOperatorNamesRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityOperatorNamesResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityOperatorNamesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityPropertiesByBrandRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityPropertiesByBrandRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityPropertiesByBrandResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityPropertiesByBrandResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityPropertiesType>GetPortabilityProperty");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityPropertiesTypeGetPortabilityProperty.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityStatusNamesRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityStatusNamesRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityStatusNamesResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityStatusNamesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityStatusNamesType>GetPortabilityStatus");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityStatusNamesTypeGetPortabilityStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">getRecurrencyTypeNamesRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetRecurrencyTypeNamesRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">getRecurrencyTypeNamesResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetRecurrencyTypeNamesResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">incidentCategoriesType>incidentCategory");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.IncidentCategoriesTypeIncidentCategory.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">incidentSubCategoriesType>incidentSubCategory");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.IncidentSubCategoriesTypeIncidentSubCategory.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">incidentTypesType>incidentType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.IncidentTypesTypeIncidentType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">LogisticOperatorNamesType>LogisticOperator");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.LogisticOperatorNamesTypeLogisticOperator.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">LogisticPropertiesNamesType>LogisticProperty");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.LogisticPropertiesNamesTypeLogisticProperty.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">LogisticStatusNamesType>LogisticStatusName");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.LogisticStatusNamesTypeLogisticStatusName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">ordersCatalogType>order");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.OrdersCatalogTypeOrder.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">paymentMethodNamesType>paymentMethodName");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.PaymentMethodNamesTypePaymentMethodName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">paymentOperationNamesType>paymentOperationName");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.PaymentOperationNamesTypePaymentOperationName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">paymentTypeNamesType>paymentTypeName");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.PaymentTypeNamesTypePaymentTypeName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">PortabilityOperatorNamesTypes>PortabilityOperator");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.PortabilityOperatorNamesTypesPortabilityOperator.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">productsParametersType>purchaseParameter");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.ProductsParametersTypePurchaseParameter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">recurrencyTypeNames>recurrencyTypeName");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.RecurrencyTypeNamesRecurrencyTypeName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">sendSMSRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.SendSMSRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">sendSMSResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.SendSMSResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">usageTypeNames>usageTypeName");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.UsageTypeNamesUsageTypeName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">ValidateIdentityNumRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.ValidateIdentityNumRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">ValidateIdentityNumResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.ValidateIdentityNumResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">validateSMSToken");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.ValidateSMSToken.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">validateSMSTokenResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.ValidateSMSTokenResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "brandProductsType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.BrandProductsTypeBrandProduct[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">brandProductsType>brandProduct");
            qName2 = new javax.xml.namespace.QName("", "brandProduct");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "brandPropertiesType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.BrandPropertiesTypeBrandProperty[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">brandPropertiesType>brandProperty");
            qName2 = new javax.xml.namespace.QName("", "brandProperty");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "creditCardType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.CreditCardType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "customerPropertiesType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.CustomerPropertiesTypeCustomerProperty[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">customerPropertiesType>customerProperty");
            qName2 = new javax.xml.namespace.QName("", "customerProperty");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "fileEntityTypeList");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.FileEntityTypeListFileEntityType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">fileEntityTypeList>fileEntityType");
            qName2 = new javax.xml.namespace.QName("", "fileEntityType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "GetPortabilityPropertiesType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityPropertiesTypeGetPortabilityProperty[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityPropertiesType>GetPortabilityProperty");
            qName2 = new javax.xml.namespace.QName("", "GetPortabilityProperty");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "GetPortabilityStatusNamesType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityStatusNamesTypeGetPortabilityStatus[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityStatusNamesType>GetPortabilityStatus");
            qName2 = new javax.xml.namespace.QName("", "GetPortabilityStatus");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "incidentCategoriesType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.IncidentCategoriesTypeIncidentCategory[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">incidentCategoriesType>incidentCategory");
            qName2 = new javax.xml.namespace.QName("", "incidentCategory");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "incidentSubCategoriesType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.IncidentSubCategoriesTypeIncidentSubCategory[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">incidentSubCategoriesType>incidentSubCategory");
            qName2 = new javax.xml.namespace.QName("", "incidentSubCategory");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "incidentTypesType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.IncidentTypesTypeIncidentType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">incidentTypesType>incidentType");
            qName2 = new javax.xml.namespace.QName("", "incidentType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "LogisticOperatorNamesType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.LogisticOperatorNamesTypeLogisticOperator[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">LogisticOperatorNamesType>LogisticOperator");
            qName2 = new javax.xml.namespace.QName("", "LogisticOperator");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "LogisticPropertiesNamesType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.LogisticPropertiesNamesTypeLogisticProperty[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">LogisticPropertiesNamesType>LogisticProperty");
            qName2 = new javax.xml.namespace.QName("", "LogisticProperty");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "LogisticStatusNamesType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.LogisticStatusNamesTypeLogisticStatusName[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">LogisticStatusNamesType>LogisticStatusName");
            qName2 = new javax.xml.namespace.QName("", "LogisticStatusName");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "ordersCatalogType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.OrdersCatalogTypeOrder[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">ordersCatalogType>order");
            qName2 = new javax.xml.namespace.QName("", "order");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "paymentMethodNamesType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.PaymentMethodNamesTypePaymentMethodName[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">paymentMethodNamesType>paymentMethodName");
            qName2 = new javax.xml.namespace.QName("", "paymentMethodName");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "paymentOperationNamesType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.PaymentOperationNamesTypePaymentOperationName[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">paymentOperationNamesType>paymentOperationName");
            qName2 = new javax.xml.namespace.QName("", "paymentOperationName");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "paymentTypeNamesType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.PaymentTypeNamesTypePaymentTypeName[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">paymentTypeNamesType>paymentTypeName");
            qName2 = new javax.xml.namespace.QName("", "paymentTypeName");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "PortabilityOperatorNamesTypes");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.PortabilityOperatorNamesTypesPortabilityOperator[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">PortabilityOperatorNamesTypes>PortabilityOperator");
            qName2 = new javax.xml.namespace.QName("", "PortabilityOperator");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "productsParametersType");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.ProductsParametersTypePurchaseParameter[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">productsParametersType>purchaseParameter");
            qName2 = new javax.xml.namespace.QName("", "purchaseParameter");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "recurrencyTypeNames");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.RecurrencyTypeNamesRecurrencyTypeName[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">recurrencyTypeNames>recurrencyTypeName");
            qName2 = new javax.xml.namespace.QName("", "recurrencyTypeName");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", "usageTypeNames");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.AdminService.UsageTypeNamesUsageTypeName[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">usageTypeNames>usageTypeName");
            qName2 = new javax.xml.namespace.QName("", "usageTypeName");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public void checkUsernameSecurityAnswer(java.lang.String userName, java.lang.String securityQuestion, java.lang.String securityAnswer, java.lang.String country, java.math.BigInteger brandId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/checkUsernameSecurityAnswer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "checkUsernameSecurityAnswer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {userName, securityQuestion, securityAnswer, country, brandId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void getUsernameSecurityQuestion(java.lang.String userName, java.lang.String country, java.math.BigInteger brandId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.StringHolder securityQuestion) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/getUsernameSecurityQuestion");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getUsernameSecurityQuestion"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {userName, country, brandId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                securityQuestion.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "securityQuestion"));
            } catch (java.lang.Exception _exception) {
                securityQuestion.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "securityQuestion")), java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void sendResetPasswordMail(java.lang.String userName, java.lang.String country, java.math.BigInteger brandId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/sendResetPasswordMail");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "sendResetPasswordMail"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {userName, country, brandId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.ws.clientes.simfonics.AdminService.CreateCustomerResponse createCustomer(com.addcel.ws.clientes.simfonics.AdminService.CreateCustomer createCustomerInput) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/createCustomer");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "createCustomer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {createCustomerInput});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.ws.clientes.simfonics.AdminService.CreateCustomerResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.ws.clientes.simfonics.AdminService.CreateCustomerResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.ws.clientes.simfonics.AdminService.CreateCustomerResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void getCustomerPropertiesByBrand(java.lang.String country, java.math.BigInteger brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.BrandPropertiesTypeHolder brandProperties) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/getCustomerPropertiesByBrand");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getCustomerPropertiesByBrand"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, language});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                brandProperties.value = (com.addcel.ws.clientes.simfonics.AdminService.BrandPropertiesTypeBrandProperty[]) _output.get(new javax.xml.namespace.QName("", "brandProperties"));
            } catch (java.lang.Exception _exception) {
                brandProperties.value = (com.addcel.ws.clientes.simfonics.AdminService.BrandPropertiesTypeBrandProperty[]) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "brandProperties")), com.addcel.ws.clientes.simfonics.AdminService.BrandPropertiesTypeBrandProperty[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void getBrandProducts(java.lang.String country, java.math.BigInteger brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.BrandProductsTypeHolder brandProducts) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/getBrandProducts");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getBrandProducts"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, language});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                brandProducts.value = (com.addcel.ws.clientes.simfonics.AdminService.BrandProductsTypeBrandProduct[]) _output.get(new javax.xml.namespace.QName("", "brandProducts"));
            } catch (java.lang.Exception _exception) {
                brandProducts.value = (com.addcel.ws.clientes.simfonics.AdminService.BrandProductsTypeBrandProduct[]) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "brandProducts")), com.addcel.ws.clientes.simfonics.AdminService.BrandProductsTypeBrandProduct[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void getIncidentTypesByBrand(java.lang.String country, java.math.BigInteger brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.IncidentTypesTypeHolder incidentTypes) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/getIncidentTypesByBrand");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getIncidentTypesByBrand"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, language});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                incidentTypes.value = (com.addcel.ws.clientes.simfonics.AdminService.IncidentTypesTypeIncidentType[]) _output.get(new javax.xml.namespace.QName("", "incidentTypes"));
            } catch (java.lang.Exception _exception) {
                incidentTypes.value = (com.addcel.ws.clientes.simfonics.AdminService.IncidentTypesTypeIncidentType[]) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "incidentTypes")), com.addcel.ws.clientes.simfonics.AdminService.IncidentTypesTypeIncidentType[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void getOrdersCatalogByBrand(java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger ordersTypeId, java.math.BigInteger ordersCategoryId, java.math.BigInteger ordersSubCategoryId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.OrdersCatalogTypeHolder ordersCatalog) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/getOrdersCatalogByBrand");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getOrdersCatalogByBrand"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, ordersTypeId, ordersCategoryId, ordersSubCategoryId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                ordersCatalog.value = (com.addcel.ws.clientes.simfonics.AdminService.OrdersCatalogTypeOrder[]) _output.get(new javax.xml.namespace.QName("", "ordersCatalog"));
            } catch (java.lang.Exception _exception) {
                ordersCatalog.value = (com.addcel.ws.clientes.simfonics.AdminService.OrdersCatalogTypeOrder[]) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "ordersCatalog")), com.addcel.ws.clientes.simfonics.AdminService.OrdersCatalogTypeOrder[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void getIncidentCategoriesByBrand(java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger typeId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.IncidentCategoriesTypeHolder incidentCategories) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/getIncidentCategoriesByBrand");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getIncidentCategoriesByBrand"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, typeId, language});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                incidentCategories.value = (com.addcel.ws.clientes.simfonics.AdminService.IncidentCategoriesTypeIncidentCategory[]) _output.get(new javax.xml.namespace.QName("", "incidentCategories"));
            } catch (java.lang.Exception _exception) {
                incidentCategories.value = (com.addcel.ws.clientes.simfonics.AdminService.IncidentCategoriesTypeIncidentCategory[]) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "incidentCategories")), com.addcel.ws.clientes.simfonics.AdminService.IncidentCategoriesTypeIncidentCategory[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void getIncidentSubCategoriesByBrand(java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger typeId, java.math.BigInteger categoryId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.IncidentSubCategoriesTypeHolder incidentSubCategories) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/getIncidentSubCategoriesByBrand");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getIncidentSubCategoriesByBrand"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, typeId, categoryId, language});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                incidentSubCategories.value = (com.addcel.ws.clientes.simfonics.AdminService.IncidentSubCategoriesTypeIncidentSubCategory[]) _output.get(new javax.xml.namespace.QName("", "incidentSubCategories"));
            } catch (java.lang.Exception _exception) {
                incidentSubCategories.value = (com.addcel.ws.clientes.simfonics.AdminService.IncidentSubCategoriesTypeIncidentSubCategory[]) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "incidentSubCategories")), com.addcel.ws.clientes.simfonics.AdminService.IncidentSubCategoriesTypeIncidentSubCategory[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void changeUserPassword(java.lang.String token, java.lang.String country, java.math.BigInteger brandId, java.lang.String newPassword, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.StringHolder sessionId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/changeUserPassword");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "changeUserPassword"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {token, country, brandId, newPassword});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                sessionId.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "sessionId"));
            } catch (java.lang.Exception _exception) {
                sessionId.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "sessionId")), java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void getUsageTypeNames(java.lang.String country, java.math.BigInteger brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.UsageTypeNamesHolder usageTypeNames) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/getUsageTypeNames");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getUsageTypeNames"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, language});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                usageTypeNames.value = (com.addcel.ws.clientes.simfonics.AdminService.UsageTypeNamesUsageTypeName[]) _output.get(new javax.xml.namespace.QName("", "usageTypeNames"));
            } catch (java.lang.Exception _exception) {
                usageTypeNames.value = (com.addcel.ws.clientes.simfonics.AdminService.UsageTypeNamesUsageTypeName[]) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "usageTypeNames")), com.addcel.ws.clientes.simfonics.AdminService.UsageTypeNamesUsageTypeName[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void topupValidation(java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger msisdn, java.math.BigDecimal amount, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/topupValidation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "topupValidation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, msisdn, amount});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void addVoucherByMsisdn(java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger msisdn, java.lang.String voucherPinCode, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.BigDecimalHolder balance, javax.xml.rpc.holders.StringHolder currency) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/addVoucherByMsisdn");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "addVoucherByMsisdn"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, msisdn, voucherPinCode});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                balance.value = (java.math.BigDecimal) _output.get(new javax.xml.namespace.QName("", "balance"));
            } catch (java.lang.Exception _exception) {
                balance.value = (java.math.BigDecimal) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "balance")), java.math.BigDecimal.class);
            }
            try {
                currency.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "currency"));
            } catch (java.lang.Exception _exception) {
                currency.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "currency")), java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void addBalanceByMsisdn(java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger msisdn, java.math.BigDecimal amount, java.lang.String paymentReference, java.math.BigInteger productId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.BigDecimalHolder balance, javax.xml.rpc.holders.StringHolder currency) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/addBalanceByMsisdn");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "addBalanceByMsisdn"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, msisdn, amount, paymentReference, productId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                balance.value = (java.math.BigDecimal) _output.get(new javax.xml.namespace.QName("", "balance"));
            } catch (java.lang.Exception _exception) {
                balance.value = (java.math.BigDecimal) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "balance")), java.math.BigDecimal.class);
            }
            try {
                currency.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "currency"));
            } catch (java.lang.Exception _exception) {
                currency.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "currency")), java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void checkUserName(java.lang.String country, java.math.BigInteger brandId, java.lang.String userName, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder responseMessage, javax.xml.rpc.holders.BigIntegerHolder resultCheckUserName) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/checkUserName");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "checkUserName"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, userName});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                responseMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "responseMessage"));
            } catch (java.lang.Exception _exception) {
                responseMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "responseMessage")), java.lang.String.class);
            }
            try {
                resultCheckUserName.value = (java.math.BigInteger) _output.get(new javax.xml.namespace.QName("", "resultCheckUserName"));
            } catch (java.lang.Exception _exception) {
                resultCheckUserName.value = (java.math.BigInteger) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultCheckUserName")), java.math.BigInteger.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void checkIdentityNum(java.lang.String country, java.math.BigInteger brandId, java.lang.String identityNum, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder responseMessage, javax.xml.rpc.holders.BigIntegerHolder resultCheckIdentityNum) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/checkIdentityNum");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "checkIdentityNum"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, identityNum});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                responseMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "responseMessage"));
            } catch (java.lang.Exception _exception) {
                responseMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "responseMessage")), java.lang.String.class);
            }
            try {
                resultCheckIdentityNum.value = (java.math.BigInteger) _output.get(new javax.xml.namespace.QName("", "resultCheckIdentityNum"));
            } catch (java.lang.Exception _exception) {
                resultCheckIdentityNum.value = (java.math.BigInteger) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultCheckIdentityNum")), java.math.BigInteger.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void sendEmailValidation(java.lang.String country, java.math.BigInteger brandId, java.lang.String userName, java.lang.String mailLink, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/sendEmailValidation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "sendEmailValidation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, userName, mailLink});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void validateUserMail(java.lang.String country, java.math.BigInteger brandId, java.lang.String userName, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/validateUserMail");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "validateUserMail"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, userName});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.ws.clientes.simfonics.AdminService.ValidateSMSTokenResponse validateSMSToken(com.addcel.ws.clientes.simfonics.AdminService.ValidateSMSToken validateSMSTokenInput) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/validateSMSToken");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "validateSMSToken"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {validateSMSTokenInput});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.ws.clientes.simfonics.AdminService.ValidateSMSTokenResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.ws.clientes.simfonics.AdminService.ValidateSMSTokenResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.ws.clientes.simfonics.AdminService.ValidateSMSTokenResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void checkNickName(java.lang.String country, java.math.BigInteger brandId, java.lang.String nickName, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.BigIntegerHolder isAvailable) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/checkNickName");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "checkNickName"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, nickName});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                isAvailable.value = (java.math.BigInteger) _output.get(new javax.xml.namespace.QName("", "isAvailable"));
            } catch (java.lang.Exception _exception) {
                isAvailable.value = (java.math.BigInteger) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "isAvailable")), java.math.BigInteger.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void doPayment(java.lang.String country, java.math.BigInteger brandId, java.lang.String language, java.math.BigDecimal amount, java.lang.String currency, java.math.BigInteger paymentOperationId, java.math.BigInteger paymentTypeId, java.lang.String description, com.addcel.ws.clientes.simfonics.AdminService.CreditCardType creditCard, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.StringHolder transactionResult) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/doPayment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "doPayment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, language, amount, currency, paymentOperationId, paymentTypeId, description, creditCard});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                transactionResult.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "transactionResult"));
            } catch (java.lang.Exception _exception) {
                transactionResult.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "transactionResult")), java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void getPaymentTypeNames(java.lang.String country, int brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.PaymentTypeNamesTypeHolder paymentTypeNames) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/getPaymentTypeNames");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getPaymentTypeNames"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, new java.lang.Integer(brandId), language});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                paymentTypeNames.value = (com.addcel.ws.clientes.simfonics.AdminService.PaymentTypeNamesTypePaymentTypeName[]) _output.get(new javax.xml.namespace.QName("", "paymentTypeNames"));
            } catch (java.lang.Exception _exception) {
                paymentTypeNames.value = (com.addcel.ws.clientes.simfonics.AdminService.PaymentTypeNamesTypePaymentTypeName[]) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "paymentTypeNames")), com.addcel.ws.clientes.simfonics.AdminService.PaymentTypeNamesTypePaymentTypeName[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void getPaymentMethodNames(java.lang.String country, int brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.PaymentMethodNamesTypeHolder paymentMethodNames) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/getPaymentMethodNames");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getPaymentMethodNames"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, new java.lang.Integer(brandId), language});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                paymentMethodNames.value = (com.addcel.ws.clientes.simfonics.AdminService.PaymentMethodNamesTypePaymentMethodName[]) _output.get(new javax.xml.namespace.QName("", "paymentMethodNames"));
            } catch (java.lang.Exception _exception) {
                paymentMethodNames.value = (com.addcel.ws.clientes.simfonics.AdminService.PaymentMethodNamesTypePaymentMethodName[]) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "paymentMethodNames")), com.addcel.ws.clientes.simfonics.AdminService.PaymentMethodNamesTypePaymentMethodName[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void getPurchaseProductParameters(java.lang.String country, java.math.BigInteger brandId, java.lang.String language, java.math.BigInteger productTypeId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.ProductsParametersTypeHolder purchaseParameters) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[24]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/getPurchaseProductParameters");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getPurchaseProductParameters"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, language, productTypeId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                purchaseParameters.value = (com.addcel.ws.clientes.simfonics.AdminService.ProductsParametersTypePurchaseParameter[]) _output.get(new javax.xml.namespace.QName("", "purchaseParameters"));
            } catch (java.lang.Exception _exception) {
                purchaseParameters.value = (com.addcel.ws.clientes.simfonics.AdminService.ProductsParametersTypePurchaseParameter[]) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "purchaseParameters")), com.addcel.ws.clientes.simfonics.AdminService.ProductsParametersTypePurchaseParameter[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void getPaymentOperationNames(java.lang.String country, int brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.PaymentOperationNamesTypeHolder paymentOperationNames) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[25]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/getPaymentOperationNames");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getPaymentOperationNames"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, new java.lang.Integer(brandId), language});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                paymentOperationNames.value = (com.addcel.ws.clientes.simfonics.AdminService.PaymentOperationNamesTypePaymentOperationName[]) _output.get(new javax.xml.namespace.QName("", "paymentOperationNames"));
            } catch (java.lang.Exception _exception) {
                paymentOperationNames.value = (com.addcel.ws.clientes.simfonics.AdminService.PaymentOperationNamesTypePaymentOperationName[]) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "paymentOperationNames")), com.addcel.ws.clientes.simfonics.AdminService.PaymentOperationNamesTypePaymentOperationName[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public void recruiterValidation(java.lang.String country, java.math.BigInteger brandId, java.lang.String msisdn_recruiter, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.BigIntegerHolder isValid) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[26]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/recruiterValidation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "recruiterValidation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {country, brandId, msisdn_recruiter});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            java.util.Map _output;
            _output = _call.getOutputParams();
            try {
                result.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "result"));
            } catch (java.lang.Exception _exception) {
                result.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "result")), java.lang.String.class);
            }
            try {
                resultMessage.value = (java.lang.String) _output.get(new javax.xml.namespace.QName("", "resultMessage"));
            } catch (java.lang.Exception _exception) {
                resultMessage.value = (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "resultMessage")), java.lang.String.class);
            }
            try {
                isValid.value = (java.math.BigInteger) _output.get(new javax.xml.namespace.QName("", "isValid"));
            } catch (java.lang.Exception _exception) {
                isValid.value = (java.math.BigInteger) org.apache.axis.utils.JavaUtils.convert(_output.get(new javax.xml.namespace.QName("", "isValid")), java.math.BigInteger.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.ws.clientes.simfonics.AdminService.SendSMSResponse sendSMS(com.addcel.ws.clientes.simfonics.AdminService.SendSMSRequest sendSMSInput) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[27]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/sendSMS");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "sendSMS"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {sendSMSInput});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.ws.clientes.simfonics.AdminService.SendSMSResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.ws.clientes.simfonics.AdminService.SendSMSResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.ws.clientes.simfonics.AdminService.SendSMSResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.ws.clientes.simfonics.AdminService.GetFileEntityTypeListResponse getFileEntityTypeList(com.addcel.ws.clientes.simfonics.AdminService.GetFileEntityTypeListRequest getFileEntityTypeListInput) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[28]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/GetFileEntityTypeList");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetFileEntityTypeList"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getFileEntityTypeListInput});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.ws.clientes.simfonics.AdminService.GetFileEntityTypeListResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.ws.clientes.simfonics.AdminService.GetFileEntityTypeListResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.ws.clientes.simfonics.AdminService.GetFileEntityTypeListResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.ws.clientes.simfonics.AdminService.ChangeUserPasswordTuentiResponse changeUserPasswordTuenti(com.addcel.ws.clientes.simfonics.AdminService.ChangeUserPasswordTuenti changeUserPasswordTuentiInput) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[29]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/ChangeUserPasswordTuenti");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ChangeUserPasswordTuenti"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {changeUserPasswordTuentiInput});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.ws.clientes.simfonics.AdminService.ChangeUserPasswordTuentiResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.ws.clientes.simfonics.AdminService.ChangeUserPasswordTuentiResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.ws.clientes.simfonics.AdminService.ChangeUserPasswordTuentiResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.ws.clientes.simfonics.AdminService.GetLogisticPropertiesByBrandResponse getLogisticPropertiesByBrand(com.addcel.ws.clientes.simfonics.AdminService.GetLogisticPropertiesByBrandRequest getLogisticPropertiesByBrandInput) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[30]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/GetLogisticPropertiesByBrand");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetLogisticPropertiesByBrand"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getLogisticPropertiesByBrandInput});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.ws.clientes.simfonics.AdminService.GetLogisticPropertiesByBrandResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.ws.clientes.simfonics.AdminService.GetLogisticPropertiesByBrandResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.ws.clientes.simfonics.AdminService.GetLogisticPropertiesByBrandResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.ws.clientes.simfonics.AdminService.GetLogisticOperatorNamesResponse getLogisticOperatorNames(com.addcel.ws.clientes.simfonics.AdminService.GetLogisticOperatorNamesRequest getLogisticOperatorNamesInput) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[31]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/GetLogisticOperatorNames");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetLogisticOperatorNames"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getLogisticOperatorNamesInput});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.ws.clientes.simfonics.AdminService.GetLogisticOperatorNamesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.ws.clientes.simfonics.AdminService.GetLogisticOperatorNamesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.ws.clientes.simfonics.AdminService.GetLogisticOperatorNamesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.ws.clientes.simfonics.AdminService.GetLogisticStatusNameResponse getLogisticStatusName(com.addcel.ws.clientes.simfonics.AdminService.GetLogisticStatusNameRequest getLogisticStatusNameInput) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[32]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/GetLogisticStatusName");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetLogisticStatusName"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getLogisticStatusNameInput});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.ws.clientes.simfonics.AdminService.GetLogisticStatusNameResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.ws.clientes.simfonics.AdminService.GetLogisticStatusNameResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.ws.clientes.simfonics.AdminService.GetLogisticStatusNameResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityOperatorNamesResponse getPortabilityOperatorNames(com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityOperatorNamesRequest getPortabilityOperatorNamesInput) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[33]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/GetPortabilityOperatorNames");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetPortabilityOperatorNames"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getPortabilityOperatorNamesInput});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityOperatorNamesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityOperatorNamesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityOperatorNamesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityStatusNamesResponse getPortabilityStatusNames(com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityStatusNamesRequest getPortabilityStatusNamesInput) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[34]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/GetPortabilityStatusNames");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetPortabilityStatusNames"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getPortabilityStatusNamesInput});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityStatusNamesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityStatusNamesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityStatusNamesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityPropertiesByBrandResponse getPortabilityPropertiesByBrand(com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityPropertiesByBrandRequest getPortabilityPropertiesByBrandInput) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[35]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/GetPortabilityPropertiesByBrand");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetPortabilityPropertiesByBrand"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getPortabilityPropertiesByBrandInput});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityPropertiesByBrandResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityPropertiesByBrandResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityPropertiesByBrandResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.ws.clientes.simfonics.AdminService.GetRecurrencyTypeNamesResponse getRecurrencyTypeNames(com.addcel.ws.clientes.simfonics.AdminService.GetRecurrencyTypeNamesRequest getRecurrencyTypeNamesInput) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[36]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/getRecurrencyTypeNames");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getRecurrencyTypeNames"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getRecurrencyTypeNamesInput});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.ws.clientes.simfonics.AdminService.GetRecurrencyTypeNamesResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.ws.clientes.simfonics.AdminService.GetRecurrencyTypeNamesResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.ws.clientes.simfonics.AdminService.GetRecurrencyTypeNamesResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.ws.clientes.simfonics.AdminService.ValidateIdentityNumResponse validateIdentityNum(com.addcel.ws.clientes.simfonics.AdminService.ValidateIdentityNumRequest validateIdentityNumRequestInput) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[37]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/SelfCare/AdminService/validateIdentityNum");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "validateIdentityNum"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {validateIdentityNumRequestInput});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.ws.clientes.simfonics.AdminService.ValidateIdentityNumResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.ws.clientes.simfonics.AdminService.ValidateIdentityNumResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.ws.clientes.simfonics.AdminService.ValidateIdentityNumResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
