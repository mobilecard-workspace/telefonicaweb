/**
 * LogisticPropertiesNamesTypeLogisticProperty.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class LogisticPropertiesNamesTypeLogisticProperty  implements java.io.Serializable {
    private java.math.BigDecimal logisticPropertyId;

    private java.lang.String logisticPropertyName;

    public LogisticPropertiesNamesTypeLogisticProperty() {
    }

    public LogisticPropertiesNamesTypeLogisticProperty(
           java.math.BigDecimal logisticPropertyId,
           java.lang.String logisticPropertyName) {
           this.logisticPropertyId = logisticPropertyId;
           this.logisticPropertyName = logisticPropertyName;
    }


    /**
     * Gets the logisticPropertyId value for this LogisticPropertiesNamesTypeLogisticProperty.
     * 
     * @return logisticPropertyId
     */
    public java.math.BigDecimal getLogisticPropertyId() {
        return logisticPropertyId;
    }


    /**
     * Sets the logisticPropertyId value for this LogisticPropertiesNamesTypeLogisticProperty.
     * 
     * @param logisticPropertyId
     */
    public void setLogisticPropertyId(java.math.BigDecimal logisticPropertyId) {
        this.logisticPropertyId = logisticPropertyId;
    }


    /**
     * Gets the logisticPropertyName value for this LogisticPropertiesNamesTypeLogisticProperty.
     * 
     * @return logisticPropertyName
     */
    public java.lang.String getLogisticPropertyName() {
        return logisticPropertyName;
    }


    /**
     * Sets the logisticPropertyName value for this LogisticPropertiesNamesTypeLogisticProperty.
     * 
     * @param logisticPropertyName
     */
    public void setLogisticPropertyName(java.lang.String logisticPropertyName) {
        this.logisticPropertyName = logisticPropertyName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LogisticPropertiesNamesTypeLogisticProperty)) return false;
        LogisticPropertiesNamesTypeLogisticProperty other = (LogisticPropertiesNamesTypeLogisticProperty) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.logisticPropertyId==null && other.getLogisticPropertyId()==null) || 
             (this.logisticPropertyId!=null &&
              this.logisticPropertyId.equals(other.getLogisticPropertyId()))) &&
            ((this.logisticPropertyName==null && other.getLogisticPropertyName()==null) || 
             (this.logisticPropertyName!=null &&
              this.logisticPropertyName.equals(other.getLogisticPropertyName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLogisticPropertyId() != null) {
            _hashCode += getLogisticPropertyId().hashCode();
        }
        if (getLogisticPropertyName() != null) {
            _hashCode += getLogisticPropertyName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LogisticPropertiesNamesTypeLogisticProperty.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">LogisticPropertiesNamesType>LogisticProperty"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logisticPropertyId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LogisticPropertyId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logisticPropertyName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LogisticPropertyName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
