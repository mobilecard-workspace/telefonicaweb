/**
 * PortabilityPropertyType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class PortabilityPropertyType  implements java.io.Serializable {
    private java.lang.String portabilityPropertyKey;

    private java.lang.String portabilityPropertyValue;

    public PortabilityPropertyType() {
    }

    public PortabilityPropertyType(
           java.lang.String portabilityPropertyKey,
           java.lang.String portabilityPropertyValue) {
           this.portabilityPropertyKey = portabilityPropertyKey;
           this.portabilityPropertyValue = portabilityPropertyValue;
    }


    /**
     * Gets the portabilityPropertyKey value for this PortabilityPropertyType.
     * 
     * @return portabilityPropertyKey
     */
    public java.lang.String getPortabilityPropertyKey() {
        return portabilityPropertyKey;
    }


    /**
     * Sets the portabilityPropertyKey value for this PortabilityPropertyType.
     * 
     * @param portabilityPropertyKey
     */
    public void setPortabilityPropertyKey(java.lang.String portabilityPropertyKey) {
        this.portabilityPropertyKey = portabilityPropertyKey;
    }


    /**
     * Gets the portabilityPropertyValue value for this PortabilityPropertyType.
     * 
     * @return portabilityPropertyValue
     */
    public java.lang.String getPortabilityPropertyValue() {
        return portabilityPropertyValue;
    }


    /**
     * Sets the portabilityPropertyValue value for this PortabilityPropertyType.
     * 
     * @param portabilityPropertyValue
     */
    public void setPortabilityPropertyValue(java.lang.String portabilityPropertyValue) {
        this.portabilityPropertyValue = portabilityPropertyValue;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PortabilityPropertyType)) return false;
        PortabilityPropertyType other = (PortabilityPropertyType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.portabilityPropertyKey==null && other.getPortabilityPropertyKey()==null) || 
             (this.portabilityPropertyKey!=null &&
              this.portabilityPropertyKey.equals(other.getPortabilityPropertyKey()))) &&
            ((this.portabilityPropertyValue==null && other.getPortabilityPropertyValue()==null) || 
             (this.portabilityPropertyValue!=null &&
              this.portabilityPropertyValue.equals(other.getPortabilityPropertyValue())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPortabilityPropertyKey() != null) {
            _hashCode += getPortabilityPropertyKey().hashCode();
        }
        if (getPortabilityPropertyValue() != null) {
            _hashCode += getPortabilityPropertyValue().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PortabilityPropertyType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", "portabilityPropertyType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("portabilityPropertyKey");
        elemField.setXmlName(new javax.xml.namespace.QName("", "portabilityPropertyKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("portabilityPropertyValue");
        elemField.setXmlName(new javax.xml.namespace.QName("", "portabilityPropertyValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
