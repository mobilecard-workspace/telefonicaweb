/**
 * LogisticStatusNamesTypeLogisticStatusName.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class LogisticStatusNamesTypeLogisticStatusName  implements java.io.Serializable {
    private java.math.BigDecimal logisticStatusId;

    private java.lang.String logisticStatusName;

    public LogisticStatusNamesTypeLogisticStatusName() {
    }

    public LogisticStatusNamesTypeLogisticStatusName(
           java.math.BigDecimal logisticStatusId,
           java.lang.String logisticStatusName) {
           this.logisticStatusId = logisticStatusId;
           this.logisticStatusName = logisticStatusName;
    }


    /**
     * Gets the logisticStatusId value for this LogisticStatusNamesTypeLogisticStatusName.
     * 
     * @return logisticStatusId
     */
    public java.math.BigDecimal getLogisticStatusId() {
        return logisticStatusId;
    }


    /**
     * Sets the logisticStatusId value for this LogisticStatusNamesTypeLogisticStatusName.
     * 
     * @param logisticStatusId
     */
    public void setLogisticStatusId(java.math.BigDecimal logisticStatusId) {
        this.logisticStatusId = logisticStatusId;
    }


    /**
     * Gets the logisticStatusName value for this LogisticStatusNamesTypeLogisticStatusName.
     * 
     * @return logisticStatusName
     */
    public java.lang.String getLogisticStatusName() {
        return logisticStatusName;
    }


    /**
     * Sets the logisticStatusName value for this LogisticStatusNamesTypeLogisticStatusName.
     * 
     * @param logisticStatusName
     */
    public void setLogisticStatusName(java.lang.String logisticStatusName) {
        this.logisticStatusName = logisticStatusName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LogisticStatusNamesTypeLogisticStatusName)) return false;
        LogisticStatusNamesTypeLogisticStatusName other = (LogisticStatusNamesTypeLogisticStatusName) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.logisticStatusId==null && other.getLogisticStatusId()==null) || 
             (this.logisticStatusId!=null &&
              this.logisticStatusId.equals(other.getLogisticStatusId()))) &&
            ((this.logisticStatusName==null && other.getLogisticStatusName()==null) || 
             (this.logisticStatusName!=null &&
              this.logisticStatusName.equals(other.getLogisticStatusName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLogisticStatusId() != null) {
            _hashCode += getLogisticStatusId().hashCode();
        }
        if (getLogisticStatusName() != null) {
            _hashCode += getLogisticStatusName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LogisticStatusNamesTypeLogisticStatusName.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">LogisticStatusNamesType>LogisticStatusName"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logisticStatusId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LogisticStatusId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logisticStatusName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LogisticStatusName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
