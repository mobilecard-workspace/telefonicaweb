/**
 * ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class ProductItemFUPInfo_RowSetProductItemFUPInfo_Row  implements java.io.Serializable {
    private java.math.BigInteger id;

    private java.math.BigInteger customerserviceId;

    private java.math.BigInteger relation;

    private java.math.BigDecimal price;

    private java.math.BigInteger accountCounterId;

    private java.lang.String currentAccountCounter;

    private java.math.BigInteger initialAccountCounter;

    private java.math.BigInteger endValueAccountCounter;

    private java.math.BigInteger unitsAccountCounter;

    private java.lang.String unitsNameAccountCounter;

    private java.math.BigInteger status;

    private java.lang.String statusName;

    private java.util.Calendar startDate;

    private java.util.Calendar endDate;

    private java.util.Calendar expirationDate;

    private java.math.BigInteger productItemId;

    public ProductItemFUPInfo_RowSetProductItemFUPInfo_Row() {
    }

    public ProductItemFUPInfo_RowSetProductItemFUPInfo_Row(
           java.math.BigInteger id,
           java.math.BigInteger customerserviceId,
           java.math.BigInteger relation,
           java.math.BigDecimal price,
           java.math.BigInteger accountCounterId,
           java.lang.String currentAccountCounter,
           java.math.BigInteger initialAccountCounter,
           java.math.BigInteger endValueAccountCounter,
           java.math.BigInteger unitsAccountCounter,
           java.lang.String unitsNameAccountCounter,
           java.math.BigInteger status,
           java.lang.String statusName,
           java.util.Calendar startDate,
           java.util.Calendar endDate,
           java.util.Calendar expirationDate,
           java.math.BigInteger productItemId) {
           this.id = id;
           this.customerserviceId = customerserviceId;
           this.relation = relation;
           this.price = price;
           this.accountCounterId = accountCounterId;
           this.currentAccountCounter = currentAccountCounter;
           this.initialAccountCounter = initialAccountCounter;
           this.endValueAccountCounter = endValueAccountCounter;
           this.unitsAccountCounter = unitsAccountCounter;
           this.unitsNameAccountCounter = unitsNameAccountCounter;
           this.status = status;
           this.statusName = statusName;
           this.startDate = startDate;
           this.endDate = endDate;
           this.expirationDate = expirationDate;
           this.productItemId = productItemId;
    }


    /**
     * Gets the id value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @return id
     */
    public java.math.BigInteger getId() {
        return id;
    }


    /**
     * Sets the id value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @param id
     */
    public void setId(java.math.BigInteger id) {
        this.id = id;
    }


    /**
     * Gets the customerserviceId value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @return customerserviceId
     */
    public java.math.BigInteger getCustomerserviceId() {
        return customerserviceId;
    }


    /**
     * Sets the customerserviceId value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @param customerserviceId
     */
    public void setCustomerserviceId(java.math.BigInteger customerserviceId) {
        this.customerserviceId = customerserviceId;
    }


    /**
     * Gets the relation value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @return relation
     */
    public java.math.BigInteger getRelation() {
        return relation;
    }


    /**
     * Sets the relation value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @param relation
     */
    public void setRelation(java.math.BigInteger relation) {
        this.relation = relation;
    }


    /**
     * Gets the price value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @return price
     */
    public java.math.BigDecimal getPrice() {
        return price;
    }


    /**
     * Sets the price value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @param price
     */
    public void setPrice(java.math.BigDecimal price) {
        this.price = price;
    }


    /**
     * Gets the accountCounterId value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @return accountCounterId
     */
    public java.math.BigInteger getAccountCounterId() {
        return accountCounterId;
    }


    /**
     * Sets the accountCounterId value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @param accountCounterId
     */
    public void setAccountCounterId(java.math.BigInteger accountCounterId) {
        this.accountCounterId = accountCounterId;
    }


    /**
     * Gets the currentAccountCounter value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @return currentAccountCounter
     */
    public java.lang.String getCurrentAccountCounter() {
        return currentAccountCounter;
    }


    /**
     * Sets the currentAccountCounter value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @param currentAccountCounter
     */
    public void setCurrentAccountCounter(java.lang.String currentAccountCounter) {
        this.currentAccountCounter = currentAccountCounter;
    }


    /**
     * Gets the initialAccountCounter value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @return initialAccountCounter
     */
    public java.math.BigInteger getInitialAccountCounter() {
        return initialAccountCounter;
    }


    /**
     * Sets the initialAccountCounter value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @param initialAccountCounter
     */
    public void setInitialAccountCounter(java.math.BigInteger initialAccountCounter) {
        this.initialAccountCounter = initialAccountCounter;
    }


    /**
     * Gets the endValueAccountCounter value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @return endValueAccountCounter
     */
    public java.math.BigInteger getEndValueAccountCounter() {
        return endValueAccountCounter;
    }


    /**
     * Sets the endValueAccountCounter value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @param endValueAccountCounter
     */
    public void setEndValueAccountCounter(java.math.BigInteger endValueAccountCounter) {
        this.endValueAccountCounter = endValueAccountCounter;
    }


    /**
     * Gets the unitsAccountCounter value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @return unitsAccountCounter
     */
    public java.math.BigInteger getUnitsAccountCounter() {
        return unitsAccountCounter;
    }


    /**
     * Sets the unitsAccountCounter value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @param unitsAccountCounter
     */
    public void setUnitsAccountCounter(java.math.BigInteger unitsAccountCounter) {
        this.unitsAccountCounter = unitsAccountCounter;
    }


    /**
     * Gets the unitsNameAccountCounter value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @return unitsNameAccountCounter
     */
    public java.lang.String getUnitsNameAccountCounter() {
        return unitsNameAccountCounter;
    }


    /**
     * Sets the unitsNameAccountCounter value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @param unitsNameAccountCounter
     */
    public void setUnitsNameAccountCounter(java.lang.String unitsNameAccountCounter) {
        this.unitsNameAccountCounter = unitsNameAccountCounter;
    }


    /**
     * Gets the status value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @return status
     */
    public java.math.BigInteger getStatus() {
        return status;
    }


    /**
     * Sets the status value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @param status
     */
    public void setStatus(java.math.BigInteger status) {
        this.status = status;
    }


    /**
     * Gets the statusName value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @return statusName
     */
    public java.lang.String getStatusName() {
        return statusName;
    }


    /**
     * Sets the statusName value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @param statusName
     */
    public void setStatusName(java.lang.String statusName) {
        this.statusName = statusName;
    }


    /**
     * Gets the startDate value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @return startDate
     */
    public java.util.Calendar getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @param startDate
     */
    public void setStartDate(java.util.Calendar startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the endDate value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @return endDate
     */
    public java.util.Calendar getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @param endDate
     */
    public void setEndDate(java.util.Calendar endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the expirationDate value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @return expirationDate
     */
    public java.util.Calendar getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.util.Calendar expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the productItemId value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @return productItemId
     */
    public java.math.BigInteger getProductItemId() {
        return productItemId;
    }


    /**
     * Sets the productItemId value for this ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.
     * 
     * @param productItemId
     */
    public void setProductItemId(java.math.BigInteger productItemId) {
        this.productItemId = productItemId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProductItemFUPInfo_RowSetProductItemFUPInfo_Row)) return false;
        ProductItemFUPInfo_RowSetProductItemFUPInfo_Row other = (ProductItemFUPInfo_RowSetProductItemFUPInfo_Row) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.customerserviceId==null && other.getCustomerserviceId()==null) || 
             (this.customerserviceId!=null &&
              this.customerserviceId.equals(other.getCustomerserviceId()))) &&
            ((this.relation==null && other.getRelation()==null) || 
             (this.relation!=null &&
              this.relation.equals(other.getRelation()))) &&
            ((this.price==null && other.getPrice()==null) || 
             (this.price!=null &&
              this.price.equals(other.getPrice()))) &&
            ((this.accountCounterId==null && other.getAccountCounterId()==null) || 
             (this.accountCounterId!=null &&
              this.accountCounterId.equals(other.getAccountCounterId()))) &&
            ((this.currentAccountCounter==null && other.getCurrentAccountCounter()==null) || 
             (this.currentAccountCounter!=null &&
              this.currentAccountCounter.equals(other.getCurrentAccountCounter()))) &&
            ((this.initialAccountCounter==null && other.getInitialAccountCounter()==null) || 
             (this.initialAccountCounter!=null &&
              this.initialAccountCounter.equals(other.getInitialAccountCounter()))) &&
            ((this.endValueAccountCounter==null && other.getEndValueAccountCounter()==null) || 
             (this.endValueAccountCounter!=null &&
              this.endValueAccountCounter.equals(other.getEndValueAccountCounter()))) &&
            ((this.unitsAccountCounter==null && other.getUnitsAccountCounter()==null) || 
             (this.unitsAccountCounter!=null &&
              this.unitsAccountCounter.equals(other.getUnitsAccountCounter()))) &&
            ((this.unitsNameAccountCounter==null && other.getUnitsNameAccountCounter()==null) || 
             (this.unitsNameAccountCounter!=null &&
              this.unitsNameAccountCounter.equals(other.getUnitsNameAccountCounter()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.statusName==null && other.getStatusName()==null) || 
             (this.statusName!=null &&
              this.statusName.equals(other.getStatusName()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate()))) &&
            ((this.productItemId==null && other.getProductItemId()==null) || 
             (this.productItemId!=null &&
              this.productItemId.equals(other.getProductItemId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getCustomerserviceId() != null) {
            _hashCode += getCustomerserviceId().hashCode();
        }
        if (getRelation() != null) {
            _hashCode += getRelation().hashCode();
        }
        if (getPrice() != null) {
            _hashCode += getPrice().hashCode();
        }
        if (getAccountCounterId() != null) {
            _hashCode += getAccountCounterId().hashCode();
        }
        if (getCurrentAccountCounter() != null) {
            _hashCode += getCurrentAccountCounter().hashCode();
        }
        if (getInitialAccountCounter() != null) {
            _hashCode += getInitialAccountCounter().hashCode();
        }
        if (getEndValueAccountCounter() != null) {
            _hashCode += getEndValueAccountCounter().hashCode();
        }
        if (getUnitsAccountCounter() != null) {
            _hashCode += getUnitsAccountCounter().hashCode();
        }
        if (getUnitsNameAccountCounter() != null) {
            _hashCode += getUnitsNameAccountCounter().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getStatusName() != null) {
            _hashCode += getStatusName().hashCode();
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        if (getProductItemId() != null) {
            _hashCode += getProductItemId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProductItemFUPInfo_RowSetProductItemFUPInfo_Row.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">ProductItemFUPInfo_RowSet>ProductItemFUPInfo_Row"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerserviceId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customerserviceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("relation");
        elemField.setXmlName(new javax.xml.namespace.QName("", "relation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("price");
        elemField.setXmlName(new javax.xml.namespace.QName("", "price"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountCounterId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accountCounterId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currentAccountCounter");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currentAccountCounter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("initialAccountCounter");
        elemField.setXmlName(new javax.xml.namespace.QName("", "initialAccountCounter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endValueAccountCounter");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endValueAccountCounter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitsAccountCounter");
        elemField.setXmlName(new javax.xml.namespace.QName("", "unitsAccountCounter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitsNameAccountCounter");
        elemField.setXmlName(new javax.xml.namespace.QName("", "unitsNameAccountCounter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statusName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "startDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "expirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productItemId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productItemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
