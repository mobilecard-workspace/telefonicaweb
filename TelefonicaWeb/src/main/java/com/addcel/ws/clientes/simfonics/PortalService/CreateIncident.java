/**
 * CreateIncident.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class CreateIncident  implements java.io.Serializable {
    private java.lang.String sessionId;

    private java.lang.String country;

    private java.math.BigInteger brandId;

    private java.math.BigInteger incidentPriorityId;

    private java.math.BigInteger incidentTypeId;

    private java.math.BigInteger incidentStatusId;

    private java.lang.String summary;

    private java.lang.String description;

    private java.math.BigInteger incidentCategoryId;

    private java.math.BigInteger incidentSubCategoryId;

    public CreateIncident() {
    }

    public CreateIncident(
           java.lang.String sessionId,
           java.lang.String country,
           java.math.BigInteger brandId,
           java.math.BigInteger incidentPriorityId,
           java.math.BigInteger incidentTypeId,
           java.math.BigInteger incidentStatusId,
           java.lang.String summary,
           java.lang.String description,
           java.math.BigInteger incidentCategoryId,
           java.math.BigInteger incidentSubCategoryId) {
           this.sessionId = sessionId;
           this.country = country;
           this.brandId = brandId;
           this.incidentPriorityId = incidentPriorityId;
           this.incidentTypeId = incidentTypeId;
           this.incidentStatusId = incidentStatusId;
           this.summary = summary;
           this.description = description;
           this.incidentCategoryId = incidentCategoryId;
           this.incidentSubCategoryId = incidentSubCategoryId;
    }


    /**
     * Gets the sessionId value for this CreateIncident.
     * 
     * @return sessionId
     */
    public java.lang.String getSessionId() {
        return sessionId;
    }


    /**
     * Sets the sessionId value for this CreateIncident.
     * 
     * @param sessionId
     */
    public void setSessionId(java.lang.String sessionId) {
        this.sessionId = sessionId;
    }


    /**
     * Gets the country value for this CreateIncident.
     * 
     * @return country
     */
    public java.lang.String getCountry() {
        return country;
    }


    /**
     * Sets the country value for this CreateIncident.
     * 
     * @param country
     */
    public void setCountry(java.lang.String country) {
        this.country = country;
    }


    /**
     * Gets the brandId value for this CreateIncident.
     * 
     * @return brandId
     */
    public java.math.BigInteger getBrandId() {
        return brandId;
    }


    /**
     * Sets the brandId value for this CreateIncident.
     * 
     * @param brandId
     */
    public void setBrandId(java.math.BigInteger brandId) {
        this.brandId = brandId;
    }


    /**
     * Gets the incidentPriorityId value for this CreateIncident.
     * 
     * @return incidentPriorityId
     */
    public java.math.BigInteger getIncidentPriorityId() {
        return incidentPriorityId;
    }


    /**
     * Sets the incidentPriorityId value for this CreateIncident.
     * 
     * @param incidentPriorityId
     */
    public void setIncidentPriorityId(java.math.BigInteger incidentPriorityId) {
        this.incidentPriorityId = incidentPriorityId;
    }


    /**
     * Gets the incidentTypeId value for this CreateIncident.
     * 
     * @return incidentTypeId
     */
    public java.math.BigInteger getIncidentTypeId() {
        return incidentTypeId;
    }


    /**
     * Sets the incidentTypeId value for this CreateIncident.
     * 
     * @param incidentTypeId
     */
    public void setIncidentTypeId(java.math.BigInteger incidentTypeId) {
        this.incidentTypeId = incidentTypeId;
    }


    /**
     * Gets the incidentStatusId value for this CreateIncident.
     * 
     * @return incidentStatusId
     */
    public java.math.BigInteger getIncidentStatusId() {
        return incidentStatusId;
    }


    /**
     * Sets the incidentStatusId value for this CreateIncident.
     * 
     * @param incidentStatusId
     */
    public void setIncidentStatusId(java.math.BigInteger incidentStatusId) {
        this.incidentStatusId = incidentStatusId;
    }


    /**
     * Gets the summary value for this CreateIncident.
     * 
     * @return summary
     */
    public java.lang.String getSummary() {
        return summary;
    }


    /**
     * Sets the summary value for this CreateIncident.
     * 
     * @param summary
     */
    public void setSummary(java.lang.String summary) {
        this.summary = summary;
    }


    /**
     * Gets the description value for this CreateIncident.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this CreateIncident.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the incidentCategoryId value for this CreateIncident.
     * 
     * @return incidentCategoryId
     */
    public java.math.BigInteger getIncidentCategoryId() {
        return incidentCategoryId;
    }


    /**
     * Sets the incidentCategoryId value for this CreateIncident.
     * 
     * @param incidentCategoryId
     */
    public void setIncidentCategoryId(java.math.BigInteger incidentCategoryId) {
        this.incidentCategoryId = incidentCategoryId;
    }


    /**
     * Gets the incidentSubCategoryId value for this CreateIncident.
     * 
     * @return incidentSubCategoryId
     */
    public java.math.BigInteger getIncidentSubCategoryId() {
        return incidentSubCategoryId;
    }


    /**
     * Sets the incidentSubCategoryId value for this CreateIncident.
     * 
     * @param incidentSubCategoryId
     */
    public void setIncidentSubCategoryId(java.math.BigInteger incidentSubCategoryId) {
        this.incidentSubCategoryId = incidentSubCategoryId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreateIncident)) return false;
        CreateIncident other = (CreateIncident) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sessionId==null && other.getSessionId()==null) || 
             (this.sessionId!=null &&
              this.sessionId.equals(other.getSessionId()))) &&
            ((this.country==null && other.getCountry()==null) || 
             (this.country!=null &&
              this.country.equals(other.getCountry()))) &&
            ((this.brandId==null && other.getBrandId()==null) || 
             (this.brandId!=null &&
              this.brandId.equals(other.getBrandId()))) &&
            ((this.incidentPriorityId==null && other.getIncidentPriorityId()==null) || 
             (this.incidentPriorityId!=null &&
              this.incidentPriorityId.equals(other.getIncidentPriorityId()))) &&
            ((this.incidentTypeId==null && other.getIncidentTypeId()==null) || 
             (this.incidentTypeId!=null &&
              this.incidentTypeId.equals(other.getIncidentTypeId()))) &&
            ((this.incidentStatusId==null && other.getIncidentStatusId()==null) || 
             (this.incidentStatusId!=null &&
              this.incidentStatusId.equals(other.getIncidentStatusId()))) &&
            ((this.summary==null && other.getSummary()==null) || 
             (this.summary!=null &&
              this.summary.equals(other.getSummary()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.incidentCategoryId==null && other.getIncidentCategoryId()==null) || 
             (this.incidentCategoryId!=null &&
              this.incidentCategoryId.equals(other.getIncidentCategoryId()))) &&
            ((this.incidentSubCategoryId==null && other.getIncidentSubCategoryId()==null) || 
             (this.incidentSubCategoryId!=null &&
              this.incidentSubCategoryId.equals(other.getIncidentSubCategoryId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSessionId() != null) {
            _hashCode += getSessionId().hashCode();
        }
        if (getCountry() != null) {
            _hashCode += getCountry().hashCode();
        }
        if (getBrandId() != null) {
            _hashCode += getBrandId().hashCode();
        }
        if (getIncidentPriorityId() != null) {
            _hashCode += getIncidentPriorityId().hashCode();
        }
        if (getIncidentTypeId() != null) {
            _hashCode += getIncidentTypeId().hashCode();
        }
        if (getIncidentStatusId() != null) {
            _hashCode += getIncidentStatusId().hashCode();
        }
        if (getSummary() != null) {
            _hashCode += getSummary().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getIncidentCategoryId() != null) {
            _hashCode += getIncidentCategoryId().hashCode();
        }
        if (getIncidentSubCategoryId() != null) {
            _hashCode += getIncidentSubCategoryId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreateIncident.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">createIncident"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sessionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country");
        elemField.setXmlName(new javax.xml.namespace.QName("", "country"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("brandId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "brandId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("incidentPriorityId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "incidentPriorityId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("incidentTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "incidentTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("incidentStatusId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "incidentStatusId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("summary");
        elemField.setXmlName(new javax.xml.namespace.QName("", "summary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("incidentCategoryId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "incidentCategoryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("incidentSubCategoryId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "incidentSubCategoryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
