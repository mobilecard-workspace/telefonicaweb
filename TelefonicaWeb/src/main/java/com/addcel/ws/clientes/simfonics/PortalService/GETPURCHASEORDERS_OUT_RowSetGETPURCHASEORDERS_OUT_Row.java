/**
 * GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row  implements java.io.Serializable {
    private java.math.BigInteger ID;

    private java.math.BigInteger STATUS;

    private java.lang.String STATUSNAME;

    private java.util.Calendar STARTDATE;

    private java.util.Calendar ENDDATE;

    private java.lang.String SUBSCRIPTIONID;

    private java.math.BigInteger PRODUCTID;

    private java.lang.String PRODUCTNAME;

    private java.math.BigInteger PRODUCTTYPEID;

    private java.lang.String PRODUCTTYPENAME;

    private java.math.BigDecimal PRODUCTPRICE;

    private java.math.BigInteger CATALOGID;

    private java.math.BigInteger TYPEID;

    private java.lang.String TYPENAME;

    private java.math.BigInteger CATEGORYID;

    private java.lang.String CATEGORYNAME;

    private java.math.BigInteger SUBCATEGORYID;

    private java.lang.String SUBCATEGORYNAME;

    public GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row() {
    }

    public GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row(
           java.math.BigInteger ID,
           java.math.BigInteger STATUS,
           java.lang.String STATUSNAME,
           java.util.Calendar STARTDATE,
           java.util.Calendar ENDDATE,
           java.lang.String SUBSCRIPTIONID,
           java.math.BigInteger PRODUCTID,
           java.lang.String PRODUCTNAME,
           java.math.BigInteger PRODUCTTYPEID,
           java.lang.String PRODUCTTYPENAME,
           java.math.BigDecimal PRODUCTPRICE,
           java.math.BigInteger CATALOGID,
           java.math.BigInteger TYPEID,
           java.lang.String TYPENAME,
           java.math.BigInteger CATEGORYID,
           java.lang.String CATEGORYNAME,
           java.math.BigInteger SUBCATEGORYID,
           java.lang.String SUBCATEGORYNAME) {
           this.ID = ID;
           this.STATUS = STATUS;
           this.STATUSNAME = STATUSNAME;
           this.STARTDATE = STARTDATE;
           this.ENDDATE = ENDDATE;
           this.SUBSCRIPTIONID = SUBSCRIPTIONID;
           this.PRODUCTID = PRODUCTID;
           this.PRODUCTNAME = PRODUCTNAME;
           this.PRODUCTTYPEID = PRODUCTTYPEID;
           this.PRODUCTTYPENAME = PRODUCTTYPENAME;
           this.PRODUCTPRICE = PRODUCTPRICE;
           this.CATALOGID = CATALOGID;
           this.TYPEID = TYPEID;
           this.TYPENAME = TYPENAME;
           this.CATEGORYID = CATEGORYID;
           this.CATEGORYNAME = CATEGORYNAME;
           this.SUBCATEGORYID = SUBCATEGORYID;
           this.SUBCATEGORYNAME = SUBCATEGORYNAME;
    }


    /**
     * Gets the ID value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return ID
     */
    public java.math.BigInteger getID() {
        return ID;
    }


    /**
     * Sets the ID value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param ID
     */
    public void setID(java.math.BigInteger ID) {
        this.ID = ID;
    }


    /**
     * Gets the STATUS value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return STATUS
     */
    public java.math.BigInteger getSTATUS() {
        return STATUS;
    }


    /**
     * Sets the STATUS value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param STATUS
     */
    public void setSTATUS(java.math.BigInteger STATUS) {
        this.STATUS = STATUS;
    }


    /**
     * Gets the STATUSNAME value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return STATUSNAME
     */
    public java.lang.String getSTATUSNAME() {
        return STATUSNAME;
    }


    /**
     * Sets the STATUSNAME value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param STATUSNAME
     */
    public void setSTATUSNAME(java.lang.String STATUSNAME) {
        this.STATUSNAME = STATUSNAME;
    }


    /**
     * Gets the STARTDATE value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return STARTDATE
     */
    public java.util.Calendar getSTARTDATE() {
        return STARTDATE;
    }


    /**
     * Sets the STARTDATE value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param STARTDATE
     */
    public void setSTARTDATE(java.util.Calendar STARTDATE) {
        this.STARTDATE = STARTDATE;
    }


    /**
     * Gets the ENDDATE value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return ENDDATE
     */
    public java.util.Calendar getENDDATE() {
        return ENDDATE;
    }


    /**
     * Sets the ENDDATE value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param ENDDATE
     */
    public void setENDDATE(java.util.Calendar ENDDATE) {
        this.ENDDATE = ENDDATE;
    }


    /**
     * Gets the SUBSCRIPTIONID value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return SUBSCRIPTIONID
     */
    public java.lang.String getSUBSCRIPTIONID() {
        return SUBSCRIPTIONID;
    }


    /**
     * Sets the SUBSCRIPTIONID value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param SUBSCRIPTIONID
     */
    public void setSUBSCRIPTIONID(java.lang.String SUBSCRIPTIONID) {
        this.SUBSCRIPTIONID = SUBSCRIPTIONID;
    }


    /**
     * Gets the PRODUCTID value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return PRODUCTID
     */
    public java.math.BigInteger getPRODUCTID() {
        return PRODUCTID;
    }


    /**
     * Sets the PRODUCTID value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param PRODUCTID
     */
    public void setPRODUCTID(java.math.BigInteger PRODUCTID) {
        this.PRODUCTID = PRODUCTID;
    }


    /**
     * Gets the PRODUCTNAME value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return PRODUCTNAME
     */
    public java.lang.String getPRODUCTNAME() {
        return PRODUCTNAME;
    }


    /**
     * Sets the PRODUCTNAME value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param PRODUCTNAME
     */
    public void setPRODUCTNAME(java.lang.String PRODUCTNAME) {
        this.PRODUCTNAME = PRODUCTNAME;
    }


    /**
     * Gets the PRODUCTTYPEID value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return PRODUCTTYPEID
     */
    public java.math.BigInteger getPRODUCTTYPEID() {
        return PRODUCTTYPEID;
    }


    /**
     * Sets the PRODUCTTYPEID value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param PRODUCTTYPEID
     */
    public void setPRODUCTTYPEID(java.math.BigInteger PRODUCTTYPEID) {
        this.PRODUCTTYPEID = PRODUCTTYPEID;
    }


    /**
     * Gets the PRODUCTTYPENAME value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return PRODUCTTYPENAME
     */
    public java.lang.String getPRODUCTTYPENAME() {
        return PRODUCTTYPENAME;
    }


    /**
     * Sets the PRODUCTTYPENAME value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param PRODUCTTYPENAME
     */
    public void setPRODUCTTYPENAME(java.lang.String PRODUCTTYPENAME) {
        this.PRODUCTTYPENAME = PRODUCTTYPENAME;
    }


    /**
     * Gets the PRODUCTPRICE value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return PRODUCTPRICE
     */
    public java.math.BigDecimal getPRODUCTPRICE() {
        return PRODUCTPRICE;
    }


    /**
     * Sets the PRODUCTPRICE value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param PRODUCTPRICE
     */
    public void setPRODUCTPRICE(java.math.BigDecimal PRODUCTPRICE) {
        this.PRODUCTPRICE = PRODUCTPRICE;
    }


    /**
     * Gets the CATALOGID value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return CATALOGID
     */
    public java.math.BigInteger getCATALOGID() {
        return CATALOGID;
    }


    /**
     * Sets the CATALOGID value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param CATALOGID
     */
    public void setCATALOGID(java.math.BigInteger CATALOGID) {
        this.CATALOGID = CATALOGID;
    }


    /**
     * Gets the TYPEID value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return TYPEID
     */
    public java.math.BigInteger getTYPEID() {
        return TYPEID;
    }


    /**
     * Sets the TYPEID value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param TYPEID
     */
    public void setTYPEID(java.math.BigInteger TYPEID) {
        this.TYPEID = TYPEID;
    }


    /**
     * Gets the TYPENAME value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return TYPENAME
     */
    public java.lang.String getTYPENAME() {
        return TYPENAME;
    }


    /**
     * Sets the TYPENAME value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param TYPENAME
     */
    public void setTYPENAME(java.lang.String TYPENAME) {
        this.TYPENAME = TYPENAME;
    }


    /**
     * Gets the CATEGORYID value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return CATEGORYID
     */
    public java.math.BigInteger getCATEGORYID() {
        return CATEGORYID;
    }


    /**
     * Sets the CATEGORYID value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param CATEGORYID
     */
    public void setCATEGORYID(java.math.BigInteger CATEGORYID) {
        this.CATEGORYID = CATEGORYID;
    }


    /**
     * Gets the CATEGORYNAME value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return CATEGORYNAME
     */
    public java.lang.String getCATEGORYNAME() {
        return CATEGORYNAME;
    }


    /**
     * Sets the CATEGORYNAME value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param CATEGORYNAME
     */
    public void setCATEGORYNAME(java.lang.String CATEGORYNAME) {
        this.CATEGORYNAME = CATEGORYNAME;
    }


    /**
     * Gets the SUBCATEGORYID value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return SUBCATEGORYID
     */
    public java.math.BigInteger getSUBCATEGORYID() {
        return SUBCATEGORYID;
    }


    /**
     * Sets the SUBCATEGORYID value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param SUBCATEGORYID
     */
    public void setSUBCATEGORYID(java.math.BigInteger SUBCATEGORYID) {
        this.SUBCATEGORYID = SUBCATEGORYID;
    }


    /**
     * Gets the SUBCATEGORYNAME value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @return SUBCATEGORYNAME
     */
    public java.lang.String getSUBCATEGORYNAME() {
        return SUBCATEGORYNAME;
    }


    /**
     * Sets the SUBCATEGORYNAME value for this GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.
     * 
     * @param SUBCATEGORYNAME
     */
    public void setSUBCATEGORYNAME(java.lang.String SUBCATEGORYNAME) {
        this.SUBCATEGORYNAME = SUBCATEGORYNAME;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row)) return false;
        GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row other = (GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ID==null && other.getID()==null) || 
             (this.ID!=null &&
              this.ID.equals(other.getID()))) &&
            ((this.STATUS==null && other.getSTATUS()==null) || 
             (this.STATUS!=null &&
              this.STATUS.equals(other.getSTATUS()))) &&
            ((this.STATUSNAME==null && other.getSTATUSNAME()==null) || 
             (this.STATUSNAME!=null &&
              this.STATUSNAME.equals(other.getSTATUSNAME()))) &&
            ((this.STARTDATE==null && other.getSTARTDATE()==null) || 
             (this.STARTDATE!=null &&
              this.STARTDATE.equals(other.getSTARTDATE()))) &&
            ((this.ENDDATE==null && other.getENDDATE()==null) || 
             (this.ENDDATE!=null &&
              this.ENDDATE.equals(other.getENDDATE()))) &&
            ((this.SUBSCRIPTIONID==null && other.getSUBSCRIPTIONID()==null) || 
             (this.SUBSCRIPTIONID!=null &&
              this.SUBSCRIPTIONID.equals(other.getSUBSCRIPTIONID()))) &&
            ((this.PRODUCTID==null && other.getPRODUCTID()==null) || 
             (this.PRODUCTID!=null &&
              this.PRODUCTID.equals(other.getPRODUCTID()))) &&
            ((this.PRODUCTNAME==null && other.getPRODUCTNAME()==null) || 
             (this.PRODUCTNAME!=null &&
              this.PRODUCTNAME.equals(other.getPRODUCTNAME()))) &&
            ((this.PRODUCTTYPEID==null && other.getPRODUCTTYPEID()==null) || 
             (this.PRODUCTTYPEID!=null &&
              this.PRODUCTTYPEID.equals(other.getPRODUCTTYPEID()))) &&
            ((this.PRODUCTTYPENAME==null && other.getPRODUCTTYPENAME()==null) || 
             (this.PRODUCTTYPENAME!=null &&
              this.PRODUCTTYPENAME.equals(other.getPRODUCTTYPENAME()))) &&
            ((this.PRODUCTPRICE==null && other.getPRODUCTPRICE()==null) || 
             (this.PRODUCTPRICE!=null &&
              this.PRODUCTPRICE.equals(other.getPRODUCTPRICE()))) &&
            ((this.CATALOGID==null && other.getCATALOGID()==null) || 
             (this.CATALOGID!=null &&
              this.CATALOGID.equals(other.getCATALOGID()))) &&
            ((this.TYPEID==null && other.getTYPEID()==null) || 
             (this.TYPEID!=null &&
              this.TYPEID.equals(other.getTYPEID()))) &&
            ((this.TYPENAME==null && other.getTYPENAME()==null) || 
             (this.TYPENAME!=null &&
              this.TYPENAME.equals(other.getTYPENAME()))) &&
            ((this.CATEGORYID==null && other.getCATEGORYID()==null) || 
             (this.CATEGORYID!=null &&
              this.CATEGORYID.equals(other.getCATEGORYID()))) &&
            ((this.CATEGORYNAME==null && other.getCATEGORYNAME()==null) || 
             (this.CATEGORYNAME!=null &&
              this.CATEGORYNAME.equals(other.getCATEGORYNAME()))) &&
            ((this.SUBCATEGORYID==null && other.getSUBCATEGORYID()==null) || 
             (this.SUBCATEGORYID!=null &&
              this.SUBCATEGORYID.equals(other.getSUBCATEGORYID()))) &&
            ((this.SUBCATEGORYNAME==null && other.getSUBCATEGORYNAME()==null) || 
             (this.SUBCATEGORYNAME!=null &&
              this.SUBCATEGORYNAME.equals(other.getSUBCATEGORYNAME())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getID() != null) {
            _hashCode += getID().hashCode();
        }
        if (getSTATUS() != null) {
            _hashCode += getSTATUS().hashCode();
        }
        if (getSTATUSNAME() != null) {
            _hashCode += getSTATUSNAME().hashCode();
        }
        if (getSTARTDATE() != null) {
            _hashCode += getSTARTDATE().hashCode();
        }
        if (getENDDATE() != null) {
            _hashCode += getENDDATE().hashCode();
        }
        if (getSUBSCRIPTIONID() != null) {
            _hashCode += getSUBSCRIPTIONID().hashCode();
        }
        if (getPRODUCTID() != null) {
            _hashCode += getPRODUCTID().hashCode();
        }
        if (getPRODUCTNAME() != null) {
            _hashCode += getPRODUCTNAME().hashCode();
        }
        if (getPRODUCTTYPEID() != null) {
            _hashCode += getPRODUCTTYPEID().hashCode();
        }
        if (getPRODUCTTYPENAME() != null) {
            _hashCode += getPRODUCTTYPENAME().hashCode();
        }
        if (getPRODUCTPRICE() != null) {
            _hashCode += getPRODUCTPRICE().hashCode();
        }
        if (getCATALOGID() != null) {
            _hashCode += getCATALOGID().hashCode();
        }
        if (getTYPEID() != null) {
            _hashCode += getTYPEID().hashCode();
        }
        if (getTYPENAME() != null) {
            _hashCode += getTYPENAME().hashCode();
        }
        if (getCATEGORYID() != null) {
            _hashCode += getCATEGORYID().hashCode();
        }
        if (getCATEGORYNAME() != null) {
            _hashCode += getCATEGORYNAME().hashCode();
        }
        if (getSUBCATEGORYID() != null) {
            _hashCode += getSUBCATEGORYID().hashCode();
        }
        if (getSUBCATEGORYNAME() != null) {
            _hashCode += getSUBCATEGORYNAME().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">GETPURCHASEORDERS_OUT_RowSet>GETPURCHASEORDERS_OUT_Row"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STATUS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "STATUS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STATUSNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "STATUSNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STARTDATE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "STARTDATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ENDDATE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ENDDATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SUBSCRIPTIONID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SUBSCRIPTIONID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRODUCTID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PRODUCTID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRODUCTNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PRODUCTNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRODUCTTYPEID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PRODUCTTYPEID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRODUCTTYPENAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PRODUCTTYPENAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRODUCTPRICE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PRODUCTPRICE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CATALOGID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CATALOGID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TYPEID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TYPEID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TYPENAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TYPENAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CATEGORYID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CATEGORYID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CATEGORYNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CATEGORYNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SUBCATEGORYID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SUBCATEGORYID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SUBCATEGORYNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SUBCATEGORYNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
