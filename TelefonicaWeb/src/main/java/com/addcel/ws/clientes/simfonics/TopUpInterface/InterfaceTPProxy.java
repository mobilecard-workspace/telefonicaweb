package com.addcel.ws.clientes.simfonics.TopUpInterface;

public class InterfaceTPProxy implements com.addcel.ws.clientes.simfonics.TopUpInterface.InterfaceTP {
  private String _endpoint = null;
  private com.addcel.ws.clientes.simfonics.TopUpInterface.InterfaceTP interfaceTP = null;
  
  public InterfaceTPProxy() {
    _initInterfaceTPProxy();
  }
  
  public InterfaceTPProxy(String endpoint) {
    _endpoint = endpoint;
    _initInterfaceTPProxy();
  }
  
  private void _initInterfaceTPProxy() {
    try {
      interfaceTP = (new com.addcel.ws.clientes.simfonics.TopUpInterface.InterfaceTPSOAPQSServiceLocator()).getInterfaceTPSOAPQSPort();
      if (interfaceTP != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)interfaceTP)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)interfaceTP)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (interfaceTP != null)
      ((javax.xml.rpc.Stub)interfaceTP)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.ws.clientes.simfonics.TopUpInterface.InterfaceTP getInterfaceTP() {
    if (interfaceTP == null)
      _initInterfaceTPProxy();
    return interfaceTP;
  }
  
  public com.addcel.ws.clientes.simfonics.TopUpInterface.RechargeResponse recharge(com.addcel.ws.clientes.simfonics.TopUpInterface.RechargeRequest rechargeInput) throws java.rmi.RemoteException{
    if (interfaceTP == null)
      _initInterfaceTPProxy();
    return interfaceTP.recharge(rechargeInput);
  }
  
  public com.addcel.ws.clientes.simfonics.TopUpInterface.CancelationResponse cancelation(com.addcel.ws.clientes.simfonics.TopUpInterface.CancelationRequest cancelationInput) throws java.rmi.RemoteException{
    if (interfaceTP == null)
      _initInterfaceTPProxy();
    return interfaceTP.cancelation(cancelationInput);
  }
  
  public com.addcel.ws.clientes.simfonics.TopUpInterface.QueryResponse query(com.addcel.ws.clientes.simfonics.TopUpInterface.QueryRequest queryInput) throws java.rmi.RemoteException{
    if (interfaceTP == null)
      _initInterfaceTPProxy();
    return interfaceTP.query(queryInput);
  }
  
  
}