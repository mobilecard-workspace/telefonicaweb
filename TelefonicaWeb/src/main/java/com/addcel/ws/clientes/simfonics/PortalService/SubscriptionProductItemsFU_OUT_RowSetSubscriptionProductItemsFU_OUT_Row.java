/**
 * SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row  implements java.io.Serializable {
    private java.math.BigInteger productItemFUId;

    private java.util.Calendar productFUStartDate;

    private java.util.Calendar productItemFUEndDate;

    private java.util.Calendar productItemFUExpirationDate;

    private java.math.BigInteger productItemFUStatusId;

    private java.lang.String productItemFUStatus;

    private java.math.BigInteger accountCounterInitialValue;

    private java.math.BigInteger accountCounterEndValue;

    private java.lang.String accountCounterCurrentValue;

    private java.math.BigInteger accountCounterUnitId;

    private java.lang.String accountCounterUnit;

    public SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row() {
    }

    public SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row(
           java.math.BigInteger productItemFUId,
           java.util.Calendar productFUStartDate,
           java.util.Calendar productItemFUEndDate,
           java.util.Calendar productItemFUExpirationDate,
           java.math.BigInteger productItemFUStatusId,
           java.lang.String productItemFUStatus,
           java.math.BigInteger accountCounterInitialValue,
           java.math.BigInteger accountCounterEndValue,
           java.lang.String accountCounterCurrentValue,
           java.math.BigInteger accountCounterUnitId,
           java.lang.String accountCounterUnit) {
           this.productItemFUId = productItemFUId;
           this.productFUStartDate = productFUStartDate;
           this.productItemFUEndDate = productItemFUEndDate;
           this.productItemFUExpirationDate = productItemFUExpirationDate;
           this.productItemFUStatusId = productItemFUStatusId;
           this.productItemFUStatus = productItemFUStatus;
           this.accountCounterInitialValue = accountCounterInitialValue;
           this.accountCounterEndValue = accountCounterEndValue;
           this.accountCounterCurrentValue = accountCounterCurrentValue;
           this.accountCounterUnitId = accountCounterUnitId;
           this.accountCounterUnit = accountCounterUnit;
    }


    /**
     * Gets the productItemFUId value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @return productItemFUId
     */
    public java.math.BigInteger getProductItemFUId() {
        return productItemFUId;
    }


    /**
     * Sets the productItemFUId value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @param productItemFUId
     */
    public void setProductItemFUId(java.math.BigInteger productItemFUId) {
        this.productItemFUId = productItemFUId;
    }


    /**
     * Gets the productFUStartDate value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @return productFUStartDate
     */
    public java.util.Calendar getProductFUStartDate() {
        return productFUStartDate;
    }


    /**
     * Sets the productFUStartDate value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @param productFUStartDate
     */
    public void setProductFUStartDate(java.util.Calendar productFUStartDate) {
        this.productFUStartDate = productFUStartDate;
    }


    /**
     * Gets the productItemFUEndDate value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @return productItemFUEndDate
     */
    public java.util.Calendar getProductItemFUEndDate() {
        return productItemFUEndDate;
    }


    /**
     * Sets the productItemFUEndDate value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @param productItemFUEndDate
     */
    public void setProductItemFUEndDate(java.util.Calendar productItemFUEndDate) {
        this.productItemFUEndDate = productItemFUEndDate;
    }


    /**
     * Gets the productItemFUExpirationDate value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @return productItemFUExpirationDate
     */
    public java.util.Calendar getProductItemFUExpirationDate() {
        return productItemFUExpirationDate;
    }


    /**
     * Sets the productItemFUExpirationDate value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @param productItemFUExpirationDate
     */
    public void setProductItemFUExpirationDate(java.util.Calendar productItemFUExpirationDate) {
        this.productItemFUExpirationDate = productItemFUExpirationDate;
    }


    /**
     * Gets the productItemFUStatusId value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @return productItemFUStatusId
     */
    public java.math.BigInteger getProductItemFUStatusId() {
        return productItemFUStatusId;
    }


    /**
     * Sets the productItemFUStatusId value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @param productItemFUStatusId
     */
    public void setProductItemFUStatusId(java.math.BigInteger productItemFUStatusId) {
        this.productItemFUStatusId = productItemFUStatusId;
    }


    /**
     * Gets the productItemFUStatus value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @return productItemFUStatus
     */
    public java.lang.String getProductItemFUStatus() {
        return productItemFUStatus;
    }


    /**
     * Sets the productItemFUStatus value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @param productItemFUStatus
     */
    public void setProductItemFUStatus(java.lang.String productItemFUStatus) {
        this.productItemFUStatus = productItemFUStatus;
    }


    /**
     * Gets the accountCounterInitialValue value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @return accountCounterInitialValue
     */
    public java.math.BigInteger getAccountCounterInitialValue() {
        return accountCounterInitialValue;
    }


    /**
     * Sets the accountCounterInitialValue value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @param accountCounterInitialValue
     */
    public void setAccountCounterInitialValue(java.math.BigInteger accountCounterInitialValue) {
        this.accountCounterInitialValue = accountCounterInitialValue;
    }


    /**
     * Gets the accountCounterEndValue value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @return accountCounterEndValue
     */
    public java.math.BigInteger getAccountCounterEndValue() {
        return accountCounterEndValue;
    }


    /**
     * Sets the accountCounterEndValue value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @param accountCounterEndValue
     */
    public void setAccountCounterEndValue(java.math.BigInteger accountCounterEndValue) {
        this.accountCounterEndValue = accountCounterEndValue;
    }


    /**
     * Gets the accountCounterCurrentValue value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @return accountCounterCurrentValue
     */
    public java.lang.String getAccountCounterCurrentValue() {
        return accountCounterCurrentValue;
    }


    /**
     * Sets the accountCounterCurrentValue value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @param accountCounterCurrentValue
     */
    public void setAccountCounterCurrentValue(java.lang.String accountCounterCurrentValue) {
        this.accountCounterCurrentValue = accountCounterCurrentValue;
    }


    /**
     * Gets the accountCounterUnitId value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @return accountCounterUnitId
     */
    public java.math.BigInteger getAccountCounterUnitId() {
        return accountCounterUnitId;
    }


    /**
     * Sets the accountCounterUnitId value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @param accountCounterUnitId
     */
    public void setAccountCounterUnitId(java.math.BigInteger accountCounterUnitId) {
        this.accountCounterUnitId = accountCounterUnitId;
    }


    /**
     * Gets the accountCounterUnit value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @return accountCounterUnit
     */
    public java.lang.String getAccountCounterUnit() {
        return accountCounterUnit;
    }


    /**
     * Sets the accountCounterUnit value for this SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.
     * 
     * @param accountCounterUnit
     */
    public void setAccountCounterUnit(java.lang.String accountCounterUnit) {
        this.accountCounterUnit = accountCounterUnit;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row)) return false;
        SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row other = (SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.productItemFUId==null && other.getProductItemFUId()==null) || 
             (this.productItemFUId!=null &&
              this.productItemFUId.equals(other.getProductItemFUId()))) &&
            ((this.productFUStartDate==null && other.getProductFUStartDate()==null) || 
             (this.productFUStartDate!=null &&
              this.productFUStartDate.equals(other.getProductFUStartDate()))) &&
            ((this.productItemFUEndDate==null && other.getProductItemFUEndDate()==null) || 
             (this.productItemFUEndDate!=null &&
              this.productItemFUEndDate.equals(other.getProductItemFUEndDate()))) &&
            ((this.productItemFUExpirationDate==null && other.getProductItemFUExpirationDate()==null) || 
             (this.productItemFUExpirationDate!=null &&
              this.productItemFUExpirationDate.equals(other.getProductItemFUExpirationDate()))) &&
            ((this.productItemFUStatusId==null && other.getProductItemFUStatusId()==null) || 
             (this.productItemFUStatusId!=null &&
              this.productItemFUStatusId.equals(other.getProductItemFUStatusId()))) &&
            ((this.productItemFUStatus==null && other.getProductItemFUStatus()==null) || 
             (this.productItemFUStatus!=null &&
              this.productItemFUStatus.equals(other.getProductItemFUStatus()))) &&
            ((this.accountCounterInitialValue==null && other.getAccountCounterInitialValue()==null) || 
             (this.accountCounterInitialValue!=null &&
              this.accountCounterInitialValue.equals(other.getAccountCounterInitialValue()))) &&
            ((this.accountCounterEndValue==null && other.getAccountCounterEndValue()==null) || 
             (this.accountCounterEndValue!=null &&
              this.accountCounterEndValue.equals(other.getAccountCounterEndValue()))) &&
            ((this.accountCounterCurrentValue==null && other.getAccountCounterCurrentValue()==null) || 
             (this.accountCounterCurrentValue!=null &&
              this.accountCounterCurrentValue.equals(other.getAccountCounterCurrentValue()))) &&
            ((this.accountCounterUnitId==null && other.getAccountCounterUnitId()==null) || 
             (this.accountCounterUnitId!=null &&
              this.accountCounterUnitId.equals(other.getAccountCounterUnitId()))) &&
            ((this.accountCounterUnit==null && other.getAccountCounterUnit()==null) || 
             (this.accountCounterUnit!=null &&
              this.accountCounterUnit.equals(other.getAccountCounterUnit())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getProductItemFUId() != null) {
            _hashCode += getProductItemFUId().hashCode();
        }
        if (getProductFUStartDate() != null) {
            _hashCode += getProductFUStartDate().hashCode();
        }
        if (getProductItemFUEndDate() != null) {
            _hashCode += getProductItemFUEndDate().hashCode();
        }
        if (getProductItemFUExpirationDate() != null) {
            _hashCode += getProductItemFUExpirationDate().hashCode();
        }
        if (getProductItemFUStatusId() != null) {
            _hashCode += getProductItemFUStatusId().hashCode();
        }
        if (getProductItemFUStatus() != null) {
            _hashCode += getProductItemFUStatus().hashCode();
        }
        if (getAccountCounterInitialValue() != null) {
            _hashCode += getAccountCounterInitialValue().hashCode();
        }
        if (getAccountCounterEndValue() != null) {
            _hashCode += getAccountCounterEndValue().hashCode();
        }
        if (getAccountCounterCurrentValue() != null) {
            _hashCode += getAccountCounterCurrentValue().hashCode();
        }
        if (getAccountCounterUnitId() != null) {
            _hashCode += getAccountCounterUnitId().hashCode();
        }
        if (getAccountCounterUnit() != null) {
            _hashCode += getAccountCounterUnit().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">subscriptionProductItemsFU_OUT_RowSet>subscriptionProductItemsFU_OUT_Row"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productItemFUId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productItemFUId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productFUStartDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productFUStartDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productItemFUEndDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productItemFUEndDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productItemFUExpirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productItemFUExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productItemFUStatusId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productItemFUStatusId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productItemFUStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productItemFUStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountCounterInitialValue");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accountCounterInitialValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountCounterEndValue");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accountCounterEndValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountCounterCurrentValue");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accountCounterCurrentValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountCounterUnitId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accountCounterUnitId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountCounterUnit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accountCounterUnit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
