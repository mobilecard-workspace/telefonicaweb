/**
 * DoPaymentResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class DoPaymentResponse  implements java.io.Serializable {
    private java.lang.String result;

    private java.lang.String resultMessage;

    private java.lang.String transactionResult;

    private java.lang.String transactionOrder;

    private java.lang.String transactionCode;

    private java.lang.String transactionDate;

    private java.lang.String transactionReference;

    private java.lang.String transactionExternalReference;

    public DoPaymentResponse() {
    }

    public DoPaymentResponse(
           java.lang.String result,
           java.lang.String resultMessage,
           java.lang.String transactionResult,
           java.lang.String transactionOrder,
           java.lang.String transactionCode,
           java.lang.String transactionDate,
           java.lang.String transactionReference,
           java.lang.String transactionExternalReference) {
           this.result = result;
           this.resultMessage = resultMessage;
           this.transactionResult = transactionResult;
           this.transactionOrder = transactionOrder;
           this.transactionCode = transactionCode;
           this.transactionDate = transactionDate;
           this.transactionReference = transactionReference;
           this.transactionExternalReference = transactionExternalReference;
    }


    /**
     * Gets the result value for this DoPaymentResponse.
     * 
     * @return result
     */
    public java.lang.String getResult() {
        return result;
    }


    /**
     * Sets the result value for this DoPaymentResponse.
     * 
     * @param result
     */
    public void setResult(java.lang.String result) {
        this.result = result;
    }


    /**
     * Gets the resultMessage value for this DoPaymentResponse.
     * 
     * @return resultMessage
     */
    public java.lang.String getResultMessage() {
        return resultMessage;
    }


    /**
     * Sets the resultMessage value for this DoPaymentResponse.
     * 
     * @param resultMessage
     */
    public void setResultMessage(java.lang.String resultMessage) {
        this.resultMessage = resultMessage;
    }


    /**
     * Gets the transactionResult value for this DoPaymentResponse.
     * 
     * @return transactionResult
     */
    public java.lang.String getTransactionResult() {
        return transactionResult;
    }


    /**
     * Sets the transactionResult value for this DoPaymentResponse.
     * 
     * @param transactionResult
     */
    public void setTransactionResult(java.lang.String transactionResult) {
        this.transactionResult = transactionResult;
    }


    /**
     * Gets the transactionOrder value for this DoPaymentResponse.
     * 
     * @return transactionOrder
     */
    public java.lang.String getTransactionOrder() {
        return transactionOrder;
    }


    /**
     * Sets the transactionOrder value for this DoPaymentResponse.
     * 
     * @param transactionOrder
     */
    public void setTransactionOrder(java.lang.String transactionOrder) {
        this.transactionOrder = transactionOrder;
    }


    /**
     * Gets the transactionCode value for this DoPaymentResponse.
     * 
     * @return transactionCode
     */
    public java.lang.String getTransactionCode() {
        return transactionCode;
    }


    /**
     * Sets the transactionCode value for this DoPaymentResponse.
     * 
     * @param transactionCode
     */
    public void setTransactionCode(java.lang.String transactionCode) {
        this.transactionCode = transactionCode;
    }


    /**
     * Gets the transactionDate value for this DoPaymentResponse.
     * 
     * @return transactionDate
     */
    public java.lang.String getTransactionDate() {
        return transactionDate;
    }


    /**
     * Sets the transactionDate value for this DoPaymentResponse.
     * 
     * @param transactionDate
     */
    public void setTransactionDate(java.lang.String transactionDate) {
        this.transactionDate = transactionDate;
    }


    /**
     * Gets the transactionReference value for this DoPaymentResponse.
     * 
     * @return transactionReference
     */
    public java.lang.String getTransactionReference() {
        return transactionReference;
    }


    /**
     * Sets the transactionReference value for this DoPaymentResponse.
     * 
     * @param transactionReference
     */
    public void setTransactionReference(java.lang.String transactionReference) {
        this.transactionReference = transactionReference;
    }


    /**
     * Gets the transactionExternalReference value for this DoPaymentResponse.
     * 
     * @return transactionExternalReference
     */
    public java.lang.String getTransactionExternalReference() {
        return transactionExternalReference;
    }


    /**
     * Sets the transactionExternalReference value for this DoPaymentResponse.
     * 
     * @param transactionExternalReference
     */
    public void setTransactionExternalReference(java.lang.String transactionExternalReference) {
        this.transactionExternalReference = transactionExternalReference;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DoPaymentResponse)) return false;
        DoPaymentResponse other = (DoPaymentResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.result==null && other.getResult()==null) || 
             (this.result!=null &&
              this.result.equals(other.getResult()))) &&
            ((this.resultMessage==null && other.getResultMessage()==null) || 
             (this.resultMessage!=null &&
              this.resultMessage.equals(other.getResultMessage()))) &&
            ((this.transactionResult==null && other.getTransactionResult()==null) || 
             (this.transactionResult!=null &&
              this.transactionResult.equals(other.getTransactionResult()))) &&
            ((this.transactionOrder==null && other.getTransactionOrder()==null) || 
             (this.transactionOrder!=null &&
              this.transactionOrder.equals(other.getTransactionOrder()))) &&
            ((this.transactionCode==null && other.getTransactionCode()==null) || 
             (this.transactionCode!=null &&
              this.transactionCode.equals(other.getTransactionCode()))) &&
            ((this.transactionDate==null && other.getTransactionDate()==null) || 
             (this.transactionDate!=null &&
              this.transactionDate.equals(other.getTransactionDate()))) &&
            ((this.transactionReference==null && other.getTransactionReference()==null) || 
             (this.transactionReference!=null &&
              this.transactionReference.equals(other.getTransactionReference()))) &&
            ((this.transactionExternalReference==null && other.getTransactionExternalReference()==null) || 
             (this.transactionExternalReference!=null &&
              this.transactionExternalReference.equals(other.getTransactionExternalReference())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResult() != null) {
            _hashCode += getResult().hashCode();
        }
        if (getResultMessage() != null) {
            _hashCode += getResultMessage().hashCode();
        }
        if (getTransactionResult() != null) {
            _hashCode += getTransactionResult().hashCode();
        }
        if (getTransactionOrder() != null) {
            _hashCode += getTransactionOrder().hashCode();
        }
        if (getTransactionCode() != null) {
            _hashCode += getTransactionCode().hashCode();
        }
        if (getTransactionDate() != null) {
            _hashCode += getTransactionDate().hashCode();
        }
        if (getTransactionReference() != null) {
            _hashCode += getTransactionReference().hashCode();
        }
        if (getTransactionExternalReference() != null) {
            _hashCode += getTransactionExternalReference().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DoPaymentResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">doPaymentResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result");
        elemField.setXmlName(new javax.xml.namespace.QName("", "result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionResult");
        elemField.setXmlName(new javax.xml.namespace.QName("", "transactionResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionOrder");
        elemField.setXmlName(new javax.xml.namespace.QName("", "transactionOrder"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "transactionCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "transactionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionReference");
        elemField.setXmlName(new javax.xml.namespace.QName("", "transactionReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionExternalReference");
        elemField.setXmlName(new javax.xml.namespace.QName("", "transactionExternalReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
