/**
 * GetPortabilityPropertiesTypeGetPortabilityProperty.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class GetPortabilityPropertiesTypeGetPortabilityProperty  implements java.io.Serializable {
    private java.math.BigDecimal getPortabilityPropertyId;

    private java.lang.String getPortabilityPropertyName;

    public GetPortabilityPropertiesTypeGetPortabilityProperty() {
    }

    public GetPortabilityPropertiesTypeGetPortabilityProperty(
           java.math.BigDecimal getPortabilityPropertyId,
           java.lang.String getPortabilityPropertyName) {
           this.getPortabilityPropertyId = getPortabilityPropertyId;
           this.getPortabilityPropertyName = getPortabilityPropertyName;
    }


    /**
     * Gets the getPortabilityPropertyId value for this GetPortabilityPropertiesTypeGetPortabilityProperty.
     * 
     * @return getPortabilityPropertyId
     */
    public java.math.BigDecimal getGetPortabilityPropertyId() {
        return getPortabilityPropertyId;
    }


    /**
     * Sets the getPortabilityPropertyId value for this GetPortabilityPropertiesTypeGetPortabilityProperty.
     * 
     * @param getPortabilityPropertyId
     */
    public void setGetPortabilityPropertyId(java.math.BigDecimal getPortabilityPropertyId) {
        this.getPortabilityPropertyId = getPortabilityPropertyId;
    }


    /**
     * Gets the getPortabilityPropertyName value for this GetPortabilityPropertiesTypeGetPortabilityProperty.
     * 
     * @return getPortabilityPropertyName
     */
    public java.lang.String getGetPortabilityPropertyName() {
        return getPortabilityPropertyName;
    }


    /**
     * Sets the getPortabilityPropertyName value for this GetPortabilityPropertiesTypeGetPortabilityProperty.
     * 
     * @param getPortabilityPropertyName
     */
    public void setGetPortabilityPropertyName(java.lang.String getPortabilityPropertyName) {
        this.getPortabilityPropertyName = getPortabilityPropertyName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPortabilityPropertiesTypeGetPortabilityProperty)) return false;
        GetPortabilityPropertiesTypeGetPortabilityProperty other = (GetPortabilityPropertiesTypeGetPortabilityProperty) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getPortabilityPropertyId==null && other.getGetPortabilityPropertyId()==null) || 
             (this.getPortabilityPropertyId!=null &&
              this.getPortabilityPropertyId.equals(other.getGetPortabilityPropertyId()))) &&
            ((this.getPortabilityPropertyName==null && other.getGetPortabilityPropertyName()==null) || 
             (this.getPortabilityPropertyName!=null &&
              this.getPortabilityPropertyName.equals(other.getGetPortabilityPropertyName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetPortabilityPropertyId() != null) {
            _hashCode += getGetPortabilityPropertyId().hashCode();
        }
        if (getGetPortabilityPropertyName() != null) {
            _hashCode += getGetPortabilityPropertyName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPortabilityPropertiesTypeGetPortabilityProperty.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityPropertiesType>GetPortabilityProperty"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPortabilityPropertyId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "GetPortabilityPropertyId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPortabilityPropertyName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "GetPortabilityPropertyName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
