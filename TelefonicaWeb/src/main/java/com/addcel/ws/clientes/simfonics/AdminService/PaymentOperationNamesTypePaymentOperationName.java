/**
 * PaymentOperationNamesTypePaymentOperationName.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class PaymentOperationNamesTypePaymentOperationName  implements java.io.Serializable {
    private java.math.BigInteger paymentOperationId;

    private java.lang.String paymentOperationName;

    public PaymentOperationNamesTypePaymentOperationName() {
    }

    public PaymentOperationNamesTypePaymentOperationName(
           java.math.BigInteger paymentOperationId,
           java.lang.String paymentOperationName) {
           this.paymentOperationId = paymentOperationId;
           this.paymentOperationName = paymentOperationName;
    }


    /**
     * Gets the paymentOperationId value for this PaymentOperationNamesTypePaymentOperationName.
     * 
     * @return paymentOperationId
     */
    public java.math.BigInteger getPaymentOperationId() {
        return paymentOperationId;
    }


    /**
     * Sets the paymentOperationId value for this PaymentOperationNamesTypePaymentOperationName.
     * 
     * @param paymentOperationId
     */
    public void setPaymentOperationId(java.math.BigInteger paymentOperationId) {
        this.paymentOperationId = paymentOperationId;
    }


    /**
     * Gets the paymentOperationName value for this PaymentOperationNamesTypePaymentOperationName.
     * 
     * @return paymentOperationName
     */
    public java.lang.String getPaymentOperationName() {
        return paymentOperationName;
    }


    /**
     * Sets the paymentOperationName value for this PaymentOperationNamesTypePaymentOperationName.
     * 
     * @param paymentOperationName
     */
    public void setPaymentOperationName(java.lang.String paymentOperationName) {
        this.paymentOperationName = paymentOperationName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaymentOperationNamesTypePaymentOperationName)) return false;
        PaymentOperationNamesTypePaymentOperationName other = (PaymentOperationNamesTypePaymentOperationName) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.paymentOperationId==null && other.getPaymentOperationId()==null) || 
             (this.paymentOperationId!=null &&
              this.paymentOperationId.equals(other.getPaymentOperationId()))) &&
            ((this.paymentOperationName==null && other.getPaymentOperationName()==null) || 
             (this.paymentOperationName!=null &&
              this.paymentOperationName.equals(other.getPaymentOperationName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPaymentOperationId() != null) {
            _hashCode += getPaymentOperationId().hashCode();
        }
        if (getPaymentOperationName() != null) {
            _hashCode += getPaymentOperationName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaymentOperationNamesTypePaymentOperationName.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">paymentOperationNamesType>paymentOperationName"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentOperationId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paymentOperationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentOperationName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paymentOperationName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
