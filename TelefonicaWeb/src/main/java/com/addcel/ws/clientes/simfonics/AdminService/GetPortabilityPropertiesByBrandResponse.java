/**
 * GetPortabilityPropertiesByBrandResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class GetPortabilityPropertiesByBrandResponse  implements java.io.Serializable {
    private java.lang.String result;

    private java.lang.String responseMessage;

    private com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityPropertiesTypeGetPortabilityProperty[] getPortabilityProperties;

    public GetPortabilityPropertiesByBrandResponse() {
    }

    public GetPortabilityPropertiesByBrandResponse(
           java.lang.String result,
           java.lang.String responseMessage,
           com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityPropertiesTypeGetPortabilityProperty[] getPortabilityProperties) {
           this.result = result;
           this.responseMessage = responseMessage;
           this.getPortabilityProperties = getPortabilityProperties;
    }


    /**
     * Gets the result value for this GetPortabilityPropertiesByBrandResponse.
     * 
     * @return result
     */
    public java.lang.String getResult() {
        return result;
    }


    /**
     * Sets the result value for this GetPortabilityPropertiesByBrandResponse.
     * 
     * @param result
     */
    public void setResult(java.lang.String result) {
        this.result = result;
    }


    /**
     * Gets the responseMessage value for this GetPortabilityPropertiesByBrandResponse.
     * 
     * @return responseMessage
     */
    public java.lang.String getResponseMessage() {
        return responseMessage;
    }


    /**
     * Sets the responseMessage value for this GetPortabilityPropertiesByBrandResponse.
     * 
     * @param responseMessage
     */
    public void setResponseMessage(java.lang.String responseMessage) {
        this.responseMessage = responseMessage;
    }


    /**
     * Gets the getPortabilityProperties value for this GetPortabilityPropertiesByBrandResponse.
     * 
     * @return getPortabilityProperties
     */
    public com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityPropertiesTypeGetPortabilityProperty[] getGetPortabilityProperties() {
        return getPortabilityProperties;
    }


    /**
     * Sets the getPortabilityProperties value for this GetPortabilityPropertiesByBrandResponse.
     * 
     * @param getPortabilityProperties
     */
    public void setGetPortabilityProperties(com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityPropertiesTypeGetPortabilityProperty[] getPortabilityProperties) {
        this.getPortabilityProperties = getPortabilityProperties;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPortabilityPropertiesByBrandResponse)) return false;
        GetPortabilityPropertiesByBrandResponse other = (GetPortabilityPropertiesByBrandResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.result==null && other.getResult()==null) || 
             (this.result!=null &&
              this.result.equals(other.getResult()))) &&
            ((this.responseMessage==null && other.getResponseMessage()==null) || 
             (this.responseMessage!=null &&
              this.responseMessage.equals(other.getResponseMessage()))) &&
            ((this.getPortabilityProperties==null && other.getGetPortabilityProperties()==null) || 
             (this.getPortabilityProperties!=null &&
              java.util.Arrays.equals(this.getPortabilityProperties, other.getGetPortabilityProperties())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResult() != null) {
            _hashCode += getResult().hashCode();
        }
        if (getResponseMessage() != null) {
            _hashCode += getResponseMessage().hashCode();
        }
        if (getGetPortabilityProperties() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetPortabilityProperties());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetPortabilityProperties(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPortabilityPropertiesByBrandResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityPropertiesByBrandResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result");
        elemField.setXmlName(new javax.xml.namespace.QName("", "result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "responseMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPortabilityProperties");
        elemField.setXmlName(new javax.xml.namespace.QName("", "GetPortabilityProperties"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityPropertiesType>GetPortabilityProperty"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "GetPortabilityProperty"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
