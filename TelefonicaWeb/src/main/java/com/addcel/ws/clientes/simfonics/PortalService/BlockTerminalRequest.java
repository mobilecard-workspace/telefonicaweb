/**
 * BlockTerminalRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class BlockTerminalRequest  implements java.io.Serializable {
    private java.lang.String sessionId;

    private java.lang.String country;

    private java.math.BigInteger brandId;

    private java.util.Calendar lostDate;

    private java.util.Calendar operatorNotificationDate;

    private java.util.Calendar complaintDate;

    private java.lang.String deviceBrand;

    private java.lang.String deviceModel;

    private java.lang.String technologyType;

    private int modalityId;

    private java.lang.String imei;

    private java.lang.String msisdn;

    private java.lang.String blockingReason;

    public BlockTerminalRequest() {
    }

    public BlockTerminalRequest(
           java.lang.String sessionId,
           java.lang.String country,
           java.math.BigInteger brandId,
           java.util.Calendar lostDate,
           java.util.Calendar operatorNotificationDate,
           java.util.Calendar complaintDate,
           java.lang.String deviceBrand,
           java.lang.String deviceModel,
           java.lang.String technologyType,
           int modalityId,
           java.lang.String imei,
           java.lang.String msisdn,
           java.lang.String blockingReason) {
           this.sessionId = sessionId;
           this.country = country;
           this.brandId = brandId;
           this.lostDate = lostDate;
           this.operatorNotificationDate = operatorNotificationDate;
           this.complaintDate = complaintDate;
           this.deviceBrand = deviceBrand;
           this.deviceModel = deviceModel;
           this.technologyType = technologyType;
           this.modalityId = modalityId;
           this.imei = imei;
           this.msisdn = msisdn;
           this.blockingReason = blockingReason;
    }


    /**
     * Gets the sessionId value for this BlockTerminalRequest.
     * 
     * @return sessionId
     */
    public java.lang.String getSessionId() {
        return sessionId;
    }


    /**
     * Sets the sessionId value for this BlockTerminalRequest.
     * 
     * @param sessionId
     */
    public void setSessionId(java.lang.String sessionId) {
        this.sessionId = sessionId;
    }


    /**
     * Gets the country value for this BlockTerminalRequest.
     * 
     * @return country
     */
    public java.lang.String getCountry() {
        return country;
    }


    /**
     * Sets the country value for this BlockTerminalRequest.
     * 
     * @param country
     */
    public void setCountry(java.lang.String country) {
        this.country = country;
    }


    /**
     * Gets the brandId value for this BlockTerminalRequest.
     * 
     * @return brandId
     */
    public java.math.BigInteger getBrandId() {
        return brandId;
    }


    /**
     * Sets the brandId value for this BlockTerminalRequest.
     * 
     * @param brandId
     */
    public void setBrandId(java.math.BigInteger brandId) {
        this.brandId = brandId;
    }


    /**
     * Gets the lostDate value for this BlockTerminalRequest.
     * 
     * @return lostDate
     */
    public java.util.Calendar getLostDate() {
        return lostDate;
    }


    /**
     * Sets the lostDate value for this BlockTerminalRequest.
     * 
     * @param lostDate
     */
    public void setLostDate(java.util.Calendar lostDate) {
        this.lostDate = lostDate;
    }


    /**
     * Gets the operatorNotificationDate value for this BlockTerminalRequest.
     * 
     * @return operatorNotificationDate
     */
    public java.util.Calendar getOperatorNotificationDate() {
        return operatorNotificationDate;
    }


    /**
     * Sets the operatorNotificationDate value for this BlockTerminalRequest.
     * 
     * @param operatorNotificationDate
     */
    public void setOperatorNotificationDate(java.util.Calendar operatorNotificationDate) {
        this.operatorNotificationDate = operatorNotificationDate;
    }


    /**
     * Gets the complaintDate value for this BlockTerminalRequest.
     * 
     * @return complaintDate
     */
    public java.util.Calendar getComplaintDate() {
        return complaintDate;
    }


    /**
     * Sets the complaintDate value for this BlockTerminalRequest.
     * 
     * @param complaintDate
     */
    public void setComplaintDate(java.util.Calendar complaintDate) {
        this.complaintDate = complaintDate;
    }


    /**
     * Gets the deviceBrand value for this BlockTerminalRequest.
     * 
     * @return deviceBrand
     */
    public java.lang.String getDeviceBrand() {
        return deviceBrand;
    }


    /**
     * Sets the deviceBrand value for this BlockTerminalRequest.
     * 
     * @param deviceBrand
     */
    public void setDeviceBrand(java.lang.String deviceBrand) {
        this.deviceBrand = deviceBrand;
    }


    /**
     * Gets the deviceModel value for this BlockTerminalRequest.
     * 
     * @return deviceModel
     */
    public java.lang.String getDeviceModel() {
        return deviceModel;
    }


    /**
     * Sets the deviceModel value for this BlockTerminalRequest.
     * 
     * @param deviceModel
     */
    public void setDeviceModel(java.lang.String deviceModel) {
        this.deviceModel = deviceModel;
    }


    /**
     * Gets the technologyType value for this BlockTerminalRequest.
     * 
     * @return technologyType
     */
    public java.lang.String getTechnologyType() {
        return technologyType;
    }


    /**
     * Sets the technologyType value for this BlockTerminalRequest.
     * 
     * @param technologyType
     */
    public void setTechnologyType(java.lang.String technologyType) {
        this.technologyType = technologyType;
    }


    /**
     * Gets the modalityId value for this BlockTerminalRequest.
     * 
     * @return modalityId
     */
    public int getModalityId() {
        return modalityId;
    }


    /**
     * Sets the modalityId value for this BlockTerminalRequest.
     * 
     * @param modalityId
     */
    public void setModalityId(int modalityId) {
        this.modalityId = modalityId;
    }


    /**
     * Gets the imei value for this BlockTerminalRequest.
     * 
     * @return imei
     */
    public java.lang.String getImei() {
        return imei;
    }


    /**
     * Sets the imei value for this BlockTerminalRequest.
     * 
     * @param imei
     */
    public void setImei(java.lang.String imei) {
        this.imei = imei;
    }


    /**
     * Gets the msisdn value for this BlockTerminalRequest.
     * 
     * @return msisdn
     */
    public java.lang.String getMsisdn() {
        return msisdn;
    }


    /**
     * Sets the msisdn value for this BlockTerminalRequest.
     * 
     * @param msisdn
     */
    public void setMsisdn(java.lang.String msisdn) {
        this.msisdn = msisdn;
    }


    /**
     * Gets the blockingReason value for this BlockTerminalRequest.
     * 
     * @return blockingReason
     */
    public java.lang.String getBlockingReason() {
        return blockingReason;
    }


    /**
     * Sets the blockingReason value for this BlockTerminalRequest.
     * 
     * @param blockingReason
     */
    public void setBlockingReason(java.lang.String blockingReason) {
        this.blockingReason = blockingReason;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BlockTerminalRequest)) return false;
        BlockTerminalRequest other = (BlockTerminalRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sessionId==null && other.getSessionId()==null) || 
             (this.sessionId!=null &&
              this.sessionId.equals(other.getSessionId()))) &&
            ((this.country==null && other.getCountry()==null) || 
             (this.country!=null &&
              this.country.equals(other.getCountry()))) &&
            ((this.brandId==null && other.getBrandId()==null) || 
             (this.brandId!=null &&
              this.brandId.equals(other.getBrandId()))) &&
            ((this.lostDate==null && other.getLostDate()==null) || 
             (this.lostDate!=null &&
              this.lostDate.equals(other.getLostDate()))) &&
            ((this.operatorNotificationDate==null && other.getOperatorNotificationDate()==null) || 
             (this.operatorNotificationDate!=null &&
              this.operatorNotificationDate.equals(other.getOperatorNotificationDate()))) &&
            ((this.complaintDate==null && other.getComplaintDate()==null) || 
             (this.complaintDate!=null &&
              this.complaintDate.equals(other.getComplaintDate()))) &&
            ((this.deviceBrand==null && other.getDeviceBrand()==null) || 
             (this.deviceBrand!=null &&
              this.deviceBrand.equals(other.getDeviceBrand()))) &&
            ((this.deviceModel==null && other.getDeviceModel()==null) || 
             (this.deviceModel!=null &&
              this.deviceModel.equals(other.getDeviceModel()))) &&
            ((this.technologyType==null && other.getTechnologyType()==null) || 
             (this.technologyType!=null &&
              this.technologyType.equals(other.getTechnologyType()))) &&
            this.modalityId == other.getModalityId() &&
            ((this.imei==null && other.getImei()==null) || 
             (this.imei!=null &&
              this.imei.equals(other.getImei()))) &&
            ((this.msisdn==null && other.getMsisdn()==null) || 
             (this.msisdn!=null &&
              this.msisdn.equals(other.getMsisdn()))) &&
            ((this.blockingReason==null && other.getBlockingReason()==null) || 
             (this.blockingReason!=null &&
              this.blockingReason.equals(other.getBlockingReason())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSessionId() != null) {
            _hashCode += getSessionId().hashCode();
        }
        if (getCountry() != null) {
            _hashCode += getCountry().hashCode();
        }
        if (getBrandId() != null) {
            _hashCode += getBrandId().hashCode();
        }
        if (getLostDate() != null) {
            _hashCode += getLostDate().hashCode();
        }
        if (getOperatorNotificationDate() != null) {
            _hashCode += getOperatorNotificationDate().hashCode();
        }
        if (getComplaintDate() != null) {
            _hashCode += getComplaintDate().hashCode();
        }
        if (getDeviceBrand() != null) {
            _hashCode += getDeviceBrand().hashCode();
        }
        if (getDeviceModel() != null) {
            _hashCode += getDeviceModel().hashCode();
        }
        if (getTechnologyType() != null) {
            _hashCode += getTechnologyType().hashCode();
        }
        _hashCode += getModalityId();
        if (getImei() != null) {
            _hashCode += getImei().hashCode();
        }
        if (getMsisdn() != null) {
            _hashCode += getMsisdn().hashCode();
        }
        if (getBlockingReason() != null) {
            _hashCode += getBlockingReason().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BlockTerminalRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">BlockTerminalRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sessionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country");
        elemField.setXmlName(new javax.xml.namespace.QName("", "country"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("brandId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "brandId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lostDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lostDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operatorNotificationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "operatorNotificationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("complaintDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "complaintDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceBrand");
        elemField.setXmlName(new javax.xml.namespace.QName("", "deviceBrand"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deviceModel");
        elemField.setXmlName(new javax.xml.namespace.QName("", "deviceModel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("technologyType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "technologyType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modalityId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "modalityId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imei");
        elemField.setXmlName(new javax.xml.namespace.QName("", "imei"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msisdn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msisdn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("blockingReason");
        elemField.setXmlName(new javax.xml.namespace.QName("", "blockingReason"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
