/**
 * SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row  implements java.io.Serializable {
    private java.math.BigInteger subscriptionStatusId;

    private java.lang.String subscriptionStatus;

    private java.math.BigInteger msisdnId;

    private java.lang.String msisdn;

    private java.math.BigInteger imsiId;

    private java.lang.String imsi;

    private java.math.BigInteger iccId;

    private java.lang.String icc;

    private java.lang.String subscriptionType;

    private java.math.BigInteger subscriptionTypeId;

    private java.math.BigDecimal balance;

    private java.util.Calendar balanceExpirationDate;

    private java.math.BigInteger balanceStatusId;

    private java.lang.String balanceStatus;

    private com.addcel.ws.clientes.simfonics.PortalService.SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row[] subscriptionProductsFU;

    public SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row() {
    }

    public SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row(
           java.math.BigInteger subscriptionStatusId,
           java.lang.String subscriptionStatus,
           java.math.BigInteger msisdnId,
           java.lang.String msisdn,
           java.math.BigInteger imsiId,
           java.lang.String imsi,
           java.math.BigInteger iccId,
           java.lang.String icc,
           java.lang.String subscriptionType,
           java.math.BigInteger subscriptionTypeId,
           java.math.BigDecimal balance,
           java.util.Calendar balanceExpirationDate,
           java.math.BigInteger balanceStatusId,
           java.lang.String balanceStatus,
           com.addcel.ws.clientes.simfonics.PortalService.SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row[] subscriptionProductsFU) {
           this.subscriptionStatusId = subscriptionStatusId;
           this.subscriptionStatus = subscriptionStatus;
           this.msisdnId = msisdnId;
           this.msisdn = msisdn;
           this.imsiId = imsiId;
           this.imsi = imsi;
           this.iccId = iccId;
           this.icc = icc;
           this.subscriptionType = subscriptionType;
           this.subscriptionTypeId = subscriptionTypeId;
           this.balance = balance;
           this.balanceExpirationDate = balanceExpirationDate;
           this.balanceStatusId = balanceStatusId;
           this.balanceStatus = balanceStatus;
           this.subscriptionProductsFU = subscriptionProductsFU;
    }


    /**
     * Gets the subscriptionStatusId value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @return subscriptionStatusId
     */
    public java.math.BigInteger getSubscriptionStatusId() {
        return subscriptionStatusId;
    }


    /**
     * Sets the subscriptionStatusId value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @param subscriptionStatusId
     */
    public void setSubscriptionStatusId(java.math.BigInteger subscriptionStatusId) {
        this.subscriptionStatusId = subscriptionStatusId;
    }


    /**
     * Gets the subscriptionStatus value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @return subscriptionStatus
     */
    public java.lang.String getSubscriptionStatus() {
        return subscriptionStatus;
    }


    /**
     * Sets the subscriptionStatus value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @param subscriptionStatus
     */
    public void setSubscriptionStatus(java.lang.String subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }


    /**
     * Gets the msisdnId value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @return msisdnId
     */
    public java.math.BigInteger getMsisdnId() {
        return msisdnId;
    }


    /**
     * Sets the msisdnId value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @param msisdnId
     */
    public void setMsisdnId(java.math.BigInteger msisdnId) {
        this.msisdnId = msisdnId;
    }


    /**
     * Gets the msisdn value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @return msisdn
     */
    public java.lang.String getMsisdn() {
        return msisdn;
    }


    /**
     * Sets the msisdn value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @param msisdn
     */
    public void setMsisdn(java.lang.String msisdn) {
        this.msisdn = msisdn;
    }


    /**
     * Gets the imsiId value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @return imsiId
     */
    public java.math.BigInteger getImsiId() {
        return imsiId;
    }


    /**
     * Sets the imsiId value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @param imsiId
     */
    public void setImsiId(java.math.BigInteger imsiId) {
        this.imsiId = imsiId;
    }


    /**
     * Gets the imsi value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @return imsi
     */
    public java.lang.String getImsi() {
        return imsi;
    }


    /**
     * Sets the imsi value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @param imsi
     */
    public void setImsi(java.lang.String imsi) {
        this.imsi = imsi;
    }


    /**
     * Gets the iccId value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @return iccId
     */
    public java.math.BigInteger getIccId() {
        return iccId;
    }


    /**
     * Sets the iccId value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @param iccId
     */
    public void setIccId(java.math.BigInteger iccId) {
        this.iccId = iccId;
    }


    /**
     * Gets the icc value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @return icc
     */
    public java.lang.String getIcc() {
        return icc;
    }


    /**
     * Sets the icc value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @param icc
     */
    public void setIcc(java.lang.String icc) {
        this.icc = icc;
    }


    /**
     * Gets the subscriptionType value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @return subscriptionType
     */
    public java.lang.String getSubscriptionType() {
        return subscriptionType;
    }


    /**
     * Sets the subscriptionType value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @param subscriptionType
     */
    public void setSubscriptionType(java.lang.String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }


    /**
     * Gets the subscriptionTypeId value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @return subscriptionTypeId
     */
    public java.math.BigInteger getSubscriptionTypeId() {
        return subscriptionTypeId;
    }


    /**
     * Sets the subscriptionTypeId value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @param subscriptionTypeId
     */
    public void setSubscriptionTypeId(java.math.BigInteger subscriptionTypeId) {
        this.subscriptionTypeId = subscriptionTypeId;
    }


    /**
     * Gets the balance value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @return balance
     */
    public java.math.BigDecimal getBalance() {
        return balance;
    }


    /**
     * Sets the balance value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @param balance
     */
    public void setBalance(java.math.BigDecimal balance) {
        this.balance = balance;
    }


    /**
     * Gets the balanceExpirationDate value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @return balanceExpirationDate
     */
    public java.util.Calendar getBalanceExpirationDate() {
        return balanceExpirationDate;
    }


    /**
     * Sets the balanceExpirationDate value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @param balanceExpirationDate
     */
    public void setBalanceExpirationDate(java.util.Calendar balanceExpirationDate) {
        this.balanceExpirationDate = balanceExpirationDate;
    }


    /**
     * Gets the balanceStatusId value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @return balanceStatusId
     */
    public java.math.BigInteger getBalanceStatusId() {
        return balanceStatusId;
    }


    /**
     * Sets the balanceStatusId value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @param balanceStatusId
     */
    public void setBalanceStatusId(java.math.BigInteger balanceStatusId) {
        this.balanceStatusId = balanceStatusId;
    }


    /**
     * Gets the balanceStatus value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @return balanceStatus
     */
    public java.lang.String getBalanceStatus() {
        return balanceStatus;
    }


    /**
     * Sets the balanceStatus value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @param balanceStatus
     */
    public void setBalanceStatus(java.lang.String balanceStatus) {
        this.balanceStatus = balanceStatus;
    }


    /**
     * Gets the subscriptionProductsFU value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @return subscriptionProductsFU
     */
    public com.addcel.ws.clientes.simfonics.PortalService.SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row[] getSubscriptionProductsFU() {
        return subscriptionProductsFU;
    }


    /**
     * Sets the subscriptionProductsFU value for this SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.
     * 
     * @param subscriptionProductsFU
     */
    public void setSubscriptionProductsFU(com.addcel.ws.clientes.simfonics.PortalService.SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row[] subscriptionProductsFU) {
        this.subscriptionProductsFU = subscriptionProductsFU;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row)) return false;
        SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row other = (SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.subscriptionStatusId==null && other.getSubscriptionStatusId()==null) || 
             (this.subscriptionStatusId!=null &&
              this.subscriptionStatusId.equals(other.getSubscriptionStatusId()))) &&
            ((this.subscriptionStatus==null && other.getSubscriptionStatus()==null) || 
             (this.subscriptionStatus!=null &&
              this.subscriptionStatus.equals(other.getSubscriptionStatus()))) &&
            ((this.msisdnId==null && other.getMsisdnId()==null) || 
             (this.msisdnId!=null &&
              this.msisdnId.equals(other.getMsisdnId()))) &&
            ((this.msisdn==null && other.getMsisdn()==null) || 
             (this.msisdn!=null &&
              this.msisdn.equals(other.getMsisdn()))) &&
            ((this.imsiId==null && other.getImsiId()==null) || 
             (this.imsiId!=null &&
              this.imsiId.equals(other.getImsiId()))) &&
            ((this.imsi==null && other.getImsi()==null) || 
             (this.imsi!=null &&
              this.imsi.equals(other.getImsi()))) &&
            ((this.iccId==null && other.getIccId()==null) || 
             (this.iccId!=null &&
              this.iccId.equals(other.getIccId()))) &&
            ((this.icc==null && other.getIcc()==null) || 
             (this.icc!=null &&
              this.icc.equals(other.getIcc()))) &&
            ((this.subscriptionType==null && other.getSubscriptionType()==null) || 
             (this.subscriptionType!=null &&
              this.subscriptionType.equals(other.getSubscriptionType()))) &&
            ((this.subscriptionTypeId==null && other.getSubscriptionTypeId()==null) || 
             (this.subscriptionTypeId!=null &&
              this.subscriptionTypeId.equals(other.getSubscriptionTypeId()))) &&
            ((this.balance==null && other.getBalance()==null) || 
             (this.balance!=null &&
              this.balance.equals(other.getBalance()))) &&
            ((this.balanceExpirationDate==null && other.getBalanceExpirationDate()==null) || 
             (this.balanceExpirationDate!=null &&
              this.balanceExpirationDate.equals(other.getBalanceExpirationDate()))) &&
            ((this.balanceStatusId==null && other.getBalanceStatusId()==null) || 
             (this.balanceStatusId!=null &&
              this.balanceStatusId.equals(other.getBalanceStatusId()))) &&
            ((this.balanceStatus==null && other.getBalanceStatus()==null) || 
             (this.balanceStatus!=null &&
              this.balanceStatus.equals(other.getBalanceStatus()))) &&
            ((this.subscriptionProductsFU==null && other.getSubscriptionProductsFU()==null) || 
             (this.subscriptionProductsFU!=null &&
              java.util.Arrays.equals(this.subscriptionProductsFU, other.getSubscriptionProductsFU())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSubscriptionStatusId() != null) {
            _hashCode += getSubscriptionStatusId().hashCode();
        }
        if (getSubscriptionStatus() != null) {
            _hashCode += getSubscriptionStatus().hashCode();
        }
        if (getMsisdnId() != null) {
            _hashCode += getMsisdnId().hashCode();
        }
        if (getMsisdn() != null) {
            _hashCode += getMsisdn().hashCode();
        }
        if (getImsiId() != null) {
            _hashCode += getImsiId().hashCode();
        }
        if (getImsi() != null) {
            _hashCode += getImsi().hashCode();
        }
        if (getIccId() != null) {
            _hashCode += getIccId().hashCode();
        }
        if (getIcc() != null) {
            _hashCode += getIcc().hashCode();
        }
        if (getSubscriptionType() != null) {
            _hashCode += getSubscriptionType().hashCode();
        }
        if (getSubscriptionTypeId() != null) {
            _hashCode += getSubscriptionTypeId().hashCode();
        }
        if (getBalance() != null) {
            _hashCode += getBalance().hashCode();
        }
        if (getBalanceExpirationDate() != null) {
            _hashCode += getBalanceExpirationDate().hashCode();
        }
        if (getBalanceStatusId() != null) {
            _hashCode += getBalanceStatusId().hashCode();
        }
        if (getBalanceStatus() != null) {
            _hashCode += getBalanceStatus().hashCode();
        }
        if (getSubscriptionProductsFU() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSubscriptionProductsFU());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSubscriptionProductsFU(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">SubscriptionInfoFUProducts_RowSet>SubscriptionInfoFUProducts_Row"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionStatusId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriptionStatusId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriptionStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msisdnId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msisdnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msisdn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msisdn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imsiId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "imsiId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imsi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "imsi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iccId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iccId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("icc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "icc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriptionType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriptionTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balance");
        elemField.setXmlName(new javax.xml.namespace.QName("", "balance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balanceExpirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "balanceExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balanceStatusId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "balanceStatusId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balanceStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "balanceStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionProductsFU");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriptionProductsFU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">subscriptionProductsFU_OUT_RowSet>subscriptionProductsFU_OUT_Row"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "subscriptionProductsFU_OUT_Row"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
