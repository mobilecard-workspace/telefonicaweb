/**
 * CheckUsernameSecurityAnswer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class CheckUsernameSecurityAnswer  implements java.io.Serializable {
    private java.lang.String userName;

    private java.lang.String securityQuestion;

    private java.lang.String securityAnswer;

    private java.lang.String country;

    private java.math.BigInteger brandId;

    public CheckUsernameSecurityAnswer() {
    }

    public CheckUsernameSecurityAnswer(
           java.lang.String userName,
           java.lang.String securityQuestion,
           java.lang.String securityAnswer,
           java.lang.String country,
           java.math.BigInteger brandId) {
           this.userName = userName;
           this.securityQuestion = securityQuestion;
           this.securityAnswer = securityAnswer;
           this.country = country;
           this.brandId = brandId;
    }


    /**
     * Gets the userName value for this CheckUsernameSecurityAnswer.
     * 
     * @return userName
     */
    public java.lang.String getUserName() {
        return userName;
    }


    /**
     * Sets the userName value for this CheckUsernameSecurityAnswer.
     * 
     * @param userName
     */
    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }


    /**
     * Gets the securityQuestion value for this CheckUsernameSecurityAnswer.
     * 
     * @return securityQuestion
     */
    public java.lang.String getSecurityQuestion() {
        return securityQuestion;
    }


    /**
     * Sets the securityQuestion value for this CheckUsernameSecurityAnswer.
     * 
     * @param securityQuestion
     */
    public void setSecurityQuestion(java.lang.String securityQuestion) {
        this.securityQuestion = securityQuestion;
    }


    /**
     * Gets the securityAnswer value for this CheckUsernameSecurityAnswer.
     * 
     * @return securityAnswer
     */
    public java.lang.String getSecurityAnswer() {
        return securityAnswer;
    }


    /**
     * Sets the securityAnswer value for this CheckUsernameSecurityAnswer.
     * 
     * @param securityAnswer
     */
    public void setSecurityAnswer(java.lang.String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }


    /**
     * Gets the country value for this CheckUsernameSecurityAnswer.
     * 
     * @return country
     */
    public java.lang.String getCountry() {
        return country;
    }


    /**
     * Sets the country value for this CheckUsernameSecurityAnswer.
     * 
     * @param country
     */
    public void setCountry(java.lang.String country) {
        this.country = country;
    }


    /**
     * Gets the brandId value for this CheckUsernameSecurityAnswer.
     * 
     * @return brandId
     */
    public java.math.BigInteger getBrandId() {
        return brandId;
    }


    /**
     * Sets the brandId value for this CheckUsernameSecurityAnswer.
     * 
     * @param brandId
     */
    public void setBrandId(java.math.BigInteger brandId) {
        this.brandId = brandId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CheckUsernameSecurityAnswer)) return false;
        CheckUsernameSecurityAnswer other = (CheckUsernameSecurityAnswer) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.userName==null && other.getUserName()==null) || 
             (this.userName!=null &&
              this.userName.equals(other.getUserName()))) &&
            ((this.securityQuestion==null && other.getSecurityQuestion()==null) || 
             (this.securityQuestion!=null &&
              this.securityQuestion.equals(other.getSecurityQuestion()))) &&
            ((this.securityAnswer==null && other.getSecurityAnswer()==null) || 
             (this.securityAnswer!=null &&
              this.securityAnswer.equals(other.getSecurityAnswer()))) &&
            ((this.country==null && other.getCountry()==null) || 
             (this.country!=null &&
              this.country.equals(other.getCountry()))) &&
            ((this.brandId==null && other.getBrandId()==null) || 
             (this.brandId!=null &&
              this.brandId.equals(other.getBrandId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUserName() != null) {
            _hashCode += getUserName().hashCode();
        }
        if (getSecurityQuestion() != null) {
            _hashCode += getSecurityQuestion().hashCode();
        }
        if (getSecurityAnswer() != null) {
            _hashCode += getSecurityAnswer().hashCode();
        }
        if (getCountry() != null) {
            _hashCode += getCountry().hashCode();
        }
        if (getBrandId() != null) {
            _hashCode += getBrandId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CheckUsernameSecurityAnswer.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">checkUsernameSecurityAnswer"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "userName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("securityQuestion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "securityQuestion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("securityAnswer");
        elemField.setXmlName(new javax.xml.namespace.QName("", "securityAnswer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country");
        elemField.setXmlName(new javax.xml.namespace.QName("", "country"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("brandId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "brandId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
