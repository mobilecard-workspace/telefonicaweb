/**
 * GetIncidentsTypeGetIncidents.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class GetIncidentsTypeGetIncidents  implements java.io.Serializable {
    private java.math.BigInteger id;

    private java.lang.String countryId;

    private java.util.Calendar creationDate;

    private java.lang.String description;

    private java.math.BigInteger statusId;

    private java.math.BigInteger priorityId;

    private java.lang.String typeName;

    private java.lang.String priorityName;

    private java.lang.String statusName;

    private java.lang.String summary;

    private java.math.BigInteger categoryId;

    private java.lang.String categoryName;

    private java.math.BigInteger subcategoryId;

    private java.lang.String subcategoryName;

    public GetIncidentsTypeGetIncidents() {
    }

    public GetIncidentsTypeGetIncidents(
           java.math.BigInteger id,
           java.lang.String countryId,
           java.util.Calendar creationDate,
           java.lang.String description,
           java.math.BigInteger statusId,
           java.math.BigInteger priorityId,
           java.lang.String typeName,
           java.lang.String priorityName,
           java.lang.String statusName,
           java.lang.String summary,
           java.math.BigInteger categoryId,
           java.lang.String categoryName,
           java.math.BigInteger subcategoryId,
           java.lang.String subcategoryName) {
           this.id = id;
           this.countryId = countryId;
           this.creationDate = creationDate;
           this.description = description;
           this.statusId = statusId;
           this.priorityId = priorityId;
           this.typeName = typeName;
           this.priorityName = priorityName;
           this.statusName = statusName;
           this.summary = summary;
           this.categoryId = categoryId;
           this.categoryName = categoryName;
           this.subcategoryId = subcategoryId;
           this.subcategoryName = subcategoryName;
    }


    /**
     * Gets the id value for this GetIncidentsTypeGetIncidents.
     * 
     * @return id
     */
    public java.math.BigInteger getId() {
        return id;
    }


    /**
     * Sets the id value for this GetIncidentsTypeGetIncidents.
     * 
     * @param id
     */
    public void setId(java.math.BigInteger id) {
        this.id = id;
    }


    /**
     * Gets the countryId value for this GetIncidentsTypeGetIncidents.
     * 
     * @return countryId
     */
    public java.lang.String getCountryId() {
        return countryId;
    }


    /**
     * Sets the countryId value for this GetIncidentsTypeGetIncidents.
     * 
     * @param countryId
     */
    public void setCountryId(java.lang.String countryId) {
        this.countryId = countryId;
    }


    /**
     * Gets the creationDate value for this GetIncidentsTypeGetIncidents.
     * 
     * @return creationDate
     */
    public java.util.Calendar getCreationDate() {
        return creationDate;
    }


    /**
     * Sets the creationDate value for this GetIncidentsTypeGetIncidents.
     * 
     * @param creationDate
     */
    public void setCreationDate(java.util.Calendar creationDate) {
        this.creationDate = creationDate;
    }


    /**
     * Gets the description value for this GetIncidentsTypeGetIncidents.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this GetIncidentsTypeGetIncidents.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the statusId value for this GetIncidentsTypeGetIncidents.
     * 
     * @return statusId
     */
    public java.math.BigInteger getStatusId() {
        return statusId;
    }


    /**
     * Sets the statusId value for this GetIncidentsTypeGetIncidents.
     * 
     * @param statusId
     */
    public void setStatusId(java.math.BigInteger statusId) {
        this.statusId = statusId;
    }


    /**
     * Gets the priorityId value for this GetIncidentsTypeGetIncidents.
     * 
     * @return priorityId
     */
    public java.math.BigInteger getPriorityId() {
        return priorityId;
    }


    /**
     * Sets the priorityId value for this GetIncidentsTypeGetIncidents.
     * 
     * @param priorityId
     */
    public void setPriorityId(java.math.BigInteger priorityId) {
        this.priorityId = priorityId;
    }


    /**
     * Gets the typeName value for this GetIncidentsTypeGetIncidents.
     * 
     * @return typeName
     */
    public java.lang.String getTypeName() {
        return typeName;
    }


    /**
     * Sets the typeName value for this GetIncidentsTypeGetIncidents.
     * 
     * @param typeName
     */
    public void setTypeName(java.lang.String typeName) {
        this.typeName = typeName;
    }


    /**
     * Gets the priorityName value for this GetIncidentsTypeGetIncidents.
     * 
     * @return priorityName
     */
    public java.lang.String getPriorityName() {
        return priorityName;
    }


    /**
     * Sets the priorityName value for this GetIncidentsTypeGetIncidents.
     * 
     * @param priorityName
     */
    public void setPriorityName(java.lang.String priorityName) {
        this.priorityName = priorityName;
    }


    /**
     * Gets the statusName value for this GetIncidentsTypeGetIncidents.
     * 
     * @return statusName
     */
    public java.lang.String getStatusName() {
        return statusName;
    }


    /**
     * Sets the statusName value for this GetIncidentsTypeGetIncidents.
     * 
     * @param statusName
     */
    public void setStatusName(java.lang.String statusName) {
        this.statusName = statusName;
    }


    /**
     * Gets the summary value for this GetIncidentsTypeGetIncidents.
     * 
     * @return summary
     */
    public java.lang.String getSummary() {
        return summary;
    }


    /**
     * Sets the summary value for this GetIncidentsTypeGetIncidents.
     * 
     * @param summary
     */
    public void setSummary(java.lang.String summary) {
        this.summary = summary;
    }


    /**
     * Gets the categoryId value for this GetIncidentsTypeGetIncidents.
     * 
     * @return categoryId
     */
    public java.math.BigInteger getCategoryId() {
        return categoryId;
    }


    /**
     * Sets the categoryId value for this GetIncidentsTypeGetIncidents.
     * 
     * @param categoryId
     */
    public void setCategoryId(java.math.BigInteger categoryId) {
        this.categoryId = categoryId;
    }


    /**
     * Gets the categoryName value for this GetIncidentsTypeGetIncidents.
     * 
     * @return categoryName
     */
    public java.lang.String getCategoryName() {
        return categoryName;
    }


    /**
     * Sets the categoryName value for this GetIncidentsTypeGetIncidents.
     * 
     * @param categoryName
     */
    public void setCategoryName(java.lang.String categoryName) {
        this.categoryName = categoryName;
    }


    /**
     * Gets the subcategoryId value for this GetIncidentsTypeGetIncidents.
     * 
     * @return subcategoryId
     */
    public java.math.BigInteger getSubcategoryId() {
        return subcategoryId;
    }


    /**
     * Sets the subcategoryId value for this GetIncidentsTypeGetIncidents.
     * 
     * @param subcategoryId
     */
    public void setSubcategoryId(java.math.BigInteger subcategoryId) {
        this.subcategoryId = subcategoryId;
    }


    /**
     * Gets the subcategoryName value for this GetIncidentsTypeGetIncidents.
     * 
     * @return subcategoryName
     */
    public java.lang.String getSubcategoryName() {
        return subcategoryName;
    }


    /**
     * Sets the subcategoryName value for this GetIncidentsTypeGetIncidents.
     * 
     * @param subcategoryName
     */
    public void setSubcategoryName(java.lang.String subcategoryName) {
        this.subcategoryName = subcategoryName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetIncidentsTypeGetIncidents)) return false;
        GetIncidentsTypeGetIncidents other = (GetIncidentsTypeGetIncidents) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.countryId==null && other.getCountryId()==null) || 
             (this.countryId!=null &&
              this.countryId.equals(other.getCountryId()))) &&
            ((this.creationDate==null && other.getCreationDate()==null) || 
             (this.creationDate!=null &&
              this.creationDate.equals(other.getCreationDate()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.statusId==null && other.getStatusId()==null) || 
             (this.statusId!=null &&
              this.statusId.equals(other.getStatusId()))) &&
            ((this.priorityId==null && other.getPriorityId()==null) || 
             (this.priorityId!=null &&
              this.priorityId.equals(other.getPriorityId()))) &&
            ((this.typeName==null && other.getTypeName()==null) || 
             (this.typeName!=null &&
              this.typeName.equals(other.getTypeName()))) &&
            ((this.priorityName==null && other.getPriorityName()==null) || 
             (this.priorityName!=null &&
              this.priorityName.equals(other.getPriorityName()))) &&
            ((this.statusName==null && other.getStatusName()==null) || 
             (this.statusName!=null &&
              this.statusName.equals(other.getStatusName()))) &&
            ((this.summary==null && other.getSummary()==null) || 
             (this.summary!=null &&
              this.summary.equals(other.getSummary()))) &&
            ((this.categoryId==null && other.getCategoryId()==null) || 
             (this.categoryId!=null &&
              this.categoryId.equals(other.getCategoryId()))) &&
            ((this.categoryName==null && other.getCategoryName()==null) || 
             (this.categoryName!=null &&
              this.categoryName.equals(other.getCategoryName()))) &&
            ((this.subcategoryId==null && other.getSubcategoryId()==null) || 
             (this.subcategoryId!=null &&
              this.subcategoryId.equals(other.getSubcategoryId()))) &&
            ((this.subcategoryName==null && other.getSubcategoryName()==null) || 
             (this.subcategoryName!=null &&
              this.subcategoryName.equals(other.getSubcategoryName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getCountryId() != null) {
            _hashCode += getCountryId().hashCode();
        }
        if (getCreationDate() != null) {
            _hashCode += getCreationDate().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getStatusId() != null) {
            _hashCode += getStatusId().hashCode();
        }
        if (getPriorityId() != null) {
            _hashCode += getPriorityId().hashCode();
        }
        if (getTypeName() != null) {
            _hashCode += getTypeName().hashCode();
        }
        if (getPriorityName() != null) {
            _hashCode += getPriorityName().hashCode();
        }
        if (getStatusName() != null) {
            _hashCode += getStatusName().hashCode();
        }
        if (getSummary() != null) {
            _hashCode += getSummary().hashCode();
        }
        if (getCategoryId() != null) {
            _hashCode += getCategoryId().hashCode();
        }
        if (getCategoryName() != null) {
            _hashCode += getCategoryName().hashCode();
        }
        if (getSubcategoryId() != null) {
            _hashCode += getSubcategoryId().hashCode();
        }
        if (getSubcategoryName() != null) {
            _hashCode += getSubcategoryName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetIncidentsTypeGetIncidents.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">getIncidentsType>getIncidents"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "countryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "creationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statusId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priorityId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "priorityId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "typeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priorityName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "priorityName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statusName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("summary");
        elemField.setXmlName(new javax.xml.namespace.QName("", "summary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoryId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "categoryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoryName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "categoryName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subcategoryId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subcategoryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subcategoryName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subcategoryName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
