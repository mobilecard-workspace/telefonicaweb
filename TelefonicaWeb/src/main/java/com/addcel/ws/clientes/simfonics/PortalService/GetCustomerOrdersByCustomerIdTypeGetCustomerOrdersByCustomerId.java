/**
 * GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId  implements java.io.Serializable {
    private java.math.BigInteger ID;

    private java.math.BigInteger CATALOGID;

    private java.math.BigInteger STATUS;

    private java.math.BigInteger CUSTOMERID;

    private java.util.Calendar STARTDATE;

    private java.util.Calendar ENDDATE;

    public GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId() {
    }

    public GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId(
           java.math.BigInteger ID,
           java.math.BigInteger CATALOGID,
           java.math.BigInteger STATUS,
           java.math.BigInteger CUSTOMERID,
           java.util.Calendar STARTDATE,
           java.util.Calendar ENDDATE) {
           this.ID = ID;
           this.CATALOGID = CATALOGID;
           this.STATUS = STATUS;
           this.CUSTOMERID = CUSTOMERID;
           this.STARTDATE = STARTDATE;
           this.ENDDATE = ENDDATE;
    }


    /**
     * Gets the ID value for this GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId.
     * 
     * @return ID
     */
    public java.math.BigInteger getID() {
        return ID;
    }


    /**
     * Sets the ID value for this GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId.
     * 
     * @param ID
     */
    public void setID(java.math.BigInteger ID) {
        this.ID = ID;
    }


    /**
     * Gets the CATALOGID value for this GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId.
     * 
     * @return CATALOGID
     */
    public java.math.BigInteger getCATALOGID() {
        return CATALOGID;
    }


    /**
     * Sets the CATALOGID value for this GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId.
     * 
     * @param CATALOGID
     */
    public void setCATALOGID(java.math.BigInteger CATALOGID) {
        this.CATALOGID = CATALOGID;
    }


    /**
     * Gets the STATUS value for this GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId.
     * 
     * @return STATUS
     */
    public java.math.BigInteger getSTATUS() {
        return STATUS;
    }


    /**
     * Sets the STATUS value for this GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId.
     * 
     * @param STATUS
     */
    public void setSTATUS(java.math.BigInteger STATUS) {
        this.STATUS = STATUS;
    }


    /**
     * Gets the CUSTOMERID value for this GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId.
     * 
     * @return CUSTOMERID
     */
    public java.math.BigInteger getCUSTOMERID() {
        return CUSTOMERID;
    }


    /**
     * Sets the CUSTOMERID value for this GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId.
     * 
     * @param CUSTOMERID
     */
    public void setCUSTOMERID(java.math.BigInteger CUSTOMERID) {
        this.CUSTOMERID = CUSTOMERID;
    }


    /**
     * Gets the STARTDATE value for this GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId.
     * 
     * @return STARTDATE
     */
    public java.util.Calendar getSTARTDATE() {
        return STARTDATE;
    }


    /**
     * Sets the STARTDATE value for this GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId.
     * 
     * @param STARTDATE
     */
    public void setSTARTDATE(java.util.Calendar STARTDATE) {
        this.STARTDATE = STARTDATE;
    }


    /**
     * Gets the ENDDATE value for this GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId.
     * 
     * @return ENDDATE
     */
    public java.util.Calendar getENDDATE() {
        return ENDDATE;
    }


    /**
     * Sets the ENDDATE value for this GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId.
     * 
     * @param ENDDATE
     */
    public void setENDDATE(java.util.Calendar ENDDATE) {
        this.ENDDATE = ENDDATE;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId)) return false;
        GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId other = (GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ID==null && other.getID()==null) || 
             (this.ID!=null &&
              this.ID.equals(other.getID()))) &&
            ((this.CATALOGID==null && other.getCATALOGID()==null) || 
             (this.CATALOGID!=null &&
              this.CATALOGID.equals(other.getCATALOGID()))) &&
            ((this.STATUS==null && other.getSTATUS()==null) || 
             (this.STATUS!=null &&
              this.STATUS.equals(other.getSTATUS()))) &&
            ((this.CUSTOMERID==null && other.getCUSTOMERID()==null) || 
             (this.CUSTOMERID!=null &&
              this.CUSTOMERID.equals(other.getCUSTOMERID()))) &&
            ((this.STARTDATE==null && other.getSTARTDATE()==null) || 
             (this.STARTDATE!=null &&
              this.STARTDATE.equals(other.getSTARTDATE()))) &&
            ((this.ENDDATE==null && other.getENDDATE()==null) || 
             (this.ENDDATE!=null &&
              this.ENDDATE.equals(other.getENDDATE())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getID() != null) {
            _hashCode += getID().hashCode();
        }
        if (getCATALOGID() != null) {
            _hashCode += getCATALOGID().hashCode();
        }
        if (getSTATUS() != null) {
            _hashCode += getSTATUS().hashCode();
        }
        if (getCUSTOMERID() != null) {
            _hashCode += getCUSTOMERID().hashCode();
        }
        if (getSTARTDATE() != null) {
            _hashCode += getSTARTDATE().hashCode();
        }
        if (getENDDATE() != null) {
            _hashCode += getENDDATE().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">getCustomerOrdersByCustomerIdType>getCustomerOrdersByCustomerId"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CATALOGID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CATALOGID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STATUS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "STATUS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMERID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CUSTOMERID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STARTDATE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "STARTDATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ENDDATE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ENDDATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
