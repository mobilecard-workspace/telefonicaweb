/**
 * LogisticPropertyType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class LogisticPropertyType  implements java.io.Serializable {
    private java.lang.String logisticPropertyKey;

    private java.lang.String logisticPropertyValue;

    public LogisticPropertyType() {
    }

    public LogisticPropertyType(
           java.lang.String logisticPropertyKey,
           java.lang.String logisticPropertyValue) {
           this.logisticPropertyKey = logisticPropertyKey;
           this.logisticPropertyValue = logisticPropertyValue;
    }


    /**
     * Gets the logisticPropertyKey value for this LogisticPropertyType.
     * 
     * @return logisticPropertyKey
     */
    public java.lang.String getLogisticPropertyKey() {
        return logisticPropertyKey;
    }


    /**
     * Sets the logisticPropertyKey value for this LogisticPropertyType.
     * 
     * @param logisticPropertyKey
     */
    public void setLogisticPropertyKey(java.lang.String logisticPropertyKey) {
        this.logisticPropertyKey = logisticPropertyKey;
    }


    /**
     * Gets the logisticPropertyValue value for this LogisticPropertyType.
     * 
     * @return logisticPropertyValue
     */
    public java.lang.String getLogisticPropertyValue() {
        return logisticPropertyValue;
    }


    /**
     * Sets the logisticPropertyValue value for this LogisticPropertyType.
     * 
     * @param logisticPropertyValue
     */
    public void setLogisticPropertyValue(java.lang.String logisticPropertyValue) {
        this.logisticPropertyValue = logisticPropertyValue;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LogisticPropertyType)) return false;
        LogisticPropertyType other = (LogisticPropertyType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.logisticPropertyKey==null && other.getLogisticPropertyKey()==null) || 
             (this.logisticPropertyKey!=null &&
              this.logisticPropertyKey.equals(other.getLogisticPropertyKey()))) &&
            ((this.logisticPropertyValue==null && other.getLogisticPropertyValue()==null) || 
             (this.logisticPropertyValue!=null &&
              this.logisticPropertyValue.equals(other.getLogisticPropertyValue())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLogisticPropertyKey() != null) {
            _hashCode += getLogisticPropertyKey().hashCode();
        }
        if (getLogisticPropertyValue() != null) {
            _hashCode += getLogisticPropertyValue().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LogisticPropertyType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", "logisticPropertyType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logisticPropertyKey");
        elemField.setXmlName(new javax.xml.namespace.QName("", "logisticPropertyKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logisticPropertyValue");
        elemField.setXmlName(new javax.xml.namespace.QName("", "logisticPropertyValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
