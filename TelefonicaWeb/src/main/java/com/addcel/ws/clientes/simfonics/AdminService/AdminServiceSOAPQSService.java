/**
 * AdminServiceSOAPQSService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public interface AdminServiceSOAPQSService extends javax.xml.rpc.Service {
    public java.lang.String getAdminServiceSOAPQSPortAddress();

    public com.addcel.ws.clientes.simfonics.AdminService.AdminService getAdminServiceSOAPQSPort() throws javax.xml.rpc.ServiceException;

    public com.addcel.ws.clientes.simfonics.AdminService.AdminService getAdminServiceSOAPQSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
