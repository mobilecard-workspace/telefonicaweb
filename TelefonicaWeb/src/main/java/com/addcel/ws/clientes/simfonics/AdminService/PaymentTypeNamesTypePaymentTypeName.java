/**
 * PaymentTypeNamesTypePaymentTypeName.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class PaymentTypeNamesTypePaymentTypeName  implements java.io.Serializable {
    private java.math.BigInteger paymentTypeId;

    private java.lang.String paymentTypeName;

    public PaymentTypeNamesTypePaymentTypeName() {
    }

    public PaymentTypeNamesTypePaymentTypeName(
           java.math.BigInteger paymentTypeId,
           java.lang.String paymentTypeName) {
           this.paymentTypeId = paymentTypeId;
           this.paymentTypeName = paymentTypeName;
    }


    /**
     * Gets the paymentTypeId value for this PaymentTypeNamesTypePaymentTypeName.
     * 
     * @return paymentTypeId
     */
    public java.math.BigInteger getPaymentTypeId() {
        return paymentTypeId;
    }


    /**
     * Sets the paymentTypeId value for this PaymentTypeNamesTypePaymentTypeName.
     * 
     * @param paymentTypeId
     */
    public void setPaymentTypeId(java.math.BigInteger paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }


    /**
     * Gets the paymentTypeName value for this PaymentTypeNamesTypePaymentTypeName.
     * 
     * @return paymentTypeName
     */
    public java.lang.String getPaymentTypeName() {
        return paymentTypeName;
    }


    /**
     * Sets the paymentTypeName value for this PaymentTypeNamesTypePaymentTypeName.
     * 
     * @param paymentTypeName
     */
    public void setPaymentTypeName(java.lang.String paymentTypeName) {
        this.paymentTypeName = paymentTypeName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaymentTypeNamesTypePaymentTypeName)) return false;
        PaymentTypeNamesTypePaymentTypeName other = (PaymentTypeNamesTypePaymentTypeName) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.paymentTypeId==null && other.getPaymentTypeId()==null) || 
             (this.paymentTypeId!=null &&
              this.paymentTypeId.equals(other.getPaymentTypeId()))) &&
            ((this.paymentTypeName==null && other.getPaymentTypeName()==null) || 
             (this.paymentTypeName!=null &&
              this.paymentTypeName.equals(other.getPaymentTypeName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPaymentTypeId() != null) {
            _hashCode += getPaymentTypeId().hashCode();
        }
        if (getPaymentTypeName() != null) {
            _hashCode += getPaymentTypeName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaymentTypeNamesTypePaymentTypeName.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">paymentTypeNamesType>paymentTypeName"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paymentTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paymentTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
