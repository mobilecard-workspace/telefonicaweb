/**
 * IncidentTypesTypeIncidentTypeHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService.holders;

public final class IncidentTypesTypeIncidentTypeHolder implements javax.xml.rpc.holders.Holder {
    public com.addcel.ws.clientes.simfonics.AdminService.IncidentTypesTypeIncidentType value;

    public IncidentTypesTypeIncidentTypeHolder() {
    }

    public IncidentTypesTypeIncidentTypeHolder(com.addcel.ws.clientes.simfonics.AdminService.IncidentTypesTypeIncidentType value) {
        this.value = value;
    }

}
