/**
 * PurchasePropertyType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class PurchasePropertyType  implements java.io.Serializable {
    private java.math.BigInteger purchasePropertyKey;

    private java.lang.String purchasePropertyValue;

    public PurchasePropertyType() {
    }

    public PurchasePropertyType(
           java.math.BigInteger purchasePropertyKey,
           java.lang.String purchasePropertyValue) {
           this.purchasePropertyKey = purchasePropertyKey;
           this.purchasePropertyValue = purchasePropertyValue;
    }


    /**
     * Gets the purchasePropertyKey value for this PurchasePropertyType.
     * 
     * @return purchasePropertyKey
     */
    public java.math.BigInteger getPurchasePropertyKey() {
        return purchasePropertyKey;
    }


    /**
     * Sets the purchasePropertyKey value for this PurchasePropertyType.
     * 
     * @param purchasePropertyKey
     */
    public void setPurchasePropertyKey(java.math.BigInteger purchasePropertyKey) {
        this.purchasePropertyKey = purchasePropertyKey;
    }


    /**
     * Gets the purchasePropertyValue value for this PurchasePropertyType.
     * 
     * @return purchasePropertyValue
     */
    public java.lang.String getPurchasePropertyValue() {
        return purchasePropertyValue;
    }


    /**
     * Sets the purchasePropertyValue value for this PurchasePropertyType.
     * 
     * @param purchasePropertyValue
     */
    public void setPurchasePropertyValue(java.lang.String purchasePropertyValue) {
        this.purchasePropertyValue = purchasePropertyValue;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PurchasePropertyType)) return false;
        PurchasePropertyType other = (PurchasePropertyType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.purchasePropertyKey==null && other.getPurchasePropertyKey()==null) || 
             (this.purchasePropertyKey!=null &&
              this.purchasePropertyKey.equals(other.getPurchasePropertyKey()))) &&
            ((this.purchasePropertyValue==null && other.getPurchasePropertyValue()==null) || 
             (this.purchasePropertyValue!=null &&
              this.purchasePropertyValue.equals(other.getPurchasePropertyValue())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPurchasePropertyKey() != null) {
            _hashCode += getPurchasePropertyKey().hashCode();
        }
        if (getPurchasePropertyValue() != null) {
            _hashCode += getPurchasePropertyValue().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PurchasePropertyType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", "purchasePropertyType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("purchasePropertyKey");
        elemField.setXmlName(new javax.xml.namespace.QName("", "purchasePropertyKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("purchasePropertyValue");
        elemField.setXmlName(new javax.xml.namespace.QName("", "purchasePropertyValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
