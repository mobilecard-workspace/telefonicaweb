/**
 * GetLogisticOperatorNamesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class GetLogisticOperatorNamesResponse  implements java.io.Serializable {
    private java.lang.String result;

    private java.lang.String responseMessage;

    private com.addcel.ws.clientes.simfonics.AdminService.LogisticOperatorNamesTypeLogisticOperator[] logisticOperatorNames;

    public GetLogisticOperatorNamesResponse() {
    }

    public GetLogisticOperatorNamesResponse(
           java.lang.String result,
           java.lang.String responseMessage,
           com.addcel.ws.clientes.simfonics.AdminService.LogisticOperatorNamesTypeLogisticOperator[] logisticOperatorNames) {
           this.result = result;
           this.responseMessage = responseMessage;
           this.logisticOperatorNames = logisticOperatorNames;
    }


    /**
     * Gets the result value for this GetLogisticOperatorNamesResponse.
     * 
     * @return result
     */
    public java.lang.String getResult() {
        return result;
    }


    /**
     * Sets the result value for this GetLogisticOperatorNamesResponse.
     * 
     * @param result
     */
    public void setResult(java.lang.String result) {
        this.result = result;
    }


    /**
     * Gets the responseMessage value for this GetLogisticOperatorNamesResponse.
     * 
     * @return responseMessage
     */
    public java.lang.String getResponseMessage() {
        return responseMessage;
    }


    /**
     * Sets the responseMessage value for this GetLogisticOperatorNamesResponse.
     * 
     * @param responseMessage
     */
    public void setResponseMessage(java.lang.String responseMessage) {
        this.responseMessage = responseMessage;
    }


    /**
     * Gets the logisticOperatorNames value for this GetLogisticOperatorNamesResponse.
     * 
     * @return logisticOperatorNames
     */
    public com.addcel.ws.clientes.simfonics.AdminService.LogisticOperatorNamesTypeLogisticOperator[] getLogisticOperatorNames() {
        return logisticOperatorNames;
    }


    /**
     * Sets the logisticOperatorNames value for this GetLogisticOperatorNamesResponse.
     * 
     * @param logisticOperatorNames
     */
    public void setLogisticOperatorNames(com.addcel.ws.clientes.simfonics.AdminService.LogisticOperatorNamesTypeLogisticOperator[] logisticOperatorNames) {
        this.logisticOperatorNames = logisticOperatorNames;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetLogisticOperatorNamesResponse)) return false;
        GetLogisticOperatorNamesResponse other = (GetLogisticOperatorNamesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.result==null && other.getResult()==null) || 
             (this.result!=null &&
              this.result.equals(other.getResult()))) &&
            ((this.responseMessage==null && other.getResponseMessage()==null) || 
             (this.responseMessage!=null &&
              this.responseMessage.equals(other.getResponseMessage()))) &&
            ((this.logisticOperatorNames==null && other.getLogisticOperatorNames()==null) || 
             (this.logisticOperatorNames!=null &&
              java.util.Arrays.equals(this.logisticOperatorNames, other.getLogisticOperatorNames())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResult() != null) {
            _hashCode += getResult().hashCode();
        }
        if (getResponseMessage() != null) {
            _hashCode += getResponseMessage().hashCode();
        }
        if (getLogisticOperatorNames() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLogisticOperatorNames());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLogisticOperatorNames(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetLogisticOperatorNamesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetLogisticOperatorNamesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result");
        elemField.setXmlName(new javax.xml.namespace.QName("", "result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "responseMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logisticOperatorNames");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LogisticOperatorNames"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">LogisticOperatorNamesType>LogisticOperator"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "LogisticOperator"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
