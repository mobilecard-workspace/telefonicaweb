/**
 * InterfaceTPSOAPStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.TopUpInterface;

import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.rpc.handler.HandlerInfo;
import javax.xml.rpc.handler.HandlerRegistry;

import com.addcel.ws.clientes.handler.SimpleHandler;

public class InterfaceTPSOAPStub extends org.apache.axis.client.Stub implements com.addcel.ws.clientes.simfonics.TopUpInterface.InterfaceTP {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[3];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Recharge");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", "RechargeRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", ">RechargeRequest"), com.addcel.ws.clientes.simfonics.TopUpInterface.RechargeRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", ">RechargeResponse"));
        oper.setReturnClass(com.addcel.ws.clientes.simfonics.TopUpInterface.RechargeResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", "RechargeResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Cancelation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", "CancelationRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", ">CancelationRequest"), com.addcel.ws.clientes.simfonics.TopUpInterface.CancelationRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", ">CancelationResponse"));
        oper.setReturnClass(com.addcel.ws.clientes.simfonics.TopUpInterface.CancelationResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", "CancelationResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Query");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", "QueryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", ">QueryRequest"), com.addcel.ws.clientes.simfonics.TopUpInterface.QueryRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", ">QueryResponse"));
        oper.setReturnClass(com.addcel.ws.clientes.simfonics.TopUpInterface.QueryResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", "QueryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

    }

    public InterfaceTPSOAPStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public InterfaceTPSOAPStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public InterfaceTPSOAPStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", ">CancelationRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.TopUpInterface.CancelationRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", ">CancelationResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.TopUpInterface.CancelationResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", ">QueryRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.TopUpInterface.QueryRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", ">QueryResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.TopUpInterface.QueryResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", ">RechargeRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.TopUpInterface.RechargeRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", ">RechargeResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.TopUpInterface.RechargeResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", "credentials");
            cachedSerQNames.add(qName);
            cls = com.addcel.ws.clientes.simfonics.TopUpInterface.Credentials.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);
            
            try{
              HandlerRegistry hr = service.getHandlerRegistry();

              QName  portName = new QName("http://www.simfonics.com/TopUpInterface", "InterfaceTPSOAPQSPort");

              List handlerChain = hr.getHandlerChain(portName);

              HandlerInfo hi = new HandlerInfo();
              hi.setHandlerClass(SimpleHandler.class);
              handlerChain.add(hi);
              
          }catch(Exception e) {
              e.printStackTrace();
          }

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.addcel.ws.clientes.simfonics.TopUpInterface.RechargeResponse recharge(com.addcel.ws.clientes.simfonics.TopUpInterface.RechargeRequest rechargeInput) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/TopUpInterface/Recharge");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "Recharge"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {rechargeInput});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.ws.clientes.simfonics.TopUpInterface.RechargeResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.ws.clientes.simfonics.TopUpInterface.RechargeResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.ws.clientes.simfonics.TopUpInterface.RechargeResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.ws.clientes.simfonics.TopUpInterface.CancelationResponse cancelation(com.addcel.ws.clientes.simfonics.TopUpInterface.CancelationRequest cancelationInput) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/TopUpInterface/Cancelation");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "Cancelation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {cancelationInput});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.ws.clientes.simfonics.TopUpInterface.CancelationResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.ws.clientes.simfonics.TopUpInterface.CancelationResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.ws.clientes.simfonics.TopUpInterface.CancelationResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.ws.clientes.simfonics.TopUpInterface.QueryResponse query(com.addcel.ws.clientes.simfonics.TopUpInterface.QueryRequest queryInput) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.simfonics.com/TopUpInterface/Query");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "Query"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {queryInput});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.ws.clientes.simfonics.TopUpInterface.QueryResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.ws.clientes.simfonics.TopUpInterface.QueryResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.ws.clientes.simfonics.TopUpInterface.QueryResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
