/**
 * InterfaceTP.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.TopUpInterface;

public interface InterfaceTP extends java.rmi.Remote {
    public com.addcel.ws.clientes.simfonics.TopUpInterface.RechargeResponse recharge(com.addcel.ws.clientes.simfonics.TopUpInterface.RechargeRequest rechargeInput) throws java.rmi.RemoteException;
    public com.addcel.ws.clientes.simfonics.TopUpInterface.CancelationResponse cancelation(com.addcel.ws.clientes.simfonics.TopUpInterface.CancelationRequest cancelationInput) throws java.rmi.RemoteException;
    public com.addcel.ws.clientes.simfonics.TopUpInterface.QueryResponse query(com.addcel.ws.clientes.simfonics.TopUpInterface.QueryRequest queryInput) throws java.rmi.RemoteException;
}
