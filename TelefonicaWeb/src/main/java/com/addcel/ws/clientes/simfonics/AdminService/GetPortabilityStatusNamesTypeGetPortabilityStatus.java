/**
 * GetPortabilityStatusNamesTypeGetPortabilityStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class GetPortabilityStatusNamesTypeGetPortabilityStatus  implements java.io.Serializable {
    private java.math.BigDecimal getPortabilityStatusId;

    private java.lang.String getPortabilityStatusName;

    public GetPortabilityStatusNamesTypeGetPortabilityStatus() {
    }

    public GetPortabilityStatusNamesTypeGetPortabilityStatus(
           java.math.BigDecimal getPortabilityStatusId,
           java.lang.String getPortabilityStatusName) {
           this.getPortabilityStatusId = getPortabilityStatusId;
           this.getPortabilityStatusName = getPortabilityStatusName;
    }


    /**
     * Gets the getPortabilityStatusId value for this GetPortabilityStatusNamesTypeGetPortabilityStatus.
     * 
     * @return getPortabilityStatusId
     */
    public java.math.BigDecimal getGetPortabilityStatusId() {
        return getPortabilityStatusId;
    }


    /**
     * Sets the getPortabilityStatusId value for this GetPortabilityStatusNamesTypeGetPortabilityStatus.
     * 
     * @param getPortabilityStatusId
     */
    public void setGetPortabilityStatusId(java.math.BigDecimal getPortabilityStatusId) {
        this.getPortabilityStatusId = getPortabilityStatusId;
    }


    /**
     * Gets the getPortabilityStatusName value for this GetPortabilityStatusNamesTypeGetPortabilityStatus.
     * 
     * @return getPortabilityStatusName
     */
    public java.lang.String getGetPortabilityStatusName() {
        return getPortabilityStatusName;
    }


    /**
     * Sets the getPortabilityStatusName value for this GetPortabilityStatusNamesTypeGetPortabilityStatus.
     * 
     * @param getPortabilityStatusName
     */
    public void setGetPortabilityStatusName(java.lang.String getPortabilityStatusName) {
        this.getPortabilityStatusName = getPortabilityStatusName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPortabilityStatusNamesTypeGetPortabilityStatus)) return false;
        GetPortabilityStatusNamesTypeGetPortabilityStatus other = (GetPortabilityStatusNamesTypeGetPortabilityStatus) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getPortabilityStatusId==null && other.getGetPortabilityStatusId()==null) || 
             (this.getPortabilityStatusId!=null &&
              this.getPortabilityStatusId.equals(other.getGetPortabilityStatusId()))) &&
            ((this.getPortabilityStatusName==null && other.getGetPortabilityStatusName()==null) || 
             (this.getPortabilityStatusName!=null &&
              this.getPortabilityStatusName.equals(other.getGetPortabilityStatusName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetPortabilityStatusId() != null) {
            _hashCode += getGetPortabilityStatusId().hashCode();
        }
        if (getGetPortabilityStatusName() != null) {
            _hashCode += getGetPortabilityStatusName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPortabilityStatusNamesTypeGetPortabilityStatus.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">GetPortabilityStatusNamesType>GetPortabilityStatus"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPortabilityStatusId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "GetPortabilityStatusId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPortabilityStatusName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "GetPortabilityStatusName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
