/**
 * SubscriptionInfoFUProducts_RowSetHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService.holders;

public final class SubscriptionInfoFUProducts_RowSetHolder implements javax.xml.rpc.holders.Holder {
    public com.addcel.ws.clientes.simfonics.PortalService.SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row[] value;

    public SubscriptionInfoFUProducts_RowSetHolder() {
    }

    public SubscriptionInfoFUProducts_RowSetHolder(com.addcel.ws.clientes.simfonics.PortalService.SubscriptionInfoFUProducts_RowSetSubscriptionInfoFUProducts_Row[] value) {
        this.value = value;
    }

}
