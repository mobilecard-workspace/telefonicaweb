/**
 * BrandPropertiesTypeBrandProperty.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class BrandPropertiesTypeBrandProperty  implements java.io.Serializable {
    private java.math.BigInteger propertyId;

    private java.lang.String propertyName;

    private java.math.BigInteger propertyTypeId;

    private java.lang.String propertyTypeName;

    public BrandPropertiesTypeBrandProperty() {
    }

    public BrandPropertiesTypeBrandProperty(
           java.math.BigInteger propertyId,
           java.lang.String propertyName,
           java.math.BigInteger propertyTypeId,
           java.lang.String propertyTypeName) {
           this.propertyId = propertyId;
           this.propertyName = propertyName;
           this.propertyTypeId = propertyTypeId;
           this.propertyTypeName = propertyTypeName;
    }


    /**
     * Gets the propertyId value for this BrandPropertiesTypeBrandProperty.
     * 
     * @return propertyId
     */
    public java.math.BigInteger getPropertyId() {
        return propertyId;
    }


    /**
     * Sets the propertyId value for this BrandPropertiesTypeBrandProperty.
     * 
     * @param propertyId
     */
    public void setPropertyId(java.math.BigInteger propertyId) {
        this.propertyId = propertyId;
    }


    /**
     * Gets the propertyName value for this BrandPropertiesTypeBrandProperty.
     * 
     * @return propertyName
     */
    public java.lang.String getPropertyName() {
        return propertyName;
    }


    /**
     * Sets the propertyName value for this BrandPropertiesTypeBrandProperty.
     * 
     * @param propertyName
     */
    public void setPropertyName(java.lang.String propertyName) {
        this.propertyName = propertyName;
    }


    /**
     * Gets the propertyTypeId value for this BrandPropertiesTypeBrandProperty.
     * 
     * @return propertyTypeId
     */
    public java.math.BigInteger getPropertyTypeId() {
        return propertyTypeId;
    }


    /**
     * Sets the propertyTypeId value for this BrandPropertiesTypeBrandProperty.
     * 
     * @param propertyTypeId
     */
    public void setPropertyTypeId(java.math.BigInteger propertyTypeId) {
        this.propertyTypeId = propertyTypeId;
    }


    /**
     * Gets the propertyTypeName value for this BrandPropertiesTypeBrandProperty.
     * 
     * @return propertyTypeName
     */
    public java.lang.String getPropertyTypeName() {
        return propertyTypeName;
    }


    /**
     * Sets the propertyTypeName value for this BrandPropertiesTypeBrandProperty.
     * 
     * @param propertyTypeName
     */
    public void setPropertyTypeName(java.lang.String propertyTypeName) {
        this.propertyTypeName = propertyTypeName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BrandPropertiesTypeBrandProperty)) return false;
        BrandPropertiesTypeBrandProperty other = (BrandPropertiesTypeBrandProperty) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.propertyId==null && other.getPropertyId()==null) || 
             (this.propertyId!=null &&
              this.propertyId.equals(other.getPropertyId()))) &&
            ((this.propertyName==null && other.getPropertyName()==null) || 
             (this.propertyName!=null &&
              this.propertyName.equals(other.getPropertyName()))) &&
            ((this.propertyTypeId==null && other.getPropertyTypeId()==null) || 
             (this.propertyTypeId!=null &&
              this.propertyTypeId.equals(other.getPropertyTypeId()))) &&
            ((this.propertyTypeName==null && other.getPropertyTypeName()==null) || 
             (this.propertyTypeName!=null &&
              this.propertyTypeName.equals(other.getPropertyTypeName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPropertyId() != null) {
            _hashCode += getPropertyId().hashCode();
        }
        if (getPropertyName() != null) {
            _hashCode += getPropertyName().hashCode();
        }
        if (getPropertyTypeId() != null) {
            _hashCode += getPropertyTypeId().hashCode();
        }
        if (getPropertyTypeName() != null) {
            _hashCode += getPropertyTypeName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BrandPropertiesTypeBrandProperty.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">brandPropertiesType>brandProperty"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "propertyId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "propertyName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "propertyTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "propertyTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
