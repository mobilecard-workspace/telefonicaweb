/**
 * OrdersCatalogTypeOrder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class OrdersCatalogTypeOrder  implements java.io.Serializable {
    private java.math.BigInteger id;

    private java.math.BigInteger typeId;

    private java.math.BigInteger categoryId;

    private java.math.BigInteger subCategoryId;

    private java.math.BigInteger billingTypeId;

    public OrdersCatalogTypeOrder() {
    }

    public OrdersCatalogTypeOrder(
           java.math.BigInteger id,
           java.math.BigInteger typeId,
           java.math.BigInteger categoryId,
           java.math.BigInteger subCategoryId,
           java.math.BigInteger billingTypeId) {
           this.id = id;
           this.typeId = typeId;
           this.categoryId = categoryId;
           this.subCategoryId = subCategoryId;
           this.billingTypeId = billingTypeId;
    }


    /**
     * Gets the id value for this OrdersCatalogTypeOrder.
     * 
     * @return id
     */
    public java.math.BigInteger getId() {
        return id;
    }


    /**
     * Sets the id value for this OrdersCatalogTypeOrder.
     * 
     * @param id
     */
    public void setId(java.math.BigInteger id) {
        this.id = id;
    }


    /**
     * Gets the typeId value for this OrdersCatalogTypeOrder.
     * 
     * @return typeId
     */
    public java.math.BigInteger getTypeId() {
        return typeId;
    }


    /**
     * Sets the typeId value for this OrdersCatalogTypeOrder.
     * 
     * @param typeId
     */
    public void setTypeId(java.math.BigInteger typeId) {
        this.typeId = typeId;
    }


    /**
     * Gets the categoryId value for this OrdersCatalogTypeOrder.
     * 
     * @return categoryId
     */
    public java.math.BigInteger getCategoryId() {
        return categoryId;
    }


    /**
     * Sets the categoryId value for this OrdersCatalogTypeOrder.
     * 
     * @param categoryId
     */
    public void setCategoryId(java.math.BigInteger categoryId) {
        this.categoryId = categoryId;
    }


    /**
     * Gets the subCategoryId value for this OrdersCatalogTypeOrder.
     * 
     * @return subCategoryId
     */
    public java.math.BigInteger getSubCategoryId() {
        return subCategoryId;
    }


    /**
     * Sets the subCategoryId value for this OrdersCatalogTypeOrder.
     * 
     * @param subCategoryId
     */
    public void setSubCategoryId(java.math.BigInteger subCategoryId) {
        this.subCategoryId = subCategoryId;
    }


    /**
     * Gets the billingTypeId value for this OrdersCatalogTypeOrder.
     * 
     * @return billingTypeId
     */
    public java.math.BigInteger getBillingTypeId() {
        return billingTypeId;
    }


    /**
     * Sets the billingTypeId value for this OrdersCatalogTypeOrder.
     * 
     * @param billingTypeId
     */
    public void setBillingTypeId(java.math.BigInteger billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrdersCatalogTypeOrder)) return false;
        OrdersCatalogTypeOrder other = (OrdersCatalogTypeOrder) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.typeId==null && other.getTypeId()==null) || 
             (this.typeId!=null &&
              this.typeId.equals(other.getTypeId()))) &&
            ((this.categoryId==null && other.getCategoryId()==null) || 
             (this.categoryId!=null &&
              this.categoryId.equals(other.getCategoryId()))) &&
            ((this.subCategoryId==null && other.getSubCategoryId()==null) || 
             (this.subCategoryId!=null &&
              this.subCategoryId.equals(other.getSubCategoryId()))) &&
            ((this.billingTypeId==null && other.getBillingTypeId()==null) || 
             (this.billingTypeId!=null &&
              this.billingTypeId.equals(other.getBillingTypeId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getTypeId() != null) {
            _hashCode += getTypeId().hashCode();
        }
        if (getCategoryId() != null) {
            _hashCode += getCategoryId().hashCode();
        }
        if (getSubCategoryId() != null) {
            _hashCode += getSubCategoryId().hashCode();
        }
        if (getBillingTypeId() != null) {
            _hashCode += getBillingTypeId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrdersCatalogTypeOrder.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">ordersCatalogType>order"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "typeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoryId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "categoryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subCategoryId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subCategoryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billingTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "billingTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
