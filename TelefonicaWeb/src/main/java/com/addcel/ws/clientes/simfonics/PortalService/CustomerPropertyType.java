/**
 * CustomerPropertyType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class CustomerPropertyType  implements java.io.Serializable {
    private java.math.BigInteger customerPropertyType;

    private com.addcel.ws.clientes.simfonics.PortalService.KeyValue[] keyValues;

    public CustomerPropertyType() {
    }

    public CustomerPropertyType(
           java.math.BigInteger customerPropertyType,
           com.addcel.ws.clientes.simfonics.PortalService.KeyValue[] keyValues) {
           this.customerPropertyType = customerPropertyType;
           this.keyValues = keyValues;
    }


    /**
     * Gets the customerPropertyType value for this CustomerPropertyType.
     * 
     * @return customerPropertyType
     */
    public java.math.BigInteger getCustomerPropertyType() {
        return customerPropertyType;
    }


    /**
     * Sets the customerPropertyType value for this CustomerPropertyType.
     * 
     * @param customerPropertyType
     */
    public void setCustomerPropertyType(java.math.BigInteger customerPropertyType) {
        this.customerPropertyType = customerPropertyType;
    }


    /**
     * Gets the keyValues value for this CustomerPropertyType.
     * 
     * @return keyValues
     */
    public com.addcel.ws.clientes.simfonics.PortalService.KeyValue[] getKeyValues() {
        return keyValues;
    }


    /**
     * Sets the keyValues value for this CustomerPropertyType.
     * 
     * @param keyValues
     */
    public void setKeyValues(com.addcel.ws.clientes.simfonics.PortalService.KeyValue[] keyValues) {
        this.keyValues = keyValues;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerPropertyType)) return false;
        CustomerPropertyType other = (CustomerPropertyType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.customerPropertyType==null && other.getCustomerPropertyType()==null) || 
             (this.customerPropertyType!=null &&
              this.customerPropertyType.equals(other.getCustomerPropertyType()))) &&
            ((this.keyValues==null && other.getKeyValues()==null) || 
             (this.keyValues!=null &&
              java.util.Arrays.equals(this.keyValues, other.getKeyValues())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCustomerPropertyType() != null) {
            _hashCode += getCustomerPropertyType().hashCode();
        }
        if (getKeyValues() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getKeyValues());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getKeyValues(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerPropertyType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", "CustomerPropertyType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerPropertyType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customerPropertyType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("keyValues");
        elemField.setXmlName(new javax.xml.namespace.QName("", "keyValues"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", "keyValue"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "keyValue"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
