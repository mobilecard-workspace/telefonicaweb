/**
 * RecurrencyTypeNamesRecurrencyTypeName.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class RecurrencyTypeNamesRecurrencyTypeName  implements java.io.Serializable {
    private java.math.BigInteger id;

    private java.lang.String recurrencyTypeName;

    public RecurrencyTypeNamesRecurrencyTypeName() {
    }

    public RecurrencyTypeNamesRecurrencyTypeName(
           java.math.BigInteger id,
           java.lang.String recurrencyTypeName) {
           this.id = id;
           this.recurrencyTypeName = recurrencyTypeName;
    }


    /**
     * Gets the id value for this RecurrencyTypeNamesRecurrencyTypeName.
     * 
     * @return id
     */
    public java.math.BigInteger getId() {
        return id;
    }


    /**
     * Sets the id value for this RecurrencyTypeNamesRecurrencyTypeName.
     * 
     * @param id
     */
    public void setId(java.math.BigInteger id) {
        this.id = id;
    }


    /**
     * Gets the recurrencyTypeName value for this RecurrencyTypeNamesRecurrencyTypeName.
     * 
     * @return recurrencyTypeName
     */
    public java.lang.String getRecurrencyTypeName() {
        return recurrencyTypeName;
    }


    /**
     * Sets the recurrencyTypeName value for this RecurrencyTypeNamesRecurrencyTypeName.
     * 
     * @param recurrencyTypeName
     */
    public void setRecurrencyTypeName(java.lang.String recurrencyTypeName) {
        this.recurrencyTypeName = recurrencyTypeName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RecurrencyTypeNamesRecurrencyTypeName)) return false;
        RecurrencyTypeNamesRecurrencyTypeName other = (RecurrencyTypeNamesRecurrencyTypeName) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.recurrencyTypeName==null && other.getRecurrencyTypeName()==null) || 
             (this.recurrencyTypeName!=null &&
              this.recurrencyTypeName.equals(other.getRecurrencyTypeName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getRecurrencyTypeName() != null) {
            _hashCode += getRecurrencyTypeName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RecurrencyTypeNamesRecurrencyTypeName.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">recurrencyTypeNames>recurrencyTypeName"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recurrencyTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recurrencyTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
