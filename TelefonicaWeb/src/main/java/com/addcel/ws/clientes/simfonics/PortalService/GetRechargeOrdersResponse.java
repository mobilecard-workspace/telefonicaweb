/**
 * GetRechargeOrdersResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class GetRechargeOrdersResponse  implements java.io.Serializable {
    private java.lang.String result;

    private java.lang.String resultMessage;

    private com.addcel.ws.clientes.simfonics.PortalService.GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row[] resultGetRechargeOrders;

    public GetRechargeOrdersResponse() {
    }

    public GetRechargeOrdersResponse(
           java.lang.String result,
           java.lang.String resultMessage,
           com.addcel.ws.clientes.simfonics.PortalService.GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row[] resultGetRechargeOrders) {
           this.result = result;
           this.resultMessage = resultMessage;
           this.resultGetRechargeOrders = resultGetRechargeOrders;
    }


    /**
     * Gets the result value for this GetRechargeOrdersResponse.
     * 
     * @return result
     */
    public java.lang.String getResult() {
        return result;
    }


    /**
     * Sets the result value for this GetRechargeOrdersResponse.
     * 
     * @param result
     */
    public void setResult(java.lang.String result) {
        this.result = result;
    }


    /**
     * Gets the resultMessage value for this GetRechargeOrdersResponse.
     * 
     * @return resultMessage
     */
    public java.lang.String getResultMessage() {
        return resultMessage;
    }


    /**
     * Sets the resultMessage value for this GetRechargeOrdersResponse.
     * 
     * @param resultMessage
     */
    public void setResultMessage(java.lang.String resultMessage) {
        this.resultMessage = resultMessage;
    }


    /**
     * Gets the resultGetRechargeOrders value for this GetRechargeOrdersResponse.
     * 
     * @return resultGetRechargeOrders
     */
    public com.addcel.ws.clientes.simfonics.PortalService.GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row[] getResultGetRechargeOrders() {
        return resultGetRechargeOrders;
    }


    /**
     * Sets the resultGetRechargeOrders value for this GetRechargeOrdersResponse.
     * 
     * @param resultGetRechargeOrders
     */
    public void setResultGetRechargeOrders(com.addcel.ws.clientes.simfonics.PortalService.GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row[] resultGetRechargeOrders) {
        this.resultGetRechargeOrders = resultGetRechargeOrders;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetRechargeOrdersResponse)) return false;
        GetRechargeOrdersResponse other = (GetRechargeOrdersResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.result==null && other.getResult()==null) || 
             (this.result!=null &&
              this.result.equals(other.getResult()))) &&
            ((this.resultMessage==null && other.getResultMessage()==null) || 
             (this.resultMessage!=null &&
              this.resultMessage.equals(other.getResultMessage()))) &&
            ((this.resultGetRechargeOrders==null && other.getResultGetRechargeOrders()==null) || 
             (this.resultGetRechargeOrders!=null &&
              java.util.Arrays.equals(this.resultGetRechargeOrders, other.getResultGetRechargeOrders())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResult() != null) {
            _hashCode += getResult().hashCode();
        }
        if (getResultMessage() != null) {
            _hashCode += getResultMessage().hashCode();
        }
        if (getResultGetRechargeOrders() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResultGetRechargeOrders());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResultGetRechargeOrders(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetRechargeOrdersResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">getRechargeOrdersResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result");
        elemField.setXmlName(new javax.xml.namespace.QName("", "result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultGetRechargeOrders");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultGetRechargeOrders"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">GETRECHARGEORDERS_OUT_RowSet>GETRECHARGEORDERS_OUT_Row"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "GETRECHARGEORDERS_OUT_Row"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
