/**
 * GetLogisticOrderResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class GetLogisticOrderResponse  implements java.io.Serializable {
    private java.lang.String result;

    private java.lang.String resultMessage;

    private java.math.BigInteger logisticId;

    private java.lang.String trackingId;

    private java.math.BigInteger operatorId;

    private java.lang.String operatorName;

    private com.addcel.ws.clientes.simfonics.PortalService.LogisticPropertyType[] logisticProperties;

    public GetLogisticOrderResponse() {
    }

    public GetLogisticOrderResponse(
           java.lang.String result,
           java.lang.String resultMessage,
           java.math.BigInteger logisticId,
           java.lang.String trackingId,
           java.math.BigInteger operatorId,
           java.lang.String operatorName,
           com.addcel.ws.clientes.simfonics.PortalService.LogisticPropertyType[] logisticProperties) {
           this.result = result;
           this.resultMessage = resultMessage;
           this.logisticId = logisticId;
           this.trackingId = trackingId;
           this.operatorId = operatorId;
           this.operatorName = operatorName;
           this.logisticProperties = logisticProperties;
    }


    /**
     * Gets the result value for this GetLogisticOrderResponse.
     * 
     * @return result
     */
    public java.lang.String getResult() {
        return result;
    }


    /**
     * Sets the result value for this GetLogisticOrderResponse.
     * 
     * @param result
     */
    public void setResult(java.lang.String result) {
        this.result = result;
    }


    /**
     * Gets the resultMessage value for this GetLogisticOrderResponse.
     * 
     * @return resultMessage
     */
    public java.lang.String getResultMessage() {
        return resultMessage;
    }


    /**
     * Sets the resultMessage value for this GetLogisticOrderResponse.
     * 
     * @param resultMessage
     */
    public void setResultMessage(java.lang.String resultMessage) {
        this.resultMessage = resultMessage;
    }


    /**
     * Gets the logisticId value for this GetLogisticOrderResponse.
     * 
     * @return logisticId
     */
    public java.math.BigInteger getLogisticId() {
        return logisticId;
    }


    /**
     * Sets the logisticId value for this GetLogisticOrderResponse.
     * 
     * @param logisticId
     */
    public void setLogisticId(java.math.BigInteger logisticId) {
        this.logisticId = logisticId;
    }


    /**
     * Gets the trackingId value for this GetLogisticOrderResponse.
     * 
     * @return trackingId
     */
    public java.lang.String getTrackingId() {
        return trackingId;
    }


    /**
     * Sets the trackingId value for this GetLogisticOrderResponse.
     * 
     * @param trackingId
     */
    public void setTrackingId(java.lang.String trackingId) {
        this.trackingId = trackingId;
    }


    /**
     * Gets the operatorId value for this GetLogisticOrderResponse.
     * 
     * @return operatorId
     */
    public java.math.BigInteger getOperatorId() {
        return operatorId;
    }


    /**
     * Sets the operatorId value for this GetLogisticOrderResponse.
     * 
     * @param operatorId
     */
    public void setOperatorId(java.math.BigInteger operatorId) {
        this.operatorId = operatorId;
    }


    /**
     * Gets the operatorName value for this GetLogisticOrderResponse.
     * 
     * @return operatorName
     */
    public java.lang.String getOperatorName() {
        return operatorName;
    }


    /**
     * Sets the operatorName value for this GetLogisticOrderResponse.
     * 
     * @param operatorName
     */
    public void setOperatorName(java.lang.String operatorName) {
        this.operatorName = operatorName;
    }


    /**
     * Gets the logisticProperties value for this GetLogisticOrderResponse.
     * 
     * @return logisticProperties
     */
    public com.addcel.ws.clientes.simfonics.PortalService.LogisticPropertyType[] getLogisticProperties() {
        return logisticProperties;
    }


    /**
     * Sets the logisticProperties value for this GetLogisticOrderResponse.
     * 
     * @param logisticProperties
     */
    public void setLogisticProperties(com.addcel.ws.clientes.simfonics.PortalService.LogisticPropertyType[] logisticProperties) {
        this.logisticProperties = logisticProperties;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetLogisticOrderResponse)) return false;
        GetLogisticOrderResponse other = (GetLogisticOrderResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.result==null && other.getResult()==null) || 
             (this.result!=null &&
              this.result.equals(other.getResult()))) &&
            ((this.resultMessage==null && other.getResultMessage()==null) || 
             (this.resultMessage!=null &&
              this.resultMessage.equals(other.getResultMessage()))) &&
            ((this.logisticId==null && other.getLogisticId()==null) || 
             (this.logisticId!=null &&
              this.logisticId.equals(other.getLogisticId()))) &&
            ((this.trackingId==null && other.getTrackingId()==null) || 
             (this.trackingId!=null &&
              this.trackingId.equals(other.getTrackingId()))) &&
            ((this.operatorId==null && other.getOperatorId()==null) || 
             (this.operatorId!=null &&
              this.operatorId.equals(other.getOperatorId()))) &&
            ((this.operatorName==null && other.getOperatorName()==null) || 
             (this.operatorName!=null &&
              this.operatorName.equals(other.getOperatorName()))) &&
            ((this.logisticProperties==null && other.getLogisticProperties()==null) || 
             (this.logisticProperties!=null &&
              java.util.Arrays.equals(this.logisticProperties, other.getLogisticProperties())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResult() != null) {
            _hashCode += getResult().hashCode();
        }
        if (getResultMessage() != null) {
            _hashCode += getResultMessage().hashCode();
        }
        if (getLogisticId() != null) {
            _hashCode += getLogisticId().hashCode();
        }
        if (getTrackingId() != null) {
            _hashCode += getTrackingId().hashCode();
        }
        if (getOperatorId() != null) {
            _hashCode += getOperatorId().hashCode();
        }
        if (getOperatorName() != null) {
            _hashCode += getOperatorName().hashCode();
        }
        if (getLogisticProperties() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLogisticProperties());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLogisticProperties(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetLogisticOrderResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">GetLogisticOrderResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result");
        elemField.setXmlName(new javax.xml.namespace.QName("", "result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logisticId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "logisticId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trackingId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "trackingId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operatorId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "operatorId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operatorName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "operatorName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logisticProperties");
        elemField.setXmlName(new javax.xml.namespace.QName("", "logisticProperties"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", "logisticPropertyType"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "logisticProperty"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
