/**
 * ProductFUPInfo_RowSetProductFUPInfo_RowHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService.holders;

public final class ProductFUPInfo_RowSetProductFUPInfo_RowHolder implements javax.xml.rpc.holders.Holder {
    public com.addcel.ws.clientes.simfonics.PortalService.ProductFUPInfo_RowSetProductFUPInfo_Row value;

    public ProductFUPInfo_RowSetProductFUPInfo_RowHolder() {
    }

    public ProductFUPInfo_RowSetProductFUPInfo_RowHolder(com.addcel.ws.clientes.simfonics.PortalService.ProductFUPInfo_RowSetProductFUPInfo_Row value) {
        this.value = value;
    }

}
