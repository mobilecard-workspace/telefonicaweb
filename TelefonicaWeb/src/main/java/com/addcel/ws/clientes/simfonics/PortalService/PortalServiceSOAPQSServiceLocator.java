/**
 * PortalServiceSOAPQSServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class PortalServiceSOAPQSServiceLocator extends org.apache.axis.client.Service implements com.addcel.ws.clientes.simfonics.PortalService.PortalServiceSOAPQSService {

    public PortalServiceSOAPQSServiceLocator() {
    }


    public PortalServiceSOAPQSServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PortalServiceSOAPQSServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for PortalServiceSOAPQSPort
    private java.lang.String PortalServiceSOAPQSPort_address = "http://192.168.45.30:7000/SelfCare/PortalService";

    public java.lang.String getPortalServiceSOAPQSPortAddress() {
        return PortalServiceSOAPQSPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PortalServiceSOAPQSPortWSDDServiceName = "PortalServiceSOAPQSPort";

    public java.lang.String getPortalServiceSOAPQSPortWSDDServiceName() {
        return PortalServiceSOAPQSPortWSDDServiceName;
    }

    public void setPortalServiceSOAPQSPortWSDDServiceName(java.lang.String name) {
        PortalServiceSOAPQSPortWSDDServiceName = name;
    }

    public com.addcel.ws.clientes.simfonics.PortalService.PortalService getPortalServiceSOAPQSPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PortalServiceSOAPQSPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPortalServiceSOAPQSPort(endpoint);
    }

    public com.addcel.ws.clientes.simfonics.PortalService.PortalService getPortalServiceSOAPQSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.addcel.ws.clientes.simfonics.PortalService.PortalServiceSOAPStub _stub = new com.addcel.ws.clientes.simfonics.PortalService.PortalServiceSOAPStub(portAddress, this);
            _stub.setPortName(getPortalServiceSOAPQSPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPortalServiceSOAPQSPortEndpointAddress(java.lang.String address) {
        PortalServiceSOAPQSPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.addcel.ws.clientes.simfonics.PortalService.PortalService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.addcel.ws.clientes.simfonics.PortalService.PortalServiceSOAPStub _stub = new com.addcel.ws.clientes.simfonics.PortalService.PortalServiceSOAPStub(new java.net.URL(PortalServiceSOAPQSPort_address), this);
                _stub.setPortName(getPortalServiceSOAPQSPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("PortalServiceSOAPQSPort".equals(inputPortName)) {
            return getPortalServiceSOAPQSPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", "PortalServiceSOAPQSService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", "PortalServiceSOAPQSPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("PortalServiceSOAPQSPort".equals(portName)) {
            setPortalServiceSOAPQSPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
