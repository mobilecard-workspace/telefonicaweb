/**
 * CustomerProperties_OUT_RowSetCustomerProperty.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class CustomerProperties_OUT_RowSetCustomerProperty  implements java.io.Serializable {
    private java.lang.String propertyTypeName;

    private java.lang.String value;

    private java.math.BigInteger propertyTypeId;

    private java.math.BigInteger propertyId;

    public CustomerProperties_OUT_RowSetCustomerProperty() {
    }

    public CustomerProperties_OUT_RowSetCustomerProperty(
           java.lang.String propertyTypeName,
           java.lang.String value,
           java.math.BigInteger propertyTypeId,
           java.math.BigInteger propertyId) {
           this.propertyTypeName = propertyTypeName;
           this.value = value;
           this.propertyTypeId = propertyTypeId;
           this.propertyId = propertyId;
    }


    /**
     * Gets the propertyTypeName value for this CustomerProperties_OUT_RowSetCustomerProperty.
     * 
     * @return propertyTypeName
     */
    public java.lang.String getPropertyTypeName() {
        return propertyTypeName;
    }


    /**
     * Sets the propertyTypeName value for this CustomerProperties_OUT_RowSetCustomerProperty.
     * 
     * @param propertyTypeName
     */
    public void setPropertyTypeName(java.lang.String propertyTypeName) {
        this.propertyTypeName = propertyTypeName;
    }


    /**
     * Gets the value value for this CustomerProperties_OUT_RowSetCustomerProperty.
     * 
     * @return value
     */
    public java.lang.String getValue() {
        return value;
    }


    /**
     * Sets the value value for this CustomerProperties_OUT_RowSetCustomerProperty.
     * 
     * @param value
     */
    public void setValue(java.lang.String value) {
        this.value = value;
    }


    /**
     * Gets the propertyTypeId value for this CustomerProperties_OUT_RowSetCustomerProperty.
     * 
     * @return propertyTypeId
     */
    public java.math.BigInteger getPropertyTypeId() {
        return propertyTypeId;
    }


    /**
     * Sets the propertyTypeId value for this CustomerProperties_OUT_RowSetCustomerProperty.
     * 
     * @param propertyTypeId
     */
    public void setPropertyTypeId(java.math.BigInteger propertyTypeId) {
        this.propertyTypeId = propertyTypeId;
    }


    /**
     * Gets the propertyId value for this CustomerProperties_OUT_RowSetCustomerProperty.
     * 
     * @return propertyId
     */
    public java.math.BigInteger getPropertyId() {
        return propertyId;
    }


    /**
     * Sets the propertyId value for this CustomerProperties_OUT_RowSetCustomerProperty.
     * 
     * @param propertyId
     */
    public void setPropertyId(java.math.BigInteger propertyId) {
        this.propertyId = propertyId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerProperties_OUT_RowSetCustomerProperty)) return false;
        CustomerProperties_OUT_RowSetCustomerProperty other = (CustomerProperties_OUT_RowSetCustomerProperty) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.propertyTypeName==null && other.getPropertyTypeName()==null) || 
             (this.propertyTypeName!=null &&
              this.propertyTypeName.equals(other.getPropertyTypeName()))) &&
            ((this.value==null && other.getValue()==null) || 
             (this.value!=null &&
              this.value.equals(other.getValue()))) &&
            ((this.propertyTypeId==null && other.getPropertyTypeId()==null) || 
             (this.propertyTypeId!=null &&
              this.propertyTypeId.equals(other.getPropertyTypeId()))) &&
            ((this.propertyId==null && other.getPropertyId()==null) || 
             (this.propertyId!=null &&
              this.propertyId.equals(other.getPropertyId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPropertyTypeName() != null) {
            _hashCode += getPropertyTypeName().hashCode();
        }
        if (getValue() != null) {
            _hashCode += getValue().hashCode();
        }
        if (getPropertyTypeId() != null) {
            _hashCode += getPropertyTypeId().hashCode();
        }
        if (getPropertyId() != null) {
            _hashCode += getPropertyId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerProperties_OUT_RowSetCustomerProperty.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">customerProperties_OUT_RowSet>customerProperty"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "propertyTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("value");
        elemField.setXmlName(new javax.xml.namespace.QName("", "value"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "propertyTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("propertyId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "propertyId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
