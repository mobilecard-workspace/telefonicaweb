/**
 * GetOrderItemsTypeOrderItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class GetOrderItemsTypeOrderItem  implements java.io.Serializable {
    private java.math.BigInteger itemTypeId;

    private java.lang.String itemName;

    private java.math.BigInteger itemParameterId;

    private java.lang.String itemParameterName;

    private java.lang.String value;

    public GetOrderItemsTypeOrderItem() {
    }

    public GetOrderItemsTypeOrderItem(
           java.math.BigInteger itemTypeId,
           java.lang.String itemName,
           java.math.BigInteger itemParameterId,
           java.lang.String itemParameterName,
           java.lang.String value) {
           this.itemTypeId = itemTypeId;
           this.itemName = itemName;
           this.itemParameterId = itemParameterId;
           this.itemParameterName = itemParameterName;
           this.value = value;
    }


    /**
     * Gets the itemTypeId value for this GetOrderItemsTypeOrderItem.
     * 
     * @return itemTypeId
     */
    public java.math.BigInteger getItemTypeId() {
        return itemTypeId;
    }


    /**
     * Sets the itemTypeId value for this GetOrderItemsTypeOrderItem.
     * 
     * @param itemTypeId
     */
    public void setItemTypeId(java.math.BigInteger itemTypeId) {
        this.itemTypeId = itemTypeId;
    }


    /**
     * Gets the itemName value for this GetOrderItemsTypeOrderItem.
     * 
     * @return itemName
     */
    public java.lang.String getItemName() {
        return itemName;
    }


    /**
     * Sets the itemName value for this GetOrderItemsTypeOrderItem.
     * 
     * @param itemName
     */
    public void setItemName(java.lang.String itemName) {
        this.itemName = itemName;
    }


    /**
     * Gets the itemParameterId value for this GetOrderItemsTypeOrderItem.
     * 
     * @return itemParameterId
     */
    public java.math.BigInteger getItemParameterId() {
        return itemParameterId;
    }


    /**
     * Sets the itemParameterId value for this GetOrderItemsTypeOrderItem.
     * 
     * @param itemParameterId
     */
    public void setItemParameterId(java.math.BigInteger itemParameterId) {
        this.itemParameterId = itemParameterId;
    }


    /**
     * Gets the itemParameterName value for this GetOrderItemsTypeOrderItem.
     * 
     * @return itemParameterName
     */
    public java.lang.String getItemParameterName() {
        return itemParameterName;
    }


    /**
     * Sets the itemParameterName value for this GetOrderItemsTypeOrderItem.
     * 
     * @param itemParameterName
     */
    public void setItemParameterName(java.lang.String itemParameterName) {
        this.itemParameterName = itemParameterName;
    }


    /**
     * Gets the value value for this GetOrderItemsTypeOrderItem.
     * 
     * @return value
     */
    public java.lang.String getValue() {
        return value;
    }


    /**
     * Sets the value value for this GetOrderItemsTypeOrderItem.
     * 
     * @param value
     */
    public void setValue(java.lang.String value) {
        this.value = value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetOrderItemsTypeOrderItem)) return false;
        GetOrderItemsTypeOrderItem other = (GetOrderItemsTypeOrderItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.itemTypeId==null && other.getItemTypeId()==null) || 
             (this.itemTypeId!=null &&
              this.itemTypeId.equals(other.getItemTypeId()))) &&
            ((this.itemName==null && other.getItemName()==null) || 
             (this.itemName!=null &&
              this.itemName.equals(other.getItemName()))) &&
            ((this.itemParameterId==null && other.getItemParameterId()==null) || 
             (this.itemParameterId!=null &&
              this.itemParameterId.equals(other.getItemParameterId()))) &&
            ((this.itemParameterName==null && other.getItemParameterName()==null) || 
             (this.itemParameterName!=null &&
              this.itemParameterName.equals(other.getItemParameterName()))) &&
            ((this.value==null && other.getValue()==null) || 
             (this.value!=null &&
              this.value.equals(other.getValue())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getItemTypeId() != null) {
            _hashCode += getItemTypeId().hashCode();
        }
        if (getItemName() != null) {
            _hashCode += getItemName().hashCode();
        }
        if (getItemParameterId() != null) {
            _hashCode += getItemParameterId().hashCode();
        }
        if (getItemParameterName() != null) {
            _hashCode += getItemParameterName().hashCode();
        }
        if (getValue() != null) {
            _hashCode += getValue().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetOrderItemsTypeOrderItem.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">getOrderItemsType>orderItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "itemTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "itemName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemParameterId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "itemParameterId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemParameterName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "itemParameterName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("value");
        elemField.setXmlName(new javax.xml.namespace.QName("", "value"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
