/**
 * SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row  implements java.io.Serializable {
    private java.math.BigInteger productFUId;

    private java.math.BigInteger productId;

    private java.lang.String productCode;

    private java.lang.String productName;

    private java.lang.String productDescription;

    private java.math.BigInteger productFUStatusId;

    private java.lang.String productFUStatus;

    private java.util.Calendar productFUStartTime;

    private java.util.Calendar productFUEndTime;

    private java.util.Calendar productFUExpirationDate;

    private com.addcel.ws.clientes.simfonics.PortalService.SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row[] subscriptionProductItemsFU;

    public SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row() {
    }

    public SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row(
           java.math.BigInteger productFUId,
           java.math.BigInteger productId,
           java.lang.String productCode,
           java.lang.String productName,
           java.lang.String productDescription,
           java.math.BigInteger productFUStatusId,
           java.lang.String productFUStatus,
           java.util.Calendar productFUStartTime,
           java.util.Calendar productFUEndTime,
           java.util.Calendar productFUExpirationDate,
           com.addcel.ws.clientes.simfonics.PortalService.SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row[] subscriptionProductItemsFU) {
           this.productFUId = productFUId;
           this.productId = productId;
           this.productCode = productCode;
           this.productName = productName;
           this.productDescription = productDescription;
           this.productFUStatusId = productFUStatusId;
           this.productFUStatus = productFUStatus;
           this.productFUStartTime = productFUStartTime;
           this.productFUEndTime = productFUEndTime;
           this.productFUExpirationDate = productFUExpirationDate;
           this.subscriptionProductItemsFU = subscriptionProductItemsFU;
    }


    /**
     * Gets the productFUId value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @return productFUId
     */
    public java.math.BigInteger getProductFUId() {
        return productFUId;
    }


    /**
     * Sets the productFUId value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @param productFUId
     */
    public void setProductFUId(java.math.BigInteger productFUId) {
        this.productFUId = productFUId;
    }


    /**
     * Gets the productId value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @return productId
     */
    public java.math.BigInteger getProductId() {
        return productId;
    }


    /**
     * Sets the productId value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @param productId
     */
    public void setProductId(java.math.BigInteger productId) {
        this.productId = productId;
    }


    /**
     * Gets the productCode value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @return productCode
     */
    public java.lang.String getProductCode() {
        return productCode;
    }


    /**
     * Sets the productCode value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @param productCode
     */
    public void setProductCode(java.lang.String productCode) {
        this.productCode = productCode;
    }


    /**
     * Gets the productName value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @return productName
     */
    public java.lang.String getProductName() {
        return productName;
    }


    /**
     * Sets the productName value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @param productName
     */
    public void setProductName(java.lang.String productName) {
        this.productName = productName;
    }


    /**
     * Gets the productDescription value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @return productDescription
     */
    public java.lang.String getProductDescription() {
        return productDescription;
    }


    /**
     * Sets the productDescription value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @param productDescription
     */
    public void setProductDescription(java.lang.String productDescription) {
        this.productDescription = productDescription;
    }


    /**
     * Gets the productFUStatusId value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @return productFUStatusId
     */
    public java.math.BigInteger getProductFUStatusId() {
        return productFUStatusId;
    }


    /**
     * Sets the productFUStatusId value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @param productFUStatusId
     */
    public void setProductFUStatusId(java.math.BigInteger productFUStatusId) {
        this.productFUStatusId = productFUStatusId;
    }


    /**
     * Gets the productFUStatus value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @return productFUStatus
     */
    public java.lang.String getProductFUStatus() {
        return productFUStatus;
    }


    /**
     * Sets the productFUStatus value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @param productFUStatus
     */
    public void setProductFUStatus(java.lang.String productFUStatus) {
        this.productFUStatus = productFUStatus;
    }


    /**
     * Gets the productFUStartTime value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @return productFUStartTime
     */
    public java.util.Calendar getProductFUStartTime() {
        return productFUStartTime;
    }


    /**
     * Sets the productFUStartTime value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @param productFUStartTime
     */
    public void setProductFUStartTime(java.util.Calendar productFUStartTime) {
        this.productFUStartTime = productFUStartTime;
    }


    /**
     * Gets the productFUEndTime value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @return productFUEndTime
     */
    public java.util.Calendar getProductFUEndTime() {
        return productFUEndTime;
    }


    /**
     * Sets the productFUEndTime value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @param productFUEndTime
     */
    public void setProductFUEndTime(java.util.Calendar productFUEndTime) {
        this.productFUEndTime = productFUEndTime;
    }


    /**
     * Gets the productFUExpirationDate value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @return productFUExpirationDate
     */
    public java.util.Calendar getProductFUExpirationDate() {
        return productFUExpirationDate;
    }


    /**
     * Sets the productFUExpirationDate value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @param productFUExpirationDate
     */
    public void setProductFUExpirationDate(java.util.Calendar productFUExpirationDate) {
        this.productFUExpirationDate = productFUExpirationDate;
    }


    /**
     * Gets the subscriptionProductItemsFU value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @return subscriptionProductItemsFU
     */
    public com.addcel.ws.clientes.simfonics.PortalService.SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row[] getSubscriptionProductItemsFU() {
        return subscriptionProductItemsFU;
    }


    /**
     * Sets the subscriptionProductItemsFU value for this SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.
     * 
     * @param subscriptionProductItemsFU
     */
    public void setSubscriptionProductItemsFU(com.addcel.ws.clientes.simfonics.PortalService.SubscriptionProductItemsFU_OUT_RowSetSubscriptionProductItemsFU_OUT_Row[] subscriptionProductItemsFU) {
        this.subscriptionProductItemsFU = subscriptionProductItemsFU;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row)) return false;
        SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row other = (SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.productFUId==null && other.getProductFUId()==null) || 
             (this.productFUId!=null &&
              this.productFUId.equals(other.getProductFUId()))) &&
            ((this.productId==null && other.getProductId()==null) || 
             (this.productId!=null &&
              this.productId.equals(other.getProductId()))) &&
            ((this.productCode==null && other.getProductCode()==null) || 
             (this.productCode!=null &&
              this.productCode.equals(other.getProductCode()))) &&
            ((this.productName==null && other.getProductName()==null) || 
             (this.productName!=null &&
              this.productName.equals(other.getProductName()))) &&
            ((this.productDescription==null && other.getProductDescription()==null) || 
             (this.productDescription!=null &&
              this.productDescription.equals(other.getProductDescription()))) &&
            ((this.productFUStatusId==null && other.getProductFUStatusId()==null) || 
             (this.productFUStatusId!=null &&
              this.productFUStatusId.equals(other.getProductFUStatusId()))) &&
            ((this.productFUStatus==null && other.getProductFUStatus()==null) || 
             (this.productFUStatus!=null &&
              this.productFUStatus.equals(other.getProductFUStatus()))) &&
            ((this.productFUStartTime==null && other.getProductFUStartTime()==null) || 
             (this.productFUStartTime!=null &&
              this.productFUStartTime.equals(other.getProductFUStartTime()))) &&
            ((this.productFUEndTime==null && other.getProductFUEndTime()==null) || 
             (this.productFUEndTime!=null &&
              this.productFUEndTime.equals(other.getProductFUEndTime()))) &&
            ((this.productFUExpirationDate==null && other.getProductFUExpirationDate()==null) || 
             (this.productFUExpirationDate!=null &&
              this.productFUExpirationDate.equals(other.getProductFUExpirationDate()))) &&
            ((this.subscriptionProductItemsFU==null && other.getSubscriptionProductItemsFU()==null) || 
             (this.subscriptionProductItemsFU!=null &&
              java.util.Arrays.equals(this.subscriptionProductItemsFU, other.getSubscriptionProductItemsFU())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getProductFUId() != null) {
            _hashCode += getProductFUId().hashCode();
        }
        if (getProductId() != null) {
            _hashCode += getProductId().hashCode();
        }
        if (getProductCode() != null) {
            _hashCode += getProductCode().hashCode();
        }
        if (getProductName() != null) {
            _hashCode += getProductName().hashCode();
        }
        if (getProductDescription() != null) {
            _hashCode += getProductDescription().hashCode();
        }
        if (getProductFUStatusId() != null) {
            _hashCode += getProductFUStatusId().hashCode();
        }
        if (getProductFUStatus() != null) {
            _hashCode += getProductFUStatus().hashCode();
        }
        if (getProductFUStartTime() != null) {
            _hashCode += getProductFUStartTime().hashCode();
        }
        if (getProductFUEndTime() != null) {
            _hashCode += getProductFUEndTime().hashCode();
        }
        if (getProductFUExpirationDate() != null) {
            _hashCode += getProductFUExpirationDate().hashCode();
        }
        if (getSubscriptionProductItemsFU() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSubscriptionProductItemsFU());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSubscriptionProductItemsFU(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubscriptionProductsFU_OUT_RowSetSubscriptionProductsFU_OUT_Row.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">subscriptionProductsFU_OUT_RowSet>subscriptionProductsFU_OUT_Row"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productFUId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productFUId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productFUStatusId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productFUStatusId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productFUStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productFUStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productFUStartTime");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productFUStartTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productFUEndTime");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productFUEndTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productFUExpirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productFUExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionProductItemsFU");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriptionProductItemsFU"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">subscriptionProductItemsFU_OUT_RowSet>subscriptionProductItemsFU_OUT_Row"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "subscriptionProductItemsFU_OUT_Row"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
