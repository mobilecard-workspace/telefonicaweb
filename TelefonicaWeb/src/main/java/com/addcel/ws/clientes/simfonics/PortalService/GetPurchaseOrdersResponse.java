/**
 * GetPurchaseOrdersResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class GetPurchaseOrdersResponse  implements java.io.Serializable {
    private java.lang.String result;

    private java.lang.String resultMessage;

    private com.addcel.ws.clientes.simfonics.PortalService.GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row[] resultGetPurchaseOrders;

    public GetPurchaseOrdersResponse() {
    }

    public GetPurchaseOrdersResponse(
           java.lang.String result,
           java.lang.String resultMessage,
           com.addcel.ws.clientes.simfonics.PortalService.GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row[] resultGetPurchaseOrders) {
           this.result = result;
           this.resultMessage = resultMessage;
           this.resultGetPurchaseOrders = resultGetPurchaseOrders;
    }


    /**
     * Gets the result value for this GetPurchaseOrdersResponse.
     * 
     * @return result
     */
    public java.lang.String getResult() {
        return result;
    }


    /**
     * Sets the result value for this GetPurchaseOrdersResponse.
     * 
     * @param result
     */
    public void setResult(java.lang.String result) {
        this.result = result;
    }


    /**
     * Gets the resultMessage value for this GetPurchaseOrdersResponse.
     * 
     * @return resultMessage
     */
    public java.lang.String getResultMessage() {
        return resultMessage;
    }


    /**
     * Sets the resultMessage value for this GetPurchaseOrdersResponse.
     * 
     * @param resultMessage
     */
    public void setResultMessage(java.lang.String resultMessage) {
        this.resultMessage = resultMessage;
    }


    /**
     * Gets the resultGetPurchaseOrders value for this GetPurchaseOrdersResponse.
     * 
     * @return resultGetPurchaseOrders
     */
    public com.addcel.ws.clientes.simfonics.PortalService.GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row[] getResultGetPurchaseOrders() {
        return resultGetPurchaseOrders;
    }


    /**
     * Sets the resultGetPurchaseOrders value for this GetPurchaseOrdersResponse.
     * 
     * @param resultGetPurchaseOrders
     */
    public void setResultGetPurchaseOrders(com.addcel.ws.clientes.simfonics.PortalService.GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row[] resultGetPurchaseOrders) {
        this.resultGetPurchaseOrders = resultGetPurchaseOrders;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPurchaseOrdersResponse)) return false;
        GetPurchaseOrdersResponse other = (GetPurchaseOrdersResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.result==null && other.getResult()==null) || 
             (this.result!=null &&
              this.result.equals(other.getResult()))) &&
            ((this.resultMessage==null && other.getResultMessage()==null) || 
             (this.resultMessage!=null &&
              this.resultMessage.equals(other.getResultMessage()))) &&
            ((this.resultGetPurchaseOrders==null && other.getResultGetPurchaseOrders()==null) || 
             (this.resultGetPurchaseOrders!=null &&
              java.util.Arrays.equals(this.resultGetPurchaseOrders, other.getResultGetPurchaseOrders())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResult() != null) {
            _hashCode += getResult().hashCode();
        }
        if (getResultMessage() != null) {
            _hashCode += getResultMessage().hashCode();
        }
        if (getResultGetPurchaseOrders() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResultGetPurchaseOrders());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResultGetPurchaseOrders(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPurchaseOrdersResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">getPurchaseOrdersResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result");
        elemField.setXmlName(new javax.xml.namespace.QName("", "result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultGetPurchaseOrders");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultGetPurchaseOrders"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">GETPURCHASEORDERS_OUT_RowSet>GETPURCHASEORDERS_OUT_Row"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "GETPURCHASEORDERS_OUT_Row"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
