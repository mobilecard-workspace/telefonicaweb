/**
 * GetOrderItemsTypeHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService.holders;

public final class GetOrderItemsTypeHolder implements javax.xml.rpc.holders.Holder {
    public com.addcel.ws.clientes.simfonics.PortalService.GetOrderItemsTypeOrderItem[] value;

    public GetOrderItemsTypeHolder() {
    }

    public GetOrderItemsTypeHolder(com.addcel.ws.clientes.simfonics.PortalService.GetOrderItemsTypeOrderItem[] value) {
        this.value = value;
    }

}
