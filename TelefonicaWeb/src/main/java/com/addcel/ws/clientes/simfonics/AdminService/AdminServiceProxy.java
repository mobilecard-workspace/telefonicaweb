package com.addcel.ws.clientes.simfonics.AdminService;

public class AdminServiceProxy implements com.addcel.ws.clientes.simfonics.AdminService.AdminService {
  private String _endpoint = null;
  private com.addcel.ws.clientes.simfonics.AdminService.AdminService adminService = null;
  
  public AdminServiceProxy() {
    _initAdminServiceProxy();
  }
  
  public AdminServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initAdminServiceProxy();
  }
  
  private void _initAdminServiceProxy() {
    try {
      adminService = (new com.addcel.ws.clientes.simfonics.AdminService.AdminServiceSOAPQSServiceLocator()).getAdminServiceSOAPQSPort();
      if (adminService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)adminService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)adminService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (adminService != null)
      ((javax.xml.rpc.Stub)adminService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.ws.clientes.simfonics.AdminService.AdminService getAdminService() {
    if (adminService == null)
      _initAdminServiceProxy();
    return adminService;
  }
  
  public void checkNickName(java.lang.String country, java.math.BigInteger brandId, java.lang.String nickName, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.BigIntegerHolder isAvailable) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.checkNickName(country, brandId, nickName, result, resultMessage, isAvailable);
  }
  
  public void sendEmailValidation(java.lang.String country, java.math.BigInteger brandId, java.lang.String userName, java.lang.String mailLink, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.sendEmailValidation(country, brandId, userName, mailLink, result, resultMessage);
  }
  
  public void validateUserMail(java.lang.String country, java.math.BigInteger brandId, java.lang.String userName, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.validateUserMail(country, brandId, userName, result, resultMessage);
  }
  
  public void checkUserName(java.lang.String country, java.math.BigInteger brandId, java.lang.String userName, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder responseMessage, javax.xml.rpc.holders.BigIntegerHolder resultCheckUserName) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.checkUserName(country, brandId, userName, result, responseMessage, resultCheckUserName);
  }
  
  public void checkIdentityNum(java.lang.String country, java.math.BigInteger brandId, java.lang.String identityNum, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder responseMessage, javax.xml.rpc.holders.BigIntegerHolder resultCheckIdentityNum) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.checkIdentityNum(country, brandId, identityNum, result, responseMessage, resultCheckIdentityNum);
  }
  
  public void topupValidation(java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger msisdn, java.math.BigDecimal amount, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.topupValidation(country, brandId, msisdn, amount, result, resultMessage);
  }
  
  public void addVoucherByMsisdn(java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger msisdn, java.lang.String voucherPinCode, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.BigDecimalHolder balance, javax.xml.rpc.holders.StringHolder currency) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.addVoucherByMsisdn(country, brandId, msisdn, voucherPinCode, result, resultMessage, balance, currency);
  }
  
  public void addBalanceByMsisdn(java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger msisdn, java.math.BigDecimal amount, java.lang.String paymentReference, java.math.BigInteger productId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.BigDecimalHolder balance, javax.xml.rpc.holders.StringHolder currency) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.addBalanceByMsisdn(country, brandId, msisdn, amount, paymentReference, productId, result, resultMessage, balance, currency);
  }
  
  public void checkUsernameSecurityAnswer(java.lang.String userName, java.lang.String securityQuestion, java.lang.String securityAnswer, java.lang.String country, java.math.BigInteger brandId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.checkUsernameSecurityAnswer(userName, securityQuestion, securityAnswer, country, brandId, result, resultMessage);
  }
  
  public void getUsernameSecurityQuestion(java.lang.String userName, java.lang.String country, java.math.BigInteger brandId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.StringHolder securityQuestion) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.getUsernameSecurityQuestion(userName, country, brandId, result, resultMessage, securityQuestion);
  }
  
  public void sendResetPasswordMail(java.lang.String userName, java.lang.String country, java.math.BigInteger brandId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.sendResetPasswordMail(userName, country, brandId, result, resultMessage);
  }
  
  public void changeUserPassword(java.lang.String token, java.lang.String country, java.math.BigInteger brandId, java.lang.String newPassword, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.StringHolder sessionId) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.changeUserPassword(token, country, brandId, newPassword, result, resultMessage, sessionId);
  }
  
  public com.addcel.ws.clientes.simfonics.AdminService.CreateCustomerResponse createCustomer(com.addcel.ws.clientes.simfonics.AdminService.CreateCustomer createCustomerInput) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    return adminService.createCustomer(createCustomerInput);
  }
  
  public void getCustomerPropertiesByBrand(java.lang.String country, java.math.BigInteger brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.BrandPropertiesTypeHolder brandProperties) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.getCustomerPropertiesByBrand(country, brandId, language, result, resultMessage, brandProperties);
  }
  
  public void getBrandProducts(java.lang.String country, java.math.BigInteger brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.BrandProductsTypeHolder brandProducts) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.getBrandProducts(country, brandId, language, result, resultMessage, brandProducts);
  }
  
  public void getIncidentTypesByBrand(java.lang.String country, java.math.BigInteger brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.IncidentTypesTypeHolder incidentTypes) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.getIncidentTypesByBrand(country, brandId, language, result, resultMessage, incidentTypes);
  }
  
  public com.addcel.ws.clientes.simfonics.AdminService.ValidateSMSTokenResponse validateSMSToken(com.addcel.ws.clientes.simfonics.AdminService.ValidateSMSToken validateSMSTokenInput) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    return adminService.validateSMSToken(validateSMSTokenInput);
  }
  
  public void getIncidentCategoriesByBrand(java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger typeId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.IncidentCategoriesTypeHolder incidentCategories) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.getIncidentCategoriesByBrand(country, brandId, typeId, language, result, resultMessage, incidentCategories);
  }
  
  public void getIncidentSubCategoriesByBrand(java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger typeId, java.math.BigInteger categoryId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.IncidentSubCategoriesTypeHolder incidentSubCategories) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.getIncidentSubCategoriesByBrand(country, brandId, typeId, categoryId, language, result, resultMessage, incidentSubCategories);
  }
  
  public void getOrdersCatalogByBrand(java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger ordersTypeId, java.math.BigInteger ordersCategoryId, java.math.BigInteger ordersSubCategoryId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.OrdersCatalogTypeHolder ordersCatalog) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.getOrdersCatalogByBrand(country, brandId, ordersTypeId, ordersCategoryId, ordersSubCategoryId, result, resultMessage, ordersCatalog);
  }
  
  public void getUsageTypeNames(java.lang.String country, java.math.BigInteger brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.UsageTypeNamesHolder usageTypeNames) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.getUsageTypeNames(country, brandId, language, result, resultMessage, usageTypeNames);
  }
  
  public void doPayment(java.lang.String country, java.math.BigInteger brandId, java.lang.String language, java.math.BigDecimal amount, java.lang.String currency, java.math.BigInteger paymentOperationId, java.math.BigInteger paymentTypeId, java.lang.String description, com.addcel.ws.clientes.simfonics.AdminService.CreditCardType creditCard, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.StringHolder transactionResult) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.doPayment(country, brandId, language, amount, currency, paymentOperationId, paymentTypeId, description, creditCard, result, resultMessage, transactionResult);
  }
  
  public void getPaymentTypeNames(java.lang.String country, int brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.PaymentTypeNamesTypeHolder paymentTypeNames) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.getPaymentTypeNames(country, brandId, language, result, resultMessage, paymentTypeNames);
  }
  
  public void getPaymentMethodNames(java.lang.String country, int brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.PaymentMethodNamesTypeHolder paymentMethodNames) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.getPaymentMethodNames(country, brandId, language, result, resultMessage, paymentMethodNames);
  }
  
  public void getPurchaseProductParameters(java.lang.String country, java.math.BigInteger brandId, java.lang.String language, java.math.BigInteger productTypeId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.ProductsParametersTypeHolder purchaseParameters) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.getPurchaseProductParameters(country, brandId, language, productTypeId, result, resultMessage, purchaseParameters);
  }
  
  public void getPaymentOperationNames(java.lang.String country, int brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.AdminService.holders.PaymentOperationNamesTypeHolder paymentOperationNames) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.getPaymentOperationNames(country, brandId, language, result, resultMessage, paymentOperationNames);
  }
  
  public void recruiterValidation(java.lang.String country, java.math.BigInteger brandId, java.lang.String msisdn_recruiter, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.BigIntegerHolder isValid) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    adminService.recruiterValidation(country, brandId, msisdn_recruiter, result, resultMessage, isValid);
  }
  
  public com.addcel.ws.clientes.simfonics.AdminService.SendSMSResponse sendSMS(com.addcel.ws.clientes.simfonics.AdminService.SendSMSRequest sendSMSInput) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    return adminService.sendSMS(sendSMSInput);
  }
  
  public com.addcel.ws.clientes.simfonics.AdminService.GetFileEntityTypeListResponse getFileEntityTypeList(com.addcel.ws.clientes.simfonics.AdminService.GetFileEntityTypeListRequest getFileEntityTypeListInput) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    return adminService.getFileEntityTypeList(getFileEntityTypeListInput);
  }
  
  public com.addcel.ws.clientes.simfonics.AdminService.ChangeUserPasswordTuentiResponse changeUserPasswordTuenti(com.addcel.ws.clientes.simfonics.AdminService.ChangeUserPasswordTuenti changeUserPasswordTuentiInput) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    return adminService.changeUserPasswordTuenti(changeUserPasswordTuentiInput);
  }
  
  public com.addcel.ws.clientes.simfonics.AdminService.GetLogisticPropertiesByBrandResponse getLogisticPropertiesByBrand(com.addcel.ws.clientes.simfonics.AdminService.GetLogisticPropertiesByBrandRequest getLogisticPropertiesByBrandInput) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    return adminService.getLogisticPropertiesByBrand(getLogisticPropertiesByBrandInput);
  }
  
  public com.addcel.ws.clientes.simfonics.AdminService.GetLogisticOperatorNamesResponse getLogisticOperatorNames(com.addcel.ws.clientes.simfonics.AdminService.GetLogisticOperatorNamesRequest getLogisticOperatorNamesInput) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    return adminService.getLogisticOperatorNames(getLogisticOperatorNamesInput);
  }
  
  public com.addcel.ws.clientes.simfonics.AdminService.GetLogisticStatusNameResponse getLogisticStatusName(com.addcel.ws.clientes.simfonics.AdminService.GetLogisticStatusNameRequest getLogisticStatusNameInput) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    return adminService.getLogisticStatusName(getLogisticStatusNameInput);
  }
  
  public com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityOperatorNamesResponse getPortabilityOperatorNames(com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityOperatorNamesRequest getPortabilityOperatorNamesInput) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    return adminService.getPortabilityOperatorNames(getPortabilityOperatorNamesInput);
  }
  
  public com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityStatusNamesResponse getPortabilityStatusNames(com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityStatusNamesRequest getPortabilityStatusNamesInput) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    return adminService.getPortabilityStatusNames(getPortabilityStatusNamesInput);
  }
  
  public com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityPropertiesByBrandResponse getPortabilityPropertiesByBrand(com.addcel.ws.clientes.simfonics.AdminService.GetPortabilityPropertiesByBrandRequest getPortabilityPropertiesByBrandInput) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    return adminService.getPortabilityPropertiesByBrand(getPortabilityPropertiesByBrandInput);
  }
  
  public com.addcel.ws.clientes.simfonics.AdminService.GetRecurrencyTypeNamesResponse getRecurrencyTypeNames(com.addcel.ws.clientes.simfonics.AdminService.GetRecurrencyTypeNamesRequest getRecurrencyTypeNamesInput) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    return adminService.getRecurrencyTypeNames(getRecurrencyTypeNamesInput);
  }
  
  public com.addcel.ws.clientes.simfonics.AdminService.ValidateIdentityNumResponse validateIdentityNum(com.addcel.ws.clientes.simfonics.AdminService.ValidateIdentityNumRequest validateIdentityNumRequestInput) throws java.rmi.RemoteException{
    if (adminService == null)
      _initAdminServiceProxy();
    return adminService.validateIdentityNum(validateIdentityNumRequestInput);
  }
  
  
}