/**
 * GetOrdersResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class GetOrdersResponse  implements java.io.Serializable {
    private java.lang.String result;

    private java.lang.String resultMessage;

    private com.addcel.ws.clientes.simfonics.PortalService.GetOrdersTypeOrder[] resultGetOrders;

    public GetOrdersResponse() {
    }

    public GetOrdersResponse(
           java.lang.String result,
           java.lang.String resultMessage,
           com.addcel.ws.clientes.simfonics.PortalService.GetOrdersTypeOrder[] resultGetOrders) {
           this.result = result;
           this.resultMessage = resultMessage;
           this.resultGetOrders = resultGetOrders;
    }


    /**
     * Gets the result value for this GetOrdersResponse.
     * 
     * @return result
     */
    public java.lang.String getResult() {
        return result;
    }


    /**
     * Sets the result value for this GetOrdersResponse.
     * 
     * @param result
     */
    public void setResult(java.lang.String result) {
        this.result = result;
    }


    /**
     * Gets the resultMessage value for this GetOrdersResponse.
     * 
     * @return resultMessage
     */
    public java.lang.String getResultMessage() {
        return resultMessage;
    }


    /**
     * Sets the resultMessage value for this GetOrdersResponse.
     * 
     * @param resultMessage
     */
    public void setResultMessage(java.lang.String resultMessage) {
        this.resultMessage = resultMessage;
    }


    /**
     * Gets the resultGetOrders value for this GetOrdersResponse.
     * 
     * @return resultGetOrders
     */
    public com.addcel.ws.clientes.simfonics.PortalService.GetOrdersTypeOrder[] getResultGetOrders() {
        return resultGetOrders;
    }


    /**
     * Sets the resultGetOrders value for this GetOrdersResponse.
     * 
     * @param resultGetOrders
     */
    public void setResultGetOrders(com.addcel.ws.clientes.simfonics.PortalService.GetOrdersTypeOrder[] resultGetOrders) {
        this.resultGetOrders = resultGetOrders;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetOrdersResponse)) return false;
        GetOrdersResponse other = (GetOrdersResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.result==null && other.getResult()==null) || 
             (this.result!=null &&
              this.result.equals(other.getResult()))) &&
            ((this.resultMessage==null && other.getResultMessage()==null) || 
             (this.resultMessage!=null &&
              this.resultMessage.equals(other.getResultMessage()))) &&
            ((this.resultGetOrders==null && other.getResultGetOrders()==null) || 
             (this.resultGetOrders!=null &&
              java.util.Arrays.equals(this.resultGetOrders, other.getResultGetOrders())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResult() != null) {
            _hashCode += getResult().hashCode();
        }
        if (getResultMessage() != null) {
            _hashCode += getResultMessage().hashCode();
        }
        if (getResultGetOrders() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResultGetOrders());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResultGetOrders(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetOrdersResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">getOrdersResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result");
        elemField.setXmlName(new javax.xml.namespace.QName("", "result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultGetOrders");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultGetOrders"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">getOrdersType>order"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "order"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
