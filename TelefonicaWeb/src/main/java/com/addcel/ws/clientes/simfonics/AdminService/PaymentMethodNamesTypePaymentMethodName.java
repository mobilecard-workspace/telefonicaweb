/**
 * PaymentMethodNamesTypePaymentMethodName.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class PaymentMethodNamesTypePaymentMethodName  implements java.io.Serializable {
    private java.math.BigInteger paymentMethodId;

    private java.lang.String paymentMethodName;

    public PaymentMethodNamesTypePaymentMethodName() {
    }

    public PaymentMethodNamesTypePaymentMethodName(
           java.math.BigInteger paymentMethodId,
           java.lang.String paymentMethodName) {
           this.paymentMethodId = paymentMethodId;
           this.paymentMethodName = paymentMethodName;
    }


    /**
     * Gets the paymentMethodId value for this PaymentMethodNamesTypePaymentMethodName.
     * 
     * @return paymentMethodId
     */
    public java.math.BigInteger getPaymentMethodId() {
        return paymentMethodId;
    }


    /**
     * Sets the paymentMethodId value for this PaymentMethodNamesTypePaymentMethodName.
     * 
     * @param paymentMethodId
     */
    public void setPaymentMethodId(java.math.BigInteger paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }


    /**
     * Gets the paymentMethodName value for this PaymentMethodNamesTypePaymentMethodName.
     * 
     * @return paymentMethodName
     */
    public java.lang.String getPaymentMethodName() {
        return paymentMethodName;
    }


    /**
     * Sets the paymentMethodName value for this PaymentMethodNamesTypePaymentMethodName.
     * 
     * @param paymentMethodName
     */
    public void setPaymentMethodName(java.lang.String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaymentMethodNamesTypePaymentMethodName)) return false;
        PaymentMethodNamesTypePaymentMethodName other = (PaymentMethodNamesTypePaymentMethodName) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.paymentMethodId==null && other.getPaymentMethodId()==null) || 
             (this.paymentMethodId!=null &&
              this.paymentMethodId.equals(other.getPaymentMethodId()))) &&
            ((this.paymentMethodName==null && other.getPaymentMethodName()==null) || 
             (this.paymentMethodName!=null &&
              this.paymentMethodName.equals(other.getPaymentMethodName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPaymentMethodId() != null) {
            _hashCode += getPaymentMethodId().hashCode();
        }
        if (getPaymentMethodName() != null) {
            _hashCode += getPaymentMethodName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaymentMethodNamesTypePaymentMethodName.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">paymentMethodNamesType>paymentMethodName"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMethodId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paymentMethodId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMethodName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paymentMethodName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
