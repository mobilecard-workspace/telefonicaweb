/**
 * GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row  implements java.io.Serializable {
    private java.math.BigInteger ID;

    private java.math.BigInteger STATUS;

    private java.lang.String STATUSNAME;

    private java.util.Calendar STARTDATE;

    private java.util.Calendar ENDDATE;

    private java.lang.String SUBSCRIPTIONID;

    private java.math.BigDecimal AMOUNT;

    private java.lang.String CURRENCY;

    private java.math.BigInteger CATALOGID;

    private java.math.BigInteger TYPEID;

    private java.lang.String TYPENAME;

    private java.math.BigInteger CATEGORYID;

    private java.lang.String CATEGORYNAME;

    private java.math.BigInteger SUBCATEGORYID;

    private java.lang.String SUBCATEGORYNAME;

    private java.math.BigDecimal SYSTEMID;

    private java.lang.String SYSTEMNAME;

    private java.lang.String PAYMENTTYPE;

    public GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row() {
    }

    public GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row(
           java.math.BigInteger ID,
           java.math.BigInteger STATUS,
           java.lang.String STATUSNAME,
           java.util.Calendar STARTDATE,
           java.util.Calendar ENDDATE,
           java.lang.String SUBSCRIPTIONID,
           java.math.BigDecimal AMOUNT,
           java.lang.String CURRENCY,
           java.math.BigInteger CATALOGID,
           java.math.BigInteger TYPEID,
           java.lang.String TYPENAME,
           java.math.BigInteger CATEGORYID,
           java.lang.String CATEGORYNAME,
           java.math.BigInteger SUBCATEGORYID,
           java.lang.String SUBCATEGORYNAME,
           java.math.BigDecimal SYSTEMID,
           java.lang.String SYSTEMNAME,
           java.lang.String PAYMENTTYPE) {
           this.ID = ID;
           this.STATUS = STATUS;
           this.STATUSNAME = STATUSNAME;
           this.STARTDATE = STARTDATE;
           this.ENDDATE = ENDDATE;
           this.SUBSCRIPTIONID = SUBSCRIPTIONID;
           this.AMOUNT = AMOUNT;
           this.CURRENCY = CURRENCY;
           this.CATALOGID = CATALOGID;
           this.TYPEID = TYPEID;
           this.TYPENAME = TYPENAME;
           this.CATEGORYID = CATEGORYID;
           this.CATEGORYNAME = CATEGORYNAME;
           this.SUBCATEGORYID = SUBCATEGORYID;
           this.SUBCATEGORYNAME = SUBCATEGORYNAME;
           this.SYSTEMID = SYSTEMID;
           this.SYSTEMNAME = SYSTEMNAME;
           this.PAYMENTTYPE = PAYMENTTYPE;
    }


    /**
     * Gets the ID value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return ID
     */
    public java.math.BigInteger getID() {
        return ID;
    }


    /**
     * Sets the ID value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param ID
     */
    public void setID(java.math.BigInteger ID) {
        this.ID = ID;
    }


    /**
     * Gets the STATUS value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return STATUS
     */
    public java.math.BigInteger getSTATUS() {
        return STATUS;
    }


    /**
     * Sets the STATUS value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param STATUS
     */
    public void setSTATUS(java.math.BigInteger STATUS) {
        this.STATUS = STATUS;
    }


    /**
     * Gets the STATUSNAME value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return STATUSNAME
     */
    public java.lang.String getSTATUSNAME() {
        return STATUSNAME;
    }


    /**
     * Sets the STATUSNAME value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param STATUSNAME
     */
    public void setSTATUSNAME(java.lang.String STATUSNAME) {
        this.STATUSNAME = STATUSNAME;
    }


    /**
     * Gets the STARTDATE value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return STARTDATE
     */
    public java.util.Calendar getSTARTDATE() {
        return STARTDATE;
    }


    /**
     * Sets the STARTDATE value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param STARTDATE
     */
    public void setSTARTDATE(java.util.Calendar STARTDATE) {
        this.STARTDATE = STARTDATE;
    }


    /**
     * Gets the ENDDATE value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return ENDDATE
     */
    public java.util.Calendar getENDDATE() {
        return ENDDATE;
    }


    /**
     * Sets the ENDDATE value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param ENDDATE
     */
    public void setENDDATE(java.util.Calendar ENDDATE) {
        this.ENDDATE = ENDDATE;
    }


    /**
     * Gets the SUBSCRIPTIONID value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return SUBSCRIPTIONID
     */
    public java.lang.String getSUBSCRIPTIONID() {
        return SUBSCRIPTIONID;
    }


    /**
     * Sets the SUBSCRIPTIONID value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param SUBSCRIPTIONID
     */
    public void setSUBSCRIPTIONID(java.lang.String SUBSCRIPTIONID) {
        this.SUBSCRIPTIONID = SUBSCRIPTIONID;
    }


    /**
     * Gets the AMOUNT value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return AMOUNT
     */
    public java.math.BigDecimal getAMOUNT() {
        return AMOUNT;
    }


    /**
     * Sets the AMOUNT value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param AMOUNT
     */
    public void setAMOUNT(java.math.BigDecimal AMOUNT) {
        this.AMOUNT = AMOUNT;
    }


    /**
     * Gets the CURRENCY value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return CURRENCY
     */
    public java.lang.String getCURRENCY() {
        return CURRENCY;
    }


    /**
     * Sets the CURRENCY value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param CURRENCY
     */
    public void setCURRENCY(java.lang.String CURRENCY) {
        this.CURRENCY = CURRENCY;
    }


    /**
     * Gets the CATALOGID value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return CATALOGID
     */
    public java.math.BigInteger getCATALOGID() {
        return CATALOGID;
    }


    /**
     * Sets the CATALOGID value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param CATALOGID
     */
    public void setCATALOGID(java.math.BigInteger CATALOGID) {
        this.CATALOGID = CATALOGID;
    }


    /**
     * Gets the TYPEID value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return TYPEID
     */
    public java.math.BigInteger getTYPEID() {
        return TYPEID;
    }


    /**
     * Sets the TYPEID value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param TYPEID
     */
    public void setTYPEID(java.math.BigInteger TYPEID) {
        this.TYPEID = TYPEID;
    }


    /**
     * Gets the TYPENAME value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return TYPENAME
     */
    public java.lang.String getTYPENAME() {
        return TYPENAME;
    }


    /**
     * Sets the TYPENAME value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param TYPENAME
     */
    public void setTYPENAME(java.lang.String TYPENAME) {
        this.TYPENAME = TYPENAME;
    }


    /**
     * Gets the CATEGORYID value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return CATEGORYID
     */
    public java.math.BigInteger getCATEGORYID() {
        return CATEGORYID;
    }


    /**
     * Sets the CATEGORYID value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param CATEGORYID
     */
    public void setCATEGORYID(java.math.BigInteger CATEGORYID) {
        this.CATEGORYID = CATEGORYID;
    }


    /**
     * Gets the CATEGORYNAME value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return CATEGORYNAME
     */
    public java.lang.String getCATEGORYNAME() {
        return CATEGORYNAME;
    }


    /**
     * Sets the CATEGORYNAME value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param CATEGORYNAME
     */
    public void setCATEGORYNAME(java.lang.String CATEGORYNAME) {
        this.CATEGORYNAME = CATEGORYNAME;
    }


    /**
     * Gets the SUBCATEGORYID value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return SUBCATEGORYID
     */
    public java.math.BigInteger getSUBCATEGORYID() {
        return SUBCATEGORYID;
    }


    /**
     * Sets the SUBCATEGORYID value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param SUBCATEGORYID
     */
    public void setSUBCATEGORYID(java.math.BigInteger SUBCATEGORYID) {
        this.SUBCATEGORYID = SUBCATEGORYID;
    }


    /**
     * Gets the SUBCATEGORYNAME value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return SUBCATEGORYNAME
     */
    public java.lang.String getSUBCATEGORYNAME() {
        return SUBCATEGORYNAME;
    }


    /**
     * Sets the SUBCATEGORYNAME value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param SUBCATEGORYNAME
     */
    public void setSUBCATEGORYNAME(java.lang.String SUBCATEGORYNAME) {
        this.SUBCATEGORYNAME = SUBCATEGORYNAME;
    }


    /**
     * Gets the SYSTEMID value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return SYSTEMID
     */
    public java.math.BigDecimal getSYSTEMID() {
        return SYSTEMID;
    }


    /**
     * Sets the SYSTEMID value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param SYSTEMID
     */
    public void setSYSTEMID(java.math.BigDecimal SYSTEMID) {
        this.SYSTEMID = SYSTEMID;
    }


    /**
     * Gets the SYSTEMNAME value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return SYSTEMNAME
     */
    public java.lang.String getSYSTEMNAME() {
        return SYSTEMNAME;
    }


    /**
     * Sets the SYSTEMNAME value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param SYSTEMNAME
     */
    public void setSYSTEMNAME(java.lang.String SYSTEMNAME) {
        this.SYSTEMNAME = SYSTEMNAME;
    }


    /**
     * Gets the PAYMENTTYPE value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @return PAYMENTTYPE
     */
    public java.lang.String getPAYMENTTYPE() {
        return PAYMENTTYPE;
    }


    /**
     * Sets the PAYMENTTYPE value for this GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.
     * 
     * @param PAYMENTTYPE
     */
    public void setPAYMENTTYPE(java.lang.String PAYMENTTYPE) {
        this.PAYMENTTYPE = PAYMENTTYPE;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row)) return false;
        GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row other = (GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ID==null && other.getID()==null) || 
             (this.ID!=null &&
              this.ID.equals(other.getID()))) &&
            ((this.STATUS==null && other.getSTATUS()==null) || 
             (this.STATUS!=null &&
              this.STATUS.equals(other.getSTATUS()))) &&
            ((this.STATUSNAME==null && other.getSTATUSNAME()==null) || 
             (this.STATUSNAME!=null &&
              this.STATUSNAME.equals(other.getSTATUSNAME()))) &&
            ((this.STARTDATE==null && other.getSTARTDATE()==null) || 
             (this.STARTDATE!=null &&
              this.STARTDATE.equals(other.getSTARTDATE()))) &&
            ((this.ENDDATE==null && other.getENDDATE()==null) || 
             (this.ENDDATE!=null &&
              this.ENDDATE.equals(other.getENDDATE()))) &&
            ((this.SUBSCRIPTIONID==null && other.getSUBSCRIPTIONID()==null) || 
             (this.SUBSCRIPTIONID!=null &&
              this.SUBSCRIPTIONID.equals(other.getSUBSCRIPTIONID()))) &&
            ((this.AMOUNT==null && other.getAMOUNT()==null) || 
             (this.AMOUNT!=null &&
              this.AMOUNT.equals(other.getAMOUNT()))) &&
            ((this.CURRENCY==null && other.getCURRENCY()==null) || 
             (this.CURRENCY!=null &&
              this.CURRENCY.equals(other.getCURRENCY()))) &&
            ((this.CATALOGID==null && other.getCATALOGID()==null) || 
             (this.CATALOGID!=null &&
              this.CATALOGID.equals(other.getCATALOGID()))) &&
            ((this.TYPEID==null && other.getTYPEID()==null) || 
             (this.TYPEID!=null &&
              this.TYPEID.equals(other.getTYPEID()))) &&
            ((this.TYPENAME==null && other.getTYPENAME()==null) || 
             (this.TYPENAME!=null &&
              this.TYPENAME.equals(other.getTYPENAME()))) &&
            ((this.CATEGORYID==null && other.getCATEGORYID()==null) || 
             (this.CATEGORYID!=null &&
              this.CATEGORYID.equals(other.getCATEGORYID()))) &&
            ((this.CATEGORYNAME==null && other.getCATEGORYNAME()==null) || 
             (this.CATEGORYNAME!=null &&
              this.CATEGORYNAME.equals(other.getCATEGORYNAME()))) &&
            ((this.SUBCATEGORYID==null && other.getSUBCATEGORYID()==null) || 
             (this.SUBCATEGORYID!=null &&
              this.SUBCATEGORYID.equals(other.getSUBCATEGORYID()))) &&
            ((this.SUBCATEGORYNAME==null && other.getSUBCATEGORYNAME()==null) || 
             (this.SUBCATEGORYNAME!=null &&
              this.SUBCATEGORYNAME.equals(other.getSUBCATEGORYNAME()))) &&
            ((this.SYSTEMID==null && other.getSYSTEMID()==null) || 
             (this.SYSTEMID!=null &&
              this.SYSTEMID.equals(other.getSYSTEMID()))) &&
            ((this.SYSTEMNAME==null && other.getSYSTEMNAME()==null) || 
             (this.SYSTEMNAME!=null &&
              this.SYSTEMNAME.equals(other.getSYSTEMNAME()))) &&
            ((this.PAYMENTTYPE==null && other.getPAYMENTTYPE()==null) || 
             (this.PAYMENTTYPE!=null &&
              this.PAYMENTTYPE.equals(other.getPAYMENTTYPE())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getID() != null) {
            _hashCode += getID().hashCode();
        }
        if (getSTATUS() != null) {
            _hashCode += getSTATUS().hashCode();
        }
        if (getSTATUSNAME() != null) {
            _hashCode += getSTATUSNAME().hashCode();
        }
        if (getSTARTDATE() != null) {
            _hashCode += getSTARTDATE().hashCode();
        }
        if (getENDDATE() != null) {
            _hashCode += getENDDATE().hashCode();
        }
        if (getSUBSCRIPTIONID() != null) {
            _hashCode += getSUBSCRIPTIONID().hashCode();
        }
        if (getAMOUNT() != null) {
            _hashCode += getAMOUNT().hashCode();
        }
        if (getCURRENCY() != null) {
            _hashCode += getCURRENCY().hashCode();
        }
        if (getCATALOGID() != null) {
            _hashCode += getCATALOGID().hashCode();
        }
        if (getTYPEID() != null) {
            _hashCode += getTYPEID().hashCode();
        }
        if (getTYPENAME() != null) {
            _hashCode += getTYPENAME().hashCode();
        }
        if (getCATEGORYID() != null) {
            _hashCode += getCATEGORYID().hashCode();
        }
        if (getCATEGORYNAME() != null) {
            _hashCode += getCATEGORYNAME().hashCode();
        }
        if (getSUBCATEGORYID() != null) {
            _hashCode += getSUBCATEGORYID().hashCode();
        }
        if (getSUBCATEGORYNAME() != null) {
            _hashCode += getSUBCATEGORYNAME().hashCode();
        }
        if (getSYSTEMID() != null) {
            _hashCode += getSYSTEMID().hashCode();
        }
        if (getSYSTEMNAME() != null) {
            _hashCode += getSYSTEMNAME().hashCode();
        }
        if (getPAYMENTTYPE() != null) {
            _hashCode += getPAYMENTTYPE().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">GETRECHARGEORDERS_OUT_RowSet>GETRECHARGEORDERS_OUT_Row"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STATUS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "STATUS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STATUSNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "STATUSNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STARTDATE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "STARTDATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ENDDATE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ENDDATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SUBSCRIPTIONID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SUBSCRIPTIONID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AMOUNT");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AMOUNT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CURRENCY");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CURRENCY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CATALOGID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CATALOGID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TYPEID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TYPEID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TYPENAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TYPENAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CATEGORYID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CATEGORYID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CATEGORYNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CATEGORYNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SUBCATEGORYID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SUBCATEGORYID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SUBCATEGORYNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SUBCATEGORYNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SYSTEMID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SYSTEMID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SYSTEMNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SYSTEMNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PAYMENTTYPE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PAYMENTTYPE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
