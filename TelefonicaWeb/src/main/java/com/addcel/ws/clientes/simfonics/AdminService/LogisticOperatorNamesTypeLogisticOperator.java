/**
 * LogisticOperatorNamesTypeLogisticOperator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class LogisticOperatorNamesTypeLogisticOperator  implements java.io.Serializable {
    private java.math.BigDecimal logisticOperatorId;

    private java.lang.String logisticOperatorName;

    public LogisticOperatorNamesTypeLogisticOperator() {
    }

    public LogisticOperatorNamesTypeLogisticOperator(
           java.math.BigDecimal logisticOperatorId,
           java.lang.String logisticOperatorName) {
           this.logisticOperatorId = logisticOperatorId;
           this.logisticOperatorName = logisticOperatorName;
    }


    /**
     * Gets the logisticOperatorId value for this LogisticOperatorNamesTypeLogisticOperator.
     * 
     * @return logisticOperatorId
     */
    public java.math.BigDecimal getLogisticOperatorId() {
        return logisticOperatorId;
    }


    /**
     * Sets the logisticOperatorId value for this LogisticOperatorNamesTypeLogisticOperator.
     * 
     * @param logisticOperatorId
     */
    public void setLogisticOperatorId(java.math.BigDecimal logisticOperatorId) {
        this.logisticOperatorId = logisticOperatorId;
    }


    /**
     * Gets the logisticOperatorName value for this LogisticOperatorNamesTypeLogisticOperator.
     * 
     * @return logisticOperatorName
     */
    public java.lang.String getLogisticOperatorName() {
        return logisticOperatorName;
    }


    /**
     * Sets the logisticOperatorName value for this LogisticOperatorNamesTypeLogisticOperator.
     * 
     * @param logisticOperatorName
     */
    public void setLogisticOperatorName(java.lang.String logisticOperatorName) {
        this.logisticOperatorName = logisticOperatorName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LogisticOperatorNamesTypeLogisticOperator)) return false;
        LogisticOperatorNamesTypeLogisticOperator other = (LogisticOperatorNamesTypeLogisticOperator) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.logisticOperatorId==null && other.getLogisticOperatorId()==null) || 
             (this.logisticOperatorId!=null &&
              this.logisticOperatorId.equals(other.getLogisticOperatorId()))) &&
            ((this.logisticOperatorName==null && other.getLogisticOperatorName()==null) || 
             (this.logisticOperatorName!=null &&
              this.logisticOperatorName.equals(other.getLogisticOperatorName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLogisticOperatorId() != null) {
            _hashCode += getLogisticOperatorId().hashCode();
        }
        if (getLogisticOperatorName() != null) {
            _hashCode += getLogisticOperatorName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LogisticOperatorNamesTypeLogisticOperator.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">LogisticOperatorNamesType>LogisticOperator"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logisticOperatorId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LogisticOperatorId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logisticOperatorName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LogisticOperatorName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
