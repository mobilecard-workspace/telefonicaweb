/**
 * Portability_RowSetPortability_Row.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class Portability_RowSetPortability_Row  implements java.io.Serializable {
    private java.math.BigDecimal id;

    private java.math.BigDecimal brandId;

    private java.math.BigDecimal orderId;

    private java.math.BigDecimal statusId;

    private java.lang.String statusName;

    private java.math.BigDecimal typeId;

    private java.lang.String typeName;

    private java.math.BigDecimal operatorId;

    private java.lang.String operatorName;

    private java.util.Calendar effectiveDate;

    private java.math.BigDecimal cancelable;

    private java.util.Calendar creationDate;

    private java.util.Calendar lastUpdate;

    private java.math.BigDecimal msisdnId;

    public Portability_RowSetPortability_Row() {
    }

    public Portability_RowSetPortability_Row(
           java.math.BigDecimal id,
           java.math.BigDecimal brandId,
           java.math.BigDecimal orderId,
           java.math.BigDecimal statusId,
           java.lang.String statusName,
           java.math.BigDecimal typeId,
           java.lang.String typeName,
           java.math.BigDecimal operatorId,
           java.lang.String operatorName,
           java.util.Calendar effectiveDate,
           java.math.BigDecimal cancelable,
           java.util.Calendar creationDate,
           java.util.Calendar lastUpdate,
           java.math.BigDecimal msisdnId) {
           this.id = id;
           this.brandId = brandId;
           this.orderId = orderId;
           this.statusId = statusId;
           this.statusName = statusName;
           this.typeId = typeId;
           this.typeName = typeName;
           this.operatorId = operatorId;
           this.operatorName = operatorName;
           this.effectiveDate = effectiveDate;
           this.cancelable = cancelable;
           this.creationDate = creationDate;
           this.lastUpdate = lastUpdate;
           this.msisdnId = msisdnId;
    }


    /**
     * Gets the id value for this Portability_RowSetPortability_Row.
     * 
     * @return id
     */
    public java.math.BigDecimal getId() {
        return id;
    }


    /**
     * Sets the id value for this Portability_RowSetPortability_Row.
     * 
     * @param id
     */
    public void setId(java.math.BigDecimal id) {
        this.id = id;
    }


    /**
     * Gets the brandId value for this Portability_RowSetPortability_Row.
     * 
     * @return brandId
     */
    public java.math.BigDecimal getBrandId() {
        return brandId;
    }


    /**
     * Sets the brandId value for this Portability_RowSetPortability_Row.
     * 
     * @param brandId
     */
    public void setBrandId(java.math.BigDecimal brandId) {
        this.brandId = brandId;
    }


    /**
     * Gets the orderId value for this Portability_RowSetPortability_Row.
     * 
     * @return orderId
     */
    public java.math.BigDecimal getOrderId() {
        return orderId;
    }


    /**
     * Sets the orderId value for this Portability_RowSetPortability_Row.
     * 
     * @param orderId
     */
    public void setOrderId(java.math.BigDecimal orderId) {
        this.orderId = orderId;
    }


    /**
     * Gets the statusId value for this Portability_RowSetPortability_Row.
     * 
     * @return statusId
     */
    public java.math.BigDecimal getStatusId() {
        return statusId;
    }


    /**
     * Sets the statusId value for this Portability_RowSetPortability_Row.
     * 
     * @param statusId
     */
    public void setStatusId(java.math.BigDecimal statusId) {
        this.statusId = statusId;
    }


    /**
     * Gets the statusName value for this Portability_RowSetPortability_Row.
     * 
     * @return statusName
     */
    public java.lang.String getStatusName() {
        return statusName;
    }


    /**
     * Sets the statusName value for this Portability_RowSetPortability_Row.
     * 
     * @param statusName
     */
    public void setStatusName(java.lang.String statusName) {
        this.statusName = statusName;
    }


    /**
     * Gets the typeId value for this Portability_RowSetPortability_Row.
     * 
     * @return typeId
     */
    public java.math.BigDecimal getTypeId() {
        return typeId;
    }


    /**
     * Sets the typeId value for this Portability_RowSetPortability_Row.
     * 
     * @param typeId
     */
    public void setTypeId(java.math.BigDecimal typeId) {
        this.typeId = typeId;
    }


    /**
     * Gets the typeName value for this Portability_RowSetPortability_Row.
     * 
     * @return typeName
     */
    public java.lang.String getTypeName() {
        return typeName;
    }


    /**
     * Sets the typeName value for this Portability_RowSetPortability_Row.
     * 
     * @param typeName
     */
    public void setTypeName(java.lang.String typeName) {
        this.typeName = typeName;
    }


    /**
     * Gets the operatorId value for this Portability_RowSetPortability_Row.
     * 
     * @return operatorId
     */
    public java.math.BigDecimal getOperatorId() {
        return operatorId;
    }


    /**
     * Sets the operatorId value for this Portability_RowSetPortability_Row.
     * 
     * @param operatorId
     */
    public void setOperatorId(java.math.BigDecimal operatorId) {
        this.operatorId = operatorId;
    }


    /**
     * Gets the operatorName value for this Portability_RowSetPortability_Row.
     * 
     * @return operatorName
     */
    public java.lang.String getOperatorName() {
        return operatorName;
    }


    /**
     * Sets the operatorName value for this Portability_RowSetPortability_Row.
     * 
     * @param operatorName
     */
    public void setOperatorName(java.lang.String operatorName) {
        this.operatorName = operatorName;
    }


    /**
     * Gets the effectiveDate value for this Portability_RowSetPortability_Row.
     * 
     * @return effectiveDate
     */
    public java.util.Calendar getEffectiveDate() {
        return effectiveDate;
    }


    /**
     * Sets the effectiveDate value for this Portability_RowSetPortability_Row.
     * 
     * @param effectiveDate
     */
    public void setEffectiveDate(java.util.Calendar effectiveDate) {
        this.effectiveDate = effectiveDate;
    }


    /**
     * Gets the cancelable value for this Portability_RowSetPortability_Row.
     * 
     * @return cancelable
     */
    public java.math.BigDecimal getCancelable() {
        return cancelable;
    }


    /**
     * Sets the cancelable value for this Portability_RowSetPortability_Row.
     * 
     * @param cancelable
     */
    public void setCancelable(java.math.BigDecimal cancelable) {
        this.cancelable = cancelable;
    }


    /**
     * Gets the creationDate value for this Portability_RowSetPortability_Row.
     * 
     * @return creationDate
     */
    public java.util.Calendar getCreationDate() {
        return creationDate;
    }


    /**
     * Sets the creationDate value for this Portability_RowSetPortability_Row.
     * 
     * @param creationDate
     */
    public void setCreationDate(java.util.Calendar creationDate) {
        this.creationDate = creationDate;
    }


    /**
     * Gets the lastUpdate value for this Portability_RowSetPortability_Row.
     * 
     * @return lastUpdate
     */
    public java.util.Calendar getLastUpdate() {
        return lastUpdate;
    }


    /**
     * Sets the lastUpdate value for this Portability_RowSetPortability_Row.
     * 
     * @param lastUpdate
     */
    public void setLastUpdate(java.util.Calendar lastUpdate) {
        this.lastUpdate = lastUpdate;
    }


    /**
     * Gets the msisdnId value for this Portability_RowSetPortability_Row.
     * 
     * @return msisdnId
     */
    public java.math.BigDecimal getMsisdnId() {
        return msisdnId;
    }


    /**
     * Sets the msisdnId value for this Portability_RowSetPortability_Row.
     * 
     * @param msisdnId
     */
    public void setMsisdnId(java.math.BigDecimal msisdnId) {
        this.msisdnId = msisdnId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Portability_RowSetPortability_Row)) return false;
        Portability_RowSetPortability_Row other = (Portability_RowSetPortability_Row) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.brandId==null && other.getBrandId()==null) || 
             (this.brandId!=null &&
              this.brandId.equals(other.getBrandId()))) &&
            ((this.orderId==null && other.getOrderId()==null) || 
             (this.orderId!=null &&
              this.orderId.equals(other.getOrderId()))) &&
            ((this.statusId==null && other.getStatusId()==null) || 
             (this.statusId!=null &&
              this.statusId.equals(other.getStatusId()))) &&
            ((this.statusName==null && other.getStatusName()==null) || 
             (this.statusName!=null &&
              this.statusName.equals(other.getStatusName()))) &&
            ((this.typeId==null && other.getTypeId()==null) || 
             (this.typeId!=null &&
              this.typeId.equals(other.getTypeId()))) &&
            ((this.typeName==null && other.getTypeName()==null) || 
             (this.typeName!=null &&
              this.typeName.equals(other.getTypeName()))) &&
            ((this.operatorId==null && other.getOperatorId()==null) || 
             (this.operatorId!=null &&
              this.operatorId.equals(other.getOperatorId()))) &&
            ((this.operatorName==null && other.getOperatorName()==null) || 
             (this.operatorName!=null &&
              this.operatorName.equals(other.getOperatorName()))) &&
            ((this.effectiveDate==null && other.getEffectiveDate()==null) || 
             (this.effectiveDate!=null &&
              this.effectiveDate.equals(other.getEffectiveDate()))) &&
            ((this.cancelable==null && other.getCancelable()==null) || 
             (this.cancelable!=null &&
              this.cancelable.equals(other.getCancelable()))) &&
            ((this.creationDate==null && other.getCreationDate()==null) || 
             (this.creationDate!=null &&
              this.creationDate.equals(other.getCreationDate()))) &&
            ((this.lastUpdate==null && other.getLastUpdate()==null) || 
             (this.lastUpdate!=null &&
              this.lastUpdate.equals(other.getLastUpdate()))) &&
            ((this.msisdnId==null && other.getMsisdnId()==null) || 
             (this.msisdnId!=null &&
              this.msisdnId.equals(other.getMsisdnId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getBrandId() != null) {
            _hashCode += getBrandId().hashCode();
        }
        if (getOrderId() != null) {
            _hashCode += getOrderId().hashCode();
        }
        if (getStatusId() != null) {
            _hashCode += getStatusId().hashCode();
        }
        if (getStatusName() != null) {
            _hashCode += getStatusName().hashCode();
        }
        if (getTypeId() != null) {
            _hashCode += getTypeId().hashCode();
        }
        if (getTypeName() != null) {
            _hashCode += getTypeName().hashCode();
        }
        if (getOperatorId() != null) {
            _hashCode += getOperatorId().hashCode();
        }
        if (getOperatorName() != null) {
            _hashCode += getOperatorName().hashCode();
        }
        if (getEffectiveDate() != null) {
            _hashCode += getEffectiveDate().hashCode();
        }
        if (getCancelable() != null) {
            _hashCode += getCancelable().hashCode();
        }
        if (getCreationDate() != null) {
            _hashCode += getCreationDate().hashCode();
        }
        if (getLastUpdate() != null) {
            _hashCode += getLastUpdate().hashCode();
        }
        if (getMsisdnId() != null) {
            _hashCode += getMsisdnId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Portability_RowSetPortability_Row.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">portability_RowSet>portability_Row"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("brandId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "brandId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "orderId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statusId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statusName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "typeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "typeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operatorId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "operatorId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operatorName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "operatorName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("effectiveDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "effectiveDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cancelable");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cancelable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "creationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lastUpdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msisdnId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msisdnId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
