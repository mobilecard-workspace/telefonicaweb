/**
 * RechargeRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.TopUpInterface;

public class RechargeRequest  implements java.io.Serializable {
    private com.addcel.ws.clientes.simfonics.TopUpInterface.Credentials credentials;

    private java.math.BigInteger mvno;

    private java.math.BigInteger msisdn;

    private java.math.BigInteger amount;

    private java.lang.String currency;

    private java.lang.String origin;

    private java.math.BigInteger creditCard;

    private java.math.BigInteger topupId;

    private java.math.BigInteger transactionTypeId;

    private java.math.BigInteger zipcode;

    private java.lang.String city;

    private java.lang.String state;

    private java.lang.String pointPurchase;

    public RechargeRequest() {
    }

    public RechargeRequest(
           com.addcel.ws.clientes.simfonics.TopUpInterface.Credentials credentials,
           java.math.BigInteger mvno,
           java.math.BigInteger msisdn,
           java.math.BigInteger amount,
           java.lang.String currency,
           java.lang.String origin,
           java.math.BigInteger creditCard,
           java.math.BigInteger topupId,
           java.math.BigInteger transactionTypeId,
           java.math.BigInteger zipcode,
           java.lang.String city,
           java.lang.String state,
           java.lang.String pointPurchase) {
           this.credentials = credentials;
           this.mvno = mvno;
           this.msisdn = msisdn;
           this.amount = amount;
           this.currency = currency;
           this.origin = origin;
           this.creditCard = creditCard;
           this.topupId = topupId;
           this.transactionTypeId = transactionTypeId;
           this.zipcode = zipcode;
           this.city = city;
           this.state = state;
           this.pointPurchase = pointPurchase;
    }


    /**
     * Gets the credentials value for this RechargeRequest.
     * 
     * @return credentials
     */
    public com.addcel.ws.clientes.simfonics.TopUpInterface.Credentials getCredentials() {
        return credentials;
    }


    /**
     * Sets the credentials value for this RechargeRequest.
     * 
     * @param credentials
     */
    public void setCredentials(com.addcel.ws.clientes.simfonics.TopUpInterface.Credentials credentials) {
        this.credentials = credentials;
    }


    /**
     * Gets the mvno value for this RechargeRequest.
     * 
     * @return mvno
     */
    public java.math.BigInteger getMvno() {
        return mvno;
    }


    /**
     * Sets the mvno value for this RechargeRequest.
     * 
     * @param mvno
     */
    public void setMvno(java.math.BigInteger mvno) {
        this.mvno = mvno;
    }


    /**
     * Gets the msisdn value for this RechargeRequest.
     * 
     * @return msisdn
     */
    public java.math.BigInteger getMsisdn() {
        return msisdn;
    }


    /**
     * Sets the msisdn value for this RechargeRequest.
     * 
     * @param msisdn
     */
    public void setMsisdn(java.math.BigInteger msisdn) {
        this.msisdn = msisdn;
    }


    /**
     * Gets the amount value for this RechargeRequest.
     * 
     * @return amount
     */
    public java.math.BigInteger getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this RechargeRequest.
     * 
     * @param amount
     */
    public void setAmount(java.math.BigInteger amount) {
        this.amount = amount;
    }


    /**
     * Gets the currency value for this RechargeRequest.
     * 
     * @return currency
     */
    public java.lang.String getCurrency() {
        return currency;
    }


    /**
     * Sets the currency value for this RechargeRequest.
     * 
     * @param currency
     */
    public void setCurrency(java.lang.String currency) {
        this.currency = currency;
    }


    /**
     * Gets the origin value for this RechargeRequest.
     * 
     * @return origin
     */
    public java.lang.String getOrigin() {
        return origin;
    }


    /**
     * Sets the origin value for this RechargeRequest.
     * 
     * @param origin
     */
    public void setOrigin(java.lang.String origin) {
        this.origin = origin;
    }


    /**
     * Gets the creditCard value for this RechargeRequest.
     * 
     * @return creditCard
     */
    public java.math.BigInteger getCreditCard() {
        return creditCard;
    }


    /**
     * Sets the creditCard value for this RechargeRequest.
     * 
     * @param creditCard
     */
    public void setCreditCard(java.math.BigInteger creditCard) {
        this.creditCard = creditCard;
    }


    /**
     * Gets the topupId value for this RechargeRequest.
     * 
     * @return topupId
     */
    public java.math.BigInteger getTopupId() {
        return topupId;
    }


    /**
     * Sets the topupId value for this RechargeRequest.
     * 
     * @param topupId
     */
    public void setTopupId(java.math.BigInteger topupId) {
        this.topupId = topupId;
    }


    /**
     * Gets the transactionTypeId value for this RechargeRequest.
     * 
     * @return transactionTypeId
     */
    public java.math.BigInteger getTransactionTypeId() {
        return transactionTypeId;
    }


    /**
     * Sets the transactionTypeId value for this RechargeRequest.
     * 
     * @param transactionTypeId
     */
    public void setTransactionTypeId(java.math.BigInteger transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }


    /**
     * Gets the zipcode value for this RechargeRequest.
     * 
     * @return zipcode
     */
    public java.math.BigInteger getZipcode() {
        return zipcode;
    }


    /**
     * Sets the zipcode value for this RechargeRequest.
     * 
     * @param zipcode
     */
    public void setZipcode(java.math.BigInteger zipcode) {
        this.zipcode = zipcode;
    }


    /**
     * Gets the city value for this RechargeRequest.
     * 
     * @return city
     */
    public java.lang.String getCity() {
        return city;
    }


    /**
     * Sets the city value for this RechargeRequest.
     * 
     * @param city
     */
    public void setCity(java.lang.String city) {
        this.city = city;
    }


    /**
     * Gets the state value for this RechargeRequest.
     * 
     * @return state
     */
    public java.lang.String getState() {
        return state;
    }


    /**
     * Sets the state value for this RechargeRequest.
     * 
     * @param state
     */
    public void setState(java.lang.String state) {
        this.state = state;
    }


    /**
     * Gets the pointPurchase value for this RechargeRequest.
     * 
     * @return pointPurchase
     */
    public java.lang.String getPointPurchase() {
        return pointPurchase;
    }


    /**
     * Sets the pointPurchase value for this RechargeRequest.
     * 
     * @param pointPurchase
     */
    public void setPointPurchase(java.lang.String pointPurchase) {
        this.pointPurchase = pointPurchase;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RechargeRequest)) return false;
        RechargeRequest other = (RechargeRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.credentials==null && other.getCredentials()==null) || 
             (this.credentials!=null &&
              this.credentials.equals(other.getCredentials()))) &&
            ((this.mvno==null && other.getMvno()==null) || 
             (this.mvno!=null &&
              this.mvno.equals(other.getMvno()))) &&
            ((this.msisdn==null && other.getMsisdn()==null) || 
             (this.msisdn!=null &&
              this.msisdn.equals(other.getMsisdn()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.currency==null && other.getCurrency()==null) || 
             (this.currency!=null &&
              this.currency.equals(other.getCurrency()))) &&
            ((this.origin==null && other.getOrigin()==null) || 
             (this.origin!=null &&
              this.origin.equals(other.getOrigin()))) &&
            ((this.creditCard==null && other.getCreditCard()==null) || 
             (this.creditCard!=null &&
              this.creditCard.equals(other.getCreditCard()))) &&
            ((this.topupId==null && other.getTopupId()==null) || 
             (this.topupId!=null &&
              this.topupId.equals(other.getTopupId()))) &&
            ((this.transactionTypeId==null && other.getTransactionTypeId()==null) || 
             (this.transactionTypeId!=null &&
              this.transactionTypeId.equals(other.getTransactionTypeId()))) &&
            ((this.zipcode==null && other.getZipcode()==null) || 
             (this.zipcode!=null &&
              this.zipcode.equals(other.getZipcode()))) &&
            ((this.city==null && other.getCity()==null) || 
             (this.city!=null &&
              this.city.equals(other.getCity()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState()))) &&
            ((this.pointPurchase==null && other.getPointPurchase()==null) || 
             (this.pointPurchase!=null &&
              this.pointPurchase.equals(other.getPointPurchase())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCredentials() != null) {
            _hashCode += getCredentials().hashCode();
        }
        if (getMvno() != null) {
            _hashCode += getMvno().hashCode();
        }
        if (getMsisdn() != null) {
            _hashCode += getMsisdn().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getCurrency() != null) {
            _hashCode += getCurrency().hashCode();
        }
        if (getOrigin() != null) {
            _hashCode += getOrigin().hashCode();
        }
        if (getCreditCard() != null) {
            _hashCode += getCreditCard().hashCode();
        }
        if (getTopupId() != null) {
            _hashCode += getTopupId().hashCode();
        }
        if (getTransactionTypeId() != null) {
            _hashCode += getTransactionTypeId().hashCode();
        }
        if (getZipcode() != null) {
            _hashCode += getZipcode().hashCode();
        }
        if (getCity() != null) {
            _hashCode += getCity().hashCode();
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        if (getPointPurchase() != null) {
            _hashCode += getPointPurchase().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RechargeRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", ">RechargeRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("credentials");
        elemField.setXmlName(new javax.xml.namespace.QName("", "credentials"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", "credentials"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mvno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mvno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msisdn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msisdn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("origin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "origin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCard");
        elemField.setXmlName(new javax.xml.namespace.QName("", "creditCard"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("topupId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "topupId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "transactionTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zipcode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Zipcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city");
        elemField.setXmlName(new javax.xml.namespace.QName("", "City"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("", "State"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointPurchase");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pointPurchase"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
