/**
 * BrandProductsTypeBrandProduct.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class BrandProductsTypeBrandProduct  implements java.io.Serializable {
    private java.math.BigInteger id;

    private java.math.BigInteger accountTypeId;

    private java.lang.String accountTypeName;

    private java.lang.String productName;

    private java.math.BigDecimal price;

    private java.math.BigInteger productTypeId;

    private java.lang.String productTypeName;

    private java.math.BigInteger subscriptionTypeId;

    private java.lang.String subscriptionTypeName;

    public BrandProductsTypeBrandProduct() {
    }

    public BrandProductsTypeBrandProduct(
           java.math.BigInteger id,
           java.math.BigInteger accountTypeId,
           java.lang.String accountTypeName,
           java.lang.String productName,
           java.math.BigDecimal price,
           java.math.BigInteger productTypeId,
           java.lang.String productTypeName,
           java.math.BigInteger subscriptionTypeId,
           java.lang.String subscriptionTypeName) {
           this.id = id;
           this.accountTypeId = accountTypeId;
           this.accountTypeName = accountTypeName;
           this.productName = productName;
           this.price = price;
           this.productTypeId = productTypeId;
           this.productTypeName = productTypeName;
           this.subscriptionTypeId = subscriptionTypeId;
           this.subscriptionTypeName = subscriptionTypeName;
    }


    /**
     * Gets the id value for this BrandProductsTypeBrandProduct.
     * 
     * @return id
     */
    public java.math.BigInteger getId() {
        return id;
    }


    /**
     * Sets the id value for this BrandProductsTypeBrandProduct.
     * 
     * @param id
     */
    public void setId(java.math.BigInteger id) {
        this.id = id;
    }


    /**
     * Gets the accountTypeId value for this BrandProductsTypeBrandProduct.
     * 
     * @return accountTypeId
     */
    public java.math.BigInteger getAccountTypeId() {
        return accountTypeId;
    }


    /**
     * Sets the accountTypeId value for this BrandProductsTypeBrandProduct.
     * 
     * @param accountTypeId
     */
    public void setAccountTypeId(java.math.BigInteger accountTypeId) {
        this.accountTypeId = accountTypeId;
    }


    /**
     * Gets the accountTypeName value for this BrandProductsTypeBrandProduct.
     * 
     * @return accountTypeName
     */
    public java.lang.String getAccountTypeName() {
        return accountTypeName;
    }


    /**
     * Sets the accountTypeName value for this BrandProductsTypeBrandProduct.
     * 
     * @param accountTypeName
     */
    public void setAccountTypeName(java.lang.String accountTypeName) {
        this.accountTypeName = accountTypeName;
    }


    /**
     * Gets the productName value for this BrandProductsTypeBrandProduct.
     * 
     * @return productName
     */
    public java.lang.String getProductName() {
        return productName;
    }


    /**
     * Sets the productName value for this BrandProductsTypeBrandProduct.
     * 
     * @param productName
     */
    public void setProductName(java.lang.String productName) {
        this.productName = productName;
    }


    /**
     * Gets the price value for this BrandProductsTypeBrandProduct.
     * 
     * @return price
     */
    public java.math.BigDecimal getPrice() {
        return price;
    }


    /**
     * Sets the price value for this BrandProductsTypeBrandProduct.
     * 
     * @param price
     */
    public void setPrice(java.math.BigDecimal price) {
        this.price = price;
    }


    /**
     * Gets the productTypeId value for this BrandProductsTypeBrandProduct.
     * 
     * @return productTypeId
     */
    public java.math.BigInteger getProductTypeId() {
        return productTypeId;
    }


    /**
     * Sets the productTypeId value for this BrandProductsTypeBrandProduct.
     * 
     * @param productTypeId
     */
    public void setProductTypeId(java.math.BigInteger productTypeId) {
        this.productTypeId = productTypeId;
    }


    /**
     * Gets the productTypeName value for this BrandProductsTypeBrandProduct.
     * 
     * @return productTypeName
     */
    public java.lang.String getProductTypeName() {
        return productTypeName;
    }


    /**
     * Sets the productTypeName value for this BrandProductsTypeBrandProduct.
     * 
     * @param productTypeName
     */
    public void setProductTypeName(java.lang.String productTypeName) {
        this.productTypeName = productTypeName;
    }


    /**
     * Gets the subscriptionTypeId value for this BrandProductsTypeBrandProduct.
     * 
     * @return subscriptionTypeId
     */
    public java.math.BigInteger getSubscriptionTypeId() {
        return subscriptionTypeId;
    }


    /**
     * Sets the subscriptionTypeId value for this BrandProductsTypeBrandProduct.
     * 
     * @param subscriptionTypeId
     */
    public void setSubscriptionTypeId(java.math.BigInteger subscriptionTypeId) {
        this.subscriptionTypeId = subscriptionTypeId;
    }


    /**
     * Gets the subscriptionTypeName value for this BrandProductsTypeBrandProduct.
     * 
     * @return subscriptionTypeName
     */
    public java.lang.String getSubscriptionTypeName() {
        return subscriptionTypeName;
    }


    /**
     * Sets the subscriptionTypeName value for this BrandProductsTypeBrandProduct.
     * 
     * @param subscriptionTypeName
     */
    public void setSubscriptionTypeName(java.lang.String subscriptionTypeName) {
        this.subscriptionTypeName = subscriptionTypeName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BrandProductsTypeBrandProduct)) return false;
        BrandProductsTypeBrandProduct other = (BrandProductsTypeBrandProduct) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.accountTypeId==null && other.getAccountTypeId()==null) || 
             (this.accountTypeId!=null &&
              this.accountTypeId.equals(other.getAccountTypeId()))) &&
            ((this.accountTypeName==null && other.getAccountTypeName()==null) || 
             (this.accountTypeName!=null &&
              this.accountTypeName.equals(other.getAccountTypeName()))) &&
            ((this.productName==null && other.getProductName()==null) || 
             (this.productName!=null &&
              this.productName.equals(other.getProductName()))) &&
            ((this.price==null && other.getPrice()==null) || 
             (this.price!=null &&
              this.price.equals(other.getPrice()))) &&
            ((this.productTypeId==null && other.getProductTypeId()==null) || 
             (this.productTypeId!=null &&
              this.productTypeId.equals(other.getProductTypeId()))) &&
            ((this.productTypeName==null && other.getProductTypeName()==null) || 
             (this.productTypeName!=null &&
              this.productTypeName.equals(other.getProductTypeName()))) &&
            ((this.subscriptionTypeId==null && other.getSubscriptionTypeId()==null) || 
             (this.subscriptionTypeId!=null &&
              this.subscriptionTypeId.equals(other.getSubscriptionTypeId()))) &&
            ((this.subscriptionTypeName==null && other.getSubscriptionTypeName()==null) || 
             (this.subscriptionTypeName!=null &&
              this.subscriptionTypeName.equals(other.getSubscriptionTypeName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getAccountTypeId() != null) {
            _hashCode += getAccountTypeId().hashCode();
        }
        if (getAccountTypeName() != null) {
            _hashCode += getAccountTypeName().hashCode();
        }
        if (getProductName() != null) {
            _hashCode += getProductName().hashCode();
        }
        if (getPrice() != null) {
            _hashCode += getPrice().hashCode();
        }
        if (getProductTypeId() != null) {
            _hashCode += getProductTypeId().hashCode();
        }
        if (getProductTypeName() != null) {
            _hashCode += getProductTypeName().hashCode();
        }
        if (getSubscriptionTypeId() != null) {
            _hashCode += getSubscriptionTypeId().hashCode();
        }
        if (getSubscriptionTypeName() != null) {
            _hashCode += getSubscriptionTypeName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BrandProductsTypeBrandProduct.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">brandProductsType>brandProduct"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accountTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accountTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("price");
        elemField.setXmlName(new javax.xml.namespace.QName("", "price"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriptionTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriptionTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
