/**
 * PortabilityOperatorNamesTypesPortabilityOperator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class PortabilityOperatorNamesTypesPortabilityOperator  implements java.io.Serializable {
    private java.math.BigDecimal portabilityOperatorId;

    private java.lang.String portabilityOperatorName;

    public PortabilityOperatorNamesTypesPortabilityOperator() {
    }

    public PortabilityOperatorNamesTypesPortabilityOperator(
           java.math.BigDecimal portabilityOperatorId,
           java.lang.String portabilityOperatorName) {
           this.portabilityOperatorId = portabilityOperatorId;
           this.portabilityOperatorName = portabilityOperatorName;
    }


    /**
     * Gets the portabilityOperatorId value for this PortabilityOperatorNamesTypesPortabilityOperator.
     * 
     * @return portabilityOperatorId
     */
    public java.math.BigDecimal getPortabilityOperatorId() {
        return portabilityOperatorId;
    }


    /**
     * Sets the portabilityOperatorId value for this PortabilityOperatorNamesTypesPortabilityOperator.
     * 
     * @param portabilityOperatorId
     */
    public void setPortabilityOperatorId(java.math.BigDecimal portabilityOperatorId) {
        this.portabilityOperatorId = portabilityOperatorId;
    }


    /**
     * Gets the portabilityOperatorName value for this PortabilityOperatorNamesTypesPortabilityOperator.
     * 
     * @return portabilityOperatorName
     */
    public java.lang.String getPortabilityOperatorName() {
        return portabilityOperatorName;
    }


    /**
     * Sets the portabilityOperatorName value for this PortabilityOperatorNamesTypesPortabilityOperator.
     * 
     * @param portabilityOperatorName
     */
    public void setPortabilityOperatorName(java.lang.String portabilityOperatorName) {
        this.portabilityOperatorName = portabilityOperatorName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PortabilityOperatorNamesTypesPortabilityOperator)) return false;
        PortabilityOperatorNamesTypesPortabilityOperator other = (PortabilityOperatorNamesTypesPortabilityOperator) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.portabilityOperatorId==null && other.getPortabilityOperatorId()==null) || 
             (this.portabilityOperatorId!=null &&
              this.portabilityOperatorId.equals(other.getPortabilityOperatorId()))) &&
            ((this.portabilityOperatorName==null && other.getPortabilityOperatorName()==null) || 
             (this.portabilityOperatorName!=null &&
              this.portabilityOperatorName.equals(other.getPortabilityOperatorName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPortabilityOperatorId() != null) {
            _hashCode += getPortabilityOperatorId().hashCode();
        }
        if (getPortabilityOperatorName() != null) {
            _hashCode += getPortabilityOperatorName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PortabilityOperatorNamesTypesPortabilityOperator.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">PortabilityOperatorNamesTypes>PortabilityOperator"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("portabilityOperatorId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PortabilityOperatorId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("portabilityOperatorName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PortabilityOperatorName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
