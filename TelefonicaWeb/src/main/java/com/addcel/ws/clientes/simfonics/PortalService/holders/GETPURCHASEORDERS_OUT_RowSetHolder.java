/**
 * GETPURCHASEORDERS_OUT_RowSetHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService.holders;

public final class GETPURCHASEORDERS_OUT_RowSetHolder implements javax.xml.rpc.holders.Holder {
    public com.addcel.ws.clientes.simfonics.PortalService.GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row[] value;

    public GETPURCHASEORDERS_OUT_RowSetHolder() {
    }

    public GETPURCHASEORDERS_OUT_RowSetHolder(com.addcel.ws.clientes.simfonics.PortalService.GETPURCHASEORDERS_OUT_RowSetGETPURCHASEORDERS_OUT_Row[] value) {
        this.value = value;
    }

}
