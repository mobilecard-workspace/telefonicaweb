/**
 * InterfaceTPSOAPQSService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.TopUpInterface;

public interface InterfaceTPSOAPQSService extends javax.xml.rpc.Service {
    public java.lang.String getInterfaceTPSOAPQSPortAddress();

    public com.addcel.ws.clientes.simfonics.TopUpInterface.InterfaceTP getInterfaceTPSOAPQSPort() throws javax.xml.rpc.ServiceException;

    public com.addcel.ws.clientes.simfonics.TopUpInterface.InterfaceTP getInterfaceTPSOAPQSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
