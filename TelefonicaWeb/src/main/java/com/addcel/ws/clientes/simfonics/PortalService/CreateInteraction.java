/**
 * CreateInteraction.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class CreateInteraction  implements java.io.Serializable {
    private java.lang.String sessionId;

    private java.lang.String country;

    private java.math.BigInteger brandId;

    private java.math.BigInteger incidentId;

    private java.math.BigInteger interactionTypeId;

    private java.lang.String userName;

    private java.lang.String interactionText;

    public CreateInteraction() {
    }

    public CreateInteraction(
           java.lang.String sessionId,
           java.lang.String country,
           java.math.BigInteger brandId,
           java.math.BigInteger incidentId,
           java.math.BigInteger interactionTypeId,
           java.lang.String userName,
           java.lang.String interactionText) {
           this.sessionId = sessionId;
           this.country = country;
           this.brandId = brandId;
           this.incidentId = incidentId;
           this.interactionTypeId = interactionTypeId;
           this.userName = userName;
           this.interactionText = interactionText;
    }


    /**
     * Gets the sessionId value for this CreateInteraction.
     * 
     * @return sessionId
     */
    public java.lang.String getSessionId() {
        return sessionId;
    }


    /**
     * Sets the sessionId value for this CreateInteraction.
     * 
     * @param sessionId
     */
    public void setSessionId(java.lang.String sessionId) {
        this.sessionId = sessionId;
    }


    /**
     * Gets the country value for this CreateInteraction.
     * 
     * @return country
     */
    public java.lang.String getCountry() {
        return country;
    }


    /**
     * Sets the country value for this CreateInteraction.
     * 
     * @param country
     */
    public void setCountry(java.lang.String country) {
        this.country = country;
    }


    /**
     * Gets the brandId value for this CreateInteraction.
     * 
     * @return brandId
     */
    public java.math.BigInteger getBrandId() {
        return brandId;
    }


    /**
     * Sets the brandId value for this CreateInteraction.
     * 
     * @param brandId
     */
    public void setBrandId(java.math.BigInteger brandId) {
        this.brandId = brandId;
    }


    /**
     * Gets the incidentId value for this CreateInteraction.
     * 
     * @return incidentId
     */
    public java.math.BigInteger getIncidentId() {
        return incidentId;
    }


    /**
     * Sets the incidentId value for this CreateInteraction.
     * 
     * @param incidentId
     */
    public void setIncidentId(java.math.BigInteger incidentId) {
        this.incidentId = incidentId;
    }


    /**
     * Gets the interactionTypeId value for this CreateInteraction.
     * 
     * @return interactionTypeId
     */
    public java.math.BigInteger getInteractionTypeId() {
        return interactionTypeId;
    }


    /**
     * Sets the interactionTypeId value for this CreateInteraction.
     * 
     * @param interactionTypeId
     */
    public void setInteractionTypeId(java.math.BigInteger interactionTypeId) {
        this.interactionTypeId = interactionTypeId;
    }


    /**
     * Gets the userName value for this CreateInteraction.
     * 
     * @return userName
     */
    public java.lang.String getUserName() {
        return userName;
    }


    /**
     * Sets the userName value for this CreateInteraction.
     * 
     * @param userName
     */
    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }


    /**
     * Gets the interactionText value for this CreateInteraction.
     * 
     * @return interactionText
     */
    public java.lang.String getInteractionText() {
        return interactionText;
    }


    /**
     * Sets the interactionText value for this CreateInteraction.
     * 
     * @param interactionText
     */
    public void setInteractionText(java.lang.String interactionText) {
        this.interactionText = interactionText;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreateInteraction)) return false;
        CreateInteraction other = (CreateInteraction) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sessionId==null && other.getSessionId()==null) || 
             (this.sessionId!=null &&
              this.sessionId.equals(other.getSessionId()))) &&
            ((this.country==null && other.getCountry()==null) || 
             (this.country!=null &&
              this.country.equals(other.getCountry()))) &&
            ((this.brandId==null && other.getBrandId()==null) || 
             (this.brandId!=null &&
              this.brandId.equals(other.getBrandId()))) &&
            ((this.incidentId==null && other.getIncidentId()==null) || 
             (this.incidentId!=null &&
              this.incidentId.equals(other.getIncidentId()))) &&
            ((this.interactionTypeId==null && other.getInteractionTypeId()==null) || 
             (this.interactionTypeId!=null &&
              this.interactionTypeId.equals(other.getInteractionTypeId()))) &&
            ((this.userName==null && other.getUserName()==null) || 
             (this.userName!=null &&
              this.userName.equals(other.getUserName()))) &&
            ((this.interactionText==null && other.getInteractionText()==null) || 
             (this.interactionText!=null &&
              this.interactionText.equals(other.getInteractionText())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSessionId() != null) {
            _hashCode += getSessionId().hashCode();
        }
        if (getCountry() != null) {
            _hashCode += getCountry().hashCode();
        }
        if (getBrandId() != null) {
            _hashCode += getBrandId().hashCode();
        }
        if (getIncidentId() != null) {
            _hashCode += getIncidentId().hashCode();
        }
        if (getInteractionTypeId() != null) {
            _hashCode += getInteractionTypeId().hashCode();
        }
        if (getUserName() != null) {
            _hashCode += getUserName().hashCode();
        }
        if (getInteractionText() != null) {
            _hashCode += getInteractionText().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreateInteraction.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">createInteraction"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sessionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country");
        elemField.setXmlName(new javax.xml.namespace.QName("", "country"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("brandId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "brandId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("incidentId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "incidentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interactionTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "interactionTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "userName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interactionText");
        elemField.setXmlName(new javax.xml.namespace.QName("", "interactionText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
