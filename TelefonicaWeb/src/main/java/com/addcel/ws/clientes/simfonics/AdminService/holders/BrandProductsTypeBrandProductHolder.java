/**
 * BrandProductsTypeBrandProductHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService.holders;

public final class BrandProductsTypeBrandProductHolder implements javax.xml.rpc.holders.Holder {
    public com.addcel.ws.clientes.simfonics.AdminService.BrandProductsTypeBrandProduct value;

    public BrandProductsTypeBrandProductHolder() {
    }

    public BrandProductsTypeBrandProductHolder(com.addcel.ws.clientes.simfonics.AdminService.BrandProductsTypeBrandProduct value) {
        this.value = value;
    }

}
