/**
 * BrandProductsTypeHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService.holders;

public final class BrandProductsTypeHolder implements javax.xml.rpc.holders.Holder {
    public com.addcel.ws.clientes.simfonics.AdminService.BrandProductsTypeBrandProduct[] value;

    public BrandProductsTypeHolder() {
    }

    public BrandProductsTypeHolder(com.addcel.ws.clientes.simfonics.AdminService.BrandProductsTypeBrandProduct[] value) {
        this.value = value;
    }

}
