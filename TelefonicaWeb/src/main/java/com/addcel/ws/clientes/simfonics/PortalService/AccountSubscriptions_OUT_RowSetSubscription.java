/**
 * AccountSubscriptions_OUT_RowSetSubscription.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class AccountSubscriptions_OUT_RowSetSubscription  implements java.io.Serializable {
    private java.lang.String msisdn;

    private java.lang.String imsi;

    private java.lang.String iccId;

    private java.lang.String handSet;

    private java.lang.String subscriptionTypeName;

    private java.math.BigInteger subscriptionType;

    private java.math.BigInteger subscriptionStatus;

    private java.lang.String subscriptionStatusName;

    private com.addcel.ws.clientes.simfonics.PortalService.CustomerProductsFU_OUT_RowSetCustomerProductsFU_OUT_Row[] subscriptionProducts;

    public AccountSubscriptions_OUT_RowSetSubscription() {
    }

    public AccountSubscriptions_OUT_RowSetSubscription(
           java.lang.String msisdn,
           java.lang.String imsi,
           java.lang.String iccId,
           java.lang.String handSet,
           java.lang.String subscriptionTypeName,
           java.math.BigInteger subscriptionType,
           java.math.BigInteger subscriptionStatus,
           java.lang.String subscriptionStatusName,
           com.addcel.ws.clientes.simfonics.PortalService.CustomerProductsFU_OUT_RowSetCustomerProductsFU_OUT_Row[] subscriptionProducts) {
           this.msisdn = msisdn;
           this.imsi = imsi;
           this.iccId = iccId;
           this.handSet = handSet;
           this.subscriptionTypeName = subscriptionTypeName;
           this.subscriptionType = subscriptionType;
           this.subscriptionStatus = subscriptionStatus;
           this.subscriptionStatusName = subscriptionStatusName;
           this.subscriptionProducts = subscriptionProducts;
    }


    /**
     * Gets the msisdn value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @return msisdn
     */
    public java.lang.String getMsisdn() {
        return msisdn;
    }


    /**
     * Sets the msisdn value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @param msisdn
     */
    public void setMsisdn(java.lang.String msisdn) {
        this.msisdn = msisdn;
    }


    /**
     * Gets the imsi value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @return imsi
     */
    public java.lang.String getImsi() {
        return imsi;
    }


    /**
     * Sets the imsi value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @param imsi
     */
    public void setImsi(java.lang.String imsi) {
        this.imsi = imsi;
    }


    /**
     * Gets the iccId value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @return iccId
     */
    public java.lang.String getIccId() {
        return iccId;
    }


    /**
     * Sets the iccId value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @param iccId
     */
    public void setIccId(java.lang.String iccId) {
        this.iccId = iccId;
    }


    /**
     * Gets the handSet value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @return handSet
     */
    public java.lang.String getHandSet() {
        return handSet;
    }


    /**
     * Sets the handSet value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @param handSet
     */
    public void setHandSet(java.lang.String handSet) {
        this.handSet = handSet;
    }


    /**
     * Gets the subscriptionTypeName value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @return subscriptionTypeName
     */
    public java.lang.String getSubscriptionTypeName() {
        return subscriptionTypeName;
    }


    /**
     * Sets the subscriptionTypeName value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @param subscriptionTypeName
     */
    public void setSubscriptionTypeName(java.lang.String subscriptionTypeName) {
        this.subscriptionTypeName = subscriptionTypeName;
    }


    /**
     * Gets the subscriptionType value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @return subscriptionType
     */
    public java.math.BigInteger getSubscriptionType() {
        return subscriptionType;
    }


    /**
     * Sets the subscriptionType value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @param subscriptionType
     */
    public void setSubscriptionType(java.math.BigInteger subscriptionType) {
        this.subscriptionType = subscriptionType;
    }


    /**
     * Gets the subscriptionStatus value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @return subscriptionStatus
     */
    public java.math.BigInteger getSubscriptionStatus() {
        return subscriptionStatus;
    }


    /**
     * Sets the subscriptionStatus value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @param subscriptionStatus
     */
    public void setSubscriptionStatus(java.math.BigInteger subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }


    /**
     * Gets the subscriptionStatusName value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @return subscriptionStatusName
     */
    public java.lang.String getSubscriptionStatusName() {
        return subscriptionStatusName;
    }


    /**
     * Sets the subscriptionStatusName value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @param subscriptionStatusName
     */
    public void setSubscriptionStatusName(java.lang.String subscriptionStatusName) {
        this.subscriptionStatusName = subscriptionStatusName;
    }


    /**
     * Gets the subscriptionProducts value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @return subscriptionProducts
     */
    public com.addcel.ws.clientes.simfonics.PortalService.CustomerProductsFU_OUT_RowSetCustomerProductsFU_OUT_Row[] getSubscriptionProducts() {
        return subscriptionProducts;
    }


    /**
     * Sets the subscriptionProducts value for this AccountSubscriptions_OUT_RowSetSubscription.
     * 
     * @param subscriptionProducts
     */
    public void setSubscriptionProducts(com.addcel.ws.clientes.simfonics.PortalService.CustomerProductsFU_OUT_RowSetCustomerProductsFU_OUT_Row[] subscriptionProducts) {
        this.subscriptionProducts = subscriptionProducts;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AccountSubscriptions_OUT_RowSetSubscription)) return false;
        AccountSubscriptions_OUT_RowSetSubscription other = (AccountSubscriptions_OUT_RowSetSubscription) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.msisdn==null && other.getMsisdn()==null) || 
             (this.msisdn!=null &&
              this.msisdn.equals(other.getMsisdn()))) &&
            ((this.imsi==null && other.getImsi()==null) || 
             (this.imsi!=null &&
              this.imsi.equals(other.getImsi()))) &&
            ((this.iccId==null && other.getIccId()==null) || 
             (this.iccId!=null &&
              this.iccId.equals(other.getIccId()))) &&
            ((this.handSet==null && other.getHandSet()==null) || 
             (this.handSet!=null &&
              this.handSet.equals(other.getHandSet()))) &&
            ((this.subscriptionTypeName==null && other.getSubscriptionTypeName()==null) || 
             (this.subscriptionTypeName!=null &&
              this.subscriptionTypeName.equals(other.getSubscriptionTypeName()))) &&
            ((this.subscriptionType==null && other.getSubscriptionType()==null) || 
             (this.subscriptionType!=null &&
              this.subscriptionType.equals(other.getSubscriptionType()))) &&
            ((this.subscriptionStatus==null && other.getSubscriptionStatus()==null) || 
             (this.subscriptionStatus!=null &&
              this.subscriptionStatus.equals(other.getSubscriptionStatus()))) &&
            ((this.subscriptionStatusName==null && other.getSubscriptionStatusName()==null) || 
             (this.subscriptionStatusName!=null &&
              this.subscriptionStatusName.equals(other.getSubscriptionStatusName()))) &&
            ((this.subscriptionProducts==null && other.getSubscriptionProducts()==null) || 
             (this.subscriptionProducts!=null &&
              java.util.Arrays.equals(this.subscriptionProducts, other.getSubscriptionProducts())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMsisdn() != null) {
            _hashCode += getMsisdn().hashCode();
        }
        if (getImsi() != null) {
            _hashCode += getImsi().hashCode();
        }
        if (getIccId() != null) {
            _hashCode += getIccId().hashCode();
        }
        if (getHandSet() != null) {
            _hashCode += getHandSet().hashCode();
        }
        if (getSubscriptionTypeName() != null) {
            _hashCode += getSubscriptionTypeName().hashCode();
        }
        if (getSubscriptionType() != null) {
            _hashCode += getSubscriptionType().hashCode();
        }
        if (getSubscriptionStatus() != null) {
            _hashCode += getSubscriptionStatus().hashCode();
        }
        if (getSubscriptionStatusName() != null) {
            _hashCode += getSubscriptionStatusName().hashCode();
        }
        if (getSubscriptionProducts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSubscriptionProducts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSubscriptionProducts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AccountSubscriptions_OUT_RowSetSubscription.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">accountSubscriptions_OUT_RowSet>subscription"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msisdn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msisdn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imsi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "imsi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("iccId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "iccId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("handSet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "handSet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriptionTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriptionType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriptionStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionStatusName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriptionStatusName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionProducts");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriptionProducts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">customerProductsFU_OUT_RowSet>customerProductsFU_OUT_Row"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "customerProductsFU_OUT_Row"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
