package com.addcel.ws.clientes.simfonics.PortalService;

public class PortalServiceProxy implements com.addcel.ws.clientes.simfonics.PortalService.PortalService {
  private String _endpoint = null;
  private com.addcel.ws.clientes.simfonics.PortalService.PortalService portalService = null;
  
  public PortalServiceProxy() {
    _initPortalServiceProxy();
  }
  
  public PortalServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initPortalServiceProxy();
  }
  
  private void _initPortalServiceProxy() {
    try {
      portalService = (new com.addcel.ws.clientes.simfonics.PortalService.PortalServiceSOAPQSServiceLocator()).getPortalServiceSOAPQSPort();
      if (portalService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)portalService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)portalService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (portalService != null)
      ((javax.xml.rpc.Stub)portalService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.PortalService getPortalService() {
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService;
  }
  
  public void getPurchaseOrders(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.lang.String lang, java.math.BigInteger subscriptionId, java.math.BigInteger productId, java.math.BigInteger productTypeId, java.math.BigInteger statusId, java.util.Date startDate, java.util.Date endDate, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.PortalService.holders.GETPURCHASEORDERS_OUT_RowSetHolder resultGetPurchaseOrders) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getPurchaseOrders(sessionId, country, brandId, lang, subscriptionId, productId, productTypeId, statusId, startDate, endDate, result, resultMessage, resultGetPurchaseOrders);
  }
  
  public void getRechargeOrders(java.lang.String sessionId, java.lang.String country, int brandId, java.lang.String lang, java.math.BigInteger subscriptionId, java.math.BigInteger statusId, java.util.Date startDate, java.util.Date endDate, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.PortalService.holders.GETRECHARGEORDERS_OUT_RowSetHolder resultGetRechargeOrders) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getRechargeOrders(sessionId, country, brandId, lang, subscriptionId, statusId, startDate, endDate, result, resultMessage, resultGetRechargeOrders);
  }
  
  public void addSubscription(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger msisdn, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.addSubscription(sessionId, country, brandId, msisdn, result, resultMessage);
  }
  
  public void addRememberToken(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.lang.String token, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.addRememberToken(sessionId, country, brandId, token, result, resultMessage);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.LoginRememberTokenResponse loginRememberToken(com.addcel.ws.clientes.simfonics.PortalService.LoginRememberToken loginRememberTokenInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.loginRememberToken(loginRememberTokenInput);
  }
  
  public void getOrderItems(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, int orderId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.PortalService.holders.GetOrderItemsTypeHolder resultGetOrderItems) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getOrderItems(sessionId, country, brandId, orderId, language, result, resultMessage, resultGetOrderItems);
  }
  
  public void getOrders(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.lang.Integer catalogId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.PortalService.holders.GetOrdersTypeHolder resultGetOrders) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getOrders(sessionId, country, brandId, catalogId, language, result, resultMessage, resultGetOrders);
  }
  
  public void unlockResource(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger resourceId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.unlockResource(sessionId, country, brandId, resourceId, result, resultMessage);
  }
  
  public void getMsisdnByDDN(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger ddn, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.BigIntegerHolder msisdn, javax.xml.rpc.holders.BigIntegerHolder msisdnId) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getMsisdnByDDN(sessionId, country, brandId, ddn, result, resultMessage, msisdn, msisdnId);
  }
  
  public void validateIcc(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.lang.String icc, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.BigIntegerHolder resourceId) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.validateIcc(sessionId, country, brandId, icc, result, resultMessage, resourceId);
  }
  
  public void activateSubscription(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger msisdnId, java.math.BigInteger iccId, java.util.Date birthDate, java.lang.String identityNum, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.activateSubscription(sessionId, country, brandId, msisdnId, iccId, birthDate, identityNum, result, resultMessage);
  }
  
  public void socialLogin(java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger networkType, java.lang.String token, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.StringHolder sessionId, javax.xml.rpc.holders.BigIntegerHolder customerId) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.socialLogin(country, brandId, networkType, token, result, resultMessage, sessionId, customerId);
  }
  
  public void topupValidation(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger msisdn, java.math.BigDecimal amount, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.topupValidation(sessionId, country, brandId, msisdn, amount, result, resultMessage);
  }
  
  public void getIncidents(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.PortalService.holders.GetIncidentsTypeHolder getIncidents) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getIncidents(sessionId, country, brandId, language, result, resultMessage, getIncidents);
  }
  
  public void getInteractions(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.lang.String language, java.math.BigInteger incidentId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.PortalService.holders.GetInteractionsTypeHolder getInteractions) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getInteractions(sessionId, country, brandId, language, incidentId, result, resultMessage, getInteractions);
  }
  
  public void createIncident(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger incidentPriorityId, java.math.BigInteger incidentTypeId, java.math.BigInteger incidentStatusId, java.lang.String summary, java.lang.String description, java.math.BigInteger incidentCategoryId, java.math.BigInteger incidentSubCategoryId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.BigIntegerHolder incidentId) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.createIncident(sessionId, country, brandId, incidentPriorityId, incidentTypeId, incidentStatusId, summary, description, incidentCategoryId, incidentSubCategoryId, result, resultMessage, incidentId);
  }
  
  public void createInteraction(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger incidentId, java.math.BigInteger interactionTypeId, java.lang.String userName, java.lang.String interactionText, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.BigIntegerHolder resultAddInteraction) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.createInteraction(sessionId, country, brandId, incidentId, interactionTypeId, userName, interactionText, result, resultMessage, resultAddInteraction);
  }
  
  public void getCustomerOrdersByCustomerId(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger customerId, java.math.BigInteger catalogId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.PortalService.holders.GetCustomerOrdersByCustomerIdTypeHolder getCustomerOrdersByCustomerId) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getCustomerOrdersByCustomerId(sessionId, country, brandId, customerId, catalogId, result, resultMessage, getCustomerOrdersByCustomerId);
  }
  
  public void changeUserAccount(java.lang.String sessionId, java.lang.String country, java.math.BigInteger customerId, java.lang.String currentUserName, java.lang.String currentPassword, java.lang.String newUserName, java.lang.String newPassword, java.lang.String newSecurityQuestion, java.lang.String newSecurityQuestionAns, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.changeUserAccount(sessionId, country, customerId, currentUserName, currentPassword, newUserName, newPassword, newSecurityQuestion, newSecurityQuestionAns, result, resultMessage);
  }
  
  public void getCustomerProperties(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.PortalService.holders.CustomerTypeHolder getCustomerProperties) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getCustomerProperties(sessionId, country, brandId, language, result, resultMessage, getCustomerProperties);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.LoginResponse login(com.addcel.ws.clientes.simfonics.PortalService.Login loginInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.login(loginInput);
  }
  
  public void logout(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.logout(sessionId, country, brandId, result, resultMessage);
  }
  
  public void setCustomerProperties(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, com.addcel.ws.clientes.simfonics.PortalService.CustomerPropertyType[] customerProperties, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.setCustomerProperties(sessionId, country, brandId, customerProperties, result, resultMessage);
  }
  
  public void getCustomerOrdersBySubscriptionId(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger subscriptionId, java.math.BigInteger catalogId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.PortalService.holders.GetCustomerOrdersBySubscriptionIdTypeHolder getCustomerOrdersBySubscriptionId) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getCustomerOrdersBySubscriptionId(sessionId, country, brandId, subscriptionId, catalogId, result, resultMessage, getCustomerOrdersBySubscriptionId);
  }
  
  public void addBalanceByMsisdn(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger msisdn, java.math.BigDecimal amount, java.lang.String paymentReference, java.math.BigInteger productId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.BigDecimalHolder balance, javax.xml.rpc.holders.StringHolder currency) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.addBalanceByMsisdn(sessionId, country, brandId, msisdn, amount, paymentReference, productId, result, resultMessage, balance, currency);
  }
  
  public void getBalanceByMsisdn(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger msisdn, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.StringHolder currency, javax.xml.rpc.holders.BigDecimalHolder balance, javax.xml.rpc.holders.CalendarHolder balanceExpirationDate, javax.xml.rpc.holders.StringHolder balanceStatus) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getBalanceByMsisdn(sessionId, country, brandId, msisdn, language, result, resultMessage, currency, balance, balanceExpirationDate, balanceStatus);
  }
  
  public void addVoucherByMsisdn(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger msisdn, java.lang.String voucherPinCode, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.BigDecimalHolder balance, javax.xml.rpc.holders.StringHolder currency) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.addVoucherByMsisdn(sessionId, country, brandId, msisdn, voucherPinCode, result, resultMessage, balance, currency);
  }
  
  public void getActiveServices(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger msisdn, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getActiveServices(sessionId, country, brandId, msisdn, result, resultMessage);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.GetAvailableProductsResponse getAvailableProducts(com.addcel.ws.clientes.simfonics.PortalService.GetAvailableProductsRequest getAvailableProductsInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.getAvailableProducts(getAvailableProductsInput);
  }
  
  public void getServiceDetails(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger id, java.math.BigInteger msisdn, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getServiceDetails(sessionId, country, brandId, id, msisdn, result, resultMessage);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.GetProductDetailsResponse getProductDetails(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger id, java.math.BigInteger msisdn) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.getProductDetails(sessionId, country, brandId, id, msisdn);
  }
  
  public void activateService(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger serviceId, java.math.BigInteger subscriptionId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.activateService(sessionId, country, brandId, serviceId, subscriptionId, result, resultMessage);
  }
  
  public void deactivateService(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger serviceId, java.math.BigInteger subscriptionId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.deactivateService(sessionId, country, brandId, serviceId, subscriptionId, result, resultMessage);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.PurchaseProductResponse purchaseProduct(com.addcel.ws.clientes.simfonics.PortalService.PurchaseProduct purchaseProductInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.purchaseProduct(purchaseProductInput);
  }
  
  public void unblockSubscription(java.lang.String sessionId, java.lang.String country, int brandId, java.math.BigInteger subscriptionId, java.lang.String iccId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.unblockSubscription(sessionId, country, brandId, subscriptionId, iccId, result, resultMessage);
  }
  
  public void blockSubscription(java.lang.String sessionId, java.lang.String country, int brandId, java.math.BigInteger subscriptionId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.blockSubscription(sessionId, country, brandId, subscriptionId, result, resultMessage);
  }
  
  public void changeUserPassword(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.lang.String newPassword, java.lang.String oldPassword, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.changeUserPassword(sessionId, country, brandId, newPassword, oldPassword, result, resultMessage);
  }
  
  public void getAccountTypesIdByBrand(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getAccountTypesIdByBrand(sessionId, country, brandId, result, resultMessage);
  }
  
  public void getSubscriptionTypesByBrand(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getSubscriptionTypesByBrand(sessionId, country, brandId, result, resultMessage);
  }
  
  public void getProductFollowUpInfo(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.lang.String language, java.math.BigInteger msisdn, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.PortalService.holders.ProductFUPInfo_RowSetHolder resultGetProductFollowUpInfo) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getProductFollowUpInfo(sessionId, country, brandId, language, msisdn, result, resultMessage, resultGetProductFollowUpInfo);
  }
  
  public void getCustomerFullByCustomerId(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.lang.String lang, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.PortalService.holders.CustomerTypeHolder resultGetCustomerFullByCustomerId) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getCustomerFullByCustomerId(sessionId, country, brandId, lang, result, resultMessage, resultGetCustomerFullByCustomerId);
  }
  
  public void getSubscriptionProductsFUInfo(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.lang.String language, java.math.BigInteger subscriptionId, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.PortalService.holders.SubscriptionInfoFUProducts_RowSetHolder resultGetSubscriptionProductsFU) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getSubscriptionProductsFUInfo(sessionId, country, brandId, language, subscriptionId, result, resultMessage, resultGetSubscriptionProductsFU);
  }
  
  public void getUsage(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.lang.String language, java.math.BigInteger subscriptionId, java.math.BigInteger usageType, java.util.Calendar startDate, java.util.Calendar endDate, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.PortalService.holders.GetUsageTypeHolder getUsage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getUsage(sessionId, country, brandId, language, subscriptionId, usageType, startDate, endDate, result, resultMessage, getUsage);
  }
  
  public void doPayment(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.lang.String language, java.math.BigDecimal amount, java.lang.String currency, java.math.BigInteger paymentOperationId, java.math.BigInteger paymentTypeId, java.lang.String description, com.addcel.ws.clientes.simfonics.PortalService.CreditCardType creditCard, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.StringHolder transactionResult, javax.xml.rpc.holders.StringHolder transactionOrder, javax.xml.rpc.holders.StringHolder transactionCode, javax.xml.rpc.holders.StringHolder transactionDate, javax.xml.rpc.holders.StringHolder transactionReference, javax.xml.rpc.holders.StringHolder transactionExternalReference) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.doPayment(sessionId, country, brandId, language, amount, currency, paymentOperationId, paymentTypeId, description, creditCard, result, resultMessage, transactionResult, transactionOrder, transactionCode, transactionDate, transactionReference, transactionExternalReference);
  }
  
  public void addPaymentToken(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.lang.String language, java.lang.String name, java.math.BigInteger paymentMethod, java.lang.String number, java.lang.String expirationDate, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.addPaymentToken(sessionId, country, brandId, language, name, paymentMethod, number, expirationDate, result, resultMessage);
  }
  
  public void hasPaymentToken(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.IntHolder hasPaymentTokenResult) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.hasPaymentToken(sessionId, country, brandId, language, result, resultMessage, hasPaymentTokenResult);
  }
  
  public void removePaymentToken(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.lang.String language, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.removePaymentToken(sessionId, country, brandId, language, result, resultMessage);
  }
  
  public void addPortabilityOrder(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger msisdn, java.math.BigInteger operatorOrig, java.math.BigInteger planOrig, com.addcel.ws.clientes.simfonics.PortalService.PortabilityPropertyType[] portabilityProperties, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.BigIntegerHolder resultAddPortabilityOrder) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.addPortabilityOrder(sessionId, country, brandId, msisdn, operatorOrig, planOrig, portabilityProperties, result, resultMessage, resultAddPortabilityOrder);
  }
  
  public void getPortabilityOrder(java.lang.String sessionId, java.lang.String country, int brandId, java.lang.String language, java.lang.Integer msisdn, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, com.addcel.ws.clientes.simfonics.PortalService.holders.Portability_RowSetHolder portabilityRowSet) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getPortabilityOrder(sessionId, country, brandId, language, msisdn, result, resultMessage, portabilityRowSet);
  }
  
  public void modifyPortabilityOrder(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger msisdn, com.addcel.ws.clientes.simfonics.PortalService.PortabilityPropertyType[] portabilityProperties, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.modifyPortabilityOrder(sessionId, country, brandId, msisdn, portabilityProperties, result, resultMessage);
  }
  
  public void recruiterValidation(java.lang.String sessionId, java.lang.String country, java.math.BigInteger brandId, java.math.BigInteger msisdn_recruiter, javax.xml.rpc.holders.StringHolder result, javax.xml.rpc.holders.StringHolder resultMessage, javax.xml.rpc.holders.BigIntegerHolder isAvailable) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.recruiterValidation(sessionId, country, brandId, msisdn_recruiter, result, resultMessage, isAvailable);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.AddRecruitResponse addRecruit(com.addcel.ws.clientes.simfonics.PortalService.AddRecruitRequest parameters) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.addRecruit(parameters);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.SendSMSResponse sendSMS(com.addcel.ws.clientes.simfonics.PortalService.SendSMSRequest sendSMSInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.sendSMS(sendSMSInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.AddFileResponse addFile(com.addcel.ws.clientes.simfonics.PortalService.AddFileRequest addFileInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.addFile(addFileInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.GetFileResponse getFile(com.addcel.ws.clientes.simfonics.PortalService.GetFileRequest getFileInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.getFile(getFileInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.GetFileListResponse getFileList(com.addcel.ws.clientes.simfonics.PortalService.GetFileListRequest getFileListInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.getFileList(getFileListInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.ValidateIdentityNumResponse validateIdentityNum(com.addcel.ws.clientes.simfonics.PortalService.ValidateIdentityNumRequest validateIdentityNumInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.validateIdentityNum(validateIdentityNumInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.GetLogisticOrderResponse getLogisticOrder(com.addcel.ws.clientes.simfonics.PortalService.GetLogisticOrderRequest getLogisticOrderInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.getLogisticOrder(getLogisticOrderInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.ModifyLogisticOrderResponse modifyLogisticOrder(com.addcel.ws.clientes.simfonics.PortalService.ModifyLogisticOrderRequest modifyLogisticOrderInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.modifyLogisticOrder(modifyLogisticOrderInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.CancelLogisticOrderResponse cancelLogisticOrder(com.addcel.ws.clientes.simfonics.PortalService.CancelLogisticOrderRequest cancelLogisticOrderInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.cancelLogisticOrder(cancelLogisticOrderInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.AddRecurrentPaymentResponse addRecurrentPayment(com.addcel.ws.clientes.simfonics.PortalService.AddRecurrentPaymentRequest addRecurrentPaymentInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.addRecurrentPayment(addRecurrentPaymentInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.GetRecurrentPaymentResponse getRecurrentPayment(com.addcel.ws.clientes.simfonics.PortalService.GetRecurrentPaymentRequest getRecurrentPaymentInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.getRecurrentPayment(getRecurrentPaymentInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.ModifyRecurrentPaymentResponse modifyRecurrentPayment(com.addcel.ws.clientes.simfonics.PortalService.ModifyRecurrentPaymentRequest modifyRecurrentPaymentInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.modifyRecurrentPayment(modifyRecurrentPaymentInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.RemoveRecurrentPaymentResponse removeRecurrentPayment(com.addcel.ws.clientes.simfonics.PortalService.RemoveRecurrentPaymentRequest removeRecurrentPaymentInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.removeRecurrentPayment(removeRecurrentPaymentInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.AddRecurrentPaymentTokenResponse addRecurrentPaymentToken(com.addcel.ws.clientes.simfonics.PortalService.AddRecurrentPaymentTokenRequest addRecurrentPaymentTokenInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.addRecurrentPaymentToken(addRecurrentPaymentTokenInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.RemoveRecurrentPaymentTokenResponse removeRecurrentPaymentToken(com.addcel.ws.clientes.simfonics.PortalService.RemoveRecurrentPaymentTokenRequest removeRecurrentPaymentTokenInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.removeRecurrentPaymentToken(removeRecurrentPaymentTokenInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.ModifyRecurrentPaymentTokenResponse modifyRecurrentPaymentToken(com.addcel.ws.clientes.simfonics.PortalService.ModifyRecurrentPaymentTokenRequest modifyRecurrentPaymentTokenInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.modifyRecurrentPaymentToken(modifyRecurrentPaymentTokenInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.GetRecurrentPaymentTokenResponse getRecurrentPaymentToken(com.addcel.ws.clientes.simfonics.PortalService.GetRecurrentPaymentTokenRequest getRecurrentPaymentTokenInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.getRecurrentPaymentToken(getRecurrentPaymentTokenInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.BlockTerminalResponse blockTerminal(com.addcel.ws.clientes.simfonics.PortalService.BlockTerminalRequest blockTerminalInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.blockTerminal(blockTerminalInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.UnblockTerminalResponse unblockTerminal(com.addcel.ws.clientes.simfonics.PortalService.UnblockTerminalRequest unblockTerminalInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.unblockTerminal(unblockTerminalInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.CapCorrelationResponse capCorrelation(com.addcel.ws.clientes.simfonics.PortalService.CapCorrelationRequest capCorrelationInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.capCorrelation(capCorrelationInput);
  }
  
  public void getPortabilityOrderProperties(com.addcel.ws.clientes.simfonics.PortalService.holders.GetPortabilityOrderPropertiesRequestHolder getPortPropInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    portalService.getPortabilityOrderProperties(getPortPropInput);
  }
  
  public com.addcel.ws.clientes.simfonics.PortalService.CancelPortabilityOrderResponse cancelPortabilityOrder(com.addcel.ws.clientes.simfonics.PortalService.CancelPortabilityOrderRequest cancelPortabilityOrderInput) throws java.rmi.RemoteException{
    if (portalService == null)
      _initPortalServiceProxy();
    return portalService.cancelPortabilityOrder(cancelPortabilityOrderInput);
  }
  
  
}