/**
 * ChangeUserAccount.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class ChangeUserAccount  implements java.io.Serializable {
    private java.lang.String sessionId;

    private java.lang.String country;

    private java.math.BigInteger customerId;

    private java.lang.String currentUserName;

    private java.lang.String currentPassword;

    private java.lang.String newUserName;

    private java.lang.String newPassword;

    private java.lang.String newSecurityQuestion;

    private java.lang.String newSecurityQuestionAns;

    public ChangeUserAccount() {
    }

    public ChangeUserAccount(
           java.lang.String sessionId,
           java.lang.String country,
           java.math.BigInteger customerId,
           java.lang.String currentUserName,
           java.lang.String currentPassword,
           java.lang.String newUserName,
           java.lang.String newPassword,
           java.lang.String newSecurityQuestion,
           java.lang.String newSecurityQuestionAns) {
           this.sessionId = sessionId;
           this.country = country;
           this.customerId = customerId;
           this.currentUserName = currentUserName;
           this.currentPassword = currentPassword;
           this.newUserName = newUserName;
           this.newPassword = newPassword;
           this.newSecurityQuestion = newSecurityQuestion;
           this.newSecurityQuestionAns = newSecurityQuestionAns;
    }


    /**
     * Gets the sessionId value for this ChangeUserAccount.
     * 
     * @return sessionId
     */
    public java.lang.String getSessionId() {
        return sessionId;
    }


    /**
     * Sets the sessionId value for this ChangeUserAccount.
     * 
     * @param sessionId
     */
    public void setSessionId(java.lang.String sessionId) {
        this.sessionId = sessionId;
    }


    /**
     * Gets the country value for this ChangeUserAccount.
     * 
     * @return country
     */
    public java.lang.String getCountry() {
        return country;
    }


    /**
     * Sets the country value for this ChangeUserAccount.
     * 
     * @param country
     */
    public void setCountry(java.lang.String country) {
        this.country = country;
    }


    /**
     * Gets the customerId value for this ChangeUserAccount.
     * 
     * @return customerId
     */
    public java.math.BigInteger getCustomerId() {
        return customerId;
    }


    /**
     * Sets the customerId value for this ChangeUserAccount.
     * 
     * @param customerId
     */
    public void setCustomerId(java.math.BigInteger customerId) {
        this.customerId = customerId;
    }


    /**
     * Gets the currentUserName value for this ChangeUserAccount.
     * 
     * @return currentUserName
     */
    public java.lang.String getCurrentUserName() {
        return currentUserName;
    }


    /**
     * Sets the currentUserName value for this ChangeUserAccount.
     * 
     * @param currentUserName
     */
    public void setCurrentUserName(java.lang.String currentUserName) {
        this.currentUserName = currentUserName;
    }


    /**
     * Gets the currentPassword value for this ChangeUserAccount.
     * 
     * @return currentPassword
     */
    public java.lang.String getCurrentPassword() {
        return currentPassword;
    }


    /**
     * Sets the currentPassword value for this ChangeUserAccount.
     * 
     * @param currentPassword
     */
    public void setCurrentPassword(java.lang.String currentPassword) {
        this.currentPassword = currentPassword;
    }


    /**
     * Gets the newUserName value for this ChangeUserAccount.
     * 
     * @return newUserName
     */
    public java.lang.String getNewUserName() {
        return newUserName;
    }


    /**
     * Sets the newUserName value for this ChangeUserAccount.
     * 
     * @param newUserName
     */
    public void setNewUserName(java.lang.String newUserName) {
        this.newUserName = newUserName;
    }


    /**
     * Gets the newPassword value for this ChangeUserAccount.
     * 
     * @return newPassword
     */
    public java.lang.String getNewPassword() {
        return newPassword;
    }


    /**
     * Sets the newPassword value for this ChangeUserAccount.
     * 
     * @param newPassword
     */
    public void setNewPassword(java.lang.String newPassword) {
        this.newPassword = newPassword;
    }


    /**
     * Gets the newSecurityQuestion value for this ChangeUserAccount.
     * 
     * @return newSecurityQuestion
     */
    public java.lang.String getNewSecurityQuestion() {
        return newSecurityQuestion;
    }


    /**
     * Sets the newSecurityQuestion value for this ChangeUserAccount.
     * 
     * @param newSecurityQuestion
     */
    public void setNewSecurityQuestion(java.lang.String newSecurityQuestion) {
        this.newSecurityQuestion = newSecurityQuestion;
    }


    /**
     * Gets the newSecurityQuestionAns value for this ChangeUserAccount.
     * 
     * @return newSecurityQuestionAns
     */
    public java.lang.String getNewSecurityQuestionAns() {
        return newSecurityQuestionAns;
    }


    /**
     * Sets the newSecurityQuestionAns value for this ChangeUserAccount.
     * 
     * @param newSecurityQuestionAns
     */
    public void setNewSecurityQuestionAns(java.lang.String newSecurityQuestionAns) {
        this.newSecurityQuestionAns = newSecurityQuestionAns;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ChangeUserAccount)) return false;
        ChangeUserAccount other = (ChangeUserAccount) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sessionId==null && other.getSessionId()==null) || 
             (this.sessionId!=null &&
              this.sessionId.equals(other.getSessionId()))) &&
            ((this.country==null && other.getCountry()==null) || 
             (this.country!=null &&
              this.country.equals(other.getCountry()))) &&
            ((this.customerId==null && other.getCustomerId()==null) || 
             (this.customerId!=null &&
              this.customerId.equals(other.getCustomerId()))) &&
            ((this.currentUserName==null && other.getCurrentUserName()==null) || 
             (this.currentUserName!=null &&
              this.currentUserName.equals(other.getCurrentUserName()))) &&
            ((this.currentPassword==null && other.getCurrentPassword()==null) || 
             (this.currentPassword!=null &&
              this.currentPassword.equals(other.getCurrentPassword()))) &&
            ((this.newUserName==null && other.getNewUserName()==null) || 
             (this.newUserName!=null &&
              this.newUserName.equals(other.getNewUserName()))) &&
            ((this.newPassword==null && other.getNewPassword()==null) || 
             (this.newPassword!=null &&
              this.newPassword.equals(other.getNewPassword()))) &&
            ((this.newSecurityQuestion==null && other.getNewSecurityQuestion()==null) || 
             (this.newSecurityQuestion!=null &&
              this.newSecurityQuestion.equals(other.getNewSecurityQuestion()))) &&
            ((this.newSecurityQuestionAns==null && other.getNewSecurityQuestionAns()==null) || 
             (this.newSecurityQuestionAns!=null &&
              this.newSecurityQuestionAns.equals(other.getNewSecurityQuestionAns())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSessionId() != null) {
            _hashCode += getSessionId().hashCode();
        }
        if (getCountry() != null) {
            _hashCode += getCountry().hashCode();
        }
        if (getCustomerId() != null) {
            _hashCode += getCustomerId().hashCode();
        }
        if (getCurrentUserName() != null) {
            _hashCode += getCurrentUserName().hashCode();
        }
        if (getCurrentPassword() != null) {
            _hashCode += getCurrentPassword().hashCode();
        }
        if (getNewUserName() != null) {
            _hashCode += getNewUserName().hashCode();
        }
        if (getNewPassword() != null) {
            _hashCode += getNewPassword().hashCode();
        }
        if (getNewSecurityQuestion() != null) {
            _hashCode += getNewSecurityQuestion().hashCode();
        }
        if (getNewSecurityQuestionAns() != null) {
            _hashCode += getNewSecurityQuestionAns().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ChangeUserAccount.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">changeUserAccount"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sessionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country");
        elemField.setXmlName(new javax.xml.namespace.QName("", "country"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currentUserName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currentUserName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currentPassword");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currentPassword"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("newUserName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "newUserName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("newPassword");
        elemField.setXmlName(new javax.xml.namespace.QName("", "newPassword"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("newSecurityQuestion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "newSecurityQuestion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("newSecurityQuestionAns");
        elemField.setXmlName(new javax.xml.namespace.QName("", "newSecurityQuestionAns"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
