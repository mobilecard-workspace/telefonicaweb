/**
 * PaymentTokenPropertyType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class PaymentTokenPropertyType  implements java.io.Serializable {
    private java.math.BigInteger paymentTokenPropertyKey;

    private java.lang.String paymentTokenPropertyValue;

    public PaymentTokenPropertyType() {
    }

    public PaymentTokenPropertyType(
           java.math.BigInteger paymentTokenPropertyKey,
           java.lang.String paymentTokenPropertyValue) {
           this.paymentTokenPropertyKey = paymentTokenPropertyKey;
           this.paymentTokenPropertyValue = paymentTokenPropertyValue;
    }


    /**
     * Gets the paymentTokenPropertyKey value for this PaymentTokenPropertyType.
     * 
     * @return paymentTokenPropertyKey
     */
    public java.math.BigInteger getPaymentTokenPropertyKey() {
        return paymentTokenPropertyKey;
    }


    /**
     * Sets the paymentTokenPropertyKey value for this PaymentTokenPropertyType.
     * 
     * @param paymentTokenPropertyKey
     */
    public void setPaymentTokenPropertyKey(java.math.BigInteger paymentTokenPropertyKey) {
        this.paymentTokenPropertyKey = paymentTokenPropertyKey;
    }


    /**
     * Gets the paymentTokenPropertyValue value for this PaymentTokenPropertyType.
     * 
     * @return paymentTokenPropertyValue
     */
    public java.lang.String getPaymentTokenPropertyValue() {
        return paymentTokenPropertyValue;
    }


    /**
     * Sets the paymentTokenPropertyValue value for this PaymentTokenPropertyType.
     * 
     * @param paymentTokenPropertyValue
     */
    public void setPaymentTokenPropertyValue(java.lang.String paymentTokenPropertyValue) {
        this.paymentTokenPropertyValue = paymentTokenPropertyValue;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaymentTokenPropertyType)) return false;
        PaymentTokenPropertyType other = (PaymentTokenPropertyType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.paymentTokenPropertyKey==null && other.getPaymentTokenPropertyKey()==null) || 
             (this.paymentTokenPropertyKey!=null &&
              this.paymentTokenPropertyKey.equals(other.getPaymentTokenPropertyKey()))) &&
            ((this.paymentTokenPropertyValue==null && other.getPaymentTokenPropertyValue()==null) || 
             (this.paymentTokenPropertyValue!=null &&
              this.paymentTokenPropertyValue.equals(other.getPaymentTokenPropertyValue())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPaymentTokenPropertyKey() != null) {
            _hashCode += getPaymentTokenPropertyKey().hashCode();
        }
        if (getPaymentTokenPropertyValue() != null) {
            _hashCode += getPaymentTokenPropertyValue().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaymentTokenPropertyType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", "paymentTokenPropertyType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentTokenPropertyKey");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paymentTokenPropertyKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentTokenPropertyValue");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paymentTokenPropertyValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
