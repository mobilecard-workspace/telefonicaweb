/**
 * GetCustomerOrdersByCustomerIdTypeHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService.holders;

public final class GetCustomerOrdersByCustomerIdTypeHolder implements javax.xml.rpc.holders.Holder {
    public com.addcel.ws.clientes.simfonics.PortalService.GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId[] value;

    public GetCustomerOrdersByCustomerIdTypeHolder() {
    }

    public GetCustomerOrdersByCustomerIdTypeHolder(com.addcel.ws.clientes.simfonics.PortalService.GetCustomerOrdersByCustomerIdTypeGetCustomerOrdersByCustomerId[] value) {
        this.value = value;
    }

}
