/**
 * Portability_RowSetPortability_RowHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService.holders;

public final class Portability_RowSetPortability_RowHolder implements javax.xml.rpc.holders.Holder {
    public com.addcel.ws.clientes.simfonics.PortalService.Portability_RowSetPortability_Row value;

    public Portability_RowSetPortability_RowHolder() {
    }

    public Portability_RowSetPortability_RowHolder(com.addcel.ws.clientes.simfonics.PortalService.Portability_RowSetPortability_Row value) {
        this.value = value;
    }

}
