/**
 * QueryRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.TopUpInterface;

public class QueryRequest  implements java.io.Serializable {
    private com.addcel.ws.clientes.simfonics.TopUpInterface.Credentials credentials;

    private java.math.BigInteger mvno;

    private java.math.BigInteger msisdn;

    private java.math.BigInteger transactionId;

    private java.math.BigInteger transactionTypeId;

    private java.lang.String origin;

    public QueryRequest() {
    }

    public QueryRequest(
           com.addcel.ws.clientes.simfonics.TopUpInterface.Credentials credentials,
           java.math.BigInteger mvno,
           java.math.BigInteger msisdn,
           java.math.BigInteger transactionId,
           java.math.BigInteger transactionTypeId,
           java.lang.String origin) {
           this.credentials = credentials;
           this.mvno = mvno;
           this.msisdn = msisdn;
           this.transactionId = transactionId;
           this.transactionTypeId = transactionTypeId;
           this.origin = origin;
    }


    /**
     * Gets the credentials value for this QueryRequest.
     * 
     * @return credentials
     */
    public com.addcel.ws.clientes.simfonics.TopUpInterface.Credentials getCredentials() {
        return credentials;
    }


    /**
     * Sets the credentials value for this QueryRequest.
     * 
     * @param credentials
     */
    public void setCredentials(com.addcel.ws.clientes.simfonics.TopUpInterface.Credentials credentials) {
        this.credentials = credentials;
    }


    /**
     * Gets the mvno value for this QueryRequest.
     * 
     * @return mvno
     */
    public java.math.BigInteger getMvno() {
        return mvno;
    }


    /**
     * Sets the mvno value for this QueryRequest.
     * 
     * @param mvno
     */
    public void setMvno(java.math.BigInteger mvno) {
        this.mvno = mvno;
    }


    /**
     * Gets the msisdn value for this QueryRequest.
     * 
     * @return msisdn
     */
    public java.math.BigInteger getMsisdn() {
        return msisdn;
    }


    /**
     * Sets the msisdn value for this QueryRequest.
     * 
     * @param msisdn
     */
    public void setMsisdn(java.math.BigInteger msisdn) {
        this.msisdn = msisdn;
    }


    /**
     * Gets the transactionId value for this QueryRequest.
     * 
     * @return transactionId
     */
    public java.math.BigInteger getTransactionId() {
        return transactionId;
    }


    /**
     * Sets the transactionId value for this QueryRequest.
     * 
     * @param transactionId
     */
    public void setTransactionId(java.math.BigInteger transactionId) {
        this.transactionId = transactionId;
    }


    /**
     * Gets the transactionTypeId value for this QueryRequest.
     * 
     * @return transactionTypeId
     */
    public java.math.BigInteger getTransactionTypeId() {
        return transactionTypeId;
    }


    /**
     * Sets the transactionTypeId value for this QueryRequest.
     * 
     * @param transactionTypeId
     */
    public void setTransactionTypeId(java.math.BigInteger transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }


    /**
     * Gets the origin value for this QueryRequest.
     * 
     * @return origin
     */
    public java.lang.String getOrigin() {
        return origin;
    }


    /**
     * Sets the origin value for this QueryRequest.
     * 
     * @param origin
     */
    public void setOrigin(java.lang.String origin) {
        this.origin = origin;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof QueryRequest)) return false;
        QueryRequest other = (QueryRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.credentials==null && other.getCredentials()==null) || 
             (this.credentials!=null &&
              this.credentials.equals(other.getCredentials()))) &&
            ((this.mvno==null && other.getMvno()==null) || 
             (this.mvno!=null &&
              this.mvno.equals(other.getMvno()))) &&
            ((this.msisdn==null && other.getMsisdn()==null) || 
             (this.msisdn!=null &&
              this.msisdn.equals(other.getMsisdn()))) &&
            ((this.transactionId==null && other.getTransactionId()==null) || 
             (this.transactionId!=null &&
              this.transactionId.equals(other.getTransactionId()))) &&
            ((this.transactionTypeId==null && other.getTransactionTypeId()==null) || 
             (this.transactionTypeId!=null &&
              this.transactionTypeId.equals(other.getTransactionTypeId()))) &&
            ((this.origin==null && other.getOrigin()==null) || 
             (this.origin!=null &&
              this.origin.equals(other.getOrigin())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCredentials() != null) {
            _hashCode += getCredentials().hashCode();
        }
        if (getMvno() != null) {
            _hashCode += getMvno().hashCode();
        }
        if (getMsisdn() != null) {
            _hashCode += getMsisdn().hashCode();
        }
        if (getTransactionId() != null) {
            _hashCode += getTransactionId().hashCode();
        }
        if (getTransactionTypeId() != null) {
            _hashCode += getTransactionTypeId().hashCode();
        }
        if (getOrigin() != null) {
            _hashCode += getOrigin().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(QueryRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", ">QueryRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("credentials");
        elemField.setXmlName(new javax.xml.namespace.QName("", "credentials"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", "credentials"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mvno");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mvno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msisdn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msisdn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "transactionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "transactionTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("origin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "origin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
