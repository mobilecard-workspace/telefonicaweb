/**
 * GetOrdersTypeOrder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class GetOrdersTypeOrder  implements java.io.Serializable {
    private java.math.BigInteger id;

    private java.math.BigInteger catalogId;

    private java.math.BigInteger status;

    private java.lang.String statusName;

    private java.util.Calendar startDate;

    private java.util.Calendar endDate;

    private java.math.BigInteger typeId;

    private java.lang.String typeName;

    private java.math.BigInteger categoryId;

    private java.lang.String categoryName;

    private java.math.BigInteger subCategoryId;

    private java.lang.String subCategoryName;

    public GetOrdersTypeOrder() {
    }

    public GetOrdersTypeOrder(
           java.math.BigInteger id,
           java.math.BigInteger catalogId,
           java.math.BigInteger status,
           java.lang.String statusName,
           java.util.Calendar startDate,
           java.util.Calendar endDate,
           java.math.BigInteger typeId,
           java.lang.String typeName,
           java.math.BigInteger categoryId,
           java.lang.String categoryName,
           java.math.BigInteger subCategoryId,
           java.lang.String subCategoryName) {
           this.id = id;
           this.catalogId = catalogId;
           this.status = status;
           this.statusName = statusName;
           this.startDate = startDate;
           this.endDate = endDate;
           this.typeId = typeId;
           this.typeName = typeName;
           this.categoryId = categoryId;
           this.categoryName = categoryName;
           this.subCategoryId = subCategoryId;
           this.subCategoryName = subCategoryName;
    }


    /**
     * Gets the id value for this GetOrdersTypeOrder.
     * 
     * @return id
     */
    public java.math.BigInteger getId() {
        return id;
    }


    /**
     * Sets the id value for this GetOrdersTypeOrder.
     * 
     * @param id
     */
    public void setId(java.math.BigInteger id) {
        this.id = id;
    }


    /**
     * Gets the catalogId value for this GetOrdersTypeOrder.
     * 
     * @return catalogId
     */
    public java.math.BigInteger getCatalogId() {
        return catalogId;
    }


    /**
     * Sets the catalogId value for this GetOrdersTypeOrder.
     * 
     * @param catalogId
     */
    public void setCatalogId(java.math.BigInteger catalogId) {
        this.catalogId = catalogId;
    }


    /**
     * Gets the status value for this GetOrdersTypeOrder.
     * 
     * @return status
     */
    public java.math.BigInteger getStatus() {
        return status;
    }


    /**
     * Sets the status value for this GetOrdersTypeOrder.
     * 
     * @param status
     */
    public void setStatus(java.math.BigInteger status) {
        this.status = status;
    }


    /**
     * Gets the statusName value for this GetOrdersTypeOrder.
     * 
     * @return statusName
     */
    public java.lang.String getStatusName() {
        return statusName;
    }


    /**
     * Sets the statusName value for this GetOrdersTypeOrder.
     * 
     * @param statusName
     */
    public void setStatusName(java.lang.String statusName) {
        this.statusName = statusName;
    }


    /**
     * Gets the startDate value for this GetOrdersTypeOrder.
     * 
     * @return startDate
     */
    public java.util.Calendar getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this GetOrdersTypeOrder.
     * 
     * @param startDate
     */
    public void setStartDate(java.util.Calendar startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the endDate value for this GetOrdersTypeOrder.
     * 
     * @return endDate
     */
    public java.util.Calendar getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this GetOrdersTypeOrder.
     * 
     * @param endDate
     */
    public void setEndDate(java.util.Calendar endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the typeId value for this GetOrdersTypeOrder.
     * 
     * @return typeId
     */
    public java.math.BigInteger getTypeId() {
        return typeId;
    }


    /**
     * Sets the typeId value for this GetOrdersTypeOrder.
     * 
     * @param typeId
     */
    public void setTypeId(java.math.BigInteger typeId) {
        this.typeId = typeId;
    }


    /**
     * Gets the typeName value for this GetOrdersTypeOrder.
     * 
     * @return typeName
     */
    public java.lang.String getTypeName() {
        return typeName;
    }


    /**
     * Sets the typeName value for this GetOrdersTypeOrder.
     * 
     * @param typeName
     */
    public void setTypeName(java.lang.String typeName) {
        this.typeName = typeName;
    }


    /**
     * Gets the categoryId value for this GetOrdersTypeOrder.
     * 
     * @return categoryId
     */
    public java.math.BigInteger getCategoryId() {
        return categoryId;
    }


    /**
     * Sets the categoryId value for this GetOrdersTypeOrder.
     * 
     * @param categoryId
     */
    public void setCategoryId(java.math.BigInteger categoryId) {
        this.categoryId = categoryId;
    }


    /**
     * Gets the categoryName value for this GetOrdersTypeOrder.
     * 
     * @return categoryName
     */
    public java.lang.String getCategoryName() {
        return categoryName;
    }


    /**
     * Sets the categoryName value for this GetOrdersTypeOrder.
     * 
     * @param categoryName
     */
    public void setCategoryName(java.lang.String categoryName) {
        this.categoryName = categoryName;
    }


    /**
     * Gets the subCategoryId value for this GetOrdersTypeOrder.
     * 
     * @return subCategoryId
     */
    public java.math.BigInteger getSubCategoryId() {
        return subCategoryId;
    }


    /**
     * Sets the subCategoryId value for this GetOrdersTypeOrder.
     * 
     * @param subCategoryId
     */
    public void setSubCategoryId(java.math.BigInteger subCategoryId) {
        this.subCategoryId = subCategoryId;
    }


    /**
     * Gets the subCategoryName value for this GetOrdersTypeOrder.
     * 
     * @return subCategoryName
     */
    public java.lang.String getSubCategoryName() {
        return subCategoryName;
    }


    /**
     * Sets the subCategoryName value for this GetOrdersTypeOrder.
     * 
     * @param subCategoryName
     */
    public void setSubCategoryName(java.lang.String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetOrdersTypeOrder)) return false;
        GetOrdersTypeOrder other = (GetOrdersTypeOrder) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.catalogId==null && other.getCatalogId()==null) || 
             (this.catalogId!=null &&
              this.catalogId.equals(other.getCatalogId()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.statusName==null && other.getStatusName()==null) || 
             (this.statusName!=null &&
              this.statusName.equals(other.getStatusName()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.typeId==null && other.getTypeId()==null) || 
             (this.typeId!=null &&
              this.typeId.equals(other.getTypeId()))) &&
            ((this.typeName==null && other.getTypeName()==null) || 
             (this.typeName!=null &&
              this.typeName.equals(other.getTypeName()))) &&
            ((this.categoryId==null && other.getCategoryId()==null) || 
             (this.categoryId!=null &&
              this.categoryId.equals(other.getCategoryId()))) &&
            ((this.categoryName==null && other.getCategoryName()==null) || 
             (this.categoryName!=null &&
              this.categoryName.equals(other.getCategoryName()))) &&
            ((this.subCategoryId==null && other.getSubCategoryId()==null) || 
             (this.subCategoryId!=null &&
              this.subCategoryId.equals(other.getSubCategoryId()))) &&
            ((this.subCategoryName==null && other.getSubCategoryName()==null) || 
             (this.subCategoryName!=null &&
              this.subCategoryName.equals(other.getSubCategoryName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getCatalogId() != null) {
            _hashCode += getCatalogId().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getStatusName() != null) {
            _hashCode += getStatusName().hashCode();
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getTypeId() != null) {
            _hashCode += getTypeId().hashCode();
        }
        if (getTypeName() != null) {
            _hashCode += getTypeName().hashCode();
        }
        if (getCategoryId() != null) {
            _hashCode += getCategoryId().hashCode();
        }
        if (getCategoryName() != null) {
            _hashCode += getCategoryName().hashCode();
        }
        if (getSubCategoryId() != null) {
            _hashCode += getSubCategoryId().hashCode();
        }
        if (getSubCategoryName() != null) {
            _hashCode += getSubCategoryName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetOrdersTypeOrder.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">getOrdersType>order"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("catalogId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "catalogId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statusName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "startDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "typeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "typeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoryId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "categoryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoryName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "categoryName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subCategoryId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subCategoryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subCategoryName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subCategoryName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
