/**
 * PaymentMethodNamesTypeHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService.holders;

public final class PaymentMethodNamesTypeHolder implements javax.xml.rpc.holders.Holder {
    public com.addcel.ws.clientes.simfonics.AdminService.PaymentMethodNamesTypePaymentMethodName[] value;

    public PaymentMethodNamesTypeHolder() {
    }

    public PaymentMethodNamesTypeHolder(com.addcel.ws.clientes.simfonics.AdminService.PaymentMethodNamesTypePaymentMethodName[] value) {
        this.value = value;
    }

}
