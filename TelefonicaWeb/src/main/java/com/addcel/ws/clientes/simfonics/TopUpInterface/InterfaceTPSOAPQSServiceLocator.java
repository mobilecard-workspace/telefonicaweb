/**
 * InterfaceTPSOAPQSServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.TopUpInterface;

public class InterfaceTPSOAPQSServiceLocator extends org.apache.axis.client.Service implements com.addcel.ws.clientes.simfonics.TopUpInterface.InterfaceTPSOAPQSService {

    public InterfaceTPSOAPQSServiceLocator() {
    }


    public InterfaceTPSOAPQSServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public InterfaceTPSOAPQSServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for InterfaceTPSOAPQSPort
    //URL original -> http://195.230.105.3:9000/TopUpInterface";
    private java.lang.String InterfaceTPSOAPQSPort_address = "http://195.230.105.245:9000/TopUpInterface";

    public java.lang.String getInterfaceTPSOAPQSPortAddress() {
        return InterfaceTPSOAPQSPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String InterfaceTPSOAPQSPortWSDDServiceName = "InterfaceTPSOAPQSPort";

    public java.lang.String getInterfaceTPSOAPQSPortWSDDServiceName() {
        return InterfaceTPSOAPQSPortWSDDServiceName;
    }

    public void setInterfaceTPSOAPQSPortWSDDServiceName(java.lang.String name) {
        InterfaceTPSOAPQSPortWSDDServiceName = name;
    }

    public com.addcel.ws.clientes.simfonics.TopUpInterface.InterfaceTP getInterfaceTPSOAPQSPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(InterfaceTPSOAPQSPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getInterfaceTPSOAPQSPort(endpoint);
    }

    public com.addcel.ws.clientes.simfonics.TopUpInterface.InterfaceTP getInterfaceTPSOAPQSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.addcel.ws.clientes.simfonics.TopUpInterface.InterfaceTPSOAPStub _stub = new com.addcel.ws.clientes.simfonics.TopUpInterface.InterfaceTPSOAPStub(portAddress, this);
            _stub.setPortName(getInterfaceTPSOAPQSPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setInterfaceTPSOAPQSPortEndpointAddress(java.lang.String address) {
        InterfaceTPSOAPQSPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.addcel.ws.clientes.simfonics.TopUpInterface.InterfaceTP.class.isAssignableFrom(serviceEndpointInterface)) {
                com.addcel.ws.clientes.simfonics.TopUpInterface.InterfaceTPSOAPStub _stub = new com.addcel.ws.clientes.simfonics.TopUpInterface.InterfaceTPSOAPStub(new java.net.URL(InterfaceTPSOAPQSPort_address), this);
                _stub.setPortName(getInterfaceTPSOAPQSPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("InterfaceTPSOAPQSPort".equals(inputPortName)) {
            return getInterfaceTPSOAPQSPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", "InterfaceTPSOAPQSService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.simfonics.com/TopUpInterface", "InterfaceTPSOAPQSPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("InterfaceTPSOAPQSPort".equals(portName)) {
            setInterfaceTPSOAPQSPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
