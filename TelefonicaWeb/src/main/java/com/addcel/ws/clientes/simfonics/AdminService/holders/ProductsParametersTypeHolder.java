/**
 * ProductsParametersTypeHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService.holders;

public final class ProductsParametersTypeHolder implements javax.xml.rpc.holders.Holder {
    public com.addcel.ws.clientes.simfonics.AdminService.ProductsParametersTypePurchaseParameter[] value;

    public ProductsParametersTypeHolder() {
    }

    public ProductsParametersTypeHolder(com.addcel.ws.clientes.simfonics.AdminService.ProductsParametersTypePurchaseParameter[] value) {
        this.value = value;
    }

}
