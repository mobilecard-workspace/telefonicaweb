/**
 * AvailableProductsTypeAvailableProduct.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class AvailableProductsTypeAvailableProduct  implements java.io.Serializable {
    private java.math.BigInteger id;

    private java.math.BigInteger accountTypeId;

    private java.lang.String accountTypeName;

    private java.lang.String productName;

    private java.math.BigDecimal price;

    private java.math.BigInteger productTypeId;

    private java.lang.String productTypeName;

    private java.math.BigInteger subscriptionTypeId;

    private java.lang.String subscriptionTypeName;

    private java.lang.String duration;

    private java.math.BigInteger maxActive;

    private java.lang.String description;

    private java.math.BigInteger[][] incompatibleProducts;

    public AvailableProductsTypeAvailableProduct() {
    }

    public AvailableProductsTypeAvailableProduct(
           java.math.BigInteger id,
           java.math.BigInteger accountTypeId,
           java.lang.String accountTypeName,
           java.lang.String productName,
           java.math.BigDecimal price,
           java.math.BigInteger productTypeId,
           java.lang.String productTypeName,
           java.math.BigInteger subscriptionTypeId,
           java.lang.String subscriptionTypeName,
           java.lang.String duration,
           java.math.BigInteger maxActive,
           java.lang.String description,
           java.math.BigInteger[][] incompatibleProducts) {
           this.id = id;
           this.accountTypeId = accountTypeId;
           this.accountTypeName = accountTypeName;
           this.productName = productName;
           this.price = price;
           this.productTypeId = productTypeId;
           this.productTypeName = productTypeName;
           this.subscriptionTypeId = subscriptionTypeId;
           this.subscriptionTypeName = subscriptionTypeName;
           this.duration = duration;
           this.maxActive = maxActive;
           this.description = description;
           this.incompatibleProducts = incompatibleProducts;
    }


    /**
     * Gets the id value for this AvailableProductsTypeAvailableProduct.
     * 
     * @return id
     */
    public java.math.BigInteger getId() {
        return id;
    }


    /**
     * Sets the id value for this AvailableProductsTypeAvailableProduct.
     * 
     * @param id
     */
    public void setId(java.math.BigInteger id) {
        this.id = id;
    }


    /**
     * Gets the accountTypeId value for this AvailableProductsTypeAvailableProduct.
     * 
     * @return accountTypeId
     */
    public java.math.BigInteger getAccountTypeId() {
        return accountTypeId;
    }


    /**
     * Sets the accountTypeId value for this AvailableProductsTypeAvailableProduct.
     * 
     * @param accountTypeId
     */
    public void setAccountTypeId(java.math.BigInteger accountTypeId) {
        this.accountTypeId = accountTypeId;
    }


    /**
     * Gets the accountTypeName value for this AvailableProductsTypeAvailableProduct.
     * 
     * @return accountTypeName
     */
    public java.lang.String getAccountTypeName() {
        return accountTypeName;
    }


    /**
     * Sets the accountTypeName value for this AvailableProductsTypeAvailableProduct.
     * 
     * @param accountTypeName
     */
    public void setAccountTypeName(java.lang.String accountTypeName) {
        this.accountTypeName = accountTypeName;
    }


    /**
     * Gets the productName value for this AvailableProductsTypeAvailableProduct.
     * 
     * @return productName
     */
    public java.lang.String getProductName() {
        return productName;
    }


    /**
     * Sets the productName value for this AvailableProductsTypeAvailableProduct.
     * 
     * @param productName
     */
    public void setProductName(java.lang.String productName) {
        this.productName = productName;
    }


    /**
     * Gets the price value for this AvailableProductsTypeAvailableProduct.
     * 
     * @return price
     */
    public java.math.BigDecimal getPrice() {
        return price;
    }


    /**
     * Sets the price value for this AvailableProductsTypeAvailableProduct.
     * 
     * @param price
     */
    public void setPrice(java.math.BigDecimal price) {
        this.price = price;
    }


    /**
     * Gets the productTypeId value for this AvailableProductsTypeAvailableProduct.
     * 
     * @return productTypeId
     */
    public java.math.BigInteger getProductTypeId() {
        return productTypeId;
    }


    /**
     * Sets the productTypeId value for this AvailableProductsTypeAvailableProduct.
     * 
     * @param productTypeId
     */
    public void setProductTypeId(java.math.BigInteger productTypeId) {
        this.productTypeId = productTypeId;
    }


    /**
     * Gets the productTypeName value for this AvailableProductsTypeAvailableProduct.
     * 
     * @return productTypeName
     */
    public java.lang.String getProductTypeName() {
        return productTypeName;
    }


    /**
     * Sets the productTypeName value for this AvailableProductsTypeAvailableProduct.
     * 
     * @param productTypeName
     */
    public void setProductTypeName(java.lang.String productTypeName) {
        this.productTypeName = productTypeName;
    }


    /**
     * Gets the subscriptionTypeId value for this AvailableProductsTypeAvailableProduct.
     * 
     * @return subscriptionTypeId
     */
    public java.math.BigInteger getSubscriptionTypeId() {
        return subscriptionTypeId;
    }


    /**
     * Sets the subscriptionTypeId value for this AvailableProductsTypeAvailableProduct.
     * 
     * @param subscriptionTypeId
     */
    public void setSubscriptionTypeId(java.math.BigInteger subscriptionTypeId) {
        this.subscriptionTypeId = subscriptionTypeId;
    }


    /**
     * Gets the subscriptionTypeName value for this AvailableProductsTypeAvailableProduct.
     * 
     * @return subscriptionTypeName
     */
    public java.lang.String getSubscriptionTypeName() {
        return subscriptionTypeName;
    }


    /**
     * Sets the subscriptionTypeName value for this AvailableProductsTypeAvailableProduct.
     * 
     * @param subscriptionTypeName
     */
    public void setSubscriptionTypeName(java.lang.String subscriptionTypeName) {
        this.subscriptionTypeName = subscriptionTypeName;
    }


    /**
     * Gets the duration value for this AvailableProductsTypeAvailableProduct.
     * 
     * @return duration
     */
    public java.lang.String getDuration() {
        return duration;
    }


    /**
     * Sets the duration value for this AvailableProductsTypeAvailableProduct.
     * 
     * @param duration
     */
    public void setDuration(java.lang.String duration) {
        this.duration = duration;
    }


    /**
     * Gets the maxActive value for this AvailableProductsTypeAvailableProduct.
     * 
     * @return maxActive
     */
    public java.math.BigInteger getMaxActive() {
        return maxActive;
    }


    /**
     * Sets the maxActive value for this AvailableProductsTypeAvailableProduct.
     * 
     * @param maxActive
     */
    public void setMaxActive(java.math.BigInteger maxActive) {
        this.maxActive = maxActive;
    }


    /**
     * Gets the description value for this AvailableProductsTypeAvailableProduct.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this AvailableProductsTypeAvailableProduct.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the incompatibleProducts value for this AvailableProductsTypeAvailableProduct.
     * 
     * @return incompatibleProducts
     */
    public java.math.BigInteger[][] getIncompatibleProducts() {
        return incompatibleProducts;
    }


    /**
     * Sets the incompatibleProducts value for this AvailableProductsTypeAvailableProduct.
     * 
     * @param incompatibleProducts
     */
    public void setIncompatibleProducts(java.math.BigInteger[][] incompatibleProducts) {
        this.incompatibleProducts = incompatibleProducts;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AvailableProductsTypeAvailableProduct)) return false;
        AvailableProductsTypeAvailableProduct other = (AvailableProductsTypeAvailableProduct) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.accountTypeId==null && other.getAccountTypeId()==null) || 
             (this.accountTypeId!=null &&
              this.accountTypeId.equals(other.getAccountTypeId()))) &&
            ((this.accountTypeName==null && other.getAccountTypeName()==null) || 
             (this.accountTypeName!=null &&
              this.accountTypeName.equals(other.getAccountTypeName()))) &&
            ((this.productName==null && other.getProductName()==null) || 
             (this.productName!=null &&
              this.productName.equals(other.getProductName()))) &&
            ((this.price==null && other.getPrice()==null) || 
             (this.price!=null &&
              this.price.equals(other.getPrice()))) &&
            ((this.productTypeId==null && other.getProductTypeId()==null) || 
             (this.productTypeId!=null &&
              this.productTypeId.equals(other.getProductTypeId()))) &&
            ((this.productTypeName==null && other.getProductTypeName()==null) || 
             (this.productTypeName!=null &&
              this.productTypeName.equals(other.getProductTypeName()))) &&
            ((this.subscriptionTypeId==null && other.getSubscriptionTypeId()==null) || 
             (this.subscriptionTypeId!=null &&
              this.subscriptionTypeId.equals(other.getSubscriptionTypeId()))) &&
            ((this.subscriptionTypeName==null && other.getSubscriptionTypeName()==null) || 
             (this.subscriptionTypeName!=null &&
              this.subscriptionTypeName.equals(other.getSubscriptionTypeName()))) &&
            ((this.duration==null && other.getDuration()==null) || 
             (this.duration!=null &&
              this.duration.equals(other.getDuration()))) &&
            ((this.maxActive==null && other.getMaxActive()==null) || 
             (this.maxActive!=null &&
              this.maxActive.equals(other.getMaxActive()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.incompatibleProducts==null && other.getIncompatibleProducts()==null) || 
             (this.incompatibleProducts!=null &&
              java.util.Arrays.equals(this.incompatibleProducts, other.getIncompatibleProducts())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getAccountTypeId() != null) {
            _hashCode += getAccountTypeId().hashCode();
        }
        if (getAccountTypeName() != null) {
            _hashCode += getAccountTypeName().hashCode();
        }
        if (getProductName() != null) {
            _hashCode += getProductName().hashCode();
        }
        if (getPrice() != null) {
            _hashCode += getPrice().hashCode();
        }
        if (getProductTypeId() != null) {
            _hashCode += getProductTypeId().hashCode();
        }
        if (getProductTypeName() != null) {
            _hashCode += getProductTypeName().hashCode();
        }
        if (getSubscriptionTypeId() != null) {
            _hashCode += getSubscriptionTypeId().hashCode();
        }
        if (getSubscriptionTypeName() != null) {
            _hashCode += getSubscriptionTypeName().hashCode();
        }
        if (getDuration() != null) {
            _hashCode += getDuration().hashCode();
        }
        if (getMaxActive() != null) {
            _hashCode += getMaxActive().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getIncompatibleProducts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getIncompatibleProducts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getIncompatibleProducts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AvailableProductsTypeAvailableProduct.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">availableProductsType>availableProduct"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accountTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accountTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("price");
        elemField.setXmlName(new javax.xml.namespace.QName("", "price"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriptionTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriptionTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("duration");
        elemField.setXmlName(new javax.xml.namespace.QName("", "duration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxActive");
        elemField.setXmlName(new javax.xml.namespace.QName("", "maxActive"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("incompatibleProducts");
        elemField.setXmlName(new javax.xml.namespace.QName("", "incompatibleProducts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">incompatibleProductType>incompatibleProduct"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "incompatibleProduct"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
