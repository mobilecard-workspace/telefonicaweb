/**
 * GetUsageTypeUsage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class GetUsageTypeUsage  implements java.io.Serializable {
    private java.math.BigDecimal CUSTOMERID;

    private java.math.BigDecimal SUBSCRIPTIONID;

    private java.math.BigDecimal BRANDID;

    private java.lang.String USAGETYPE;

    private java.math.BigInteger PRODUCTID;

    private java.lang.String PRODUCTNAME;

    private java.lang.String ANO;

    private java.lang.String BNO;

    private java.lang.String CNO;

    private java.lang.String MOBILECOUNTRYCODE;

    private java.lang.String MOBILENETWORKCODE;

    private java.lang.String CALLTYPE;

    private java.lang.String CALLTYPENAME;

    private java.util.Calendar CREATIONTIME;

    private java.util.Calendar STARTTIME;

    private java.util.Calendar LOCALSTARTTIME;

    private java.math.BigDecimal DURATION;

    private java.math.BigDecimal CHARGEDDURATION;

    private java.math.BigDecimal COST;

    private java.math.BigDecimal INITIALCOST;

    private java.math.BigDecimal MINUTECOSTCOST;

    private java.math.BigDecimal TAXAMOUNT;

    private java.math.BigDecimal POSTBALANCE;

    private java.lang.String CURRENCYID;

    private java.lang.String DISCONNECTREASON;

    private java.lang.String ORIGZONE;

    private java.lang.String TERMZONE;

    private java.lang.String TIMEZONEID;

    private java.lang.String ADJUSTMENTTYPENAME;

    private java.lang.String ADJUSTMENTREASONNAME;

    private java.math.BigDecimal STATUS;

    private java.lang.String EVENTID;

    private java.lang.String ADDITIONALINFO1;

    private java.lang.String ADDITIONALINFO2;

    private java.lang.String ADDITIONALINFO3;

    private java.lang.String MESSAGEID;

    private java.math.BigDecimal CONTENTTYPE;

    private java.math.BigDecimal MESSAGESIZE;

    private java.lang.String SUBSCRIBERIPV4;

    private java.lang.String APN;

    private java.math.BigDecimal SERVICEID;

    private java.math.BigDecimal DATAVOLUMESENT;

    private java.math.BigDecimal DATAVOLUMERECEIVED;

    private java.math.BigDecimal USEDQUOTA;

    private java.lang.String QUOTAUNIT;

    private java.lang.String SGSNADDRESS;

    private java.math.BigDecimal USEDCOUNTERVALUESAMOUNT;

    private java.math.BigDecimal USEDCOUNTERVALUESTIME;

    private java.math.BigDecimal USEDCOUNTERVALUESINTEGER;

    private java.math.BigDecimal USEDCOUNTERVALUESDATAVOLUME;

    public GetUsageTypeUsage() {
    }

    public GetUsageTypeUsage(
           java.math.BigDecimal CUSTOMERID,
           java.math.BigDecimal SUBSCRIPTIONID,
           java.math.BigDecimal BRANDID,
           java.lang.String USAGETYPE,
           java.math.BigInteger PRODUCTID,
           java.lang.String PRODUCTNAME,
           java.lang.String ANO,
           java.lang.String BNO,
           java.lang.String CNO,
           java.lang.String MOBILECOUNTRYCODE,
           java.lang.String MOBILENETWORKCODE,
           java.lang.String CALLTYPE,
           java.lang.String CALLTYPENAME,
           java.util.Calendar CREATIONTIME,
           java.util.Calendar STARTTIME,
           java.util.Calendar LOCALSTARTTIME,
           java.math.BigDecimal DURATION,
           java.math.BigDecimal CHARGEDDURATION,
           java.math.BigDecimal COST,
           java.math.BigDecimal INITIALCOST,
           java.math.BigDecimal MINUTECOSTCOST,
           java.math.BigDecimal TAXAMOUNT,
           java.math.BigDecimal POSTBALANCE,
           java.lang.String CURRENCYID,
           java.lang.String DISCONNECTREASON,
           java.lang.String ORIGZONE,
           java.lang.String TERMZONE,
           java.lang.String TIMEZONEID,
           java.lang.String ADJUSTMENTTYPENAME,
           java.lang.String ADJUSTMENTREASONNAME,
           java.math.BigDecimal STATUS,
           java.lang.String EVENTID,
           java.lang.String ADDITIONALINFO1,
           java.lang.String ADDITIONALINFO2,
           java.lang.String ADDITIONALINFO3,
           java.lang.String MESSAGEID,
           java.math.BigDecimal CONTENTTYPE,
           java.math.BigDecimal MESSAGESIZE,
           java.lang.String SUBSCRIBERIPV4,
           java.lang.String APN,
           java.math.BigDecimal SERVICEID,
           java.math.BigDecimal DATAVOLUMESENT,
           java.math.BigDecimal DATAVOLUMERECEIVED,
           java.math.BigDecimal USEDQUOTA,
           java.lang.String QUOTAUNIT,
           java.lang.String SGSNADDRESS,
           java.math.BigDecimal USEDCOUNTERVALUESAMOUNT,
           java.math.BigDecimal USEDCOUNTERVALUESTIME,
           java.math.BigDecimal USEDCOUNTERVALUESINTEGER,
           java.math.BigDecimal USEDCOUNTERVALUESDATAVOLUME) {
           this.CUSTOMERID = CUSTOMERID;
           this.SUBSCRIPTIONID = SUBSCRIPTIONID;
           this.BRANDID = BRANDID;
           this.USAGETYPE = USAGETYPE;
           this.PRODUCTID = PRODUCTID;
           this.PRODUCTNAME = PRODUCTNAME;
           this.ANO = ANO;
           this.BNO = BNO;
           this.CNO = CNO;
           this.MOBILECOUNTRYCODE = MOBILECOUNTRYCODE;
           this.MOBILENETWORKCODE = MOBILENETWORKCODE;
           this.CALLTYPE = CALLTYPE;
           this.CALLTYPENAME = CALLTYPENAME;
           this.CREATIONTIME = CREATIONTIME;
           this.STARTTIME = STARTTIME;
           this.LOCALSTARTTIME = LOCALSTARTTIME;
           this.DURATION = DURATION;
           this.CHARGEDDURATION = CHARGEDDURATION;
           this.COST = COST;
           this.INITIALCOST = INITIALCOST;
           this.MINUTECOSTCOST = MINUTECOSTCOST;
           this.TAXAMOUNT = TAXAMOUNT;
           this.POSTBALANCE = POSTBALANCE;
           this.CURRENCYID = CURRENCYID;
           this.DISCONNECTREASON = DISCONNECTREASON;
           this.ORIGZONE = ORIGZONE;
           this.TERMZONE = TERMZONE;
           this.TIMEZONEID = TIMEZONEID;
           this.ADJUSTMENTTYPENAME = ADJUSTMENTTYPENAME;
           this.ADJUSTMENTREASONNAME = ADJUSTMENTREASONNAME;
           this.STATUS = STATUS;
           this.EVENTID = EVENTID;
           this.ADDITIONALINFO1 = ADDITIONALINFO1;
           this.ADDITIONALINFO2 = ADDITIONALINFO2;
           this.ADDITIONALINFO3 = ADDITIONALINFO3;
           this.MESSAGEID = MESSAGEID;
           this.CONTENTTYPE = CONTENTTYPE;
           this.MESSAGESIZE = MESSAGESIZE;
           this.SUBSCRIBERIPV4 = SUBSCRIBERIPV4;
           this.APN = APN;
           this.SERVICEID = SERVICEID;
           this.DATAVOLUMESENT = DATAVOLUMESENT;
           this.DATAVOLUMERECEIVED = DATAVOLUMERECEIVED;
           this.USEDQUOTA = USEDQUOTA;
           this.QUOTAUNIT = QUOTAUNIT;
           this.SGSNADDRESS = SGSNADDRESS;
           this.USEDCOUNTERVALUESAMOUNT = USEDCOUNTERVALUESAMOUNT;
           this.USEDCOUNTERVALUESTIME = USEDCOUNTERVALUESTIME;
           this.USEDCOUNTERVALUESINTEGER = USEDCOUNTERVALUESINTEGER;
           this.USEDCOUNTERVALUESDATAVOLUME = USEDCOUNTERVALUESDATAVOLUME;
    }


    /**
     * Gets the CUSTOMERID value for this GetUsageTypeUsage.
     * 
     * @return CUSTOMERID
     */
    public java.math.BigDecimal getCUSTOMERID() {
        return CUSTOMERID;
    }


    /**
     * Sets the CUSTOMERID value for this GetUsageTypeUsage.
     * 
     * @param CUSTOMERID
     */
    public void setCUSTOMERID(java.math.BigDecimal CUSTOMERID) {
        this.CUSTOMERID = CUSTOMERID;
    }


    /**
     * Gets the SUBSCRIPTIONID value for this GetUsageTypeUsage.
     * 
     * @return SUBSCRIPTIONID
     */
    public java.math.BigDecimal getSUBSCRIPTIONID() {
        return SUBSCRIPTIONID;
    }


    /**
     * Sets the SUBSCRIPTIONID value for this GetUsageTypeUsage.
     * 
     * @param SUBSCRIPTIONID
     */
    public void setSUBSCRIPTIONID(java.math.BigDecimal SUBSCRIPTIONID) {
        this.SUBSCRIPTIONID = SUBSCRIPTIONID;
    }


    /**
     * Gets the BRANDID value for this GetUsageTypeUsage.
     * 
     * @return BRANDID
     */
    public java.math.BigDecimal getBRANDID() {
        return BRANDID;
    }


    /**
     * Sets the BRANDID value for this GetUsageTypeUsage.
     * 
     * @param BRANDID
     */
    public void setBRANDID(java.math.BigDecimal BRANDID) {
        this.BRANDID = BRANDID;
    }


    /**
     * Gets the USAGETYPE value for this GetUsageTypeUsage.
     * 
     * @return USAGETYPE
     */
    public java.lang.String getUSAGETYPE() {
        return USAGETYPE;
    }


    /**
     * Sets the USAGETYPE value for this GetUsageTypeUsage.
     * 
     * @param USAGETYPE
     */
    public void setUSAGETYPE(java.lang.String USAGETYPE) {
        this.USAGETYPE = USAGETYPE;
    }


    /**
     * Gets the PRODUCTID value for this GetUsageTypeUsage.
     * 
     * @return PRODUCTID
     */
    public java.math.BigInteger getPRODUCTID() {
        return PRODUCTID;
    }


    /**
     * Sets the PRODUCTID value for this GetUsageTypeUsage.
     * 
     * @param PRODUCTID
     */
    public void setPRODUCTID(java.math.BigInteger PRODUCTID) {
        this.PRODUCTID = PRODUCTID;
    }


    /**
     * Gets the PRODUCTNAME value for this GetUsageTypeUsage.
     * 
     * @return PRODUCTNAME
     */
    public java.lang.String getPRODUCTNAME() {
        return PRODUCTNAME;
    }


    /**
     * Sets the PRODUCTNAME value for this GetUsageTypeUsage.
     * 
     * @param PRODUCTNAME
     */
    public void setPRODUCTNAME(java.lang.String PRODUCTNAME) {
        this.PRODUCTNAME = PRODUCTNAME;
    }


    /**
     * Gets the ANO value for this GetUsageTypeUsage.
     * 
     * @return ANO
     */
    public java.lang.String getANO() {
        return ANO;
    }


    /**
     * Sets the ANO value for this GetUsageTypeUsage.
     * 
     * @param ANO
     */
    public void setANO(java.lang.String ANO) {
        this.ANO = ANO;
    }


    /**
     * Gets the BNO value for this GetUsageTypeUsage.
     * 
     * @return BNO
     */
    public java.lang.String getBNO() {
        return BNO;
    }


    /**
     * Sets the BNO value for this GetUsageTypeUsage.
     * 
     * @param BNO
     */
    public void setBNO(java.lang.String BNO) {
        this.BNO = BNO;
    }


    /**
     * Gets the CNO value for this GetUsageTypeUsage.
     * 
     * @return CNO
     */
    public java.lang.String getCNO() {
        return CNO;
    }


    /**
     * Sets the CNO value for this GetUsageTypeUsage.
     * 
     * @param CNO
     */
    public void setCNO(java.lang.String CNO) {
        this.CNO = CNO;
    }


    /**
     * Gets the MOBILECOUNTRYCODE value for this GetUsageTypeUsage.
     * 
     * @return MOBILECOUNTRYCODE
     */
    public java.lang.String getMOBILECOUNTRYCODE() {
        return MOBILECOUNTRYCODE;
    }


    /**
     * Sets the MOBILECOUNTRYCODE value for this GetUsageTypeUsage.
     * 
     * @param MOBILECOUNTRYCODE
     */
    public void setMOBILECOUNTRYCODE(java.lang.String MOBILECOUNTRYCODE) {
        this.MOBILECOUNTRYCODE = MOBILECOUNTRYCODE;
    }


    /**
     * Gets the MOBILENETWORKCODE value for this GetUsageTypeUsage.
     * 
     * @return MOBILENETWORKCODE
     */
    public java.lang.String getMOBILENETWORKCODE() {
        return MOBILENETWORKCODE;
    }


    /**
     * Sets the MOBILENETWORKCODE value for this GetUsageTypeUsage.
     * 
     * @param MOBILENETWORKCODE
     */
    public void setMOBILENETWORKCODE(java.lang.String MOBILENETWORKCODE) {
        this.MOBILENETWORKCODE = MOBILENETWORKCODE;
    }


    /**
     * Gets the CALLTYPE value for this GetUsageTypeUsage.
     * 
     * @return CALLTYPE
     */
    public java.lang.String getCALLTYPE() {
        return CALLTYPE;
    }


    /**
     * Sets the CALLTYPE value for this GetUsageTypeUsage.
     * 
     * @param CALLTYPE
     */
    public void setCALLTYPE(java.lang.String CALLTYPE) {
        this.CALLTYPE = CALLTYPE;
    }


    /**
     * Gets the CALLTYPENAME value for this GetUsageTypeUsage.
     * 
     * @return CALLTYPENAME
     */
    public java.lang.String getCALLTYPENAME() {
        return CALLTYPENAME;
    }


    /**
     * Sets the CALLTYPENAME value for this GetUsageTypeUsage.
     * 
     * @param CALLTYPENAME
     */
    public void setCALLTYPENAME(java.lang.String CALLTYPENAME) {
        this.CALLTYPENAME = CALLTYPENAME;
    }


    /**
     * Gets the CREATIONTIME value for this GetUsageTypeUsage.
     * 
     * @return CREATIONTIME
     */
    public java.util.Calendar getCREATIONTIME() {
        return CREATIONTIME;
    }


    /**
     * Sets the CREATIONTIME value for this GetUsageTypeUsage.
     * 
     * @param CREATIONTIME
     */
    public void setCREATIONTIME(java.util.Calendar CREATIONTIME) {
        this.CREATIONTIME = CREATIONTIME;
    }


    /**
     * Gets the STARTTIME value for this GetUsageTypeUsage.
     * 
     * @return STARTTIME
     */
    public java.util.Calendar getSTARTTIME() {
        return STARTTIME;
    }


    /**
     * Sets the STARTTIME value for this GetUsageTypeUsage.
     * 
     * @param STARTTIME
     */
    public void setSTARTTIME(java.util.Calendar STARTTIME) {
        this.STARTTIME = STARTTIME;
    }


    /**
     * Gets the LOCALSTARTTIME value for this GetUsageTypeUsage.
     * 
     * @return LOCALSTARTTIME
     */
    public java.util.Calendar getLOCALSTARTTIME() {
        return LOCALSTARTTIME;
    }


    /**
     * Sets the LOCALSTARTTIME value for this GetUsageTypeUsage.
     * 
     * @param LOCALSTARTTIME
     */
    public void setLOCALSTARTTIME(java.util.Calendar LOCALSTARTTIME) {
        this.LOCALSTARTTIME = LOCALSTARTTIME;
    }


    /**
     * Gets the DURATION value for this GetUsageTypeUsage.
     * 
     * @return DURATION
     */
    public java.math.BigDecimal getDURATION() {
        return DURATION;
    }


    /**
     * Sets the DURATION value for this GetUsageTypeUsage.
     * 
     * @param DURATION
     */
    public void setDURATION(java.math.BigDecimal DURATION) {
        this.DURATION = DURATION;
    }


    /**
     * Gets the CHARGEDDURATION value for this GetUsageTypeUsage.
     * 
     * @return CHARGEDDURATION
     */
    public java.math.BigDecimal getCHARGEDDURATION() {
        return CHARGEDDURATION;
    }


    /**
     * Sets the CHARGEDDURATION value for this GetUsageTypeUsage.
     * 
     * @param CHARGEDDURATION
     */
    public void setCHARGEDDURATION(java.math.BigDecimal CHARGEDDURATION) {
        this.CHARGEDDURATION = CHARGEDDURATION;
    }


    /**
     * Gets the COST value for this GetUsageTypeUsage.
     * 
     * @return COST
     */
    public java.math.BigDecimal getCOST() {
        return COST;
    }


    /**
     * Sets the COST value for this GetUsageTypeUsage.
     * 
     * @param COST
     */
    public void setCOST(java.math.BigDecimal COST) {
        this.COST = COST;
    }


    /**
     * Gets the INITIALCOST value for this GetUsageTypeUsage.
     * 
     * @return INITIALCOST
     */
    public java.math.BigDecimal getINITIALCOST() {
        return INITIALCOST;
    }


    /**
     * Sets the INITIALCOST value for this GetUsageTypeUsage.
     * 
     * @param INITIALCOST
     */
    public void setINITIALCOST(java.math.BigDecimal INITIALCOST) {
        this.INITIALCOST = INITIALCOST;
    }


    /**
     * Gets the MINUTECOSTCOST value for this GetUsageTypeUsage.
     * 
     * @return MINUTECOSTCOST
     */
    public java.math.BigDecimal getMINUTECOSTCOST() {
        return MINUTECOSTCOST;
    }


    /**
     * Sets the MINUTECOSTCOST value for this GetUsageTypeUsage.
     * 
     * @param MINUTECOSTCOST
     */
    public void setMINUTECOSTCOST(java.math.BigDecimal MINUTECOSTCOST) {
        this.MINUTECOSTCOST = MINUTECOSTCOST;
    }


    /**
     * Gets the TAXAMOUNT value for this GetUsageTypeUsage.
     * 
     * @return TAXAMOUNT
     */
    public java.math.BigDecimal getTAXAMOUNT() {
        return TAXAMOUNT;
    }


    /**
     * Sets the TAXAMOUNT value for this GetUsageTypeUsage.
     * 
     * @param TAXAMOUNT
     */
    public void setTAXAMOUNT(java.math.BigDecimal TAXAMOUNT) {
        this.TAXAMOUNT = TAXAMOUNT;
    }


    /**
     * Gets the POSTBALANCE value for this GetUsageTypeUsage.
     * 
     * @return POSTBALANCE
     */
    public java.math.BigDecimal getPOSTBALANCE() {
        return POSTBALANCE;
    }


    /**
     * Sets the POSTBALANCE value for this GetUsageTypeUsage.
     * 
     * @param POSTBALANCE
     */
    public void setPOSTBALANCE(java.math.BigDecimal POSTBALANCE) {
        this.POSTBALANCE = POSTBALANCE;
    }


    /**
     * Gets the CURRENCYID value for this GetUsageTypeUsage.
     * 
     * @return CURRENCYID
     */
    public java.lang.String getCURRENCYID() {
        return CURRENCYID;
    }


    /**
     * Sets the CURRENCYID value for this GetUsageTypeUsage.
     * 
     * @param CURRENCYID
     */
    public void setCURRENCYID(java.lang.String CURRENCYID) {
        this.CURRENCYID = CURRENCYID;
    }


    /**
     * Gets the DISCONNECTREASON value for this GetUsageTypeUsage.
     * 
     * @return DISCONNECTREASON
     */
    public java.lang.String getDISCONNECTREASON() {
        return DISCONNECTREASON;
    }


    /**
     * Sets the DISCONNECTREASON value for this GetUsageTypeUsage.
     * 
     * @param DISCONNECTREASON
     */
    public void setDISCONNECTREASON(java.lang.String DISCONNECTREASON) {
        this.DISCONNECTREASON = DISCONNECTREASON;
    }


    /**
     * Gets the ORIGZONE value for this GetUsageTypeUsage.
     * 
     * @return ORIGZONE
     */
    public java.lang.String getORIGZONE() {
        return ORIGZONE;
    }


    /**
     * Sets the ORIGZONE value for this GetUsageTypeUsage.
     * 
     * @param ORIGZONE
     */
    public void setORIGZONE(java.lang.String ORIGZONE) {
        this.ORIGZONE = ORIGZONE;
    }


    /**
     * Gets the TERMZONE value for this GetUsageTypeUsage.
     * 
     * @return TERMZONE
     */
    public java.lang.String getTERMZONE() {
        return TERMZONE;
    }


    /**
     * Sets the TERMZONE value for this GetUsageTypeUsage.
     * 
     * @param TERMZONE
     */
    public void setTERMZONE(java.lang.String TERMZONE) {
        this.TERMZONE = TERMZONE;
    }


    /**
     * Gets the TIMEZONEID value for this GetUsageTypeUsage.
     * 
     * @return TIMEZONEID
     */
    public java.lang.String getTIMEZONEID() {
        return TIMEZONEID;
    }


    /**
     * Sets the TIMEZONEID value for this GetUsageTypeUsage.
     * 
     * @param TIMEZONEID
     */
    public void setTIMEZONEID(java.lang.String TIMEZONEID) {
        this.TIMEZONEID = TIMEZONEID;
    }


    /**
     * Gets the ADJUSTMENTTYPENAME value for this GetUsageTypeUsage.
     * 
     * @return ADJUSTMENTTYPENAME
     */
    public java.lang.String getADJUSTMENTTYPENAME() {
        return ADJUSTMENTTYPENAME;
    }


    /**
     * Sets the ADJUSTMENTTYPENAME value for this GetUsageTypeUsage.
     * 
     * @param ADJUSTMENTTYPENAME
     */
    public void setADJUSTMENTTYPENAME(java.lang.String ADJUSTMENTTYPENAME) {
        this.ADJUSTMENTTYPENAME = ADJUSTMENTTYPENAME;
    }


    /**
     * Gets the ADJUSTMENTREASONNAME value for this GetUsageTypeUsage.
     * 
     * @return ADJUSTMENTREASONNAME
     */
    public java.lang.String getADJUSTMENTREASONNAME() {
        return ADJUSTMENTREASONNAME;
    }


    /**
     * Sets the ADJUSTMENTREASONNAME value for this GetUsageTypeUsage.
     * 
     * @param ADJUSTMENTREASONNAME
     */
    public void setADJUSTMENTREASONNAME(java.lang.String ADJUSTMENTREASONNAME) {
        this.ADJUSTMENTREASONNAME = ADJUSTMENTREASONNAME;
    }


    /**
     * Gets the STATUS value for this GetUsageTypeUsage.
     * 
     * @return STATUS
     */
    public java.math.BigDecimal getSTATUS() {
        return STATUS;
    }


    /**
     * Sets the STATUS value for this GetUsageTypeUsage.
     * 
     * @param STATUS
     */
    public void setSTATUS(java.math.BigDecimal STATUS) {
        this.STATUS = STATUS;
    }


    /**
     * Gets the EVENTID value for this GetUsageTypeUsage.
     * 
     * @return EVENTID
     */
    public java.lang.String getEVENTID() {
        return EVENTID;
    }


    /**
     * Sets the EVENTID value for this GetUsageTypeUsage.
     * 
     * @param EVENTID
     */
    public void setEVENTID(java.lang.String EVENTID) {
        this.EVENTID = EVENTID;
    }


    /**
     * Gets the ADDITIONALINFO1 value for this GetUsageTypeUsage.
     * 
     * @return ADDITIONALINFO1
     */
    public java.lang.String getADDITIONALINFO1() {
        return ADDITIONALINFO1;
    }


    /**
     * Sets the ADDITIONALINFO1 value for this GetUsageTypeUsage.
     * 
     * @param ADDITIONALINFO1
     */
    public void setADDITIONALINFO1(java.lang.String ADDITIONALINFO1) {
        this.ADDITIONALINFO1 = ADDITIONALINFO1;
    }


    /**
     * Gets the ADDITIONALINFO2 value for this GetUsageTypeUsage.
     * 
     * @return ADDITIONALINFO2
     */
    public java.lang.String getADDITIONALINFO2() {
        return ADDITIONALINFO2;
    }


    /**
     * Sets the ADDITIONALINFO2 value for this GetUsageTypeUsage.
     * 
     * @param ADDITIONALINFO2
     */
    public void setADDITIONALINFO2(java.lang.String ADDITIONALINFO2) {
        this.ADDITIONALINFO2 = ADDITIONALINFO2;
    }


    /**
     * Gets the ADDITIONALINFO3 value for this GetUsageTypeUsage.
     * 
     * @return ADDITIONALINFO3
     */
    public java.lang.String getADDITIONALINFO3() {
        return ADDITIONALINFO3;
    }


    /**
     * Sets the ADDITIONALINFO3 value for this GetUsageTypeUsage.
     * 
     * @param ADDITIONALINFO3
     */
    public void setADDITIONALINFO3(java.lang.String ADDITIONALINFO3) {
        this.ADDITIONALINFO3 = ADDITIONALINFO3;
    }


    /**
     * Gets the MESSAGEID value for this GetUsageTypeUsage.
     * 
     * @return MESSAGEID
     */
    public java.lang.String getMESSAGEID() {
        return MESSAGEID;
    }


    /**
     * Sets the MESSAGEID value for this GetUsageTypeUsage.
     * 
     * @param MESSAGEID
     */
    public void setMESSAGEID(java.lang.String MESSAGEID) {
        this.MESSAGEID = MESSAGEID;
    }


    /**
     * Gets the CONTENTTYPE value for this GetUsageTypeUsage.
     * 
     * @return CONTENTTYPE
     */
    public java.math.BigDecimal getCONTENTTYPE() {
        return CONTENTTYPE;
    }


    /**
     * Sets the CONTENTTYPE value for this GetUsageTypeUsage.
     * 
     * @param CONTENTTYPE
     */
    public void setCONTENTTYPE(java.math.BigDecimal CONTENTTYPE) {
        this.CONTENTTYPE = CONTENTTYPE;
    }


    /**
     * Gets the MESSAGESIZE value for this GetUsageTypeUsage.
     * 
     * @return MESSAGESIZE
     */
    public java.math.BigDecimal getMESSAGESIZE() {
        return MESSAGESIZE;
    }


    /**
     * Sets the MESSAGESIZE value for this GetUsageTypeUsage.
     * 
     * @param MESSAGESIZE
     */
    public void setMESSAGESIZE(java.math.BigDecimal MESSAGESIZE) {
        this.MESSAGESIZE = MESSAGESIZE;
    }


    /**
     * Gets the SUBSCRIBERIPV4 value for this GetUsageTypeUsage.
     * 
     * @return SUBSCRIBERIPV4
     */
    public java.lang.String getSUBSCRIBERIPV4() {
        return SUBSCRIBERIPV4;
    }


    /**
     * Sets the SUBSCRIBERIPV4 value for this GetUsageTypeUsage.
     * 
     * @param SUBSCRIBERIPV4
     */
    public void setSUBSCRIBERIPV4(java.lang.String SUBSCRIBERIPV4) {
        this.SUBSCRIBERIPV4 = SUBSCRIBERIPV4;
    }


    /**
     * Gets the APN value for this GetUsageTypeUsage.
     * 
     * @return APN
     */
    public java.lang.String getAPN() {
        return APN;
    }


    /**
     * Sets the APN value for this GetUsageTypeUsage.
     * 
     * @param APN
     */
    public void setAPN(java.lang.String APN) {
        this.APN = APN;
    }


    /**
     * Gets the SERVICEID value for this GetUsageTypeUsage.
     * 
     * @return SERVICEID
     */
    public java.math.BigDecimal getSERVICEID() {
        return SERVICEID;
    }


    /**
     * Sets the SERVICEID value for this GetUsageTypeUsage.
     * 
     * @param SERVICEID
     */
    public void setSERVICEID(java.math.BigDecimal SERVICEID) {
        this.SERVICEID = SERVICEID;
    }


    /**
     * Gets the DATAVOLUMESENT value for this GetUsageTypeUsage.
     * 
     * @return DATAVOLUMESENT
     */
    public java.math.BigDecimal getDATAVOLUMESENT() {
        return DATAVOLUMESENT;
    }


    /**
     * Sets the DATAVOLUMESENT value for this GetUsageTypeUsage.
     * 
     * @param DATAVOLUMESENT
     */
    public void setDATAVOLUMESENT(java.math.BigDecimal DATAVOLUMESENT) {
        this.DATAVOLUMESENT = DATAVOLUMESENT;
    }


    /**
     * Gets the DATAVOLUMERECEIVED value for this GetUsageTypeUsage.
     * 
     * @return DATAVOLUMERECEIVED
     */
    public java.math.BigDecimal getDATAVOLUMERECEIVED() {
        return DATAVOLUMERECEIVED;
    }


    /**
     * Sets the DATAVOLUMERECEIVED value for this GetUsageTypeUsage.
     * 
     * @param DATAVOLUMERECEIVED
     */
    public void setDATAVOLUMERECEIVED(java.math.BigDecimal DATAVOLUMERECEIVED) {
        this.DATAVOLUMERECEIVED = DATAVOLUMERECEIVED;
    }


    /**
     * Gets the USEDQUOTA value for this GetUsageTypeUsage.
     * 
     * @return USEDQUOTA
     */
    public java.math.BigDecimal getUSEDQUOTA() {
        return USEDQUOTA;
    }


    /**
     * Sets the USEDQUOTA value for this GetUsageTypeUsage.
     * 
     * @param USEDQUOTA
     */
    public void setUSEDQUOTA(java.math.BigDecimal USEDQUOTA) {
        this.USEDQUOTA = USEDQUOTA;
    }


    /**
     * Gets the QUOTAUNIT value for this GetUsageTypeUsage.
     * 
     * @return QUOTAUNIT
     */
    public java.lang.String getQUOTAUNIT() {
        return QUOTAUNIT;
    }


    /**
     * Sets the QUOTAUNIT value for this GetUsageTypeUsage.
     * 
     * @param QUOTAUNIT
     */
    public void setQUOTAUNIT(java.lang.String QUOTAUNIT) {
        this.QUOTAUNIT = QUOTAUNIT;
    }


    /**
     * Gets the SGSNADDRESS value for this GetUsageTypeUsage.
     * 
     * @return SGSNADDRESS
     */
    public java.lang.String getSGSNADDRESS() {
        return SGSNADDRESS;
    }


    /**
     * Sets the SGSNADDRESS value for this GetUsageTypeUsage.
     * 
     * @param SGSNADDRESS
     */
    public void setSGSNADDRESS(java.lang.String SGSNADDRESS) {
        this.SGSNADDRESS = SGSNADDRESS;
    }


    /**
     * Gets the USEDCOUNTERVALUESAMOUNT value for this GetUsageTypeUsage.
     * 
     * @return USEDCOUNTERVALUESAMOUNT
     */
    public java.math.BigDecimal getUSEDCOUNTERVALUESAMOUNT() {
        return USEDCOUNTERVALUESAMOUNT;
    }


    /**
     * Sets the USEDCOUNTERVALUESAMOUNT value for this GetUsageTypeUsage.
     * 
     * @param USEDCOUNTERVALUESAMOUNT
     */
    public void setUSEDCOUNTERVALUESAMOUNT(java.math.BigDecimal USEDCOUNTERVALUESAMOUNT) {
        this.USEDCOUNTERVALUESAMOUNT = USEDCOUNTERVALUESAMOUNT;
    }


    /**
     * Gets the USEDCOUNTERVALUESTIME value for this GetUsageTypeUsage.
     * 
     * @return USEDCOUNTERVALUESTIME
     */
    public java.math.BigDecimal getUSEDCOUNTERVALUESTIME() {
        return USEDCOUNTERVALUESTIME;
    }


    /**
     * Sets the USEDCOUNTERVALUESTIME value for this GetUsageTypeUsage.
     * 
     * @param USEDCOUNTERVALUESTIME
     */
    public void setUSEDCOUNTERVALUESTIME(java.math.BigDecimal USEDCOUNTERVALUESTIME) {
        this.USEDCOUNTERVALUESTIME = USEDCOUNTERVALUESTIME;
    }


    /**
     * Gets the USEDCOUNTERVALUESINTEGER value for this GetUsageTypeUsage.
     * 
     * @return USEDCOUNTERVALUESINTEGER
     */
    public java.math.BigDecimal getUSEDCOUNTERVALUESINTEGER() {
        return USEDCOUNTERVALUESINTEGER;
    }


    /**
     * Sets the USEDCOUNTERVALUESINTEGER value for this GetUsageTypeUsage.
     * 
     * @param USEDCOUNTERVALUESINTEGER
     */
    public void setUSEDCOUNTERVALUESINTEGER(java.math.BigDecimal USEDCOUNTERVALUESINTEGER) {
        this.USEDCOUNTERVALUESINTEGER = USEDCOUNTERVALUESINTEGER;
    }


    /**
     * Gets the USEDCOUNTERVALUESDATAVOLUME value for this GetUsageTypeUsage.
     * 
     * @return USEDCOUNTERVALUESDATAVOLUME
     */
    public java.math.BigDecimal getUSEDCOUNTERVALUESDATAVOLUME() {
        return USEDCOUNTERVALUESDATAVOLUME;
    }


    /**
     * Sets the USEDCOUNTERVALUESDATAVOLUME value for this GetUsageTypeUsage.
     * 
     * @param USEDCOUNTERVALUESDATAVOLUME
     */
    public void setUSEDCOUNTERVALUESDATAVOLUME(java.math.BigDecimal USEDCOUNTERVALUESDATAVOLUME) {
        this.USEDCOUNTERVALUESDATAVOLUME = USEDCOUNTERVALUESDATAVOLUME;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetUsageTypeUsage)) return false;
        GetUsageTypeUsage other = (GetUsageTypeUsage) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CUSTOMERID==null && other.getCUSTOMERID()==null) || 
             (this.CUSTOMERID!=null &&
              this.CUSTOMERID.equals(other.getCUSTOMERID()))) &&
            ((this.SUBSCRIPTIONID==null && other.getSUBSCRIPTIONID()==null) || 
             (this.SUBSCRIPTIONID!=null &&
              this.SUBSCRIPTIONID.equals(other.getSUBSCRIPTIONID()))) &&
            ((this.BRANDID==null && other.getBRANDID()==null) || 
             (this.BRANDID!=null &&
              this.BRANDID.equals(other.getBRANDID()))) &&
            ((this.USAGETYPE==null && other.getUSAGETYPE()==null) || 
             (this.USAGETYPE!=null &&
              this.USAGETYPE.equals(other.getUSAGETYPE()))) &&
            ((this.PRODUCTID==null && other.getPRODUCTID()==null) || 
             (this.PRODUCTID!=null &&
              this.PRODUCTID.equals(other.getPRODUCTID()))) &&
            ((this.PRODUCTNAME==null && other.getPRODUCTNAME()==null) || 
             (this.PRODUCTNAME!=null &&
              this.PRODUCTNAME.equals(other.getPRODUCTNAME()))) &&
            ((this.ANO==null && other.getANO()==null) || 
             (this.ANO!=null &&
              this.ANO.equals(other.getANO()))) &&
            ((this.BNO==null && other.getBNO()==null) || 
             (this.BNO!=null &&
              this.BNO.equals(other.getBNO()))) &&
            ((this.CNO==null && other.getCNO()==null) || 
             (this.CNO!=null &&
              this.CNO.equals(other.getCNO()))) &&
            ((this.MOBILECOUNTRYCODE==null && other.getMOBILECOUNTRYCODE()==null) || 
             (this.MOBILECOUNTRYCODE!=null &&
              this.MOBILECOUNTRYCODE.equals(other.getMOBILECOUNTRYCODE()))) &&
            ((this.MOBILENETWORKCODE==null && other.getMOBILENETWORKCODE()==null) || 
             (this.MOBILENETWORKCODE!=null &&
              this.MOBILENETWORKCODE.equals(other.getMOBILENETWORKCODE()))) &&
            ((this.CALLTYPE==null && other.getCALLTYPE()==null) || 
             (this.CALLTYPE!=null &&
              this.CALLTYPE.equals(other.getCALLTYPE()))) &&
            ((this.CALLTYPENAME==null && other.getCALLTYPENAME()==null) || 
             (this.CALLTYPENAME!=null &&
              this.CALLTYPENAME.equals(other.getCALLTYPENAME()))) &&
            ((this.CREATIONTIME==null && other.getCREATIONTIME()==null) || 
             (this.CREATIONTIME!=null &&
              this.CREATIONTIME.equals(other.getCREATIONTIME()))) &&
            ((this.STARTTIME==null && other.getSTARTTIME()==null) || 
             (this.STARTTIME!=null &&
              this.STARTTIME.equals(other.getSTARTTIME()))) &&
            ((this.LOCALSTARTTIME==null && other.getLOCALSTARTTIME()==null) || 
             (this.LOCALSTARTTIME!=null &&
              this.LOCALSTARTTIME.equals(other.getLOCALSTARTTIME()))) &&
            ((this.DURATION==null && other.getDURATION()==null) || 
             (this.DURATION!=null &&
              this.DURATION.equals(other.getDURATION()))) &&
            ((this.CHARGEDDURATION==null && other.getCHARGEDDURATION()==null) || 
             (this.CHARGEDDURATION!=null &&
              this.CHARGEDDURATION.equals(other.getCHARGEDDURATION()))) &&
            ((this.COST==null && other.getCOST()==null) || 
             (this.COST!=null &&
              this.COST.equals(other.getCOST()))) &&
            ((this.INITIALCOST==null && other.getINITIALCOST()==null) || 
             (this.INITIALCOST!=null &&
              this.INITIALCOST.equals(other.getINITIALCOST()))) &&
            ((this.MINUTECOSTCOST==null && other.getMINUTECOSTCOST()==null) || 
             (this.MINUTECOSTCOST!=null &&
              this.MINUTECOSTCOST.equals(other.getMINUTECOSTCOST()))) &&
            ((this.TAXAMOUNT==null && other.getTAXAMOUNT()==null) || 
             (this.TAXAMOUNT!=null &&
              this.TAXAMOUNT.equals(other.getTAXAMOUNT()))) &&
            ((this.POSTBALANCE==null && other.getPOSTBALANCE()==null) || 
             (this.POSTBALANCE!=null &&
              this.POSTBALANCE.equals(other.getPOSTBALANCE()))) &&
            ((this.CURRENCYID==null && other.getCURRENCYID()==null) || 
             (this.CURRENCYID!=null &&
              this.CURRENCYID.equals(other.getCURRENCYID()))) &&
            ((this.DISCONNECTREASON==null && other.getDISCONNECTREASON()==null) || 
             (this.DISCONNECTREASON!=null &&
              this.DISCONNECTREASON.equals(other.getDISCONNECTREASON()))) &&
            ((this.ORIGZONE==null && other.getORIGZONE()==null) || 
             (this.ORIGZONE!=null &&
              this.ORIGZONE.equals(other.getORIGZONE()))) &&
            ((this.TERMZONE==null && other.getTERMZONE()==null) || 
             (this.TERMZONE!=null &&
              this.TERMZONE.equals(other.getTERMZONE()))) &&
            ((this.TIMEZONEID==null && other.getTIMEZONEID()==null) || 
             (this.TIMEZONEID!=null &&
              this.TIMEZONEID.equals(other.getTIMEZONEID()))) &&
            ((this.ADJUSTMENTTYPENAME==null && other.getADJUSTMENTTYPENAME()==null) || 
             (this.ADJUSTMENTTYPENAME!=null &&
              this.ADJUSTMENTTYPENAME.equals(other.getADJUSTMENTTYPENAME()))) &&
            ((this.ADJUSTMENTREASONNAME==null && other.getADJUSTMENTREASONNAME()==null) || 
             (this.ADJUSTMENTREASONNAME!=null &&
              this.ADJUSTMENTREASONNAME.equals(other.getADJUSTMENTREASONNAME()))) &&
            ((this.STATUS==null && other.getSTATUS()==null) || 
             (this.STATUS!=null &&
              this.STATUS.equals(other.getSTATUS()))) &&
            ((this.EVENTID==null && other.getEVENTID()==null) || 
             (this.EVENTID!=null &&
              this.EVENTID.equals(other.getEVENTID()))) &&
            ((this.ADDITIONALINFO1==null && other.getADDITIONALINFO1()==null) || 
             (this.ADDITIONALINFO1!=null &&
              this.ADDITIONALINFO1.equals(other.getADDITIONALINFO1()))) &&
            ((this.ADDITIONALINFO2==null && other.getADDITIONALINFO2()==null) || 
             (this.ADDITIONALINFO2!=null &&
              this.ADDITIONALINFO2.equals(other.getADDITIONALINFO2()))) &&
            ((this.ADDITIONALINFO3==null && other.getADDITIONALINFO3()==null) || 
             (this.ADDITIONALINFO3!=null &&
              this.ADDITIONALINFO3.equals(other.getADDITIONALINFO3()))) &&
            ((this.MESSAGEID==null && other.getMESSAGEID()==null) || 
             (this.MESSAGEID!=null &&
              this.MESSAGEID.equals(other.getMESSAGEID()))) &&
            ((this.CONTENTTYPE==null && other.getCONTENTTYPE()==null) || 
             (this.CONTENTTYPE!=null &&
              this.CONTENTTYPE.equals(other.getCONTENTTYPE()))) &&
            ((this.MESSAGESIZE==null && other.getMESSAGESIZE()==null) || 
             (this.MESSAGESIZE!=null &&
              this.MESSAGESIZE.equals(other.getMESSAGESIZE()))) &&
            ((this.SUBSCRIBERIPV4==null && other.getSUBSCRIBERIPV4()==null) || 
             (this.SUBSCRIBERIPV4!=null &&
              this.SUBSCRIBERIPV4.equals(other.getSUBSCRIBERIPV4()))) &&
            ((this.APN==null && other.getAPN()==null) || 
             (this.APN!=null &&
              this.APN.equals(other.getAPN()))) &&
            ((this.SERVICEID==null && other.getSERVICEID()==null) || 
             (this.SERVICEID!=null &&
              this.SERVICEID.equals(other.getSERVICEID()))) &&
            ((this.DATAVOLUMESENT==null && other.getDATAVOLUMESENT()==null) || 
             (this.DATAVOLUMESENT!=null &&
              this.DATAVOLUMESENT.equals(other.getDATAVOLUMESENT()))) &&
            ((this.DATAVOLUMERECEIVED==null && other.getDATAVOLUMERECEIVED()==null) || 
             (this.DATAVOLUMERECEIVED!=null &&
              this.DATAVOLUMERECEIVED.equals(other.getDATAVOLUMERECEIVED()))) &&
            ((this.USEDQUOTA==null && other.getUSEDQUOTA()==null) || 
             (this.USEDQUOTA!=null &&
              this.USEDQUOTA.equals(other.getUSEDQUOTA()))) &&
            ((this.QUOTAUNIT==null && other.getQUOTAUNIT()==null) || 
             (this.QUOTAUNIT!=null &&
              this.QUOTAUNIT.equals(other.getQUOTAUNIT()))) &&
            ((this.SGSNADDRESS==null && other.getSGSNADDRESS()==null) || 
             (this.SGSNADDRESS!=null &&
              this.SGSNADDRESS.equals(other.getSGSNADDRESS()))) &&
            ((this.USEDCOUNTERVALUESAMOUNT==null && other.getUSEDCOUNTERVALUESAMOUNT()==null) || 
             (this.USEDCOUNTERVALUESAMOUNT!=null &&
              this.USEDCOUNTERVALUESAMOUNT.equals(other.getUSEDCOUNTERVALUESAMOUNT()))) &&
            ((this.USEDCOUNTERVALUESTIME==null && other.getUSEDCOUNTERVALUESTIME()==null) || 
             (this.USEDCOUNTERVALUESTIME!=null &&
              this.USEDCOUNTERVALUESTIME.equals(other.getUSEDCOUNTERVALUESTIME()))) &&
            ((this.USEDCOUNTERVALUESINTEGER==null && other.getUSEDCOUNTERVALUESINTEGER()==null) || 
             (this.USEDCOUNTERVALUESINTEGER!=null &&
              this.USEDCOUNTERVALUESINTEGER.equals(other.getUSEDCOUNTERVALUESINTEGER()))) &&
            ((this.USEDCOUNTERVALUESDATAVOLUME==null && other.getUSEDCOUNTERVALUESDATAVOLUME()==null) || 
             (this.USEDCOUNTERVALUESDATAVOLUME!=null &&
              this.USEDCOUNTERVALUESDATAVOLUME.equals(other.getUSEDCOUNTERVALUESDATAVOLUME())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCUSTOMERID() != null) {
            _hashCode += getCUSTOMERID().hashCode();
        }
        if (getSUBSCRIPTIONID() != null) {
            _hashCode += getSUBSCRIPTIONID().hashCode();
        }
        if (getBRANDID() != null) {
            _hashCode += getBRANDID().hashCode();
        }
        if (getUSAGETYPE() != null) {
            _hashCode += getUSAGETYPE().hashCode();
        }
        if (getPRODUCTID() != null) {
            _hashCode += getPRODUCTID().hashCode();
        }
        if (getPRODUCTNAME() != null) {
            _hashCode += getPRODUCTNAME().hashCode();
        }
        if (getANO() != null) {
            _hashCode += getANO().hashCode();
        }
        if (getBNO() != null) {
            _hashCode += getBNO().hashCode();
        }
        if (getCNO() != null) {
            _hashCode += getCNO().hashCode();
        }
        if (getMOBILECOUNTRYCODE() != null) {
            _hashCode += getMOBILECOUNTRYCODE().hashCode();
        }
        if (getMOBILENETWORKCODE() != null) {
            _hashCode += getMOBILENETWORKCODE().hashCode();
        }
        if (getCALLTYPE() != null) {
            _hashCode += getCALLTYPE().hashCode();
        }
        if (getCALLTYPENAME() != null) {
            _hashCode += getCALLTYPENAME().hashCode();
        }
        if (getCREATIONTIME() != null) {
            _hashCode += getCREATIONTIME().hashCode();
        }
        if (getSTARTTIME() != null) {
            _hashCode += getSTARTTIME().hashCode();
        }
        if (getLOCALSTARTTIME() != null) {
            _hashCode += getLOCALSTARTTIME().hashCode();
        }
        if (getDURATION() != null) {
            _hashCode += getDURATION().hashCode();
        }
        if (getCHARGEDDURATION() != null) {
            _hashCode += getCHARGEDDURATION().hashCode();
        }
        if (getCOST() != null) {
            _hashCode += getCOST().hashCode();
        }
        if (getINITIALCOST() != null) {
            _hashCode += getINITIALCOST().hashCode();
        }
        if (getMINUTECOSTCOST() != null) {
            _hashCode += getMINUTECOSTCOST().hashCode();
        }
        if (getTAXAMOUNT() != null) {
            _hashCode += getTAXAMOUNT().hashCode();
        }
        if (getPOSTBALANCE() != null) {
            _hashCode += getPOSTBALANCE().hashCode();
        }
        if (getCURRENCYID() != null) {
            _hashCode += getCURRENCYID().hashCode();
        }
        if (getDISCONNECTREASON() != null) {
            _hashCode += getDISCONNECTREASON().hashCode();
        }
        if (getORIGZONE() != null) {
            _hashCode += getORIGZONE().hashCode();
        }
        if (getTERMZONE() != null) {
            _hashCode += getTERMZONE().hashCode();
        }
        if (getTIMEZONEID() != null) {
            _hashCode += getTIMEZONEID().hashCode();
        }
        if (getADJUSTMENTTYPENAME() != null) {
            _hashCode += getADJUSTMENTTYPENAME().hashCode();
        }
        if (getADJUSTMENTREASONNAME() != null) {
            _hashCode += getADJUSTMENTREASONNAME().hashCode();
        }
        if (getSTATUS() != null) {
            _hashCode += getSTATUS().hashCode();
        }
        if (getEVENTID() != null) {
            _hashCode += getEVENTID().hashCode();
        }
        if (getADDITIONALINFO1() != null) {
            _hashCode += getADDITIONALINFO1().hashCode();
        }
        if (getADDITIONALINFO2() != null) {
            _hashCode += getADDITIONALINFO2().hashCode();
        }
        if (getADDITIONALINFO3() != null) {
            _hashCode += getADDITIONALINFO3().hashCode();
        }
        if (getMESSAGEID() != null) {
            _hashCode += getMESSAGEID().hashCode();
        }
        if (getCONTENTTYPE() != null) {
            _hashCode += getCONTENTTYPE().hashCode();
        }
        if (getMESSAGESIZE() != null) {
            _hashCode += getMESSAGESIZE().hashCode();
        }
        if (getSUBSCRIBERIPV4() != null) {
            _hashCode += getSUBSCRIBERIPV4().hashCode();
        }
        if (getAPN() != null) {
            _hashCode += getAPN().hashCode();
        }
        if (getSERVICEID() != null) {
            _hashCode += getSERVICEID().hashCode();
        }
        if (getDATAVOLUMESENT() != null) {
            _hashCode += getDATAVOLUMESENT().hashCode();
        }
        if (getDATAVOLUMERECEIVED() != null) {
            _hashCode += getDATAVOLUMERECEIVED().hashCode();
        }
        if (getUSEDQUOTA() != null) {
            _hashCode += getUSEDQUOTA().hashCode();
        }
        if (getQUOTAUNIT() != null) {
            _hashCode += getQUOTAUNIT().hashCode();
        }
        if (getSGSNADDRESS() != null) {
            _hashCode += getSGSNADDRESS().hashCode();
        }
        if (getUSEDCOUNTERVALUESAMOUNT() != null) {
            _hashCode += getUSEDCOUNTERVALUESAMOUNT().hashCode();
        }
        if (getUSEDCOUNTERVALUESTIME() != null) {
            _hashCode += getUSEDCOUNTERVALUESTIME().hashCode();
        }
        if (getUSEDCOUNTERVALUESINTEGER() != null) {
            _hashCode += getUSEDCOUNTERVALUESINTEGER().hashCode();
        }
        if (getUSEDCOUNTERVALUESDATAVOLUME() != null) {
            _hashCode += getUSEDCOUNTERVALUESDATAVOLUME().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetUsageTypeUsage.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">getUsageType>usage"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CUSTOMERID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CUSTOMERID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SUBSCRIPTIONID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SUBSCRIPTIONID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BRANDID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BRANDID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("USAGETYPE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "USAGETYPE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRODUCTID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PRODUCTID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PRODUCTNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PRODUCTNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ANO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ANO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BNO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BNO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CNO");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CNO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MOBILECOUNTRYCODE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MOBILECOUNTRYCODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MOBILENETWORKCODE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MOBILENETWORKCODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CALLTYPE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CALLTYPE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CALLTYPENAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CALLTYPENAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CREATIONTIME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CREATIONTIME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STARTTIME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "STARTTIME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LOCALSTARTTIME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LOCALSTARTTIME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DURATION");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DURATION"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CHARGEDDURATION");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CHARGEDDURATION"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("COST");
        elemField.setXmlName(new javax.xml.namespace.QName("", "COST"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("INITIALCOST");
        elemField.setXmlName(new javax.xml.namespace.QName("", "INITIALCOST"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MINUTECOSTCOST");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MINUTECOSTCOST"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TAXAMOUNT");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TAXAMOUNT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSTBALANCE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "POSTBALANCE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CURRENCYID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CURRENCYID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DISCONNECTREASON");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DISCONNECTREASON"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ORIGZONE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ORIGZONE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TERMZONE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TERMZONE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TIMEZONEID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "TIMEZONEID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADJUSTMENTTYPENAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ADJUSTMENTTYPENAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADJUSTMENTREASONNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ADJUSTMENTREASONNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STATUS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "STATUS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EVENTID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "EVENTID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDITIONALINFO1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ADDITIONALINFO1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDITIONALINFO2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ADDITIONALINFO2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDITIONALINFO3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ADDITIONALINFO3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MESSAGEID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MESSAGEID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTENTTYPE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CONTENTTYPE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MESSAGESIZE");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MESSAGESIZE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SUBSCRIBERIPV4");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SUBSCRIBERIPV4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("APN");
        elemField.setXmlName(new javax.xml.namespace.QName("", "APN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SERVICEID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SERVICEID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATAVOLUMESENT");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATAVOLUMESENT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATAVOLUMERECEIVED");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DATAVOLUMERECEIVED"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("USEDQUOTA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "USEDQUOTA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("QUOTAUNIT");
        elemField.setXmlName(new javax.xml.namespace.QName("", "QUOTAUNIT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SGSNADDRESS");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SGSNADDRESS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("USEDCOUNTERVALUESAMOUNT");
        elemField.setXmlName(new javax.xml.namespace.QName("", "USEDCOUNTERVALUESAMOUNT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("USEDCOUNTERVALUESTIME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "USEDCOUNTERVALUESTIME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("USEDCOUNTERVALUESINTEGER");
        elemField.setXmlName(new javax.xml.namespace.QName("", "USEDCOUNTERVALUESINTEGER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("USEDCOUNTERVALUESDATAVOLUME");
        elemField.setXmlName(new javax.xml.namespace.QName("", "USEDCOUNTERVALUESDATAVOLUME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
