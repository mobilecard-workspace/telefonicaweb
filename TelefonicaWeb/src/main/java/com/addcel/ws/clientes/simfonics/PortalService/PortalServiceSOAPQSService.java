/**
 * PortalServiceSOAPQSService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public interface PortalServiceSOAPQSService extends javax.xml.rpc.Service {
    public java.lang.String getPortalServiceSOAPQSPortAddress();

    public com.addcel.ws.clientes.simfonics.PortalService.PortalService getPortalServiceSOAPQSPort() throws javax.xml.rpc.ServiceException;

    public com.addcel.ws.clientes.simfonics.PortalService.PortalService getPortalServiceSOAPQSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
