/**
 * CustomerType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class CustomerType  implements java.io.Serializable {
    private java.math.BigInteger id;

    private java.math.BigInteger parentId;

    private java.lang.String language;

    private java.math.BigInteger brandId;

    private java.lang.String identityNum;

    private java.math.BigInteger customerType;

    private java.lang.String customerTypeName;

    private java.math.BigInteger customerStatus;

    private java.lang.String customerStatusName;

    private java.lang.String firstName;

    private java.lang.String lastName;

    private java.util.Calendar birthDate;

    private java.lang.String userName;

    private com.addcel.ws.clientes.simfonics.PortalService.CustomerProperties_OUT_RowSetCustomerProperty[] customerProperties;

    private com.addcel.ws.clientes.simfonics.PortalService.CustomerAccounts_OUT_RowSetCustomerAccount[] customerAccounts;

    public CustomerType() {
    }

    public CustomerType(
           java.math.BigInteger id,
           java.math.BigInteger parentId,
           java.lang.String language,
           java.math.BigInteger brandId,
           java.lang.String identityNum,
           java.math.BigInteger customerType,
           java.lang.String customerTypeName,
           java.math.BigInteger customerStatus,
           java.lang.String customerStatusName,
           java.lang.String firstName,
           java.lang.String lastName,
           java.util.Calendar birthDate,
           java.lang.String userName,
           com.addcel.ws.clientes.simfonics.PortalService.CustomerProperties_OUT_RowSetCustomerProperty[] customerProperties,
           com.addcel.ws.clientes.simfonics.PortalService.CustomerAccounts_OUT_RowSetCustomerAccount[] customerAccounts) {
           this.id = id;
           this.parentId = parentId;
           this.language = language;
           this.brandId = brandId;
           this.identityNum = identityNum;
           this.customerType = customerType;
           this.customerTypeName = customerTypeName;
           this.customerStatus = customerStatus;
           this.customerStatusName = customerStatusName;
           this.firstName = firstName;
           this.lastName = lastName;
           this.birthDate = birthDate;
           this.userName = userName;
           this.customerProperties = customerProperties;
           this.customerAccounts = customerAccounts;
    }


    /**
     * Gets the id value for this CustomerType.
     * 
     * @return id
     */
    public java.math.BigInteger getId() {
        return id;
    }


    /**
     * Sets the id value for this CustomerType.
     * 
     * @param id
     */
    public void setId(java.math.BigInteger id) {
        this.id = id;
    }


    /**
     * Gets the parentId value for this CustomerType.
     * 
     * @return parentId
     */
    public java.math.BigInteger getParentId() {
        return parentId;
    }


    /**
     * Sets the parentId value for this CustomerType.
     * 
     * @param parentId
     */
    public void setParentId(java.math.BigInteger parentId) {
        this.parentId = parentId;
    }


    /**
     * Gets the language value for this CustomerType.
     * 
     * @return language
     */
    public java.lang.String getLanguage() {
        return language;
    }


    /**
     * Sets the language value for this CustomerType.
     * 
     * @param language
     */
    public void setLanguage(java.lang.String language) {
        this.language = language;
    }


    /**
     * Gets the brandId value for this CustomerType.
     * 
     * @return brandId
     */
    public java.math.BigInteger getBrandId() {
        return brandId;
    }


    /**
     * Sets the brandId value for this CustomerType.
     * 
     * @param brandId
     */
    public void setBrandId(java.math.BigInteger brandId) {
        this.brandId = brandId;
    }


    /**
     * Gets the identityNum value for this CustomerType.
     * 
     * @return identityNum
     */
    public java.lang.String getIdentityNum() {
        return identityNum;
    }


    /**
     * Sets the identityNum value for this CustomerType.
     * 
     * @param identityNum
     */
    public void setIdentityNum(java.lang.String identityNum) {
        this.identityNum = identityNum;
    }


    /**
     * Gets the customerType value for this CustomerType.
     * 
     * @return customerType
     */
    public java.math.BigInteger getCustomerType() {
        return customerType;
    }


    /**
     * Sets the customerType value for this CustomerType.
     * 
     * @param customerType
     */
    public void setCustomerType(java.math.BigInteger customerType) {
        this.customerType = customerType;
    }


    /**
     * Gets the customerTypeName value for this CustomerType.
     * 
     * @return customerTypeName
     */
    public java.lang.String getCustomerTypeName() {
        return customerTypeName;
    }


    /**
     * Sets the customerTypeName value for this CustomerType.
     * 
     * @param customerTypeName
     */
    public void setCustomerTypeName(java.lang.String customerTypeName) {
        this.customerTypeName = customerTypeName;
    }


    /**
     * Gets the customerStatus value for this CustomerType.
     * 
     * @return customerStatus
     */
    public java.math.BigInteger getCustomerStatus() {
        return customerStatus;
    }


    /**
     * Sets the customerStatus value for this CustomerType.
     * 
     * @param customerStatus
     */
    public void setCustomerStatus(java.math.BigInteger customerStatus) {
        this.customerStatus = customerStatus;
    }


    /**
     * Gets the customerStatusName value for this CustomerType.
     * 
     * @return customerStatusName
     */
    public java.lang.String getCustomerStatusName() {
        return customerStatusName;
    }


    /**
     * Sets the customerStatusName value for this CustomerType.
     * 
     * @param customerStatusName
     */
    public void setCustomerStatusName(java.lang.String customerStatusName) {
        this.customerStatusName = customerStatusName;
    }


    /**
     * Gets the firstName value for this CustomerType.
     * 
     * @return firstName
     */
    public java.lang.String getFirstName() {
        return firstName;
    }


    /**
     * Sets the firstName value for this CustomerType.
     * 
     * @param firstName
     */
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }


    /**
     * Gets the lastName value for this CustomerType.
     * 
     * @return lastName
     */
    public java.lang.String getLastName() {
        return lastName;
    }


    /**
     * Sets the lastName value for this CustomerType.
     * 
     * @param lastName
     */
    public void setLastName(java.lang.String lastName) {
        this.lastName = lastName;
    }


    /**
     * Gets the birthDate value for this CustomerType.
     * 
     * @return birthDate
     */
    public java.util.Calendar getBirthDate() {
        return birthDate;
    }


    /**
     * Sets the birthDate value for this CustomerType.
     * 
     * @param birthDate
     */
    public void setBirthDate(java.util.Calendar birthDate) {
        this.birthDate = birthDate;
    }


    /**
     * Gets the userName value for this CustomerType.
     * 
     * @return userName
     */
    public java.lang.String getUserName() {
        return userName;
    }


    /**
     * Sets the userName value for this CustomerType.
     * 
     * @param userName
     */
    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }


    /**
     * Gets the customerProperties value for this CustomerType.
     * 
     * @return customerProperties
     */
    public com.addcel.ws.clientes.simfonics.PortalService.CustomerProperties_OUT_RowSetCustomerProperty[] getCustomerProperties() {
        return customerProperties;
    }


    /**
     * Sets the customerProperties value for this CustomerType.
     * 
     * @param customerProperties
     */
    public void setCustomerProperties(com.addcel.ws.clientes.simfonics.PortalService.CustomerProperties_OUT_RowSetCustomerProperty[] customerProperties) {
        this.customerProperties = customerProperties;
    }


    /**
     * Gets the customerAccounts value for this CustomerType.
     * 
     * @return customerAccounts
     */
    public com.addcel.ws.clientes.simfonics.PortalService.CustomerAccounts_OUT_RowSetCustomerAccount[] getCustomerAccounts() {
        return customerAccounts;
    }


    /**
     * Sets the customerAccounts value for this CustomerType.
     * 
     * @param customerAccounts
     */
    public void setCustomerAccounts(com.addcel.ws.clientes.simfonics.PortalService.CustomerAccounts_OUT_RowSetCustomerAccount[] customerAccounts) {
        this.customerAccounts = customerAccounts;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerType)) return false;
        CustomerType other = (CustomerType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.parentId==null && other.getParentId()==null) || 
             (this.parentId!=null &&
              this.parentId.equals(other.getParentId()))) &&
            ((this.language==null && other.getLanguage()==null) || 
             (this.language!=null &&
              this.language.equals(other.getLanguage()))) &&
            ((this.brandId==null && other.getBrandId()==null) || 
             (this.brandId!=null &&
              this.brandId.equals(other.getBrandId()))) &&
            ((this.identityNum==null && other.getIdentityNum()==null) || 
             (this.identityNum!=null &&
              this.identityNum.equals(other.getIdentityNum()))) &&
            ((this.customerType==null && other.getCustomerType()==null) || 
             (this.customerType!=null &&
              this.customerType.equals(other.getCustomerType()))) &&
            ((this.customerTypeName==null && other.getCustomerTypeName()==null) || 
             (this.customerTypeName!=null &&
              this.customerTypeName.equals(other.getCustomerTypeName()))) &&
            ((this.customerStatus==null && other.getCustomerStatus()==null) || 
             (this.customerStatus!=null &&
              this.customerStatus.equals(other.getCustomerStatus()))) &&
            ((this.customerStatusName==null && other.getCustomerStatusName()==null) || 
             (this.customerStatusName!=null &&
              this.customerStatusName.equals(other.getCustomerStatusName()))) &&
            ((this.firstName==null && other.getFirstName()==null) || 
             (this.firstName!=null &&
              this.firstName.equals(other.getFirstName()))) &&
            ((this.lastName==null && other.getLastName()==null) || 
             (this.lastName!=null &&
              this.lastName.equals(other.getLastName()))) &&
            ((this.birthDate==null && other.getBirthDate()==null) || 
             (this.birthDate!=null &&
              this.birthDate.equals(other.getBirthDate()))) &&
            ((this.userName==null && other.getUserName()==null) || 
             (this.userName!=null &&
              this.userName.equals(other.getUserName()))) &&
            ((this.customerProperties==null && other.getCustomerProperties()==null) || 
             (this.customerProperties!=null &&
              java.util.Arrays.equals(this.customerProperties, other.getCustomerProperties()))) &&
            ((this.customerAccounts==null && other.getCustomerAccounts()==null) || 
             (this.customerAccounts!=null &&
              java.util.Arrays.equals(this.customerAccounts, other.getCustomerAccounts())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getParentId() != null) {
            _hashCode += getParentId().hashCode();
        }
        if (getLanguage() != null) {
            _hashCode += getLanguage().hashCode();
        }
        if (getBrandId() != null) {
            _hashCode += getBrandId().hashCode();
        }
        if (getIdentityNum() != null) {
            _hashCode += getIdentityNum().hashCode();
        }
        if (getCustomerType() != null) {
            _hashCode += getCustomerType().hashCode();
        }
        if (getCustomerTypeName() != null) {
            _hashCode += getCustomerTypeName().hashCode();
        }
        if (getCustomerStatus() != null) {
            _hashCode += getCustomerStatus().hashCode();
        }
        if (getCustomerStatusName() != null) {
            _hashCode += getCustomerStatusName().hashCode();
        }
        if (getFirstName() != null) {
            _hashCode += getFirstName().hashCode();
        }
        if (getLastName() != null) {
            _hashCode += getLastName().hashCode();
        }
        if (getBirthDate() != null) {
            _hashCode += getBirthDate().hashCode();
        }
        if (getUserName() != null) {
            _hashCode += getUserName().hashCode();
        }
        if (getCustomerProperties() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCustomerProperties());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCustomerProperties(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCustomerAccounts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCustomerAccounts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCustomerAccounts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", "CustomerType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "parentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("language");
        elemField.setXmlName(new javax.xml.namespace.QName("", "language"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("brandId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "brandId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identityNum");
        elemField.setXmlName(new javax.xml.namespace.QName("", "identityNum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customerType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customerTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customerStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerStatusName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customerStatusName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "firstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("birthDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "birthDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "userName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerProperties");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customerProperties"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">customerProperties_OUT_RowSet>customerProperty"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "customerProperty"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerAccounts");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customerAccounts"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">customerAccounts_OUT_RowSet>customerAccount"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "customerAccount"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
