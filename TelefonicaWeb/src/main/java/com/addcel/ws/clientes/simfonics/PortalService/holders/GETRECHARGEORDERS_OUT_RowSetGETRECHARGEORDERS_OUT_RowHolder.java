/**
 * GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_RowHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService.holders;

public final class GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_RowHolder implements javax.xml.rpc.holders.Holder {
    public com.addcel.ws.clientes.simfonics.PortalService.GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row value;

    public GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_RowHolder() {
    }

    public GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_RowHolder(com.addcel.ws.clientes.simfonics.PortalService.GETRECHARGEORDERS_OUT_RowSetGETRECHARGEORDERS_OUT_Row value) {
        this.value = value;
    }

}
