/**
 * GetOrdersCatalogByBrand.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService;

public class GetOrdersCatalogByBrand  implements java.io.Serializable {
    private java.lang.String country;

    private java.math.BigInteger brandId;

    private java.math.BigInteger ordersTypeId;

    private java.math.BigInteger ordersCategoryId;

    private java.math.BigInteger ordersSubCategoryId;

    public GetOrdersCatalogByBrand() {
    }

    public GetOrdersCatalogByBrand(
           java.lang.String country,
           java.math.BigInteger brandId,
           java.math.BigInteger ordersTypeId,
           java.math.BigInteger ordersCategoryId,
           java.math.BigInteger ordersSubCategoryId) {
           this.country = country;
           this.brandId = brandId;
           this.ordersTypeId = ordersTypeId;
           this.ordersCategoryId = ordersCategoryId;
           this.ordersSubCategoryId = ordersSubCategoryId;
    }


    /**
     * Gets the country value for this GetOrdersCatalogByBrand.
     * 
     * @return country
     */
    public java.lang.String getCountry() {
        return country;
    }


    /**
     * Sets the country value for this GetOrdersCatalogByBrand.
     * 
     * @param country
     */
    public void setCountry(java.lang.String country) {
        this.country = country;
    }


    /**
     * Gets the brandId value for this GetOrdersCatalogByBrand.
     * 
     * @return brandId
     */
    public java.math.BigInteger getBrandId() {
        return brandId;
    }


    /**
     * Sets the brandId value for this GetOrdersCatalogByBrand.
     * 
     * @param brandId
     */
    public void setBrandId(java.math.BigInteger brandId) {
        this.brandId = brandId;
    }


    /**
     * Gets the ordersTypeId value for this GetOrdersCatalogByBrand.
     * 
     * @return ordersTypeId
     */
    public java.math.BigInteger getOrdersTypeId() {
        return ordersTypeId;
    }


    /**
     * Sets the ordersTypeId value for this GetOrdersCatalogByBrand.
     * 
     * @param ordersTypeId
     */
    public void setOrdersTypeId(java.math.BigInteger ordersTypeId) {
        this.ordersTypeId = ordersTypeId;
    }


    /**
     * Gets the ordersCategoryId value for this GetOrdersCatalogByBrand.
     * 
     * @return ordersCategoryId
     */
    public java.math.BigInteger getOrdersCategoryId() {
        return ordersCategoryId;
    }


    /**
     * Sets the ordersCategoryId value for this GetOrdersCatalogByBrand.
     * 
     * @param ordersCategoryId
     */
    public void setOrdersCategoryId(java.math.BigInteger ordersCategoryId) {
        this.ordersCategoryId = ordersCategoryId;
    }


    /**
     * Gets the ordersSubCategoryId value for this GetOrdersCatalogByBrand.
     * 
     * @return ordersSubCategoryId
     */
    public java.math.BigInteger getOrdersSubCategoryId() {
        return ordersSubCategoryId;
    }


    /**
     * Sets the ordersSubCategoryId value for this GetOrdersCatalogByBrand.
     * 
     * @param ordersSubCategoryId
     */
    public void setOrdersSubCategoryId(java.math.BigInteger ordersSubCategoryId) {
        this.ordersSubCategoryId = ordersSubCategoryId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetOrdersCatalogByBrand)) return false;
        GetOrdersCatalogByBrand other = (GetOrdersCatalogByBrand) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.country==null && other.getCountry()==null) || 
             (this.country!=null &&
              this.country.equals(other.getCountry()))) &&
            ((this.brandId==null && other.getBrandId()==null) || 
             (this.brandId!=null &&
              this.brandId.equals(other.getBrandId()))) &&
            ((this.ordersTypeId==null && other.getOrdersTypeId()==null) || 
             (this.ordersTypeId!=null &&
              this.ordersTypeId.equals(other.getOrdersTypeId()))) &&
            ((this.ordersCategoryId==null && other.getOrdersCategoryId()==null) || 
             (this.ordersCategoryId!=null &&
              this.ordersCategoryId.equals(other.getOrdersCategoryId()))) &&
            ((this.ordersSubCategoryId==null && other.getOrdersSubCategoryId()==null) || 
             (this.ordersSubCategoryId!=null &&
              this.ordersSubCategoryId.equals(other.getOrdersSubCategoryId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCountry() != null) {
            _hashCode += getCountry().hashCode();
        }
        if (getBrandId() != null) {
            _hashCode += getBrandId().hashCode();
        }
        if (getOrdersTypeId() != null) {
            _hashCode += getOrdersTypeId().hashCode();
        }
        if (getOrdersCategoryId() != null) {
            _hashCode += getOrdersCategoryId().hashCode();
        }
        if (getOrdersSubCategoryId() != null) {
            _hashCode += getOrdersSubCategoryId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetOrdersCatalogByBrand.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/AdminService/", ">getOrdersCatalogByBrand"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country");
        elemField.setXmlName(new javax.xml.namespace.QName("", "country"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("brandId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "brandId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ordersTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ordersTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ordersCategoryId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ordersCategoryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ordersSubCategoryId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ordersSubCategoryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
