package com.addcel.ws.clientes.handler;

import java.io.ByteArrayOutputStream;
import javax.xml.namespace.QName;
import javax.xml.rpc.handler.GenericHandler;
import javax.xml.rpc.handler.MessageContext;
import javax.xml.rpc.handler.soap.SOAPMessageContext;
import javax.xml.soap.SOAPMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class SimpleHandler extends GenericHandler {
  private static final Logger log = LoggerFactory.getLogger(SimpleHandler.class);

  public QName[] getHeaders() {
    return null;
  }

  public boolean handleResponse(MessageContext context) {
    log.info("Response: {}", getStringMessage(context));
    return super.handleResponse(context);
  }

  public boolean handleRequest(MessageContext context) {
    log.info("Response: {}", getStringMessage(context));
    return super.handleRequest(context);
  }

  public boolean handleFault(MessageContext context) {
    log.info("Response: {}", getStringMessage(context));
    return super.handleFault(context);
  }

  private String getStringMessage(MessageContext context) {
    String res = null;
    try {
      SOAPMessageContext ctx = (SOAPMessageContext) context;
      ByteArrayOutputStream stream = new ByteArrayOutputStream();
      ctx.getMessage().writeTo(stream);
      byte[] items = stream.toByteArray();
      res = new String(items);
    } catch (Exception localException) {
    }
    return res;
  }
}
