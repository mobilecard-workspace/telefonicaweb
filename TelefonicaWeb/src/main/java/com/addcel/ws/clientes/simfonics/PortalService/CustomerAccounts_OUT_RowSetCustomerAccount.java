/**
 * CustomerAccounts_OUT_RowSetCustomerAccount.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class CustomerAccounts_OUT_RowSetCustomerAccount  implements java.io.Serializable {
    private java.math.BigInteger id;

    private java.math.BigInteger accountType;

    private java.lang.String accountTypeName;

    private java.math.BigDecimal balance;

    private java.math.BigInteger balanceStatus;

    private java.lang.String balanceStatusName;

    private java.util.Calendar balanceExpirationDate;

    private com.addcel.ws.clientes.simfonics.PortalService.AccountSubscriptions_OUT_RowSetSubscription[] accountSubscriptions;

    public CustomerAccounts_OUT_RowSetCustomerAccount() {
    }

    public CustomerAccounts_OUT_RowSetCustomerAccount(
           java.math.BigInteger id,
           java.math.BigInteger accountType,
           java.lang.String accountTypeName,
           java.math.BigDecimal balance,
           java.math.BigInteger balanceStatus,
           java.lang.String balanceStatusName,
           java.util.Calendar balanceExpirationDate,
           com.addcel.ws.clientes.simfonics.PortalService.AccountSubscriptions_OUT_RowSetSubscription[] accountSubscriptions) {
           this.id = id;
           this.accountType = accountType;
           this.accountTypeName = accountTypeName;
           this.balance = balance;
           this.balanceStatus = balanceStatus;
           this.balanceStatusName = balanceStatusName;
           this.balanceExpirationDate = balanceExpirationDate;
           this.accountSubscriptions = accountSubscriptions;
    }


    /**
     * Gets the id value for this CustomerAccounts_OUT_RowSetCustomerAccount.
     * 
     * @return id
     */
    public java.math.BigInteger getId() {
        return id;
    }


    /**
     * Sets the id value for this CustomerAccounts_OUT_RowSetCustomerAccount.
     * 
     * @param id
     */
    public void setId(java.math.BigInteger id) {
        this.id = id;
    }


    /**
     * Gets the accountType value for this CustomerAccounts_OUT_RowSetCustomerAccount.
     * 
     * @return accountType
     */
    public java.math.BigInteger getAccountType() {
        return accountType;
    }


    /**
     * Sets the accountType value for this CustomerAccounts_OUT_RowSetCustomerAccount.
     * 
     * @param accountType
     */
    public void setAccountType(java.math.BigInteger accountType) {
        this.accountType = accountType;
    }


    /**
     * Gets the accountTypeName value for this CustomerAccounts_OUT_RowSetCustomerAccount.
     * 
     * @return accountTypeName
     */
    public java.lang.String getAccountTypeName() {
        return accountTypeName;
    }


    /**
     * Sets the accountTypeName value for this CustomerAccounts_OUT_RowSetCustomerAccount.
     * 
     * @param accountTypeName
     */
    public void setAccountTypeName(java.lang.String accountTypeName) {
        this.accountTypeName = accountTypeName;
    }


    /**
     * Gets the balance value for this CustomerAccounts_OUT_RowSetCustomerAccount.
     * 
     * @return balance
     */
    public java.math.BigDecimal getBalance() {
        return balance;
    }


    /**
     * Sets the balance value for this CustomerAccounts_OUT_RowSetCustomerAccount.
     * 
     * @param balance
     */
    public void setBalance(java.math.BigDecimal balance) {
        this.balance = balance;
    }


    /**
     * Gets the balanceStatus value for this CustomerAccounts_OUT_RowSetCustomerAccount.
     * 
     * @return balanceStatus
     */
    public java.math.BigInteger getBalanceStatus() {
        return balanceStatus;
    }


    /**
     * Sets the balanceStatus value for this CustomerAccounts_OUT_RowSetCustomerAccount.
     * 
     * @param balanceStatus
     */
    public void setBalanceStatus(java.math.BigInteger balanceStatus) {
        this.balanceStatus = balanceStatus;
    }


    /**
     * Gets the balanceStatusName value for this CustomerAccounts_OUT_RowSetCustomerAccount.
     * 
     * @return balanceStatusName
     */
    public java.lang.String getBalanceStatusName() {
        return balanceStatusName;
    }


    /**
     * Sets the balanceStatusName value for this CustomerAccounts_OUT_RowSetCustomerAccount.
     * 
     * @param balanceStatusName
     */
    public void setBalanceStatusName(java.lang.String balanceStatusName) {
        this.balanceStatusName = balanceStatusName;
    }


    /**
     * Gets the balanceExpirationDate value for this CustomerAccounts_OUT_RowSetCustomerAccount.
     * 
     * @return balanceExpirationDate
     */
    public java.util.Calendar getBalanceExpirationDate() {
        return balanceExpirationDate;
    }


    /**
     * Sets the balanceExpirationDate value for this CustomerAccounts_OUT_RowSetCustomerAccount.
     * 
     * @param balanceExpirationDate
     */
    public void setBalanceExpirationDate(java.util.Calendar balanceExpirationDate) {
        this.balanceExpirationDate = balanceExpirationDate;
    }


    /**
     * Gets the accountSubscriptions value for this CustomerAccounts_OUT_RowSetCustomerAccount.
     * 
     * @return accountSubscriptions
     */
    public com.addcel.ws.clientes.simfonics.PortalService.AccountSubscriptions_OUT_RowSetSubscription[] getAccountSubscriptions() {
        return accountSubscriptions;
    }


    /**
     * Sets the accountSubscriptions value for this CustomerAccounts_OUT_RowSetCustomerAccount.
     * 
     * @param accountSubscriptions
     */
    public void setAccountSubscriptions(com.addcel.ws.clientes.simfonics.PortalService.AccountSubscriptions_OUT_RowSetSubscription[] accountSubscriptions) {
        this.accountSubscriptions = accountSubscriptions;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerAccounts_OUT_RowSetCustomerAccount)) return false;
        CustomerAccounts_OUT_RowSetCustomerAccount other = (CustomerAccounts_OUT_RowSetCustomerAccount) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.accountType==null && other.getAccountType()==null) || 
             (this.accountType!=null &&
              this.accountType.equals(other.getAccountType()))) &&
            ((this.accountTypeName==null && other.getAccountTypeName()==null) || 
             (this.accountTypeName!=null &&
              this.accountTypeName.equals(other.getAccountTypeName()))) &&
            ((this.balance==null && other.getBalance()==null) || 
             (this.balance!=null &&
              this.balance.equals(other.getBalance()))) &&
            ((this.balanceStatus==null && other.getBalanceStatus()==null) || 
             (this.balanceStatus!=null &&
              this.balanceStatus.equals(other.getBalanceStatus()))) &&
            ((this.balanceStatusName==null && other.getBalanceStatusName()==null) || 
             (this.balanceStatusName!=null &&
              this.balanceStatusName.equals(other.getBalanceStatusName()))) &&
            ((this.balanceExpirationDate==null && other.getBalanceExpirationDate()==null) || 
             (this.balanceExpirationDate!=null &&
              this.balanceExpirationDate.equals(other.getBalanceExpirationDate()))) &&
            ((this.accountSubscriptions==null && other.getAccountSubscriptions()==null) || 
             (this.accountSubscriptions!=null &&
              java.util.Arrays.equals(this.accountSubscriptions, other.getAccountSubscriptions())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getAccountType() != null) {
            _hashCode += getAccountType().hashCode();
        }
        if (getAccountTypeName() != null) {
            _hashCode += getAccountTypeName().hashCode();
        }
        if (getBalance() != null) {
            _hashCode += getBalance().hashCode();
        }
        if (getBalanceStatus() != null) {
            _hashCode += getBalanceStatus().hashCode();
        }
        if (getBalanceStatusName() != null) {
            _hashCode += getBalanceStatusName().hashCode();
        }
        if (getBalanceExpirationDate() != null) {
            _hashCode += getBalanceExpirationDate().hashCode();
        }
        if (getAccountSubscriptions() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAccountSubscriptions());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAccountSubscriptions(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerAccounts_OUT_RowSetCustomerAccount.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">customerAccounts_OUT_RowSet>customerAccount"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accountType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accountTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balance");
        elemField.setXmlName(new javax.xml.namespace.QName("", "balance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balanceStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "balanceStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balanceStatusName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "balanceStatusName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balanceExpirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "balanceExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountSubscriptions");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accountSubscriptions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">accountSubscriptions_OUT_RowSet>subscription"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "subscription"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
