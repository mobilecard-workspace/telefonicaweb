/**
 * OrdersCatalogTypeOrderHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.AdminService.holders;

public final class OrdersCatalogTypeOrderHolder implements javax.xml.rpc.holders.Holder {
    public com.addcel.ws.clientes.simfonics.AdminService.OrdersCatalogTypeOrder value;

    public OrdersCatalogTypeOrderHolder() {
    }

    public OrdersCatalogTypeOrderHolder(com.addcel.ws.clientes.simfonics.AdminService.OrdersCatalogTypeOrder value) {
        this.value = value;
    }

}
