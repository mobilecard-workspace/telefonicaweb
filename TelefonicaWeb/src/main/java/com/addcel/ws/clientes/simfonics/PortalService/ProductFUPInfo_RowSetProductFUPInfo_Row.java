/**
 * ProductFUPInfo_RowSetProductFUPInfo_Row.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class ProductFUPInfo_RowSetProductFUPInfo_Row  implements java.io.Serializable {
    private java.math.BigInteger id;

    private java.math.BigInteger orderId;

    private java.util.Calendar startTime;

    private java.util.Calendar endTime;

    private java.math.BigInteger productId;

    private java.math.BigInteger customerId;

    private java.math.BigInteger accountId;

    private java.math.BigInteger subscriptionId;

    private java.math.BigInteger resourceId;

    private java.math.BigInteger status;

    private java.lang.String statusName;

    private java.util.Calendar expirationDate;

    private java.lang.String productCode;

    private java.lang.String productName;

    private java.lang.String productDescription;

    private com.addcel.ws.clientes.simfonics.PortalService.ProductItemFUPInfo_RowSetProductItemFUPInfo_Row[] productItemsFUP;

    public ProductFUPInfo_RowSetProductFUPInfo_Row() {
    }

    public ProductFUPInfo_RowSetProductFUPInfo_Row(
           java.math.BigInteger id,
           java.math.BigInteger orderId,
           java.util.Calendar startTime,
           java.util.Calendar endTime,
           java.math.BigInteger productId,
           java.math.BigInteger customerId,
           java.math.BigInteger accountId,
           java.math.BigInteger subscriptionId,
           java.math.BigInteger resourceId,
           java.math.BigInteger status,
           java.lang.String statusName,
           java.util.Calendar expirationDate,
           java.lang.String productCode,
           java.lang.String productName,
           java.lang.String productDescription,
           com.addcel.ws.clientes.simfonics.PortalService.ProductItemFUPInfo_RowSetProductItemFUPInfo_Row[] productItemsFUP) {
           this.id = id;
           this.orderId = orderId;
           this.startTime = startTime;
           this.endTime = endTime;
           this.productId = productId;
           this.customerId = customerId;
           this.accountId = accountId;
           this.subscriptionId = subscriptionId;
           this.resourceId = resourceId;
           this.status = status;
           this.statusName = statusName;
           this.expirationDate = expirationDate;
           this.productCode = productCode;
           this.productName = productName;
           this.productDescription = productDescription;
           this.productItemsFUP = productItemsFUP;
    }


    /**
     * Gets the id value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @return id
     */
    public java.math.BigInteger getId() {
        return id;
    }


    /**
     * Sets the id value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @param id
     */
    public void setId(java.math.BigInteger id) {
        this.id = id;
    }


    /**
     * Gets the orderId value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @return orderId
     */
    public java.math.BigInteger getOrderId() {
        return orderId;
    }


    /**
     * Sets the orderId value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @param orderId
     */
    public void setOrderId(java.math.BigInteger orderId) {
        this.orderId = orderId;
    }


    /**
     * Gets the startTime value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @return startTime
     */
    public java.util.Calendar getStartTime() {
        return startTime;
    }


    /**
     * Sets the startTime value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @param startTime
     */
    public void setStartTime(java.util.Calendar startTime) {
        this.startTime = startTime;
    }


    /**
     * Gets the endTime value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @return endTime
     */
    public java.util.Calendar getEndTime() {
        return endTime;
    }


    /**
     * Sets the endTime value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @param endTime
     */
    public void setEndTime(java.util.Calendar endTime) {
        this.endTime = endTime;
    }


    /**
     * Gets the productId value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @return productId
     */
    public java.math.BigInteger getProductId() {
        return productId;
    }


    /**
     * Sets the productId value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @param productId
     */
    public void setProductId(java.math.BigInteger productId) {
        this.productId = productId;
    }


    /**
     * Gets the customerId value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @return customerId
     */
    public java.math.BigInteger getCustomerId() {
        return customerId;
    }


    /**
     * Sets the customerId value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @param customerId
     */
    public void setCustomerId(java.math.BigInteger customerId) {
        this.customerId = customerId;
    }


    /**
     * Gets the accountId value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @return accountId
     */
    public java.math.BigInteger getAccountId() {
        return accountId;
    }


    /**
     * Sets the accountId value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @param accountId
     */
    public void setAccountId(java.math.BigInteger accountId) {
        this.accountId = accountId;
    }


    /**
     * Gets the subscriptionId value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @return subscriptionId
     */
    public java.math.BigInteger getSubscriptionId() {
        return subscriptionId;
    }


    /**
     * Sets the subscriptionId value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @param subscriptionId
     */
    public void setSubscriptionId(java.math.BigInteger subscriptionId) {
        this.subscriptionId = subscriptionId;
    }


    /**
     * Gets the resourceId value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @return resourceId
     */
    public java.math.BigInteger getResourceId() {
        return resourceId;
    }


    /**
     * Sets the resourceId value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @param resourceId
     */
    public void setResourceId(java.math.BigInteger resourceId) {
        this.resourceId = resourceId;
    }


    /**
     * Gets the status value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @return status
     */
    public java.math.BigInteger getStatus() {
        return status;
    }


    /**
     * Sets the status value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @param status
     */
    public void setStatus(java.math.BigInteger status) {
        this.status = status;
    }


    /**
     * Gets the statusName value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @return statusName
     */
    public java.lang.String getStatusName() {
        return statusName;
    }


    /**
     * Sets the statusName value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @param statusName
     */
    public void setStatusName(java.lang.String statusName) {
        this.statusName = statusName;
    }


    /**
     * Gets the expirationDate value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @return expirationDate
     */
    public java.util.Calendar getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.util.Calendar expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the productCode value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @return productCode
     */
    public java.lang.String getProductCode() {
        return productCode;
    }


    /**
     * Sets the productCode value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @param productCode
     */
    public void setProductCode(java.lang.String productCode) {
        this.productCode = productCode;
    }


    /**
     * Gets the productName value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @return productName
     */
    public java.lang.String getProductName() {
        return productName;
    }


    /**
     * Sets the productName value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @param productName
     */
    public void setProductName(java.lang.String productName) {
        this.productName = productName;
    }


    /**
     * Gets the productDescription value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @return productDescription
     */
    public java.lang.String getProductDescription() {
        return productDescription;
    }


    /**
     * Sets the productDescription value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @param productDescription
     */
    public void setProductDescription(java.lang.String productDescription) {
        this.productDescription = productDescription;
    }


    /**
     * Gets the productItemsFUP value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @return productItemsFUP
     */
    public com.addcel.ws.clientes.simfonics.PortalService.ProductItemFUPInfo_RowSetProductItemFUPInfo_Row[] getProductItemsFUP() {
        return productItemsFUP;
    }


    /**
     * Sets the productItemsFUP value for this ProductFUPInfo_RowSetProductFUPInfo_Row.
     * 
     * @param productItemsFUP
     */
    public void setProductItemsFUP(com.addcel.ws.clientes.simfonics.PortalService.ProductItemFUPInfo_RowSetProductItemFUPInfo_Row[] productItemsFUP) {
        this.productItemsFUP = productItemsFUP;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ProductFUPInfo_RowSetProductFUPInfo_Row)) return false;
        ProductFUPInfo_RowSetProductFUPInfo_Row other = (ProductFUPInfo_RowSetProductFUPInfo_Row) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.orderId==null && other.getOrderId()==null) || 
             (this.orderId!=null &&
              this.orderId.equals(other.getOrderId()))) &&
            ((this.startTime==null && other.getStartTime()==null) || 
             (this.startTime!=null &&
              this.startTime.equals(other.getStartTime()))) &&
            ((this.endTime==null && other.getEndTime()==null) || 
             (this.endTime!=null &&
              this.endTime.equals(other.getEndTime()))) &&
            ((this.productId==null && other.getProductId()==null) || 
             (this.productId!=null &&
              this.productId.equals(other.getProductId()))) &&
            ((this.customerId==null && other.getCustomerId()==null) || 
             (this.customerId!=null &&
              this.customerId.equals(other.getCustomerId()))) &&
            ((this.accountId==null && other.getAccountId()==null) || 
             (this.accountId!=null &&
              this.accountId.equals(other.getAccountId()))) &&
            ((this.subscriptionId==null && other.getSubscriptionId()==null) || 
             (this.subscriptionId!=null &&
              this.subscriptionId.equals(other.getSubscriptionId()))) &&
            ((this.resourceId==null && other.getResourceId()==null) || 
             (this.resourceId!=null &&
              this.resourceId.equals(other.getResourceId()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.statusName==null && other.getStatusName()==null) || 
             (this.statusName!=null &&
              this.statusName.equals(other.getStatusName()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate()))) &&
            ((this.productCode==null && other.getProductCode()==null) || 
             (this.productCode!=null &&
              this.productCode.equals(other.getProductCode()))) &&
            ((this.productName==null && other.getProductName()==null) || 
             (this.productName!=null &&
              this.productName.equals(other.getProductName()))) &&
            ((this.productDescription==null && other.getProductDescription()==null) || 
             (this.productDescription!=null &&
              this.productDescription.equals(other.getProductDescription()))) &&
            ((this.productItemsFUP==null && other.getProductItemsFUP()==null) || 
             (this.productItemsFUP!=null &&
              java.util.Arrays.equals(this.productItemsFUP, other.getProductItemsFUP())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getOrderId() != null) {
            _hashCode += getOrderId().hashCode();
        }
        if (getStartTime() != null) {
            _hashCode += getStartTime().hashCode();
        }
        if (getEndTime() != null) {
            _hashCode += getEndTime().hashCode();
        }
        if (getProductId() != null) {
            _hashCode += getProductId().hashCode();
        }
        if (getCustomerId() != null) {
            _hashCode += getCustomerId().hashCode();
        }
        if (getAccountId() != null) {
            _hashCode += getAccountId().hashCode();
        }
        if (getSubscriptionId() != null) {
            _hashCode += getSubscriptionId().hashCode();
        }
        if (getResourceId() != null) {
            _hashCode += getResourceId().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getStatusName() != null) {
            _hashCode += getStatusName().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        if (getProductCode() != null) {
            _hashCode += getProductCode().hashCode();
        }
        if (getProductName() != null) {
            _hashCode += getProductName().hashCode();
        }
        if (getProductDescription() != null) {
            _hashCode += getProductDescription().hashCode();
        }
        if (getProductItemsFUP() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProductItemsFUP());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProductItemsFUP(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ProductFUPInfo_RowSetProductFUPInfo_Row.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">ProductFUPInfo_RowSet>ProductFUPInfo_Row"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "orderId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startTime");
        elemField.setXmlName(new javax.xml.namespace.QName("", "startTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endTime");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "accountId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriptionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "subscriptionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resourceId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resourceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statusName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "expirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productItemsFUP");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productItemsFUP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">ProductItemFUPInfo_RowSet>ProductItemFUPInfo_Row"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("", "ProductItemFUPInfo_Row"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
