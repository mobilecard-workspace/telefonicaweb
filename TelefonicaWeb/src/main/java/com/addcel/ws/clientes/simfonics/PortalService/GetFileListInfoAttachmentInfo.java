/**
 * GetFileListInfoAttachmentInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService;

public class GetFileListInfoAttachmentInfo  implements java.io.Serializable {
    private java.math.BigInteger id;

    private java.lang.String fileName;

    private java.math.BigInteger fileEntityTypeId;

    private java.math.BigInteger referenceFileId;

    public GetFileListInfoAttachmentInfo() {
    }

    public GetFileListInfoAttachmentInfo(
           java.math.BigInteger id,
           java.lang.String fileName,
           java.math.BigInteger fileEntityTypeId,
           java.math.BigInteger referenceFileId) {
           this.id = id;
           this.fileName = fileName;
           this.fileEntityTypeId = fileEntityTypeId;
           this.referenceFileId = referenceFileId;
    }


    /**
     * Gets the id value for this GetFileListInfoAttachmentInfo.
     * 
     * @return id
     */
    public java.math.BigInteger getId() {
        return id;
    }


    /**
     * Sets the id value for this GetFileListInfoAttachmentInfo.
     * 
     * @param id
     */
    public void setId(java.math.BigInteger id) {
        this.id = id;
    }


    /**
     * Gets the fileName value for this GetFileListInfoAttachmentInfo.
     * 
     * @return fileName
     */
    public java.lang.String getFileName() {
        return fileName;
    }


    /**
     * Sets the fileName value for this GetFileListInfoAttachmentInfo.
     * 
     * @param fileName
     */
    public void setFileName(java.lang.String fileName) {
        this.fileName = fileName;
    }


    /**
     * Gets the fileEntityTypeId value for this GetFileListInfoAttachmentInfo.
     * 
     * @return fileEntityTypeId
     */
    public java.math.BigInteger getFileEntityTypeId() {
        return fileEntityTypeId;
    }


    /**
     * Sets the fileEntityTypeId value for this GetFileListInfoAttachmentInfo.
     * 
     * @param fileEntityTypeId
     */
    public void setFileEntityTypeId(java.math.BigInteger fileEntityTypeId) {
        this.fileEntityTypeId = fileEntityTypeId;
    }


    /**
     * Gets the referenceFileId value for this GetFileListInfoAttachmentInfo.
     * 
     * @return referenceFileId
     */
    public java.math.BigInteger getReferenceFileId() {
        return referenceFileId;
    }


    /**
     * Sets the referenceFileId value for this GetFileListInfoAttachmentInfo.
     * 
     * @param referenceFileId
     */
    public void setReferenceFileId(java.math.BigInteger referenceFileId) {
        this.referenceFileId = referenceFileId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetFileListInfoAttachmentInfo)) return false;
        GetFileListInfoAttachmentInfo other = (GetFileListInfoAttachmentInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.fileName==null && other.getFileName()==null) || 
             (this.fileName!=null &&
              this.fileName.equals(other.getFileName()))) &&
            ((this.fileEntityTypeId==null && other.getFileEntityTypeId()==null) || 
             (this.fileEntityTypeId!=null &&
              this.fileEntityTypeId.equals(other.getFileEntityTypeId()))) &&
            ((this.referenceFileId==null && other.getReferenceFileId()==null) || 
             (this.referenceFileId!=null &&
              this.referenceFileId.equals(other.getReferenceFileId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getFileName() != null) {
            _hashCode += getFileName().hashCode();
        }
        if (getFileEntityTypeId() != null) {
            _hashCode += getFileEntityTypeId().hashCode();
        }
        if (getReferenceFileId() != null) {
            _hashCode += getReferenceFileId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetFileListInfoAttachmentInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.simfonics.com/SelfCare/PortalService/", ">getFileListInfo>attachmentInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fileName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fileName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fileEntityTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fileEntityTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceFileId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ReferenceFileId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
