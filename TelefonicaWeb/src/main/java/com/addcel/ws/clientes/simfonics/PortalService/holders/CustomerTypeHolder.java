/**
 * CustomerTypeHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService.holders;

public final class CustomerTypeHolder implements javax.xml.rpc.holders.Holder {
    public com.addcel.ws.clientes.simfonics.PortalService.CustomerType value;

    public CustomerTypeHolder() {
    }

    public CustomerTypeHolder(com.addcel.ws.clientes.simfonics.PortalService.CustomerType value) {
        this.value = value;
    }

}
