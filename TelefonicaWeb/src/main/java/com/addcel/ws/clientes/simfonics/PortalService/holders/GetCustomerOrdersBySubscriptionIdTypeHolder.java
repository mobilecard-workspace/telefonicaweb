/**
 * GetCustomerOrdersBySubscriptionIdTypeHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.simfonics.PortalService.holders;

public final class GetCustomerOrdersBySubscriptionIdTypeHolder implements javax.xml.rpc.holders.Holder {
    public com.addcel.ws.clientes.simfonics.PortalService.GetCustomerOrdersBySubscriptionIdTypeGetCustomerOrdersBySubscriptionId[] value;

    public GetCustomerOrdersBySubscriptionIdTypeHolder() {
    }

    public GetCustomerOrdersBySubscriptionIdTypeHolder(com.addcel.ws.clientes.simfonics.PortalService.GetCustomerOrdersBySubscriptionIdTypeGetCustomerOrdersBySubscriptionId[] value) {
        this.value = value;
    }

}
