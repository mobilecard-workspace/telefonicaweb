/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: ES (Spanish; Español)
 */
(function ($) {
	
	$( "input[type=submit], button" )
		.button()
		.click(function( event ) {
		event.preventDefault();
		return false;
	});
	
	jQuery.validator.addMethod(
            "vigenciaDate",
            function(value, element) {
                return value.match(/^\d{1,2}\/\d{4}$/);
            },
            "Por favor, escribir una vigencia válida, formato MM/AAAA"
    );
	
	$.extend($.validator.messages, {
		required: "Este campo es obligatorio.",
		remote: "Por favor, rellena este campo.",
		email: "Por favor, escribir una dirección de correo válida",
		url: "Por favor, escribir una URL válida.",
		date: "Por favor, escribir una fecha válida.",
		dateISO: "Por favor, escribir una fecha (ISO) válida.",
		number: "Por favor, escribir un número entero válido.",
		digits: "Por favor, escribir sólo numeros.",
		creditcard: "Por favor, escribir un número de tarjeta válido.",
		equalTo: "Por favor, escribir el mismo valor que el campo {0}.",
		accept: "Por favor, escribir un valor con una extensión aceptada.",
		maxlength: $.validator.format("Por favor, no escribir más de {0} caracteres."),
		minlength: $.validator.format("Por favor, no escribir menos de {0} caracteres."),
		rangelength: $.validator.format("Por favor, escribir un valor entre {0} y {1} caracteres."),
		range: $.validator.format("Por favor, escribir un valor entre {0} y {1}."),
		max: $.validator.format("Por favor, escribir un valor menor o igual a {0}."),
		min: $.validator.format("Por favor, escribir un valor mayor o igual a {0}.")
	});
	
	$.datepicker.regional['es'] = {
        renderer: $.ui.datepicker.defaultRenderer,
        monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
        'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
        monthNamesShort: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
        'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
        dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
        dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        prevText: '&#x3c;Ant', prevStatus: '',
        prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
        nextText: 'Sig&#x3e;', nextStatus: '',
        nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
        currentText: 'Hoy', currentStatus: '',
        todayText: 'Hoy', todayStatus: '',
        clearText: '-', clearStatus: '',
        closeText: 'Cerrar', closeStatus: '',
        yearStatus: '', monthStatus: '',
        weekText: 'Sm', weekStatus: '',
        dayStatus: 'DD d MM',
        defaultStatus: '',
        isRTL: false
    };
    $.extend($.datepicker.defaults, $.datepicker.regional['es']);
}(jQuery));