
/* jQuery.validator.setDefaults({
	debug: true,
	success: "valid"
}); */
	
	$(function() {

		$('#modelo').val(jQuery.browser.name);
		$('#software').val(jQuery.browser.version);
			

		$( "#cargando" ).dialog({
			dialogClass: "no-close",
			modal: true,
			resizable: false,
			draggable: false,
			//closeOnEscape: false,
			autoOpen: false
		});
		
		$( "#terminosDiv" ).dialog({
			dialogClass: "no-close",
			modal: true,
			resizable: false,
			draggable: false,
			//closeOnEscape: false,
			autoOpen: false,
			height: 500,
			width: 800,
			buttons: [ 
			    {
					text: "Cancelar",
					click: function() {
						$( this ).dialog( "close" );
					}
				},
				{
					text: "Aceptar",
					click: function() {
						//$("#terminos").removeAttr("checked");
						$("#terminos").attr("checked","checked");
						//try{
						//	$("#terminos").attr("class","valid");
						//}catch(error){}
						
						$( this ).dialog( "close" );
					}
				}
			]
		});
		
		
		$("#terminosButton").click(function(){
			$( "#terminosDiv" ).dialog( "open" );
		}); 

		$(document).ajaxStart(function() {
			$('input[type="submit"]').attr('disabled','disabled');

			$( "#cargando" ).dialog( "open" );
		});
		
		$(document).ajaxStop(function() {
			$( "#cargando" ).dialog( "close" );
			$('input[type="submit"]').removeAttr('disabled');
		});
		
		$("#loginButton").click(function(){
			$("#loginForm").submit(); //SUBMIT FORM
		});  


		$("#saveButton").click(function(){

			$('#registroForm').submit(function(event) {
				$('input[type="submit"]').attr('disabled','disabled');

				if(!$( this ).valid()){
					$('input[type="submit"]').removeAttr('disabled');
				}else{
					var mensaje;
					var dataJson = {
				    		"login" : $( "#login" ).val(  ),
				    		"telefono" : $( "#telefono" ).val(  ),
				    		"mail" : $( "#email" ).val(  ),
				    		"nombre" : $( "#nombres" ).val(  ),
				    		"apellido" : $( "#apellidoPrim" ).val(  ),
				    		"materno" : $( "#apellidoSeg" ).val(  ),
				    		"dom_amex" : $( "#domicilioAMEXS" ).val(  ),
				    		"cp" : $( "#cpAMEX" ).val(  ),
				    		"tipotarjeta" : $( "#tipoTarjeta" ).val(  ),
				    		"tarjeta" : $( "#tarjeta" ).val(  ),
				    		"vigencia" : $( "#vigencia" ).val(  ),
				    		"terminos" : $( "#terminos" ).val(  ),
				    		"modelo" : $( "#modelo" ).val(  ),
				    		"software" : $( "#software" ).val(  )
				    	};

					 $.ajax({
						 url: $(this).attr('action'),
						 type: 'POST',
						 data: JSON.stringify(dataJson),
						 dataType: 'json',
						 contentType: "application/json; charset=utf-8",
						 mimeType: 'application/json; charset=utf-8',
						 async: false,
			             cache: false,    //This will force requested pages not to be cached by the browser          
			             processData:false, //To avoid making query String instead of JSON
						 statusCode: {
							 404: function() {
							 alert( "Pagina no encontrada." );
							 }
						 },
						 success: function (data) {

						 	if(data.idError == 0){
						 		mensaje = "Mensaje del sistema";
							}else{
								mensaje = "Error";
						    }
						 	
						 	var caja2 = $('<div title="' + mensaje + '">' + data.mensajeError +'</div>');
							caja2.dialog({
								dialogClass: "alert",
								modal: true,
								resizable: false,
								draggable: false,
								buttons: [ {
									text: "Aceptar",
									click: function() {
										if(data.idError == 0){
											//window.location = '${pageContext.request.contextPath}/login?json=${json}';
											$("#loginForm").submit(); 
										} else{
											 $('input[type="submit"]').removeAttr('disabled');
										}
										$( this ).dialog( "close" );
									}
								}]
								});
						 },
						 error: function (jqXHR, textStatus, errorThrown) {
							//$( "#cargando" ).dialog( "close" );
						 	alert('An error has occured!! \njqXHR: ' + jqXHR +
								 	'\ntextStatus: ' +  textStatus +
								 	'\nerrorThrown: ' + errorThrown);
						 	$('input[type="submit"]').removeAttr('disabled');
						 }
					});
				}
	
				event.preventDefault(); //STOP default action
				event.stopImmediatePropagation();
			    return false;
			});
	
			$("#registroForm").submit(); //SUBMIT FORM
		});  

		/* $("#cleanForm").click(function(event){

			$("#registroForm").clearForm();

			$('input[type="text"]').val('');
			$('input[type="select"]').val('0');
	
				event.preventDefault(); //STOP default action
		});  */ 
		$.datepicker.setDefaults($.datepicker.regional["es"]);

		$( "#vigencia" ).datepicker({
			changeMonth: true, 
			changeYear: true,
			minDate: "+0d",
			//yearRange: "c-0:c+15",
			dateFormat: 'mm/yy',
				 
			onClose: function() {
		        var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
		        var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
		        $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
		    },
		       
		    beforeShow: function() {
		       var hoja = document.createElement('style');
		       hoja.innerHTML = ".ui-datepicker-calendar { display: none;}";
		       document.body.appendChild(hoja);
		       if ((selDate = $(this).val()).length > 0) {
		          iYear = selDate.substring(selDate.length - 4, selDate.length);
		          iMonth = selDate.substring(0, selDate.length - 5);
		          //alert('Year: ' + iYear + "\nMonth: " + $(this).datepicker('option', 'numberOfMonths'));
		          $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth-1, 1)); 
		          $(this).datepicker('setDate', new Date(iYear, iMonth-1, 1));
		       }
		    }
		});

		

		
		$("#registroForm").validate({
            rules :{
            	login : {
                    required  : true,
                    minlength : 3, //para validar campo con minimo 3 caracteres
                    maxlength : 15  //para validar campo con maximo 9 caracteres
                },
                telefono : {
                    required : true, 
                    digits   : true,
                    maxlength : 15
                },
                telefonoCon : {
                    required : true,
                    digits   : true,
                    maxlength : 15,
                    equalTo: "#telefono"   
                },
                email : {
                    required :true,
                    email    : true,
                    maxlength : 45
                },
                emailCon : {
                    required :true,
                    email    : true,
                    maxlength : 45,
                    equalTo: "#email"
                },
                nombres : {
                    required :true,
                    maxlength : 45
                },
                apellidoPrim : {
                    required :true,
                    maxlength : 45
                },
                apellidoSeg : {
                    //required :true,
                    maxlength : 45
                },
                liDomicilio : {
                	required  :true,
                	maxlength : 200
                },
                liCP : {
                	required  : true,
                	digits    : true,
                    maxlength : 10
                },
                tipoTarjeta : {
                    required  :true,
                    digits    : true,
                    maxlength : 1,
                    min       : 1
                },
                tarjeta : {
                    required :true,
                    digits   : true,
                    minlength : 15,
                    maxlength : 16
                },
                vigencia : {
                    required  : true,
                    maxlength : 7
                },
                terminos : {
                    required: true,
                    min:	  1
                }
  
            },
            messages : {
            	telefonoCon : {
            		equalTo : "Por favor, escribir el mismo valor que el campo Celuar.",
                },
                emailCon : {
                	equalTo : "Por favor, escribir el mismo valor que el campo Email.",
                },
                tipoTarjeta : {
            		min : "Por favor, seleccione un Tipo de Tarjeta.",
                }
            }
        });
		
		function isAMEX(){
			if($("#tipoTarjeta").val() == 3){
				$("#liDomicilio").show();
				$("#liCP").show();
				$("#liDomicilio").removeAttr("disabled");
				$("#liCP").removeAttr("disabled");
				
			}else{
				$("#liDomicilio").hide();
				$("#liCP").hide();
				$("#liDomicilio").attr("disabled","disabled");
				$("#liCP").attr("disabled","disabled");
			}
			
		}
		
		$("#tipoTarjeta").change(function(){
	        isAMEX();
		});
		
		$( "input[type=submit], button" )
			.button()
			.click(function( event ) {
			event.preventDefault();
		});

	});