
/* jQuery.validator.setDefaults({
	debug: true,
	success: "valid"
}); */
	
	$(function() {

		$( "#cargando" ).dialog({
			dialogClass: "no-close",
			modal: true,
			resizable: false,
			draggable: false,
			//closeOnEscape: false,
			autoOpen: false
		});

		$(document).ajaxStart(function() {
			$('input[type="submit"]').attr('disabled','disabled');

			$( "#cargando" ).dialog( "open" );
		});
		
		$(document).ajaxStop(function() {
			$( "#cargando" ).dialog( "close" );
			$('input[type="submit"]').removeAttr('disabled');
		});

		$("#saveForm").click(function(){
			$("#registroForm").submit(); //SUBMIT FORM
		});  


		$("#loginSubmit").click(function(){

			$('#loginForm').submit(function(event) {
				$('input[type="submit"]').attr('disabled','disabled');

				if(!$( this ).valid()){
					$('input[type="submit"]').removeAttr('disabled');
					event.preventDefault(); //STOP default action
					event.stopImmediatePropagation();
				    return false;
				}else{
					$( "#cargando" ).dialog( "open" );
				}

			});

			$("#loginForm").submit(); //SUBMIT FORM
		});  

		$( "input[type=submit], button" )
			.button()
			.click(function( event ) {
			event.preventDefault();
		});

		
		$("#loginForm").validate({
	        rules :{
	        	login : {
	                required  : true,
	                maxlength : 15  //para validar campo con maximo 9 caracteres
	            },
	            password : {
	                required : true, 
	                maxlength : 16
	            }

	        }
	    });
	});