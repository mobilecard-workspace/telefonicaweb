
/* jQuery.validator.setDefaults({
	debug: true,
	success: "valid"
}); */
	
	$(function() {
		
		$('#modelo').val(jQuery.browser.name);
		$('#software').val(jQuery.browser.version);
		
		isAMEX();
		blockDiv();
		
		var loginForm = $("#loginForm").validate({
	        rules :{
	        	login : {
	                required  : true,
	                maxlength : 15  //para validar campo con maximo 9 caracteres
	            },
	            password : {
	                required : true, 
	                maxlength : 16
	            }

	        }
	    });
		
		
		var pagoDirectoForm = $("#pagoDirectoForm").validate({
	        rules :{
	        	nombre : {
	                required  : true,
	                maxlength : 50
	            },
	            tarjeta : {
	                required  : true,
	                digits    : true,
	                minlength : 15,
	                maxlength : 16
	            },
	            vigencia : {
	                required  : true,
	                maxlength : 7
	            },
	            cvv2 : {
	                required  : true,
	                digits    : true,
	                maxlength : 4
	            },
	            tipoTarjeta : {
	                required  : true, 
	                digits    : true,
	                maxlength : 1,
	                min       : 1
	            },
	            domicilio : {
	                required  : true,
	                maxlength : 15
	            },
	            cp : {
	                required : true, 
	                digits    : true,
	                maxlength : 16
	            },
	            email : {
	                required  : true, 
	                email     : true,
	                maxlength : 50
	            }
	        },
	        messages : {
	        	tipoTarjeta : {
	        		min : "Por favor, seleccione un Tipo de Tarjeta."
	            }
	        }
	    });

		
		var resetPassForm = $("#cambioPasswordForm").validate({
	        rules :{
	        	resetPasslogin : {
	                required   : true, 
	                maxlength  : 15
	            },
	            resetPassemail : {
	                required   : true, 
	                email      : true,
	                maxlength  : 45
	            }
	        }
	    });
		
		$.datepicker.setDefaults($.datepicker.regional["es"]);

		$( "#vigencia" ).datepicker({
			changeMonth: true, 
			changeYear: true,
			minDate: "+0d",
			//yearRange: "c-0:c+15",
			dateFormat: 'mm/y',
			'with': '100%',
				 
			onClose: function() {
		        var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
		        var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
		        $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
		    },
		       
		    beforeShow: function() {
		       var hoja = document.createElement('style');
		       hoja.innerHTML = ".ui-datepicker-calendar { display: none;}";
		       document.body.appendChild(hoja);
		       if ((selDate = $(this).val()).length > 0) {
		          iYear = selDate.substring(selDate.length - 4, selDate.length);
		          iMonth = selDate.substring(0, selDate.length - 5);
		          //alert('Year: ' + iYear + "\nMonth: " + $(this).datepicker('option', 'numberOfMonths'));
		          $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth-1, 1)); 
		          $(this).datepicker('setDate', new Date(iYear, iMonth-1, 1));
		       }
		    }
		});

//		$( "input[type=submit], button" )
//			.button()
//			.click(function( event ) {
//			event.preventDefault(); //STOP default action
//			event.stopImmediatePropagation();
//		});
		
		
//		$.blockUI({ css: { backgroundColor: '#58595b', color: '#58595b' } }); 
		
		
		
		function blockDiv(){
			if(2 == arrayBlock || 0 == arrayBlock){
				arrayBlock = 1;
				try{
					loginForm.resetForm();		//remove error class on name elements and clear history
				}catch(e){};
				
				$('input[type="text"]').removeAttr('class');
				
				$("#divPagoDirecto").unblock();
				//$( "#unblockButton" ).val(arrayBlock[0]);
				$("#divPagoUsuario").block({ 
	                message: $('#blockPagoDiv'), 
	                css: {
	                	 width: '90%',
	                	 border: 'none', 
	                     backgroundColor: 'inherit', 
	                     color: '#fff',
	                     cursor: 'flecha'
	                } 
	            }); 
			}else{
				arrayBlock = 2;
				try{
					pagoDirectoForm.resetForm();
				}catch(e){};
				
				$("#divPagoUsuario").unblock();
				//$( "#unblockButton" ).val(arrayBlock[0]);
				$("#divPagoDirecto").block({ 
	                message: $('#blockPagoDirectoDiv'), 
	                css: {
	                	 width: '90%',
	                	 border: 'none', 
	                     backgroundColor: 'inherit', 
	                     color: '#fff',
	                     cursor: 'flecha'
	                } 
	            }); 
			}
		}
		
		function isAMEX(){
			if($("#tipoTarjeta").val() == 3){
				$("#liDomicilio").show();
				$("#liCP").show();
			}else{
				$("#liDomicilio").hide();
				$("#liCP").hide();
			}
			
		}
		
		$("#tipoTarjeta").change(function(){
	        isAMEX();
		});
		
		
		$("#unblockButton").click(function(){
			blockDiv();
		});
		 
		$("#blockPagoDirectoDiv").click(function(){
			blockDiv();
		});
		
		

		$( "#cargando" ).dialog({
			dialogClass: "no-close",
			modal: true,
			resizable: false,
			draggable: false,
			//closeOnEscape: false,
			autoOpen: false
		});

		$( "#containerResetPassword" ).dialog({
			modal: true,
			resizable: false,
			draggable: false,
			//closeOnEscape: false,
			autoOpen: false,
			height: 400,
			width: 600,
			buttons: [ {
				text: "   Aceptar   ",
				click: function() {
					resetPassword();
				}
			},{
				text: "   Cancelar   ",
				click: function() {
					$( this ).dialog( "close" );
				}
			}]
		});

		$(document).ajaxStart(function() {
			$('input[type="submit"]').attr('disabled','disabled');

			$( "#cargando" ).dialog( "open" );
		});
		
		$(document).ajaxStop(function() {
			$( "#cargando" ).dialog( "close" );
			$('input[type="submit"]').removeAttr('disabled');
		});
		
		$("#saveForm").click(function(){
			$("#registroForm").submit(); //SUBMIT FORM
		});  


		$("#loginButton").click(function(){

			$('#loginForm').submit(function(event) {
				$('input[type="submit"]').attr('disabled','disabled');

				if(!$( this ).valid()){
					$('input[type="submit"]').removeAttr('disabled');
					event.preventDefault(); //STOP default action
					event.stopImmediatePropagation();
				    return false;
				}else{
					$( "#cargando" ).dialog( "open" );
				}

			});

			$("#loginForm").submit(); //SUBMIT FORM
		});
		
		$("#pagoDirectoButton").click(function(){

			$('#pagoDirectoForm').submit(function(event) {
				$('input[type="submit"]').attr('disabled','disabled');

				if(!$( this ).valid()){
					$('input[type="submit"]').removeAttr('disabled');
					event.preventDefault(); //STOP default action
					event.stopImmediatePropagation();
				    return false;
				}else{
					$( "#cargando" ).dialog( "open" );
				}
			});

			$("#pagoDirectoForm").submit(); //SUBMIT FORM
		});  


		$( "input[type=submit], button" )
			.button()
			.click(function( event ) {
			event.preventDefault();
		});

		$("#resetButton").click(function(){
			resetPassForm.resetForm();//remove error class on name elements and clear history
			$( "#resetPasslogin" ).val("");
			$('input[type="text"]').removeAttr('class');
	 		$( "#containerResetPassword" ).dialog( "open" );
		});

		$("#resetPasswordCanButton").click(function(){
			$( "#containerResetPassword" ).dialog( "close" );
		});
		
		
		$('input:radio[type]').change(function() {
			resetPassForm.resetForm();
			if ($(this).val() === '1') {
				$("#resetPassemail").val("");
				$("#resetPassemail").attr("disabled","disabled");
				
				$("#resetPasslogin").removeAttr("disabled");
				$("#resetPasslogin").focus();
				
		    } else if ($(this).val() === '2') {
		    	$("#resetPasslogin").val("");
				$("#resetPasslogin").attr("disabled","disabled");
				
				$("#resetPassemail").removeAttr("disabled");
				$("#resetPassemail").focus();
		    } 
		});
		
		function resetPassword(){
			
			$('#cambioPasswordForm').submit(function(event) {
				
				if(!$( this ).valid()){
					$('input[type="submit"]').removeAttr('disabled');
				}else{
					$('#lblErrorCamPass').text("");
					
					var dataJson = {
			    			"login" : $('input:radio[type]').val() == 1? $( "#resetPasslogin" ).val(  ):"",
			    			"email" : $('input:radio[type]').val() == 2? $( "#resetPassemail" ).val(  ):""
			    	};

					 $.ajax({
						 url: $(this).attr('action'),
						 type: 'POST',
						 data: JSON.stringify(dataJson),
						 dataType: 'json',
						 contentType: "application/json; charset=utf-8",
						 mimeType: 'application/json; charset=utf-8',
						 async: false,
			             cache: false,    //This will force requested pages not to be cached by the browser          
			             processData:false, //To avoid making query String instead of JSON
						 statusCode: {
							 404: function() {
							 alert( "Pagina no encontrada." );
							 }
						 },
						 success: function (data) {

							$('input[type="submit"]').removeAttr('disabled');

						 	var caja2 = $('<div title="' + (data.idError == 0?"Mensaje del sistema":"Error") + '" style="z-index: 9999">' + data.mensajeError +'</div>');
							caja2.dialog({
								modal: true,
								resizable: false,
								draggable: false,
								buttons: [ {
									text: "Aceptar",
									click: function() {
										if(data.idError == 0){
											$( "#containerResetPassword" ).dialog( "close" );
										}
										$( this ).dialog( "close" );
									}
								}]
								});
						 },
						 error: function (jqXHR, textStatus, errorThrown) {
							//$( "#cargando" ).dialog( "close" );
						 	alert('An error has occured!! \njqXHR: ' + jqXHR +
								 	'\ntextStatus: ' +  textStatus +
								 	'\nerrorThrown: ' + errorThrown);
						 	$('input[type="submit"]').removeAttr('disabled');
						 }
					}); 
				}
				$('input[type="submit"]').removeAttr('disabled');

				event.preventDefault(); //STOP default action
				event.stopImmediatePropagation();
			    return false;
			});
			$("#cambioPasswordForm").submit(); //SUBMIT FORM
				
		} 

	});