
/* jQuery.validator.setDefaults({
	debug: true,
	success: "valid"
}); */
	

$(function() {

	$('#software').val(jQuery.browser.name);
	$('#modelo').val(jQuery.browser.version);

	$("#pagoForm").validate({
        rules :{
        	cvv2 : {
                required : true, 
                digits   : true,
                minlength : 3,
                maxlength : 4
            },
            password : {
                required : true, 
                maxlength : 12
            }
        }
    });

	var camDatosForm = $("#cambioDatosForm").validate({
        rules :{
        	login : {
                required  : true,
                minlength : 3, //para validar campo con minimo 3 caracteres
                maxlength : 15  //para validar campo con maximo 9 caracteres
            },
            telefono : {
                required : true, 
                digits   : true,
                maxlength : 15
            },
            telefonoCon : {
                required : true,
                digits   : true,
                maxlength : 15,
                equalTo: "#telefono"   
            },
            email : {
                required :true,
                email    : true,
                maxlength : 45
            },
            emailCon : {
                required :true,
                email    : true,
                maxlength : 45,
                equalTo: "#email"
            },
            nombres : {
                required :true,
                maxlength : 45
            },
            apellidoPrim : {
                required :true,
                maxlength : 45
            },
            apellidoSeg : {
                //required :true,
                maxlength : 45
            },
            operador : {
                required :true,
                digits   : true,
                maxlength : 1,
                min      : 1
            }
        },
        messages : {
        	telefonoCon : {
        		equalTo : "Por favor, escribir el mismo valor que el campo Telefono.",
            },
            emailCon : {
            	equalTo : "Por favor, escribir el mismo valor que el campo Email.",
            }
        }
    });

	var camPassForm = $("#cambioPasswordForm").validate({
        rules :{
        	passwordCam : {
                required : true, 
                maxlength : 16,
            },
            newPassword : {
                required : true, 
                maxlength : 16,
            },
            newPasswordCon : {
                required : true, 
                maxlength : 16,
                equalTo: "#newPassword"
            }

        },
        messages : {
        	newPasswordCon : {
        		equalTo : "Por favor, escribir el mismo valor que el campo Nuevo Password.",
            },
        }
    });

	var camTarjForm =$("#cambioTarjetaForm").validate({
        rules :{
        	newTarjeta : {
                required : true, 
                minlength : 15,
                maxlength : 16,
            },
            newTarjetaCon : {
                required : true,
                minlength : 15, 
                maxlength : 16,
                equalTo: "#newTarjeta"
            },
            vigencia : {
                required :true,
                maxlength : 7
            },
            passwordTarj : {
                required : true, 
                maxlength : 16,
            }

        },
        messages : {
        	newTarjetaCon : {
        		equalTo : "Por favor, escribir el mismo valor que el campo Nueva Tarjeta.",
            },
        }
    });

	$( "#containerCambioDatos" ).dialog({
		dialogClass: "no-close",
		modal: true,
		resizable: false,
		draggable: false,
		//closeOnEscape: false,
		autoOpen: false,
		height: 540,
		width: 600,
		buttons: [ {
			text: "Aceptar",
			click: function() {
				actalizarUsuarioAjax();
			}
		},{
			text: "Cancelar",
			click: function() {
				$( this ).dialog( "close" );
			}
		}]
	});
	
	$( "#containerCambioPassword" ).dialog({
		dialogClass: "no-close",
		modal: true,
		resizable: false,
		draggable: false,
		//closeOnEscape: false,
		autoOpen: false,
		height: 380,
		width: 600,
		buttons: [ {
			text: "Aceptar",
			click: function() {
				actalizarTarjetaAjax();
			}
		},{
			text: "Cancelar",
			click: function() {
				$( this ).dialog( "close" );
			}
		}]
	});

	$( "#containerCambioTarjeta" ).dialog({
		dialogClass: "no-close",
		modal: true,
		resizable: false,
		draggable: false,
		//closeOnEscape: false,
		autoOpen: false,
		height: 500,
		width: 600,
		buttons: [ {
			text: "Aceptar",
			click: function() {
				actalizarKitAjax();
			}
		},{
			text: "Cancelar",
			click: function() {
				$( this ).dialog( "close" );
			}
		}]
	});


	
	$("#aHrefCamDatos").click(function(){
		camDatosForm.resetForm();//remove error class on name elements and clear history
		$( "#lblErrorCamDato" ).text("");
		$('input[type="text"]').removeAttr('class');
		$('input[type="password"]').removeAttr('class');
		$( "#telefono" ).val( usuario.telefono );
		$( "#telefonoCon" ).val( usuario.telefono );
		$( "#email" ).val( usuario.email );
		$( "#emailCon" ).val( usuario.email );
		$( "#nombres" ).val( usuario.nombres );
		$( "#apellidoPrim" ).val( usuario.apellidoPrim );
		$( "#apellidoSeg" ).val( usuario.apellidoSeg );
		$( "#telefonoCasa" ).val( usuario.telefonoCasa );
		$( "#telefonoOficina" ).val( usuario.telefonoOficina );
		$( "#direccion" ).val( usuario.direccion );
		$( "#containerCambioDatos" ).dialog( "open" );
	});

	$("#aHrefCamPass").click(function(){
		camPassForm.resetForm();//remove error class on name elements and clear history
		$( "#lblErrorCamPass" ).text("");
		$('input[type="text"]').removeAttr('class');
		$('input[type="password"]').removeAttr('class');
		$( "#passwordCam" ).val( "" );
		$( "#newPassword" ).val( "" );
		$( "#newPasswordCon" ).val( "" );
		$( "#containerCambioPassword" ).dialog( "open" );
	});

	$("#aHrefCamTarj").click(function(){
		camTarjForm.resetForm();//remove error class on name elements and clear history
		$( "#lblErrorCamTarj" ).text("");
		$('input[type="text"]').removeAttr('class');
		$('input[type="password"]').removeAttr('class');
		$( "#newTarjeta" ).val( "" );
		$( "#newTarjetaCon" ).val( "" );
		$( "#vigencia" ).val( "" );
		$( "#passwordTarj" ).val( "" );
		$( "#containerCambioTarjeta" ).dialog( "open" );	
	});

	$("#cambioDatosCanSubmit").click(function(){
		$( "#containerCambioDatos" ).dialog( "close" );
	});

	$("#cambioPasswordCanSubmit").click(function(){
		$( "#containerCambioPassword" ).dialog( "close" );
	});

	$("#cambioTarjetaCanSubmit").click(function(){
		$( "#containerCambioTarjeta" ).dialog( "close" );
	});

	$( "#cargando" ).dialog({
		dialogClass: "no-close",
		modal: true,
		resizable: false,
		draggable: false,
		//closeOnEscape: false,
		autoOpen: false
	});

	$(document).ajaxStart(function() {
		$('input[type="submit"]').attr('disabled','disabled');

		$( "#cargando" ).dialog( "open" );
	});
	
	$(document).ajaxStop(function() {
		$( "#cargando" ).dialog( "close" );
		$('input[type="submit"]').removeAttr('disabled');
	});

	$("#saveForm").click(function(){
		$("#registroForm").submit(); //SUBMIT FORM
	});  


	$("#pagoSubmit").click(function(){

		$('#pagoForm').submit(function(event) {
			$('input[type="submit"]').attr('disabled','disabled');
			$( "#lblErrorPago" ).text("");

			if(!$( this ).valid()){
				$('input[type="submit"]').removeAttr('disabled');
				event.preventDefault(); //STOP default action
			}else{
				var dataJson = {
		    		"password" : $( "#password" ).val(  ),
		    		"cvv2" : $( "#cvv2" ).val(  ),
		    		"idUsuario" : $( "#idUsuario" ).val(  ),
		    		"software" : $( "#software" ).val(  ),
		    		"modelo" : $( "#modelo" ).val(  )
		    	};
				
				$.ajax({
					 url: $(this).attr('action'),
					 type: 'POST',
					 data: JSON.stringify(dataJson),
					 dataType: 'json',
					 contentType: "application/json; charset=utf-8",
					 mimeType: 'application/json; charset=utf-8',
					 async: false,
		             cache: false,    //This will force requested pages not to be cached by the browser          
		             processData:false, //To avoid making query String instead of JSON
					 statusCode: {
						 404: function() {
						 alert( "Pagina no encontrada." );
						 }
					 },
					 success: function (data) {
						
						$('input[type="submit"]').removeAttr('disabled');
					 	
					 	if(data.idError != 0){
					 		var mensaje ;

					 		if(data.mensajeError != null){
					 			mensaje = data.mensajeError;

							}else if(data.idError != -1001){
								mensaje = '<table cellpadding="0" cellspacing="0" border="0" width="100%"> <tr> <td width="25%">' + 
							 		'<label class="desc"  > Estado: </label> </td> <td> <label class="detail">' + data.estado + '</label> </td> </tr> <tr> <td width="25%"> '+
							 		'<label class="desc"  > Respuesta: </label> </td> <td> <label class="detail">' + data.descRespuesta + '</label> </td> </tr> <tr> <td width="25%"> '+
							 		'<label class="desc"  > Fecha: </label> </td> <td> <label class="detail">' + data.fechaTransaccion + '</label> </td> </tr> <tr> <td width="25%"> '+
							 		'<label class="desc"  > ID Transaccion: </label> </td> <td> <label class="detail">' + data.idTransaccion + '</label> </td> </tr> </table> ';
							}else{
								mensaje = data.mensajeError;
						    }
						    
					 		$( "#lblErrorPago" ).text(data.mensajeError);
					 		$( "#password" ).val( "" );
					 		//alert('mostrando alert...');
						 	var caja2 = $('<div title="Error">' + mensaje +'</div>');
							caja2.dialog({
								dialogClass: "alert",
								modal: true,
								resizable: false,
								draggable: false,
								buttons: [ {
									text: "Aceptar",
									click: function() {
										$( this ).dialog( "close" );
										if(data.idError == ID_ERROR_SESSION ){
											$(location).attr('href', '/GrupoRichardServicios/portal/login');
										}
									}
								}]
								});

					 	}else{
					 		$(location).attr('href', '/MobilecardWeb/portal/pago-resultado?uparam=' + data.jsonNotificacion);
						}
					 },
					 error: function (response, textStatus, errorThrown) {
					 	alert('An error has occured!! \nresponse: ' + response +
							 	'\ntextStatus: ' +  textStatus +
							 	'\nerrorThrown: ' + errorThrown);
					 }
				}); 
			}

			event.preventDefault(); //STOP default action
			event.stopImmediatePropagation();
		    return false;
			 
		});

		$("#pagoForm").submit(); //SUBMIT FORM
	});  


	function actalizarUsuarioAjax(){
		
		$('#cambioDatosForm').submit(function(event) {
			
			if(!$( this ).valid()){
				$('input[type="submit"]').removeAttr('disabled');
			}else{

				 $.ajax({
					 url: $(this).attr('action'),
					 type: 'POST',
					 data: JSON.stringify($(this).serializeArray()),
					 contentType: 'application/json',
					 success: function (data) {

						 $('input[type="submit"]').removeAttr('disabled');

						try{
							data = JSON.parse(data);
						}catch(e){
							data = data;
						}

					 	
					 	var caja2 = $('<div title="' + (data.idError == 0?"Mensaje del sistema":"Error") + '">' + data.mensajeError +'</div>');
						caja2.dialog({
							dialogClass: "alert",
							modal: true,
							resizable: false,
							draggable: false,
							buttons: [ {
								text: "Aceptar",
								click: function() {
									if(data.idError == 0){
										usuario.telefono = $( "#telefono" ).val( );
										usuario.email = $( "#email" ).val( );
										usuario.nombres = $( "#nombres" ).val( );
										usuario.apellidoPrim = $( "#apellidoPrim" ).val( );
										usuario.apellidoSeg = $( "#apellidoSeg" ).val( );
										usuario.telefonoCasa = $( "#telefonoCasa" ).val( );
										usuario.telefonoOficina = $( "#telefonoOficina" ).val( );
										usuario.direccion = $( "#direccion" ).val( );
										
										$( "#containerCambioDatos" ).dialog( "close" );
									}else if(data.idError == -1001){
										$("#redireccionForm").submit();
									} else{
										$('#lblErrorCamDato').text(data.mensajeError);
									}
									$( this ).dialog( "close" );
								}
							}]
							});
					 },
					 error: function (jqXHR, textStatus, errorThrown) {
						//$( "#cargando" ).dialog( "close" );
					 	alert('An error has occured!! \njqXHR: ' + jqXHR +
							 	'\ntextStatus: ' +  textStatus +
							 	'\nerrorThrown: ' + errorThrown);
					 }
				}); 
			}
			event.preventDefault(); //STOP default action
			event.stopImmediatePropagation();
		    return false;
		});
		
		$("#cambioDatosForm").submit(); //SUBMIT FORM
			
	} 
	
	function actalizarPasswordAjax(){
			
		$('#cambioPasswordForm').submit(function(event) {
			
			if(!$( this ).valid()){
				$('input[type="submit"]').removeAttr('disabled');
			}else{
				$('#lblErrorCamPass').text("");

				 $.ajax({
					 url: $(this).attr('action'),
					 type: 'POST',
					 data: JSON.stringify($(this).serializeArray()),
					 contentType: 'application/json',
					 success: function (data) {

						$('input[type="submit"]').removeAttr('disabled');

						try{
							data = JSON.parse(data);
						}catch(e){
							data = data;
						}
					 	
					 	var caja2 = $('<div title="' + (data.idError == 0?"Mensaje del sistema":"Error") + '">' + data.mensajeError +'</div>');
						caja2.dialog({
							dialogClass: "alert",
							modal: true,
							resizable: false,
							draggable: false,
							buttons: [ {
								text: "Aceptar",
								click: function() {
									$( "#passwordCam" ).val( "" );
									$( "#newPassword" ).val( "" );
									$( "#newPasswordCon" ).val( "" );
									if(data.idError == 0){
										$( "#containerCambioPassword" ).dialog( "close" );
									} else if(data.idError == -1001){
										$("#redireccionForm").submit();
									}else{
										$('#lblErrorCamPass').text(data.mensajeError);
									}
									$( this ).dialog( "close" );
								}
							}]
							});
					 },
					 error: function (jqXHR, textStatus, errorThrown) {
						//$( "#cargando" ).dialog( "close" );
					 	alert('An error has occured!! \njqXHR: ' + jqXHR +
							 	'\ntextStatus: ' +  textStatus +
							 	'\nerrorThrown: ' + errorThrown);
					 	$('input[type="submit"]').removeAttr('disabled');
					 }
				}); 
			}
			$('input[type="submit"]').removeAttr('disabled');

			event.preventDefault(); //STOP default action
			event.stopImmediatePropagation();
		    return false;
		});
		$("#cambioPasswordForm").submit(); //SUBMIT FORM
			
			
	} 


	function actalizarTarjetaAjax(){
		
		$('#cambioTarjetaForm').submit(function(event) {
			
			$('input[type="submit"]').attr('disabled','disabled');
			if(!$( this ).valid()){
				$('input[type="submit"]').removeAttr('disabled');
			}else{
				$('#lblErrorCamTarj').text("");

				 $.ajax({
					 url: $(this).attr('action'),
					 type: 'POST',
					 data: JSON.stringify($(this).serializeArray()),
					 contentType: 'application/json',
					 success: function (data) {

						 $('input[type="submit"]').removeAttr('disabled');

						 try{
							data = JSON.parse(data);
						}catch(e){
							data = data;
						}

					 	var caja2 = $('<div title="' + (data.idError == 0?"Mensaje del sistema":"Error") + '">' + data.mensajeError +'</div>');
						caja2.dialog({
							dialogClass: "alert",
							modal: true,
							resizable: false,
							draggable: false,
							buttons: [ {
								text: "Aceptar",
								click: function() {
									var tarjeta = $( "#newTarjeta" ).val();
									
									$( "#passwordTarj" ).val( "" );
									if(data.idError == 0){
										
										$( "#tarjetaAct" ).val( "XXXX XXXX XXXX " + 
												tarjeta.substring( tarjeta.length - 4, tarjeta.length ) );
										/* $( "#containerCambioPassword" ).dialog( "close" );
										$( "#newTarjeta" ).val( "" );
										$( "#newTarjetaCon" ).val( "" );
										$( "#vigencia" ).val( "" ); */
										$( "#containerCambioTarjeta" ).dialog( "close" );	
									} else if(data.idError == -1001){
										$("#redireccionForm").submit();
									} else{
										$('#lblErrorCamTarj').text(data.mensajeError);
									}
									$( this ).dialog( "close" );
								}
							}]
							});
					 },
					 error: function (jqXHR, textStatus, errorThrown) {
						//$( "#cargando" ).dialog( "close" );
					 	alert('An error has occured!! \njqXHR: ' + jqXHR +
							 	'\ntextStatus: ' +  textStatus +
							 	'\nerrorThrown: ' + errorThrown);
					 	$('input[type="submit"]').removeAttr('disabled');
					 }
				}); 
			}
			$('input[type="submit"]').removeAttr('disabled');

			event.preventDefault(); //STOP default action
			event.stopImmediatePropagation();
		    return false;
		});
		$("#cambioTarjetaForm").submit(); //SUBMIT FORM
			
			
	} 

	$( "input[type=submit], button" )
		.button()
		.click(function( event ) {
		event.preventDefault();
	});

	
	

	$.datepicker.setDefaults($.datepicker.regional["es"]);

	$( "#fechaNacimiento" ).datepicker({
		changeMonth: true, 
		changeYear: true, 
		maxDate: "+0d",
		yearRange: "c-40:c+0",
		beforeShow: function() {
	       var hoja = document.createElement('style');
	       hoja.innerHTML = ".ui-datepicker-calendar { display: table;}";
	       document.body.appendChild(hoja);
	    }
	});
	
	$( "#vigencia" ).datepicker({
		changeMonth: true, 
		changeYear: true,
		minDate: "+0d",
		//yearRange: "c-0:c+15",
		dateFormat: 'mm/yy',
			 
		onClose: function() {
	        var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
	    },
	       
	    beforeShow: function() {
	       var hoja = document.createElement('style');
	       hoja.innerHTML = ".ui-datepicker-calendar { display: none;}";
	       document.body.appendChild(hoja);
	       if ((selDate = $(this).val()).length > 0) {
	          iYear = selDate.substring(selDate.length - 4, selDate.length);
	          iMonth = selDate.substring(0, selDate.length - 5);
	          //alert('Year: ' + iYear + "\nMonth: " + $(this).datepicker('option', 'numberOfMonths'));
	          $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth-1, 1)); 
	          $(this).datepicker('setDate', new Date(iYear, iMonth-1, 1));
	       }
	    }
	});


	$("#terminarSubmit").click(function(){
		//alert('submit...');
		$("#resultadoForm").submit(); //SUBMIT FORM
	}); 


});