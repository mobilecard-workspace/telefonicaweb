<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" 	prefix="tiles" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title><tiles:insertAttribute name="title" ignore="true" /></title>
<link rel="icon" type="image/x-icon" href="<c:url value="/resources/images/default/icono_45x45.png"/>">

<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Pragma" content="no-cache" >
<meta http-equiv="expires" content="-1" >

<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
 	
%>
<!-- CSS -->
<link href="<c:url value="/resources/css/default/menu.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/default/theme.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/default/form.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/default/smoothness/jquery-ui.css"/>" rel="stylesheet">

<!-- JavaScript -->
<script src="<c:url value="/resources/js/jquery-1.9.0.js"/>"></script>
<script src="<c:url value="/resources/js/jquery-ui.js"/>"></script>
<script src="<c:url value="/resources/js/jquery.blockUI.js"/>"></script>
<script src="<c:url value="/resources/js/jquery.validate.js"/>"></script>
<script src="<c:url value="/resources/js/additional-methods.js"/>"></script>
<script src="<c:url value="/resources/js/messages_es.js"/>"></script>


<STYLE type="text/css">
	.no-close .ui-dialog-titlebar {
		display: none;
	}
	.ui-widget {
	    font-size: 1em;
	}
</STYLE>


</head>

<body id="body" data-twttr-rendered="true">
	<div id="cargando" align="center" style="display: none; z-index: 9999">
		<p>Enviando informacion...</p>
		<p><img src="<c:url value="/resources/images/default/loading.gif"/>" /></p>
	</div>
	
	<div id="container" >
		
		<tiles:insertAttribute name="header" />
		
		<tiles:insertAttribute name="menu" ignore="true"/>
		
		<tiles:insertAttribute name="body" />

		<tiles:insertAttribute name="footer" />

	</div>
</body>
</html>
