<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>

	<script>
		$(function() {
			$( "input[type=submit], button" )
				.button()
				.click(function( event ) {
				//event.preventDefault();
			});
	
		});
	</script>
		<div class="mobilecard">
	
			<header id="header" class="info" >
				<label class="desc" style="font-size:14px"> Ocurrio un error durante el proceso de Pago MobileCard: </label>
			</header>
			<br>
			<ul>
				<li class="full complex notranslate      ">
					<span class="full " >
						<label class="desc" > Descripción del Error: </label>
					</span>
				</li>
				<li class="full complex notranslate">
					<span class="  ">
						<p style="padding-left: 3px;"> ${mensajeError} </p>
					</span>
				</li>
			</ul>
			<div align="center" class="" style="height: 60px;">
				<input id="saveForm" name="saveForm" type="submit" value="Salir" style="width: 150px"/>
			</div>
		</div>
		
		<br><br>&nbsp;

		
	<%-- Como parecía no haber actividad en PayPal, cerramos su sesión para proteger la cuenta. Se eliminó toda la información que ingresó o de transacciones 
	incompletas que no fue guardada. Si ha llegado a esta página desde otro sitio web, vuelva a ese sitio (sin utilizar el botón Atrás del navegador) y 
	comience otra vez. Si provino del sitio web de PayPal, introduzca la información de inicio de sesión. Revise también si las cookies están habilitadas 
	en su navegador. --%>

