<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

	<form:form id="loginForm" name="loginForm" class="mobilecard topLabel page"
		autocomplete="off" method="post" modelAttribute="prodDisp"
		action="${pageContext.request.contextPath}/portal/prodDisp-final">
		</br>
		<div id="header" class="info">
			<label class="desc" style="font-size: 16px"> Servicio Productos Disponibles Telefonica </label>
			<label class="desc" style="font-size: 12px"> Favor de ingresar los siguientes datos: 
		</div>

		<ul>
			<li id="foliError" class="complex notranslate      " >
				<label class="error" > 
					${mensaje}
				</label>
			</li>
			<li id="foli0" class="complex notranslate      ">
				<div>
					<span class="left zip">
						<label class="desc" id="title9" for="Field9"> sessionId: 
							<span class="req">*</span>
						</label>
						<form:input id="sessionId" path="sessionId" value=""/>
					</span>
					<span class="right country">
						<label class="desc" id="title9" for="Field9"> country: 
							<span class="req">*</span>
						</label>
						<form:input id="country" path="country" value="MX"/>
					</span> 
				</div>
			</li>

			<li id="foli1" class="complex notranslate      ">
				<div>
					<span class="left zip">
						<label class="desc" id="title9" for="Field9"> brandId: 
							<span class="req">*</span>
						</label>
						<form:input id="brandId" path="brandId" value="6"/>
					</span>
					<span class="right country">
						<label class="desc" id="title9" for="Field9"> languaje: 
							<span class="req">*</span>
						</label>
						<form:input id="language" path="language" value="ES"/>
					</span> 
				</div>
			</li>
			
			<li id="foli1" class="complex notranslate      ">
				<div>
					<span class="left zip">
						<label class="desc" id="title9" for="Field9"> msisdn: 
							<span class="req">*</span>
						</label>
						<form:input id="msisdn" path="msisdn" value="525562402955"/>
					</span>
				</div>
			</li>
			
			<li id="foli1" class="complex notranslate      ">
				<div>
					<span class="left zip">
						<label class="desc">idError</label> 
						<label ><c:if test="${resp != null}"> ${resp.idError} </c:if></label>
					</span>
					<span class="right country">
						<label class="desc">mensajeError</label> 
						<label ><c:if test="${resp != null}"> ${resp.mensajeError} </c:if></label>
					</span> 
				</div>
			</li>
			
			<li id="foli1" class="complex notranslate      ">
				<div>
					<label class="desc">productosDisponibles</label> 
					<table border="1">
						<thead>
							<tr>
								<td>id </td>
								<td>accountTypeId </td>
								<td>accountTypeName </td>
								<td>productName </td>
								<td>price </td>
								<td>productTypeId </td>
								<td>productTypeName </td>
								<td>subscriptionTypeId </td>
								<td>subscriptionTypeName </td>
								<td>duration </td>
								<td>maxActive </td>
								<td>description </td>
								<td>incompatibleProducts </td>
							</tr>
						</thead>
						<tbody>
							<c:if test="${resp != null}">
								<c:if test="${resp.productosDisponibles != null}">
									<c:forEach var="producto" items="${resp.productosDisponibles}">
										<tr>
											<td>${producto.id} </td>
											<td>${producto.accountTypeId} </td>
											<td>${producto.accountTypeName}</td>
											<td>${producto.productName} </td>
											<td>${producto.price} </td>
											<td>${producto.productTypeId} </td>
											<td>${producto.productTypeName} </td>
											<td>${producto.subscriptionTypeId} </td>
											<td>${producto.subscriptionTypeName} </td>
											<td>${producto.duration}</td>
											<td>${producto.maxActive} </td>
											<td>${producto.description} </td>
											<td>${producto.incompatibleProducts} </td>
										</tr>
									</c:forEach>
								</c:if>
							</c:if>
						</tbody>
					</table>
				</div>
			</li>
			
		</ul>
		<div align="left"  style="height: 50px;">
			<input id="loginSubmit" name="loginSubmit" type="submit" value="Enviar Transaccion" style="width: 200px"/>
			<!-- <input id="resetSubmit" name="resetSubmit" type="submit" value="Reset Password" style="width: 150px"/> -->
		</div>
	</form:form>
	


