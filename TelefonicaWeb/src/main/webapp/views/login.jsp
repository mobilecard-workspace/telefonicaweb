<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

	<form:form id="loginForm" name="loginForm" class="mobilecard topLabel page"
		autocomplete="off" method="post" modelAttribute="login"
		action="${pageContext.request.contextPath}/portal/login-final">
		</br>
		<div id="header" class="info">
			<label class="desc" style="font-size: 16px"> Servicio Login Telefonica </label>
			<label class="desc" style="font-size: 12px"> Favor de ingresar los siguientes datos: 
		</div>

		<ul>
			<li id="foliError" class="complex notranslate      " >
				<label class="error" > 
					${mensaje}
				</label>
			</li>
			<li id="foli0" class="complex notranslate      ">
				<label class="desc" id="title9" for="Field9"> username: 
					<span class="req">*</span>
				</label>
				<div>
					<span class="left zip">
						<form:input id="username" path="username" value="TESTCHED@TEST.COM"/>
					</span>
					<span class="right country">
						<label class="desc">idError</label> 
						<label ><c:if test="${resp != null}"> ${resp.idError} </c:if></label>
					</span> 
				</div>
			</li>

			<li id="foli1" class="complex notranslate      ">
				<label class="desc" id="title9" for="Field9"> password:
					<span class="req">*</span>
				</label>
				<div>
					<span class="left zip">
						<form:input id="password" path="password" value="simfonics" />
					</span>
					<span class="right country">
						<label class="desc">mensajeError</label> 
						<label ><c:if test="${resp != null}"> ${resp.mensajeError} </c:if></label>
					</span> 
				</div>
			</li>
			
			<li id="foli1" class="complex notranslate      ">
				<label class="desc" id="title9" for="Field9"> country:
					<span class="req">*</span>
				</label>
				<div>
					<span class="left zip">
						<form:input id="country" path="country" value="MX"  />
					</span>
					<span class="right country">
						<label class="desc">sessionId</label> 
						<label ><c:if test="${resp != null}"> ${resp.sessionId} </c:if></label>
					</span> 
				</div>
			</li>
			
			<li id="foli1" class="complex notranslate      ">
				<label class="desc" id="title9" for="Field9"> brandId:
					<span class="req">*</span>
				</label>
				<div>
					<span class="left zip">
						<form:input id="brandId" path="brandId"  value="6"/>
					</span>
					<span class="right country">
						<label class="desc">customerId</label> 
						<label ><c:if test="${resp != null}"> ${resp.customerId} </c:if></label>
					</span> 
				</div>
			</li>
			
		</ul>
		<div align="left"  style="height: 50px;">
			<input id="loginSubmit" name="loginSubmit" type="submit" value="Enviar Transaccion" style="width: 200px"/>
			<!-- <input id="resetSubmit" name="resetSubmit" type="submit" value="Reset Password" style="width: 150px"/> -->
		</div>
	</form:form>
	


