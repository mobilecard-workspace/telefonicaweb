<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

	<form:form id="loginForm" name="loginForm" class="mobilecard topLabel page"
		autocomplete="off" method="post" modelAttribute="consulta"
		action="${pageContext.request.contextPath}/portal/consulta-final">
		</br>
		<div id="header" class="info">
			<label class="desc" style="font-size: 16px"> Servicio Consulta transactionId Telefonica </label>
			<label class="desc" style="font-size: 12px"> Favor de ingresar los siguientes datos: 
		</div>

		<ul>
			<li id="foliError" class="complex notranslate      " >
				<label class="error" > 
					${mensaje}
				</label>
			</li>
			<li id="foli0" class="complex notranslate      ">
				<label class="desc" id="title9" for="Field9"> mvno: 
					<span class="req">*</span>
				</label>
				<div>
					<span class="left zip">
						<form:input id="mvno" path="mvno" value="1620"/>
					</span>
					<span class="right country">
						<label class="desc">idError</label> 
						<label ><c:if test="${resp != null}"> ${resp.idError} </c:if></label>
					</span> 
				</div>
			</li>

			<li id="foli1" class="complex notranslate      ">
				<label class="desc" id="title9" for="Field9"> msisdn:
					<span class="req">*</span>
				</label>
				<div>
					<span class="left zip">
						<form:input id="msisdn" path="msisdn" value="525562402955" />
					</span>
					<span class="right country">
						<label class="desc">mensajeError</label> 
						<label ><c:if test="${resp != null}"> ${resp.mensajeError} </c:if></label>
					</span> 
				</div>
			</li>
			
			<li id="foli1" class="complex notranslate      ">
				<label class="desc" id="title9" for="Field9"> transactionId:
					<span class="req">*</span>
				</label>
				<div>
					<span class="left zip">
						<form:input id="transactionId" path="transactionId"   />
					</span>
					<span class="right country">
						<label class="desc">operationStatus</label> 
						<label ><c:if test="${resp != null}"> ${resp.operationStatus} </c:if></label>
					</span>
				</div>
			</li>
			
			<li id="foli1" class="complex notranslate      ">
				<label class="desc" id="title9" for="Field9"> transactionTypeId:
				</label>
				<div>
					<span class="left zip">
						<form:input id="transactionTypeId" path="transactionTypeId" value="2" />
					</span>
				</div>
			</li>
			
			<li id="foli1" class="complex notranslate      ">
				<label class="desc" id="title9" for="Field9"> origin:
					<span class="req">*</span>
				</label>
				<div>
					<span class="left zip">
						<form:input id="origin" path="origin"  value="998"/>
					</span>
				</div>
			</li>

		</ul>
		<div align="left"  style="height: 50px;">
			<input id="loginSubmit" name="loginSubmit" type="submit" value="Enviar Transaccion" style="width: 200px"/>
			<!-- <input id="resetSubmit" name="resetSubmit" type="submit" value="Reset Password" style="width: 150px"/> -->
		</div>
	</form:form>
	


